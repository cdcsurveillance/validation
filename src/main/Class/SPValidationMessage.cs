﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WisewomanMVCSite.Class
{
    public class SPValidationMessage
    {
        private string _mdeType;
        private string _mdeName;
        private string _message;
        private string _location;

        public SPValidationMessage(string mdeType, string mdeName, string message)
        {
            string location = "";
            _mdeType = mdeType;
            _mdeName = mdeName;
            _message = message;
            _location = location;
        }

        public string MDEType
        {
            get { return _mdeType; }

            set { }
        }

        public string MDEName
        {
            get { return _mdeName; }

            set { }
        }

        public string Message
        {
            get { return _message; }

            set { }
        }

        public string Location
        {
            get { return _location; }

            set { }
        }

    }
}