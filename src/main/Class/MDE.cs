﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;

namespace WisewomanMVCSite.Class
{
    public class MDE
    {
        public string mdeid { get; set; }
        public string fileid { get; set; }
        public string addedbyuser { get; set; }
        public string addedbystate { get; set; }
        public int rownum { get; set; }
        public string mdever { get; set; }
        public string stfips { get; set; }
        public string hdansi { get; set; }
        public string enrollsiteid { get; set; }
        public string screensiteid { get; set; }
        public string timeper { get; set; }
        public string nscreen { get; set; }
        public string type { get; set; }
        public string type_f { get; set; }
        public string encodeid { get; set; }
        public string resansi { get; set; }
        public string zip { get; set; }
        public string myb { get; set; }
        public string latino { get; set; }
        public string race1 { get; set; }
        public string race2 { get; set; }
        public string education { get; set; }
        public string language { get; set; }
        public string srhc { get; set; }
        public string srhc_f { get; set; }
        public string srhb { get; set; }
        public string srhb_f { get; set; }
        public string srd { get; set; }
        public string srd_f { get; set; }
        public string srha { get; set; }
        public string srha_f { get; set; }
        public string hcmeds { get; set; }
        public string hcmeds_f { get; set; }
        public string hbpmeds { get; set; }
        public string hbpmeds_f { get; set; }
        public string dmeds { get; set; }
        public string dmeds_f { get; set; }
        public string hcadhere { get; set; }
        public string hcadhere_f { get; set; }
        public string hbpadhere { get; set; }
        public string hbpadhere_f { get; set; }
        public string dadhere { get; set; }
        public string dadhere_f { get; set; }
        public string bphome { get; set; }
        public string bphome_f { get; set; }
        public string bpfreq { get; set; }
        public string bpfreq_f { get; set; }
        public string bpsend { get; set; }
        public string bpsend_f { get; set; }
        public string fruit { get; set; }
        public string fruit_f { get; set; }
        public string vegetables { get; set; }
        public string vegetables_f { get; set; }
        public string fish { get; set; }
        public string fish_f { get; set; }
        public string grains { get; set; }
        public string grains_f { get; set; }
        public string sugar { get; set; }
        public string sugar_f { get; set; }
        public string saltwatch { get; set; }
        public string saltwatch_f { get; set; }
        public string pamod { get; set; }
        public string pamod_f { get; set; }
        public string pavig { get; set; }
        public string pavig_f { get; set; }
        public string smoker { get; set; }
        public string smoker_f { get; set; }
        public string sechand { get; set; }
        public string sechand_f { get; set; }
        public string qolph { get; set; }
        public string qolph_f { get; set; }
        public string qolmh { get; set; }
        public string qolmh_f { get; set; }
        public string qoleffect { get; set; }
        public string qoleffect_f { get; set; }
        public string height { get; set; }
        public string weight { get; set; }
        public string weight_f { get; set; }
        public string waist { get; set; }
        public string waist_f { get; set; }
        public string hip { get; set; }
        public string hip_f { get; set; }
        public string bpdate { get; set; }
        public string bpdate_f { get; set; }
        public string sbp1 { get; set; }
        public string sbp1_f { get; set; }
        public string dbp1 { get; set; }
        public string dbp1_f { get; set; }
        public string sbp2 { get; set; }
        public string sbp2_f { get; set; }
        public string dbp2 { get; set; }
        public string dbp2_f { get; set; }
        public string fast { get; set; }
        public string fast_f { get; set; }
        public string tcdate { get; set; }
        public string tcdate_f { get; set; }
        public string totchol { get; set; }
        public string totchol_f { get; set; }
        public string hdl { get; set; }
        public string hdl_f { get; set; }
        public string ldl { get; set; }
        public string ldl_f { get; set; }
        public string trigly { get; set; }
        public string trigly_f { get; set; }
        public string bgdate { get; set; }
        public string bgdate_f { get; set; }
        public string glucose { get; set; }
        public string glucose_f { get; set; }
        public string a1c { get; set; }
        public string a1c_f { get; set; }
        public string bpalert { get; set; }
        public string bpalert_f { get; set; }
        public string bpdidate { get; set; }
        public string bpdidate_f { get; set; }
        public string bgalert { get; set; }
        public string bgalert_f { get; set; }
        public string bgdidate { get; set; }
        public string bgdidate_f { get; set; }
        public string rrcdate { get; set; }
        public string rrcdate_f { get; set; }
        public string rrccomplete { get; set; }
        public string rrccomplete_f { get; set; }
        public string rrcnut { get; set; }
        public string rrcnut_f { get; set; }
        public string rrcpa { get; set; }
        public string rrcpa_f { get; set; }
        public string rrcsmoke { get; set; }
        public string rrcsmoke_f { get; set; }
        public string rrcmedadhere { get; set; }
        public string rrcmedadhere_f { get; set; }
        public string rtcdate { get; set; }
        public string rtcdate_f { get; set; }
        public string rtc { get; set; }
        public string rtc_f { get; set; }
        public string refdate { get; set; }
        public string refdate2 { get; set; }
        public string lsphcrec { get; set; }
        public string intervention { get; set; }
        public string intervention2 { get; set; }
        public string intervention3 { get; set; }
        public string intervention4 { get; set; }
        public string intervention5 { get; set; }
        public string intervention6 { get; set; }
        public string intervention7 { get; set; }
        public string intervention8 { get; set; }
        public string intervention9 { get; set; }
        public string intervention10 { get; set; }
        public string intervention11 { get; set; }
        public string intervention12 { get; set; }
        public string intervention13 { get; set; }
        public string intervention14 { get; set; }
        public string intervention15 { get; set; }
        public string intervention16 { get; set; }
        public string lsphcid { get; set; }
        public string lsphcid2 { get; set; }
        public string lsphcid3 { get; set; }
        public string lsphcid4 { get; set; }
        public string lsphcid5 { get; set; }
        public string lsphcid6 { get; set; }
        public string lsphcid7 { get; set; }
        public string lsphcid8 { get; set; }
        public string lsphcid9 { get; set; }
        public string lsphcid10 { get; set; }
        public string lsphcid11 { get; set; }
        public string lsphcid12 { get; set; }
        public string lsphcid13 { get; set; }
        public string lsphcid14 { get; set; }
        public string lsphcid15 { get; set; }
        public string lsphcid16 { get; set; }
        public string lsphctime { get; set; }
        public string lsphctime2 { get; set; }
        public string lsphctime3 { get; set; }
        public string lsphctime4 { get; set; }
        public string lsphctime5 { get; set; }
        public string lsphctime6 { get; set; }
        public string lsphctime7 { get; set; }
        public string lsphctime8 { get; set; }
        public string lsphctime9 { get; set; }
        public string lsphctime10 { get; set; }
        public string lsphctime11 { get; set; }
        public string lsphctime12 { get; set; }
        public string lsphctime13 { get; set; }
        public string lsphctime14 { get; set; }
        public string lsphctime15 { get; set; }
        public string lsphctime16 { get; set; }
        public string contacttype { get; set; }
        public string contacttype2 { get; set; }
        public string contacttype3 { get; set; }
        public string contacttype4 { get; set; }
        public string contacttype5 { get; set; }
        public string contacttype6 { get; set; }
        public string contacttype7 { get; set; }
        public string contacttype8 { get; set; }
        public string contacttype9 { get; set; }
        public string contacttype10 { get; set; }
        public string contacttype11 { get; set; }
        public string contacttype12 { get; set; }
        public string contacttype13 { get; set; }
        public string contacttype14 { get; set; }
        public string contacttype15 { get; set; }
        public string contacttype16 { get; set; }
        public string setting { get; set; }
        public string setting2 { get; set; }
        public string setting3 { get; set; }
        public string setting4 { get; set; }
        public string setting5 { get; set; }
        public string setting6 { get; set; }
        public string setting7 { get; set; }
        public string setting8 { get; set; }
        public string setting9 { get; set; }
        public string setting10 { get; set; }
        public string setting11 { get; set; }
        public string setting12 { get; set; }
        public string setting13 { get; set; }
        public string setting14 { get; set; }
        public string setting15 { get; set; }
        public string setting16 { get; set; }
        public string lsphccomp { get; set; }
        public string lsphccomp2 { get; set; }
        public string lsphccomp3 { get; set; }
        public string lsphccomp4 { get; set; }
        public string lsphccomp5 { get; set; }
        public string lsphccomp6 { get; set; }
        public string lsphccomp7 { get; set; }
        public string lsphccomp8 { get; set; }
        public string lsphccomp9 { get; set; }
        public string lsphccomp10 { get; set; }
        public string lsphccomp11 { get; set; }
        public string lsphccomp12 { get; set; }
        public string lsphccomp13 { get; set; }
        public string lsphccomp14 { get; set; }
        public string lsphccomp15 { get; set; }
        public string lsphccomp16 { get; set; }
        public string tobresdate { get; set; }
        public string tobresdate2 { get; set; }
        public string tobresdate3 { get; set; }
        public string tobrestype { get; set; }
        public string tobrestype2 { get; set; }
        public string tobrestype3 { get; set; }
        public string trescomp { get; set; }
        public string trescomp2 { get; set; }
        public string trescomp3 { get; set; }
        public bool isvalid { get; set; }

        public int intSbp1 { get; set; }
        public int intSbp2 { get; set; }
        public int intMeanSbp { get; set; }

        //[ExcludeFromEvaluation]
        public string Output { get; set; }

        //[ExcludeFromEvaluation]
        public string ErrorKey { get; set; }

        //[ExcludeFromEvaluation]
        public string ErrorMessage { get; set; }

        //[ExcludeFromEvalution]
        public bool ErrorFormat { get; set; }

        //[ExcludeFromEvaluation]
        public bool InvalidRow { get; set; }

        //ExcludeFromEvaluation
        public bool BPPlus { get; set; }

        //ExcludeFromEvaluation
        public bool IsComplete { get; set; }

        public DateTime dateNow { get { return DateTime.Now; } }

        public int convertToInt(string sValue)
        {
            int myInt;
            bool isValid = int.TryParse(sValue, out myInt);
            return myInt;
        }

        public DateTime convertToDate(string mdeElement)
        {
            DateTime myDt;
            string[] formats = new[] { "MMddyyyy", "MMyyyy" };
            DateTime.TryParseExact(mdeElement, formats, new CultureInfo("en-US"), DateTimeStyles.None, out myDt);
            return myDt;
        }

        private DateTime convertBPDate(string sDate, string sFormat)
        {
            DateTime dtVal;
            if (sDate != null && sDate.Length == sFormat.Length)
            {
                CultureInfo provider = CultureInfo.InvariantCulture;
                dtVal = DateTime.ParseExact(sDate, sFormat, provider);
            }
            else
            {
                dtVal = new DateTime();
            }

            return dtVal;
        }

        public DateTime minbpdate { get { return dtMinBPDate(); } }

        public DateTime dtMinBPDate()
        {
            string sFormat = "MMddyyyy";
            DateTime dtBPDate = convertBPDate(bpdate, sFormat);
            DateTime dtBPDate_f = convertBPDate(bpdate_f, sFormat);
            DateTime dtMinBPDate; //var to store MINIMUM BP Date

            if ((dtBPDate > dtBPDate_f) && (dtBPDate_f.ToShortDateString() != "1/1/2000"))
            {
                dtMinBPDate = dtBPDate_f;
            }
            else
            {
                dtMinBPDate = dtBPDate;
            }

            return dtMinBPDate;
        }

        public int convertToAvgInt(int i1, int i2, string s1, string s2)
        {
            return i1;
        }

        public bool IsNullOrEmpty(string s)
        {
            bool bResult = false;
            if (s == null || s == String.Empty || s == ".")
            {
                bResult = true;
            }
            return bResult;
        }

        public bool IsRegExMatch(string sRegex, string sField)
        {
            bool bRet = false;

            Regex regex = new Regex(@sRegex);
            Match match = regex.Match(sField);
            bRet = match.Success;

            return bRet;
        }

        //public string SetUniform(string e)
        //{
        //    if (e == null)
        //    {
        //        e = ".";
        //    }
        //    else
        //    {
        //        e = e.Trim();
        //        //Sets to a period when value is missing
        //        if (IsAllDups(e, ".") || e == "")
        //        {
        //            e = ".";
        //        }
        //    }

        //    return e;
        //}
        //private bool IsAllDups(string s, string val)
        //{
        //    bool bCheck = false;

        //    string ComparisonString = s.Replace(val, "");

        //    if (ComparisonString == "") { bCheck = true;  } 

        //    return bCheck;
        //}
        private string SetUniform(string element)
        {
            //Sets to a period when value is missing
            if (IsAllDups(element, ".") || IsAllDups(element, " "))
            {
                return ".";
            }
            else
            {
                return element;
            }
        }

        private bool IsAllDups(string element, string arg)
        {
            if (element == null)
            {
                return true;
            }
            else
            {
                int dupcount = 0;
                string[] elements = new string[element.Length];
                for (int i = 0; i < elements.Length; i++)
                {
                    elements[i] = element[i].ToString();
                }

                for (int i = 0; i < elements.Length; i++)
                {
                    if (elements[i] == arg) { dupcount++; }
                }
                if (dupcount == elements.Length)
                //if (dupcount == elements.Length + 1)
                { return true; }
                else
                { return false; }
            }

        }

        public MDE NormalizeData(MDE mde)
        {
            mde.mdeid = SetUniform(mde.mdeid);
            mde.fileid = SetUniform(mde.fileid);
            //public int rownum = SetUniform(mde.rownum);
            mde.mdever = SetUniform(mde.mdever);
            mde.stfips = SetUniform(mde.stfips);
            mde.hdansi = SetUniform(mde.hdansi);
            mde.enrollsiteid = SetUniform(mde.enrollsiteid);
            mde.screensiteid = SetUniform(mde.screensiteid);
            mde.timeper = SetUniform(mde.timeper);
            mde.nscreen = SetUniform(mde.nscreen);
            mde.type = SetUniform(mde.type);
            mde.type_f = SetUniform(mde.type_f);
            mde.encodeid = SetUniform(mde.encodeid);
            mde.resansi = SetUniform(mde.resansi);
            mde.zip = SetUniform(mde.zip);
            mde.myb = SetUniform(mde.myb);
            mde.latino = SetUniform(mde.latino);
            mde.race1 = SetUniform(mde.race1);
            mde.race2 = SetUniform(mde.race2);
            mde.education = SetUniform(mde.education);
            mde.language = SetUniform(mde.language);
            mde.srhc = SetUniform(mde.srhc);
            mde.srhc_f = SetUniform(mde.srhc_f);
            mde.srhb = SetUniform(mde.srhb);
            mde.srhb_f = SetUniform(mde.srhb_f);
            mde.srd = SetUniform(mde.srd);
            mde.srd_f = SetUniform(mde.srd_f);
            mde.srha = SetUniform(mde.srha);
            mde.srha_f = SetUniform(mde.srha_f);
            mde.hcmeds = SetUniform(mde.hcmeds);
            mde.hcmeds_f = SetUniform(mde.hcmeds_f);
            mde.hbpmeds = SetUniform(mde.hbpmeds);
            mde.hbpmeds_f = SetUniform(mde.hbpmeds_f);
            mde.dmeds = SetUniform(mde.dmeds);
            mde.dmeds_f = SetUniform(mde.dmeds_f);
            mde.hcadhere = SetUniform(mde.hcadhere);
            mde.hcadhere_f = SetUniform(mde.hcadhere_f);
            mde.hbpadhere = SetUniform(mde.hbpadhere);
            mde.hbpadhere_f = SetUniform(mde.hbpadhere_f);
            mde.dadhere = SetUniform(mde.dadhere);
            mde.dadhere_f = SetUniform(mde.dadhere_f);
            mde.bphome = SetUniform(mde.bphome);
            mde.bphome_f = SetUniform(mde.bphome_f);
            mde.bpfreq = SetUniform(mde.bpfreq);
            mde.bpfreq_f = SetUniform(mde.bpfreq_f);
            mde.bpsend = SetUniform(mde.bpsend);
            mde.bpsend_f = SetUniform(mde.bpsend_f);
            mde.fruit = SetUniform(mde.fruit);
            mde.fruit_f = SetUniform(mde.fruit_f);
            mde.vegetables = SetUniform(mde.vegetables);
            mde.vegetables_f = SetUniform(mde.vegetables_f);
            mde.fish = SetUniform(mde.fish);
            mde.fish_f = SetUniform(mde.fish_f);
            mde.grains = SetUniform(mde.grains);
            mde.grains_f = SetUniform(mde.grains_f);
            mde.sugar = SetUniform(mde.sugar);
            mde.sugar_f = SetUniform(mde.sugar_f);
            mde.saltwatch = SetUniform(mde.saltwatch);
            mde.saltwatch_f = SetUniform(mde.saltwatch_f);
            mde.pamod = SetUniform(mde.pamod);
            mde.pamod_f = SetUniform(mde.pamod_f);
            mde.pavig = SetUniform(mde.pavig);
            mde.pavig_f = SetUniform(mde.pavig_f);
            mde.smoker = SetUniform(mde.smoker);
            mde.smoker_f = SetUniform(mde.smoker_f);
            mde.sechand = SetUniform(mde.sechand);
            mde.sechand_f = SetUniform(mde.sechand_f);
            mde.qolph = SetUniform(mde.qolph);
            mde.qolph_f = SetUniform(mde.qolph_f);
            mde.qolmh = SetUniform(mde.qolmh);
            mde.qolmh_f = SetUniform(mde.qolmh_f);
            mde.qoleffect = SetUniform(mde.qoleffect);
            mde.qoleffect_f = SetUniform(mde.qoleffect_f);
            mde.height = SetUniform(mde.height);
            mde.weight = SetUniform(mde.weight);
            mde.weight_f = SetUniform(mde.weight_f);
            mde.waist = SetUniform(mde.waist);
            mde.waist_f = SetUniform(mde.waist_f);
            mde.hip = SetUniform(mde.hip);
            mde.hip_f = SetUniform(mde.hip_f);
            mde.bpdate = SetUniform(mde.bpdate);
            mde.bpdate_f = SetUniform(mde.bpdate_f);
            mde.sbp1 = SetUniform(mde.sbp1);
            mde.sbp1_f = SetUniform(mde.sbp1_f);
            mde.dbp1 = SetUniform(mde.dbp1);
            mde.dbp1_f = SetUniform(mde.dbp1_f);
            mde.sbp2 = SetUniform(mde.sbp2);
            mde.sbp2_f = SetUniform(mde.sbp2_f);
            mde.dbp2 = SetUniform(mde.dbp2);
            mde.dbp2_f = SetUniform(mde.dbp2_f);
            mde.fast = SetUniform(mde.fast);
            mde.fast_f = SetUniform(mde.fast_f);
            mde.tcdate = SetUniform(mde.tcdate);
            mde.tcdate_f = SetUniform(mde.tcdate_f);
            mde.totchol = SetUniform(mde.totchol);
            mde.totchol_f = SetUniform(mde.totchol_f);
            mde.hdl = SetUniform(mde.hdl);
            mde.hdl_f = SetUniform(mde.hdl_f);
            mde.ldl = SetUniform(mde.ldl);
            mde.ldl_f = SetUniform(mde.ldl_f);
            mde.trigly = SetUniform(mde.trigly);
            mde.trigly_f = SetUniform(mde.trigly_f);
            mde.bgdate = SetUniform(mde.bgdate);
            mde.bgdate_f = SetUniform(mde.bgdate_f);
            mde.glucose = SetUniform(mde.glucose);
            mde.glucose_f = SetUniform(mde.glucose_f);
            mde.a1c = SetUniform(mde.a1c);
            mde.a1c_f = SetUniform(mde.a1c_f);
            mde.bpalert = SetUniform(mde.bpalert);
            mde.bpalert_f = SetUniform(mde.bpalert_f);
            mde.bpdidate = SetUniform(mde.bpdidate);
            mde.bpdidate_f = SetUniform(mde.bpdidate_f);
            mde.bgalert = SetUniform(mde.bgalert);
            mde.bgalert_f = SetUniform(mde.bgalert_f);
            mde.bgdidate = SetUniform(mde.bgdidate);
            mde.bgdidate_f = SetUniform(mde.bgdidate_f);
            mde.rrcdate = SetUniform(mde.rrcdate);
            mde.rrcdate_f = SetUniform(mde.rrcdate_f);
            mde.rrccomplete = SetUniform(mde.rrccomplete);
            mde.rrccomplete_f = SetUniform(mde.rrccomplete_f);
            mde.rrcnut = SetUniform(mde.rrcnut);
            mde.rrcnut_f = SetUniform(mde.rrcnut_f);
            mde.rrcpa = SetUniform(mde.rrcpa);
            mde.rrcpa_f = SetUniform(mde.rrcpa_f);
            mde.rrcsmoke = SetUniform(mde.rrcsmoke);
            mde.rrcsmoke_f = SetUniform(mde.rrcsmoke_f);
            mde.rrcmedadhere = SetUniform(mde.rrcmedadhere);
            mde.rrcmedadhere_f = SetUniform(mde.rrcmedadhere_f);
            mde.rtcdate = SetUniform(mde.rtcdate);
            mde.rtcdate_f = SetUniform(mde.rtcdate_f);
            mde.rtc = SetUniform(mde.rtc);
            mde.rtc_f = SetUniform(mde.rtc_f);
            mde.refdate = SetUniform(mde.refdate);
            mde.refdate2 = SetUniform(mde.refdate2);
            mde.lsphcrec = SetUniform(mde.lsphcrec);
            mde.intervention = SetUniform(mde.intervention);
            mde.intervention2 = SetUniform(mde.intervention2);
            mde.intervention3 = SetUniform(mde.intervention3);
            mde.intervention4 = SetUniform(mde.intervention4);
            mde.intervention5 = SetUniform(mde.intervention5);
            mde.intervention6 = SetUniform(mde.intervention6);
            mde.intervention7 = SetUniform(mde.intervention7);
            mde.intervention8 = SetUniform(mde.intervention8);
            mde.intervention9 = SetUniform(mde.intervention9);
            mde.intervention10 = SetUniform(mde.intervention10);
            mde.intervention11 = SetUniform(mde.intervention11);
            mde.intervention12 = SetUniform(mde.intervention12);
            mde.intervention13 = SetUniform(mde.intervention13);
            mde.intervention14 = SetUniform(mde.intervention14);
            mde.intervention15 = SetUniform(mde.intervention15);
            mde.intervention16 = SetUniform(mde.intervention16);
            mde.lsphcid = SetUniform(mde.lsphcid);
            mde.lsphcid2 = SetUniform(mde.lsphcid2);
            mde.lsphcid3 = SetUniform(mde.lsphcid3);
            mde.lsphcid4 = SetUniform(mde.lsphcid4);
            mde.lsphcid5 = SetUniform(mde.lsphcid5);
            mde.lsphcid6 = SetUniform(mde.lsphcid6);
            mde.lsphcid7 = SetUniform(mde.lsphcid7);
            mde.lsphcid8 = SetUniform(mde.lsphcid8);
            mde.lsphcid9 = SetUniform(mde.lsphcid9);
            mde.lsphcid10 = SetUniform(mde.lsphcid10);
            mde.lsphcid11 = SetUniform(mde.lsphcid11);
            mde.lsphcid12 = SetUniform(mde.lsphcid12);
            mde.lsphcid13 = SetUniform(mde.lsphcid13);
            mde.lsphcid14 = SetUniform(mde.lsphcid14);
            mde.lsphcid15 = SetUniform(mde.lsphcid15);
            mde.lsphcid16 = SetUniform(mde.lsphcid16);
            mde.lsphctime = SetUniform(mde.lsphctime);
            mde.lsphctime2 = SetUniform(mde.lsphctime2);
            mde.lsphctime3 = SetUniform(mde.lsphctime3);
            mde.lsphctime4 = SetUniform(mde.lsphctime4);
            mde.lsphctime5 = SetUniform(mde.lsphctime5);
            mde.lsphctime6 = SetUniform(mde.lsphctime6);
            mde.lsphctime7 = SetUniform(mde.lsphctime7);
            mde.lsphctime8 = SetUniform(mde.lsphctime8);
            mde.lsphctime9 = SetUniform(mde.lsphctime9);
            mde.lsphctime10 = SetUniform(mde.lsphctime10);
            mde.lsphctime11 = SetUniform(mde.lsphctime11);
            mde.lsphctime12 = SetUniform(mde.lsphctime12);
            mde.lsphctime13 = SetUniform(mde.lsphctime13);
            mde.lsphctime14 = SetUniform(mde.lsphctime14);
            mde.lsphctime15 = SetUniform(mde.lsphctime15);
            mde.lsphctime16 = SetUniform(mde.lsphctime16);
            mde.contacttype = SetUniform(mde.contacttype);
            mde.contacttype2 = SetUniform(mde.contacttype2);
            mde.contacttype3 = SetUniform(mde.contacttype3);
            mde.contacttype4 = SetUniform(mde.contacttype4);
            mde.contacttype5 = SetUniform(mde.contacttype5);
            mde.contacttype6 = SetUniform(mde.contacttype6);
            mde.contacttype7 = SetUniform(mde.contacttype7);
            mde.contacttype8 = SetUniform(mde.contacttype8);
            mde.contacttype9 = SetUniform(mde.contacttype9);
            mde.contacttype10 = SetUniform(mde.contacttype10);
            mde.contacttype11 = SetUniform(mde.contacttype11);
            mde.contacttype12 = SetUniform(mde.contacttype12);
            mde.contacttype13 = SetUniform(mde.contacttype13);
            mde.contacttype14 = SetUniform(mde.contacttype14);
            mde.contacttype15 = SetUniform(mde.contacttype15);
            mde.contacttype16 = SetUniform(mde.contacttype16);
            mde.setting = SetUniform(mde.setting);
            mde.setting2 = SetUniform(mde.setting2);
            mde.setting3 = SetUniform(mde.setting3);
            mde.setting4 = SetUniform(mde.setting4);
            mde.setting5 = SetUniform(mde.setting5);
            mde.setting6 = SetUniform(mde.setting6);
            mde.setting7 = SetUniform(mde.setting7);
            mde.setting8 = SetUniform(mde.setting8);
            mde.setting9 = SetUniform(mde.setting9);
            mde.setting10 = SetUniform(mde.setting10);
            mde.setting11 = SetUniform(mde.setting11);
            mde.setting12 = SetUniform(mde.setting12);
            mde.setting13 = SetUniform(mde.setting13);
            mde.setting14 = SetUniform(mde.setting14);
            mde.setting15 = SetUniform(mde.setting15);
            mde.setting16 = SetUniform(mde.setting16);
            mde.lsphccomp = SetUniform(mde.lsphccomp);
            mde.lsphccomp2 = SetUniform(mde.lsphccomp2);
            mde.lsphccomp3 = SetUniform(mde.lsphccomp3);
            mde.lsphccomp4 = SetUniform(mde.lsphccomp4);
            mde.lsphccomp5 = SetUniform(mde.lsphccomp5);
            mde.lsphccomp6 = SetUniform(mde.lsphccomp6);
            mde.lsphccomp7 = SetUniform(mde.lsphccomp7);
            mde.lsphccomp8 = SetUniform(mde.lsphccomp8);
            mde.lsphccomp9 = SetUniform(mde.lsphccomp9);
            mde.lsphccomp10 = SetUniform(mde.lsphccomp10);
            mde.lsphccomp11 = SetUniform(mde.lsphccomp11);
            mde.lsphccomp12 = SetUniform(mde.lsphccomp12);
            mde.lsphccomp13 = SetUniform(mde.lsphccomp13);
            mde.lsphccomp14 = SetUniform(mde.lsphccomp14);
            mde.lsphccomp15 = SetUniform(mde.lsphccomp15);
            mde.lsphccomp16 = SetUniform(mde.lsphccomp16);
            mde.tobresdate = SetUniform(mde.tobresdate);
            mde.tobresdate2 = SetUniform(mde.tobresdate2);
            mde.tobresdate3 = SetUniform(mde.tobresdate3);
            mde.tobrestype = SetUniform(mde.tobrestype);
            mde.tobrestype2 = SetUniform(mde.tobrestype2);
            mde.tobrestype3 = SetUniform(mde.tobrestype3);
            mde.trescomp = SetUniform(mde.trescomp);
            mde.trescomp2 = SetUniform(mde.trescomp2);
            mde.trescomp3 = SetUniform(mde.trescomp3);
            //mde.isvalid = SetUniform(mde.isvalid);

            return mde;
        }

        public int intHeight { get { return convertToInt(height); } }

        public DateTime dateBPDate { get { return convertToDate(bpdate); } }

        public override bool Equals(object obj)
        {
            return Equals(obj as MDE);
        }

        public bool Equals(MDE other)
        {
            return other != null &&
                   mdeid == other.mdeid &&
                   fileid == other.fileid &&
                   addedbyuser == other.addedbyuser &&
                   addedbystate == other.addedbystate &&
                   mdever == other.mdever &&
                   stfips == other.stfips &&
                   hdansi == other.hdansi &&
                   enrollsiteid == other.enrollsiteid &&
                   screensiteid == other.screensiteid &&
                   timeper == other.timeper &&
                   nscreen == other.nscreen &&
                   type == other.type &&
                   type_f == other.type_f &&
                   encodeid == other.encodeid &&
                   resansi == other.resansi &&
                   zip == other.zip &&
                   myb == other.myb &&
                   latino == other.latino &&
                   race1 == other.race1 &&
                   race2 == other.race2 &&
                   education == other.education &&
                   language == other.language &&
                   srhc == other.srhc &&
                   srhc_f == other.srhc_f &&
                   srhb == other.srhb &&
                   srhb_f == other.srhb_f &&
                   srd == other.srd &&
                   srd_f == other.srd_f &&
                   srha == other.srha &&
                   srha_f == other.srha_f &&
                   hcmeds == other.hcmeds &&
                   hcmeds_f == other.hcmeds_f &&
                   hbpmeds == other.hbpmeds &&
                   hbpmeds_f == other.hbpmeds_f &&
                   dmeds == other.dmeds &&
                   dmeds_f == other.dmeds_f &&
                   hcadhere == other.hcadhere &&
                   hcadhere_f == other.hcadhere_f &&
                   hbpadhere == other.hbpadhere &&
                   hbpadhere_f == other.hbpadhere_f &&
                   dadhere == other.dadhere &&
                   dadhere_f == other.dadhere_f &&
                   bphome == other.bphome &&
                   bphome_f == other.bphome_f &&
                   bpfreq == other.bpfreq &&
                   bpfreq_f == other.bpfreq_f &&
                   bpsend == other.bpsend &&
                   bpsend_f == other.bpsend_f &&
                   fruit == other.fruit &&
                   fruit_f == other.fruit_f &&
                   vegetables == other.vegetables &&
                   vegetables_f == other.vegetables_f &&
                   fish == other.fish &&
                   fish_f == other.fish_f &&
                   grains == other.grains &&
                   grains_f == other.grains_f &&
                   sugar == other.sugar &&
                   sugar_f == other.sugar_f &&
                   saltwatch == other.saltwatch &&
                   saltwatch_f == other.saltwatch_f &&
                   pamod == other.pamod &&
                   pamod_f == other.pamod_f &&
                   pavig == other.pavig &&
                   pavig_f == other.pavig_f &&
                   smoker == other.smoker &&
                   smoker_f == other.smoker_f &&
                   sechand == other.sechand &&
                   sechand_f == other.sechand_f &&
                   qolph == other.qolph &&
                   qolph_f == other.qolph_f &&
                   qolmh == other.qolmh &&
                   qolmh_f == other.qolmh_f &&
                   qoleffect == other.qoleffect &&
                   qoleffect_f == other.qoleffect_f &&
                   height == other.height &&
                   weight == other.weight &&
                   weight_f == other.weight_f &&
                   waist == other.waist &&
                   waist_f == other.waist_f &&
                   hip == other.hip &&
                   hip_f == other.hip_f &&
                   bpdate == other.bpdate &&
                   bpdate_f == other.bpdate_f &&
                   sbp1 == other.sbp1 &&
                   sbp1_f == other.sbp1_f &&
                   dbp1 == other.dbp1 &&
                   dbp1_f == other.dbp1_f &&
                   sbp2 == other.sbp2 &&
                   sbp2_f == other.sbp2_f &&
                   dbp2 == other.dbp2 &&
                   dbp2_f == other.dbp2_f &&
                   fast == other.fast &&
                   fast_f == other.fast_f &&
                   tcdate == other.tcdate &&
                   tcdate_f == other.tcdate_f &&
                   totchol == other.totchol &&
                   totchol_f == other.totchol_f &&
                   hdl == other.hdl &&
                   hdl_f == other.hdl_f &&
                   ldl == other.ldl &&
                   ldl_f == other.ldl_f &&
                   trigly == other.trigly &&
                   trigly_f == other.trigly_f &&
                   bgdate == other.bgdate &&
                   bgdate_f == other.bgdate_f &&
                   glucose == other.glucose &&
                   glucose_f == other.glucose_f &&
                   a1c == other.a1c &&
                   a1c_f == other.a1c_f &&
                   bpalert == other.bpalert &&
                   bpalert_f == other.bpalert_f &&
                   bpdidate == other.bpdidate &&
                   bpdidate_f == other.bpdidate_f &&
                   bgalert == other.bgalert &&
                   bgalert_f == other.bgalert_f &&
                   bgdidate == other.bgdidate &&
                   bgdidate_f == other.bgdidate_f &&
                   rrcdate == other.rrcdate &&
                   rrcdate_f == other.rrcdate_f &&
                   rrccomplete == other.rrccomplete &&
                   rrccomplete_f == other.rrccomplete_f &&
                   rrcnut == other.rrcnut &&
                   rrcnut_f == other.rrcnut_f &&
                   rrcpa == other.rrcpa &&
                   rrcpa_f == other.rrcpa_f &&
                   rrcsmoke == other.rrcsmoke &&
                   rrcsmoke_f == other.rrcsmoke_f &&
                   rrcmedadhere == other.rrcmedadhere &&
                   rrcmedadhere_f == other.rrcmedadhere_f &&
                   rtcdate == other.rtcdate &&
                   rtcdate_f == other.rtcdate_f &&
                   rtc == other.rtc &&
                   rtc_f == other.rtc_f &&
                   refdate == other.refdate &&
                   refdate2 == other.refdate2 &&
                   lsphcrec == other.lsphcrec &&
                   intervention == other.intervention &&
                   intervention2 == other.intervention2 &&
                   intervention3 == other.intervention3 &&
                   intervention4 == other.intervention4 &&
                   intervention5 == other.intervention5 &&
                   intervention6 == other.intervention6 &&
                   intervention7 == other.intervention7 &&
                   intervention8 == other.intervention8 &&
                   intervention9 == other.intervention9 &&
                   intervention10 == other.intervention10 &&
                   intervention11 == other.intervention11 &&
                   intervention12 == other.intervention12 &&
                   intervention13 == other.intervention13 &&
                   intervention14 == other.intervention14 &&
                   intervention15 == other.intervention15 &&
                   intervention16 == other.intervention16 &&
                   lsphcid == other.lsphcid &&
                   lsphcid2 == other.lsphcid2 &&
                   lsphcid3 == other.lsphcid3 &&
                   lsphcid4 == other.lsphcid4 &&
                   lsphcid5 == other.lsphcid5 &&
                   lsphcid6 == other.lsphcid6 &&
                   lsphcid7 == other.lsphcid7 &&
                   lsphcid8 == other.lsphcid8 &&
                   lsphcid9 == other.lsphcid9 &&
                   lsphcid10 == other.lsphcid10 &&
                   lsphcid11 == other.lsphcid11 &&
                   lsphcid12 == other.lsphcid12 &&
                   lsphcid13 == other.lsphcid13 &&
                   lsphcid14 == other.lsphcid14 &&
                   lsphcid15 == other.lsphcid15 &&
                   lsphcid16 == other.lsphcid16 &&
                   lsphctime == other.lsphctime &&
                   lsphctime2 == other.lsphctime2 &&
                   lsphctime3 == other.lsphctime3 &&
                   lsphctime4 == other.lsphctime4 &&
                   lsphctime5 == other.lsphctime5 &&
                   lsphctime6 == other.lsphctime6 &&
                   lsphctime7 == other.lsphctime7 &&
                   lsphctime8 == other.lsphctime8 &&
                   lsphctime9 == other.lsphctime9 &&
                   lsphctime10 == other.lsphctime10 &&
                   lsphctime11 == other.lsphctime11 &&
                   lsphctime12 == other.lsphctime12 &&
                   lsphctime13 == other.lsphctime13 &&
                   lsphctime14 == other.lsphctime14 &&
                   lsphctime15 == other.lsphctime15 &&
                   lsphctime16 == other.lsphctime16 &&
                   contacttype == other.contacttype &&
                   contacttype2 == other.contacttype2 &&
                   contacttype3 == other.contacttype3 &&
                   contacttype4 == other.contacttype4 &&
                   contacttype5 == other.contacttype5 &&
                   contacttype6 == other.contacttype6 &&
                   contacttype7 == other.contacttype7 &&
                   contacttype8 == other.contacttype8 &&
                   contacttype9 == other.contacttype9 &&
                   contacttype10 == other.contacttype10 &&
                   contacttype11 == other.contacttype11 &&
                   contacttype12 == other.contacttype12 &&
                   contacttype13 == other.contacttype13 &&
                   contacttype14 == other.contacttype14 &&
                   contacttype15 == other.contacttype15 &&
                   contacttype16 == other.contacttype16 &&
                   setting == other.setting &&
                   setting2 == other.setting2 &&
                   setting3 == other.setting3 &&
                   setting4 == other.setting4 &&
                   setting5 == other.setting5 &&
                   setting6 == other.setting6 &&
                   setting7 == other.setting7 &&
                   setting8 == other.setting8 &&
                   setting9 == other.setting9 &&
                   setting10 == other.setting10 &&
                   setting11 == other.setting11 &&
                   setting12 == other.setting12 &&
                   setting13 == other.setting13 &&
                   setting14 == other.setting14 &&
                   setting15 == other.setting15 &&
                   setting16 == other.setting16 &&
                   lsphccomp == other.lsphccomp &&
                   lsphccomp2 == other.lsphccomp2 &&
                   lsphccomp3 == other.lsphccomp3 &&
                   lsphccomp4 == other.lsphccomp4 &&
                   lsphccomp5 == other.lsphccomp5 &&
                   lsphccomp6 == other.lsphccomp6 &&
                   lsphccomp7 == other.lsphccomp7 &&
                   lsphccomp8 == other.lsphccomp8 &&
                   lsphccomp9 == other.lsphccomp9 &&
                   lsphccomp10 == other.lsphccomp10 &&
                   lsphccomp11 == other.lsphccomp11 &&
                   lsphccomp12 == other.lsphccomp12 &&
                   lsphccomp13 == other.lsphccomp13 &&
                   lsphccomp14 == other.lsphccomp14 &&
                   lsphccomp15 == other.lsphccomp15 &&
                   lsphccomp16 == other.lsphccomp16 &&
                   tobresdate == other.tobresdate &&
                   tobresdate2 == other.tobresdate2 &&
                   tobresdate3 == other.tobresdate3 &&
                   tobrestype == other.tobrestype &&
                   tobrestype2 == other.tobrestype2 &&
                   tobrestype3 == other.tobrestype3 &&
                   trescomp == other.trescomp &&
                   trescomp2 == other.trescomp2 &&
                   trescomp3 == other.trescomp3 &&
                   isvalid == other.isvalid &&
                   intSbp1 == other.intSbp1 &&
                   intSbp2 == other.intSbp2 &&
                   intMeanSbp == other.intMeanSbp &&
                   Output == other.Output &&
                   ErrorKey == other.ErrorKey &&
                   ErrorMessage == other.ErrorMessage &&
                   ErrorFormat == other.ErrorFormat &&
                   InvalidRow == other.InvalidRow &&
                   BPPlus == other.BPPlus;
        }

        public override int GetHashCode()
        {
            var hash = new HashCode();
            hash.Add(mdeid);
            hash.Add(fileid);
            hash.Add(addedbyuser);
            hash.Add(addedbystate);
            hash.Add(mdever);
            hash.Add(stfips);
            hash.Add(hdansi);
            hash.Add(enrollsiteid);
            hash.Add(screensiteid);
            hash.Add(timeper);
            hash.Add(nscreen);
            hash.Add(type);
            hash.Add(type_f);
            hash.Add(encodeid);
            hash.Add(resansi);
            hash.Add(zip);
            hash.Add(myb);
            hash.Add(latino);
            hash.Add(race1);
            hash.Add(race2);
            hash.Add(education);
            hash.Add(language);
            hash.Add(srhc);
            hash.Add(srhc_f);
            hash.Add(srhb);
            hash.Add(srhb_f);
            hash.Add(srd);
            hash.Add(srd_f);
            hash.Add(srha);
            hash.Add(srha_f);
            hash.Add(hcmeds);
            hash.Add(hcmeds_f);
            hash.Add(hbpmeds);
            hash.Add(hbpmeds_f);
            hash.Add(dmeds);
            hash.Add(dmeds_f);
            hash.Add(hcadhere);
            hash.Add(hcadhere_f);
            hash.Add(hbpadhere);
            hash.Add(hbpadhere_f);
            hash.Add(dadhere);
            hash.Add(dadhere_f);
            hash.Add(bphome);
            hash.Add(bphome_f);
            hash.Add(bpfreq);
            hash.Add(bpfreq_f);
            hash.Add(bpsend);
            hash.Add(bpsend_f);
            hash.Add(fruit);
            hash.Add(fruit_f);
            hash.Add(vegetables);
            hash.Add(vegetables_f);
            hash.Add(fish);
            hash.Add(fish_f);
            hash.Add(grains);
            hash.Add(grains_f);
            hash.Add(sugar);
            hash.Add(sugar_f);
            hash.Add(saltwatch);
            hash.Add(saltwatch_f);
            hash.Add(pamod);
            hash.Add(pamod_f);
            hash.Add(pavig);
            hash.Add(pavig_f);
            hash.Add(smoker);
            hash.Add(smoker_f);
            hash.Add(sechand);
            hash.Add(sechand_f);
            hash.Add(qolph);
            hash.Add(qolph_f);
            hash.Add(qolmh);
            hash.Add(qolmh_f);
            hash.Add(qoleffect);
            hash.Add(qoleffect_f);
            hash.Add(height);
            hash.Add(weight);
            hash.Add(weight_f);
            hash.Add(waist);
            hash.Add(waist_f);
            hash.Add(hip);
            hash.Add(hip_f);
            hash.Add(bpdate);
            hash.Add(bpdate_f);
            hash.Add(sbp1);
            hash.Add(sbp1_f);
            hash.Add(dbp1);
            hash.Add(dbp1_f);
            hash.Add(sbp2);
            hash.Add(sbp2_f);
            hash.Add(dbp2);
            hash.Add(dbp2_f);
            hash.Add(fast);
            hash.Add(fast_f);
            hash.Add(tcdate);
            hash.Add(tcdate_f);
            hash.Add(totchol);
            hash.Add(totchol_f);
            hash.Add(hdl);
            hash.Add(hdl_f);
            hash.Add(ldl);
            hash.Add(ldl_f);
            hash.Add(trigly);
            hash.Add(trigly_f);
            hash.Add(bgdate);
            hash.Add(bgdate_f);
            hash.Add(glucose);
            hash.Add(glucose_f);
            hash.Add(a1c);
            hash.Add(a1c_f);
            hash.Add(bpalert);
            hash.Add(bpalert_f);
            hash.Add(bpdidate);
            hash.Add(bpdidate_f);
            hash.Add(bgalert);
            hash.Add(bgalert_f);
            hash.Add(bgdidate);
            hash.Add(bgdidate_f);
            hash.Add(rrcdate);
            hash.Add(rrcdate_f);
            hash.Add(rrccomplete);
            hash.Add(rrccomplete_f);
            hash.Add(rrcnut);
            hash.Add(rrcnut_f);
            hash.Add(rrcpa);
            hash.Add(rrcpa_f);
            hash.Add(rrcsmoke);
            hash.Add(rrcsmoke_f);
            hash.Add(rrcmedadhere);
            hash.Add(rrcmedadhere_f);
            hash.Add(rtcdate);
            hash.Add(rtcdate_f);
            hash.Add(rtc);
            hash.Add(rtc_f);
            hash.Add(refdate);
            hash.Add(refdate2);
            hash.Add(lsphcrec);
            hash.Add(intervention);
            hash.Add(intervention2);
            hash.Add(intervention3);
            hash.Add(intervention4);
            hash.Add(intervention5);
            hash.Add(intervention6);
            hash.Add(intervention7);
            hash.Add(intervention8);
            hash.Add(intervention9);
            hash.Add(intervention10);
            hash.Add(intervention11);
            hash.Add(intervention12);
            hash.Add(intervention13);
            hash.Add(intervention14);
            hash.Add(intervention15);
            hash.Add(intervention16);
            hash.Add(lsphcid);
            hash.Add(lsphcid2);
            hash.Add(lsphcid3);
            hash.Add(lsphcid4);
            hash.Add(lsphcid5);
            hash.Add(lsphcid6);
            hash.Add(lsphcid7);
            hash.Add(lsphcid8);
            hash.Add(lsphcid9);
            hash.Add(lsphcid10);
            hash.Add(lsphcid11);
            hash.Add(lsphcid12);
            hash.Add(lsphcid13);
            hash.Add(lsphcid14);
            hash.Add(lsphcid15);
            hash.Add(lsphcid16);
            hash.Add(lsphctime);
            hash.Add(lsphctime2);
            hash.Add(lsphctime3);
            hash.Add(lsphctime4);
            hash.Add(lsphctime5);
            hash.Add(lsphctime6);
            hash.Add(lsphctime7);
            hash.Add(lsphctime8);
            hash.Add(lsphctime9);
            hash.Add(lsphctime10);
            hash.Add(lsphctime11);
            hash.Add(lsphctime12);
            hash.Add(lsphctime13);
            hash.Add(lsphctime14);
            hash.Add(lsphctime15);
            hash.Add(lsphctime16);
            hash.Add(contacttype);
            hash.Add(contacttype2);
            hash.Add(contacttype3);
            hash.Add(contacttype4);
            hash.Add(contacttype5);
            hash.Add(contacttype6);
            hash.Add(contacttype7);
            hash.Add(contacttype8);
            hash.Add(contacttype9);
            hash.Add(contacttype10);
            hash.Add(contacttype11);
            hash.Add(contacttype12);
            hash.Add(contacttype13);
            hash.Add(contacttype14);
            hash.Add(contacttype15);
            hash.Add(contacttype16);
            hash.Add(setting);
            hash.Add(setting2);
            hash.Add(setting3);
            hash.Add(setting4);
            hash.Add(setting5);
            hash.Add(setting6);
            hash.Add(setting7);
            hash.Add(setting8);
            hash.Add(setting9);
            hash.Add(setting10);
            hash.Add(setting11);
            hash.Add(setting12);
            hash.Add(setting13);
            hash.Add(setting14);
            hash.Add(setting15);
            hash.Add(setting16);
            hash.Add(lsphccomp);
            hash.Add(lsphccomp2);
            hash.Add(lsphccomp3);
            hash.Add(lsphccomp4);
            hash.Add(lsphccomp5);
            hash.Add(lsphccomp6);
            hash.Add(lsphccomp7);
            hash.Add(lsphccomp8);
            hash.Add(lsphccomp9);
            hash.Add(lsphccomp10);
            hash.Add(lsphccomp11);
            hash.Add(lsphccomp12);
            hash.Add(lsphccomp13);
            hash.Add(lsphccomp14);
            hash.Add(lsphccomp15);
            hash.Add(lsphccomp16);
            hash.Add(tobresdate);
            hash.Add(tobresdate2);
            hash.Add(tobresdate3);
            hash.Add(tobrestype);
            hash.Add(tobrestype2);
            hash.Add(tobrestype3);
            hash.Add(trescomp);
            hash.Add(trescomp2);
            hash.Add(trescomp3);
            hash.Add(isvalid);
            hash.Add(intSbp1);
            hash.Add(intSbp2);
            hash.Add(intMeanSbp);
            hash.Add(Output);
            hash.Add(ErrorKey);
            hash.Add(ErrorMessage);
            hash.Add(ErrorFormat);
            hash.Add(InvalidRow);
            hash.Add(BPPlus);
            return hash.ToHashCode();
        }

        public static bool operator ==(MDE obj1, MDE obj2)
        {
            if (ReferenceEquals(obj1, obj2))
            {
                return true;
            }

            if (obj1 is null)
            {
                return false;
            }
            if (obj2 is null)
            {
                return false;
            }

            return (obj1.mdeid == obj2.mdeid
                && obj1.fileid == obj2.fileid
                && obj1.addedbyuser == obj2.addedbyuser
                && obj1.addedbystate == obj2.addedbystate
                && obj1.mdever == obj2.mdever
                && obj1.stfips == obj2.stfips
                && obj1.hdansi == obj2.hdansi
                && obj1.enrollsiteid == obj2.enrollsiteid
                && obj1.screensiteid == obj2.screensiteid
                && obj1.timeper == obj2.timeper
                && obj1.nscreen == obj2.nscreen
                && obj1.type == obj2.type
                && obj1.type_f == obj2.type_f
                && obj1.encodeid == obj2.encodeid
                && obj1.resansi == obj2.resansi
                && obj1.zip == obj2.zip
                && obj1.myb == obj2.myb
                && obj1.latino == obj2.latino
                && obj1.race1 == obj2.race1
                && obj1.race2 == obj2.race2
                && obj1.education == obj2.education
                && obj1.language == obj2.language
                && obj1.srhc == obj2.srhc
                && obj1.srhc_f == obj2.srhc_f
                && obj1.srhb == obj2.srhb
                && obj1.srhb_f == obj2.srhb_f
                && obj1.srd == obj2.srd
                && obj1.srd_f == obj2.srd_f
                && obj1.srha == obj2.srha
                && obj1.srha_f == obj2.srha_f
                && obj1.hcmeds == obj2.hcmeds
                && obj1.hcmeds_f == obj2.hcmeds_f
                && obj1.hbpmeds == obj2.hbpmeds
                && obj1.hbpmeds_f == obj2.hbpmeds_f
                && obj1.dmeds == obj2.dmeds
                && obj1.dmeds_f == obj2.dmeds_f
                && obj1.hcadhere == obj2.hcadhere
                && obj1.hcadhere_f == obj2.hcadhere_f
                && obj1.hbpadhere == obj2.hbpadhere
                && obj1.hbpadhere_f == obj2.hbpadhere_f
                && obj1.dadhere == obj2.dadhere
                && obj1.dadhere_f == obj2.dadhere_f
                && obj1.bphome == obj2.bphome
                && obj1.bphome_f == obj2.bphome_f
                && obj1.bpfreq == obj2.bpfreq
                && obj1.bpfreq_f == obj2.bpfreq_f
                && obj1.bpsend == obj2.bpsend
                && obj1.bpsend_f == obj2.bpsend_f
                && obj1.fruit == obj2.fruit
                && obj1.fruit_f == obj2.fruit_f
                && obj1.vegetables == obj2.vegetables
                && obj1.vegetables_f == obj2.vegetables_f
                && obj1.fish == obj2.fish
                && obj1.fish_f == obj2.fish_f
                && obj1.grains == obj2.grains
                && obj1.grains_f == obj2.grains_f
                && obj1.sugar == obj2.sugar
                && obj1.sugar_f == obj2.sugar_f
                && obj1.saltwatch == obj2.saltwatch
                && obj1.saltwatch_f == obj2.saltwatch_f
                && obj1.pamod == obj2.pamod
                && obj1.pamod_f == obj2.pamod_f
                && obj1.pavig == obj2.pavig
                && obj1.pavig_f == obj2.pavig_f
                && obj1.smoker == obj2.smoker
                && obj1.smoker_f == obj2.smoker_f
                && obj1.sechand == obj2.sechand
                && obj1.sechand_f == obj2.sechand_f
                && obj1.qolph == obj2.qolph
                && obj1.qolph_f == obj2.qolph_f
                && obj1.qolmh == obj2.qolmh
                && obj1.qolmh_f == obj2.qolmh_f
                && obj1.qoleffect == obj2.qoleffect
                && obj1.qoleffect_f == obj2.qoleffect_f
                && obj1.height == obj2.height
                && obj1.weight == obj2.weight
                && obj1.weight_f == obj2.weight_f
                && obj1.waist == obj2.waist
                && obj1.waist_f == obj2.waist_f
                && obj1.hip == obj2.hip
                && obj1.hip_f == obj2.hip_f
                && obj1.bpdate == obj2.bpdate
                && obj1.bpdate_f == obj2.bpdate_f
                && obj1.sbp1 == obj2.sbp1
                && obj1.sbp1_f == obj2.sbp1_f
                && obj1.dbp1 == obj2.dbp1
                && obj1.dbp1_f == obj2.dbp1_f
                && obj1.sbp2 == obj2.sbp2
                && obj1.sbp2_f == obj2.sbp2_f
                && obj1.dbp2 == obj2.dbp2
                && obj1.dbp2_f == obj2.dbp2_f
                && obj1.fast == obj2.fast
                && obj1.fast_f == obj2.fast_f
                && obj1.tcdate == obj2.tcdate
                && obj1.tcdate_f == obj2.tcdate_f
                && obj1.totchol == obj2.totchol
                && obj1.totchol_f == obj2.totchol_f
                && obj1.hdl == obj2.hdl
                && obj1.hdl_f == obj2.hdl_f
                && obj1.ldl == obj2.ldl
                && obj1.ldl_f == obj2.ldl_f
                && obj1.trigly == obj2.trigly
                && obj1.trigly_f == obj2.trigly_f
                && obj1.bgdate == obj2.bgdate
                && obj1.bgdate_f == obj2.bgdate_f
                && obj1.glucose == obj2.glucose
                && obj1.glucose_f == obj2.glucose_f
                && obj1.a1c == obj2.a1c
                && obj1.a1c_f == obj2.a1c_f
                && obj1.bpalert == obj2.bpalert
                && obj1.bpalert_f == obj2.bpalert_f
                && obj1.bpdidate == obj2.bpdidate
                && obj1.bpdidate_f == obj2.bpdidate_f
                && obj1.bgalert == obj2.bgalert
                && obj1.bgalert_f == obj2.bgalert_f
                && obj1.bgdidate == obj2.bgdidate
                && obj1.bgdidate_f == obj2.bgdidate_f
                && obj1.rrcdate == obj2.rrcdate
                && obj1.rrcdate_f == obj2.rrcdate_f
                && obj1.rrccomplete == obj2.rrccomplete
                && obj1.rrccomplete_f == obj2.rrccomplete_f
                && obj1.rrcnut == obj2.rrcnut
                && obj1.rrcnut_f == obj2.rrcnut_f
                && obj1.rrcpa == obj2.rrcpa
                && obj1.rrcpa_f == obj2.rrcpa_f
                && obj1.rrcsmoke == obj2.rrcsmoke
                && obj1.rrcsmoke_f == obj2.rrcsmoke_f
                && obj1.rrcmedadhere == obj2.rrcmedadhere
                && obj1.rrcmedadhere_f == obj2.rrcmedadhere_f
                && obj1.rtcdate == obj2.rtcdate
                && obj1.rtcdate_f == obj2.rtcdate_f
                && obj1.rtc == obj2.rtc
                && obj1.rtc_f == obj2.rtc_f
                && obj1.refdate == obj2.refdate
                && obj1.refdate2 == obj2.refdate2
                && obj1.lsphcrec == obj2.lsphcrec
                && obj1.intervention == obj2.intervention
                && obj1.intervention2 == obj2.intervention2
                && obj1.intervention3 == obj2.intervention3
                && obj1.intervention4 == obj2.intervention4
                && obj1.intervention5 == obj2.intervention5
                && obj1.intervention6 == obj2.intervention6
                && obj1.intervention7 == obj2.intervention7
                && obj1.intervention8 == obj2.intervention8
                && obj1.intervention9 == obj2.intervention9
                && obj1.intervention10 == obj2.intervention10
                && obj1.intervention11 == obj2.intervention11
                && obj1.intervention12 == obj2.intervention12
                && obj1.intervention13 == obj2.intervention13
                && obj1.intervention14 == obj2.intervention14
                && obj1.intervention15 == obj2.intervention15
                && obj1.intervention16 == obj2.intervention16
                && obj1.lsphcid == obj2.lsphcid
                && obj1.lsphcid2 == obj2.lsphcid2
                && obj1.lsphcid3 == obj2.lsphcid3
                && obj1.lsphcid4 == obj2.lsphcid4
                && obj1.lsphcid5 == obj2.lsphcid5
                && obj1.lsphcid6 == obj2.lsphcid6
                && obj1.lsphcid7 == obj2.lsphcid7
                && obj1.lsphcid8 == obj2.lsphcid8
                && obj1.lsphcid9 == obj2.lsphcid9
                && obj1.lsphcid10 == obj2.lsphcid10
                && obj1.lsphcid11 == obj2.lsphcid11
                && obj1.lsphcid12 == obj2.lsphcid12
                && obj1.lsphcid13 == obj2.lsphcid13
                && obj1.lsphcid14 == obj2.lsphcid14
                && obj1.lsphcid15 == obj2.lsphcid15
                && obj1.lsphcid16 == obj2.lsphcid16
                && obj1.lsphctime == obj2.lsphctime
                && obj1.lsphctime2 == obj2.lsphctime2
                && obj1.lsphctime3 == obj2.lsphctime3
                && obj1.lsphctime4 == obj2.lsphctime4
                && obj1.lsphctime5 == obj2.lsphctime5
                && obj1.lsphctime6 == obj2.lsphctime6
                && obj1.lsphctime7 == obj2.lsphctime7
                && obj1.lsphctime8 == obj2.lsphctime8
                && obj1.lsphctime9 == obj2.lsphctime9
                && obj1.lsphctime10 == obj2.lsphctime10
                && obj1.lsphctime11 == obj2.lsphctime11
                && obj1.lsphctime12 == obj2.lsphctime12
                && obj1.lsphctime13 == obj2.lsphctime13
                && obj1.lsphctime14 == obj2.lsphctime14
                && obj1.lsphctime15 == obj2.lsphctime15
                && obj1.lsphctime16 == obj2.lsphctime16
                && obj1.contacttype == obj2.contacttype
                && obj1.contacttype2 == obj2.contacttype2
                && obj1.contacttype3 == obj2.contacttype3
                && obj1.contacttype4 == obj2.contacttype4
                && obj1.contacttype5 == obj2.contacttype5
                && obj1.contacttype6 == obj2.contacttype6
                && obj1.contacttype7 == obj2.contacttype7
                && obj1.contacttype8 == obj2.contacttype8
                && obj1.contacttype9 == obj2.contacttype9
                && obj1.contacttype10 == obj2.contacttype10
                && obj1.contacttype11 == obj2.contacttype11
                && obj1.contacttype12 == obj2.contacttype12
                && obj1.contacttype13 == obj2.contacttype13
                && obj1.contacttype14 == obj2.contacttype14
                && obj1.contacttype15 == obj2.contacttype15
                && obj1.contacttype16 == obj2.contacttype16
                && obj1.setting == obj2.setting
                && obj1.setting2 == obj2.setting2
                && obj1.setting3 == obj2.setting3
                && obj1.setting4 == obj2.setting4
                && obj1.setting5 == obj2.setting5
                && obj1.setting6 == obj2.setting6
                && obj1.setting7 == obj2.setting7
                && obj1.setting8 == obj2.setting8
                && obj1.setting9 == obj2.setting9
                && obj1.setting10 == obj2.setting10
                && obj1.setting11 == obj2.setting11
                && obj1.setting12 == obj2.setting12
                && obj1.setting13 == obj2.setting13
                && obj1.setting14 == obj2.setting14
                && obj1.setting15 == obj2.setting15
                && obj1.setting16 == obj2.setting16
                && obj1.lsphccomp == obj2.lsphccomp
                && obj1.lsphccomp2 == obj2.lsphccomp2
                && obj1.lsphccomp3 == obj2.lsphccomp3
                && obj1.lsphccomp4 == obj2.lsphccomp4
                && obj1.lsphccomp5 == obj2.lsphccomp5
                && obj1.lsphccomp6 == obj2.lsphccomp6
                && obj1.lsphccomp7 == obj2.lsphccomp7
                && obj1.lsphccomp8 == obj2.lsphccomp8
                && obj1.lsphccomp9 == obj2.lsphccomp9
                && obj1.lsphccomp10 == obj2.lsphccomp10
                && obj1.lsphccomp11 == obj2.lsphccomp11
                && obj1.lsphccomp12 == obj2.lsphccomp12
                && obj1.lsphccomp13 == obj2.lsphccomp13
                && obj1.lsphccomp14 == obj2.lsphccomp14
                && obj1.lsphccomp15 == obj2.lsphccomp15
                && obj1.lsphccomp16 == obj2.lsphccomp16
                && obj1.tobresdate == obj2.tobresdate
                && obj1.tobresdate2 == obj2.tobresdate2
                && obj1.tobresdate3 == obj2.tobresdate3
                && obj1.tobrestype == obj2.tobrestype
                && obj1.tobrestype2 == obj2.tobrestype2
                && obj1.tobrestype3 == obj2.tobrestype3
                && obj1.trescomp == obj2.trescomp
                && obj1.trescomp2 == obj2.trescomp2
                && obj1.trescomp3 == obj2.trescomp3
                && obj1.isvalid == obj2.isvalid
                && obj1.intSbp1 == obj2.intSbp1
                && obj1.intSbp2 == obj2.intSbp2
                && obj1.intMeanSbp == obj2.intMeanSbp
                && obj1.Output == obj2.Output
                && obj1.ErrorKey == obj2.ErrorKey
                && obj1.ErrorMessage == obj2.ErrorMessage
                && obj1.ErrorFormat == obj2.ErrorFormat
                && obj1.InvalidRow == obj2.InvalidRow
                && obj1.BPPlus == obj2.BPPlus);
        }

        // this is second one '!='
        public static bool operator !=(MDE obj1, MDE obj2)
        {
            return !(obj1 == obj2);
        }

    }
}