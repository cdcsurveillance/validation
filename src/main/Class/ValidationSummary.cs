﻿using System;
namespace WWValidationApi.Class
{
    public class ValidationSummary
    {
        /// <summary>
        /// Gets or sets the complete records.
        /// All of the records in the MDE batch submitted for edits have been evaluated as completed by the Application, (method still to be determined)
        /// This is different from the TotalComplete below which is referencing records with no issues
        /// </summary>
        /// <value>The complete records.</value>
        public int CompleteRecords { get; set; }

        /// <summary>
        /// Gets or sets the incomplete records.
        /// These records have been exclude from the MDE batch submitted for edits (method still to be determined)
        /// This is different from the TotalComplete below which is referencing records with no issues
        /// Records where weight is missing.
        /// </summary>
        /// <value>The incomplete records.</value>
        public int IncompleteRecords { get; set; }

        /// <summary>
        /// Gets or sets the total MDE Records.
        /// Total Records Submitted in MDE file
        /// SELECT COUNT(*) as MDERecordCount FROM MDE WHERE FileId = @FileID
        /// </summary>
        /// <value>The total MDER ecords.</value>
        public int TotalMDERecords { get; set; }

        /// <summary>
        /// Gets or sets the total quality checks.
        /// Total Quality Checks found in Validation 
        /// SELECT COUNT(*) as QualityCheckCount FROM ValidationErrors WHERE FileId = @FileID AND ValidationType = 'Quality Check'
        /// </summary>
        /// <value>The total quality checks.</value>
        public int TotalQualityChecks { get; set; }

        /// <summary>
        /// Gets or sets the total records with edits.
        /// Total Number of Records with One or More Edits
        /// SELECT COUNT(DISTINCT Row) as TotalNumberOfRecordsWithEdits FROM ValidationErrors WHERE FileID = @FileID
        /// </summary>
        /// <value>The total records with edits.</value>
        public int TotalRecordsWithEdits { get; set; }

        /// <summary>
        /// Gets or sets the total errors.
        /// Total Errors found in Validation
        /// SELECT COUNT(*) as ErrorCount FROM ValidationErrors WHERE FileId = @FileID AND ValidationType = 'Error'
        /// </summary>
        /// <value>The total errors.</value>
        public int TotalErrorCount { get; set; }

        /// <summary>
        /// Gets or sets the error rate.
        /// Error Rate as calcuated by Mathematica
        /// SELECT (@TotalErrorCount/(@TotalMDERecords * 84)*100)
        /// </summary>
        /// <value>The error rate.</value>
        public float ErrorRate { get; set; }

        /// <summary>
        /// Gets or sets the total edits.
        /// Total Number of Edits on All Records
        /// SELECT COUNT(*) FROM ValidationErrors WHERE fileID = @fileID
        /// </summary>
        /// <value>The total edits.</value>
        public int TotalEdits { get; set; }

        /// <summary>
        /// Gets or sets the invalid records.
        /// Records considered "Invalid" where IsValid = 0 AND IsBPPlus = 0
        /// SELECT COUNT(1) FROM MDE WHERE IsValid = 0 AND FileID = @FileID
        /// </summary>
        /// <value>The invalid records.</value>
        public int InvalidRecords { get; set; }

        /// <summary>
        /// Gets or sets the total format errors.
        /// Total Format Errors found in Validation 
        /// SELECT COUNT(*) as FormatErrorCount FROM ValidationErrors WHERE FileId = @FileID AND ValidationType = 'Format Error'
        /// </summary>
        /// <value>The total format errors.</value>
        public int TotalFormatErrors { get; set; }

        /// <summary>
        /// Gets or sets the BPP lus records.
        /// Records considered BPPlus where IsValid = 0 AND IsBPPlus = 1
        /// SELECT COUNT(1) FROM MDE WHERE IsValid = 1 AND IsBPPlus = 1 AND FileID = @FileID
        /// </summary>
        /// <value>The BPP lus records.</value>
        public int BPPlusRecords { get; set; }

        public int RecordsWithoutEdits { get; set; }

        /// <summary>
        /// Gets or sets the first BP date.
        /// </summary>
        /// <value>The first BP date.</value>
        public DateTime? FirstBPDate { get; set; }

        /// <summary>
        /// Gets or sets the last BP date.
        /// </summary>
        /// <value>The last BP date.</value>
        public DateTime? LastBPDate { get; set; }

        /// <summary>
        /// Gets or sets the total edits on complete records.
        /// Total Number of Edits on Complete Records Only
        /// SELECT COUNT(1) FROM ValidationErrors v WHERE Row IN (SELECT RowNum FROM MDE WHERE 1=1 AND IsValid = 1 AND IsBPPlus <> 1 AND FileID = @FileID) AND FileID = @FileID
        /// </summary>
        /// <value>The total edits on complete records.</value>
        public int TotalEditsOnCompleteRecords { get; set; }

        /// <summary>
        /// Gets or sets the duplicates.
        /// Total Number of Records with Duplicates based on unique ScreenSiteID, EncodeID, BPDate
        /// (SELECT ISNULL(SUM(DupCount),0) FROM (SELECT COUNT(1) as DupCount FROM MDE WHERE FileID = @FileID GROUP BY ScreenSiteID, EncodeID, BPDate HAVING COUNT(1) > 1) as DuplicatesCount)
        /// </summary>
        /// <value>The duplicates.</value>
        public int Duplicates { get; set; }

        /// <summary>
        /// Gets or sets the exact duplicates.
        /// Total Number of Records with Duplicates based on the entire record (= total records - distinct list of records)
        /// ExactDuplicates = TotalMDERecords - Unique Records 
        /// </summary>
        /// <value>The exact duplicates.</value>
        public int ExactDuplicates { get; set; }

        /// <summary>
        /// Gets or sets the total complete.
        /// Total Number of records that do not have any issues
        /// TotalComplete = TotalMDERecords - InvalidRecords - BPPlusRecords
        /// </summary>
        /// <value>The total complete.</value>
        public int TotalComplete { get; set; }

        /// <summary>
        /// Gets or sets the total edits on records.
        /// Total Number of errors or qualitychecks completed
        /// TotalEditsOnRecords = TotalErrorCount + TotalQualityChecks
        /// </summary>
        /// <value>The total edits on records.</value>
        public int TotalEditsOnRecords { get; set; }

        public int BaselineScreenings { get; set; }

        public int Rescreenings { get; set; }

        public int FollowUpScreenings { get; set; }

        public int LifestyleProgramAndHealthCoachingSessions { get; set; }
    }
}
