﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;

namespace WisewomanMVCSite.Class
{
    public class SPValidation
    {
        public bool isvalidrow = true;
        public bool iscompleterow = false;
        public bool isbpplusrow = false;
        private int _row = 0;
        private int _totrowcount = 0;
        private DateTime currdate = DateTime.Now;
        private DateTime submissiondate;
        private string mdever;
        private string stfips;
        private string hdansi;
        private string enrollsiteid;
        private string screensiteid;
        private string timeper;
        private string nscreen;
        private string itype;
        private string itype_f;
        private string encodeid;
        private string resansi;
        private string zip;
        private DateTime myb; private string mybstr;
        private string latino;
        private string race1;
        private string race2;
        private string education;
        private string language;
        private string srhc;
        private string srhc_f;
        private string srhb;
        private string srhb_f;
        private string srd;
        private string srd_f;
        private string srha;
        private string srha_f;
        private string hcmeds;
        private string hcmeds_f;
        private string hbpmeds;
        private string hbpmeds_f;
        private string dmeds;
        private string dmeds_f;
        private string hcadhere;
        private string hcadhere_f;
        private string hbpadhere;
        private string hbpadhere_f;
        private string dadhere;
        private string dadhere_f;
        private string bphome;
        private string bphome_f;
        private string bpfreq;
        private string bpfreq_f;
        private string bpsend;
        private string bpsend_f;
        private string fruit;
        private string fruit_f;
        private string vegetables;
        private string vegetables_f;
        private string fish;
        private string fish_f;
        private string grains;
        private string grains_f;
        private string sugar;
        private string sugar_f;
        private string saltwatch;
        private string saltwatch_f;
        private string pamod;
        private string pamod_f;
        private string pavig;
        private string pavig_f;
        private string smoker;
        private string smoker_f;
        private string sechand;
        private string sechand_f;
        private string qolph;
        private string qolph_f;
        private string qolmh;
        private string qolmh_f;
        private string qoleffect;
        private string qoleffect_f;
        private string height;
        private string weight;
        private string weight_f;
        private string waist;
        private string waist_f;
        private string hip;
        private string hip_f;
        private DateTime bpdate; private string bpdatestr;
        private DateTime bpdate_f; private string bpdate_fstr;
        private string sbp1;
        private string sbp1_f;
        private string dbp1;
        private string dbp1_f;
        private string sbp2;
        private string sbp2_f;
        private string dbp2;
        private string dbp2_f;
        private string fast;
        private string fast_f;
        private DateTime tcdate; private string tcdatestr;
        private DateTime tcdate_f; private string tcdate_fstr;
        private string totchol;
        private string totchol_f;
        private string hdl;
        private string hdl_f;
        private string ldl;
        private string ldl_f;
        private string trigly;
        private string trigly_f;
        private DateTime bgdate; private string bgdatestr;
        private DateTime bgdate_f; private string bgdate_fstr;
        private string glucose;
        private string glucose_f;
        private string a1c;
        private string a1c_f;
        private string bpalert;
        private string bpalert_f;
        private DateTime bpdidate; private string bpdidatestr;
        private DateTime bpdidate_f; private string bpdidate_fstr;
        private string bgalert;
        private string bgalert_f;
        private DateTime bgdidate; private string bgdidatestr;
        private DateTime bgdidate_f; private string bgdidate_fstr;
        private DateTime rrcdate; private string rrcdatestr;
        private DateTime rrcdate_f; private string rrcdate_fstr;
        private DateTime rrccomplete; private string rrccompletestr;
        private DateTime rrccomplete_f; private string rrccomplete_fstr;
        private string rrcnut;
        private string rrcnut_f;
        private string rrcpa;
        private string rrcpa_f;
        private string rrcsmoke;
        private string rrcsmoke_f;
        private string rrcmedadhere;
        private string rrcmedadhere_f;
        private DateTime rtcdate; private string rtcdatestr;
        private DateTime rtcdate_f; private string rtcdate_fstr;
        private string rtc;
        private string rtc_f;
        private DateTime refdate; private string refdatestr;
        private DateTime refdate2; private string refdate2str;
        private string lsphcrec;
        private DateTime intervention1; private string intervention1str;
        private DateTime intervention2; private string intervention2str;
        private DateTime intervention3; private string intervention3str;
        private DateTime intervention4; private string intervention4str;
        private DateTime intervention5; private string intervention5str;
        private DateTime intervention6; private string intervention6str;
        private DateTime intervention7; private string intervention7str;
        private DateTime intervention8; private string intervention8str;
        private DateTime intervention9; private string intervention9str;
        private DateTime intervention10; private string intervention10str;
        private DateTime intervention11; private string intervention11str;
        private DateTime intervention12; private string intervention12str;
        private DateTime intervention13; private string intervention13str;
        private DateTime intervention14; private string intervention14str;
        private DateTime intervention15; private string intervention15str;
        private DateTime intervention16; private string intervention16str;
        private string lsphcid1;
        private string lsphcid2;
        private string lsphcid3;
        private string lsphcid4;
        private string lsphcid5;
        private string lsphcid6;
        private string lsphcid7;
        private string lsphcid8;
        private string lsphcid9;
        private string lsphcid10;
        private string lsphcid11;
        private string lsphcid12;
        private string lsphcid13;
        private string lsphcid14;
        private string lsphcid15;
        private string lsphcid16;
        private string slsphctime1;
        private string slsphctime2;
        private string slsphctime3;
        private string slsphctime4;
        private string slsphctime5;
        private string slsphctime6;
        private string slsphctime7;
        private string slsphctime8;
        private string slsphctime9;
        private string slsphctime10;
        private string slsphctime11;
        private string slsphctime12;
        private string slsphctime13;
        private string slsphctime14;
        private string slsphctime15;
        private string slsphctime16;
        private int lsphctime1;
        private int lsphctime2;
        private int lsphctime3;
        private int lsphctime4;
        private int lsphctime5;
        private int lsphctime6;
        private int lsphctime7;
        private int lsphctime8;
        private int lsphctime9;
        private int lsphctime10;
        private int lsphctime11;
        private int lsphctime12;
        private int lsphctime13;
        private int lsphctime14;
        private int lsphctime15;
        private int lsphctime16;
        private string contacttype1;
        private string contacttype2;
        private string contacttype3;
        private string contacttype4;
        private string contacttype5;
        private string contacttype6;
        private string contacttype7;
        private string contacttype8;
        private string contacttype9;
        private string contacttype10;
        private string contacttype11;
        private string contacttype12;
        private string contacttype13;
        private string contacttype14;
        private string contacttype15;
        private string contacttype16;
        private string setting1;
        private string setting2;
        private string setting3;
        private string setting4;
        private string setting5;
        private string setting6;
        private string setting7;
        private string setting8;
        private string setting9;
        private string setting10;
        private string setting11;
        private string setting12;
        private string setting13;
        private string setting14;
        private string setting15;
        private string setting16;
        private string lsphccomp1;
        private string lsphccomp2;
        private string lsphccomp3;
        private string lsphccomp4;
        private string lsphccomp5;
        private string lsphccomp6;
        private string lsphccomp7;
        private string lsphccomp8;
        private string lsphccomp9;
        private string lsphccomp10;
        private string lsphccomp11;
        private string lsphccomp12;
        private string lsphccomp13;
        private string lsphccomp14;
        private string lsphccomp15;
        private string lsphccomp16;
        private DateTime tobresdate; private string tobresdatestr;
        private DateTime tobresdate2; private string tobresdate2str;
        private DateTime tobresdate3; private string tobresdate3str;
        private string tobrestype;
        private string tobrestype2;
        private string tobrestype3;
        private string trescomp;
        private string trescomp2;
        private string trescomp3;

        /* Declare numeric equivalents to prepare for calculations */
        private double a1c_f_num;
        private double a1c_num;
        private int dbp1_f_num;
        private int dbp1_num;
        private int dbp2_f_num;
        private int dbp2_num;
        private int fruit_f_num;
        private int fruit_num;
        private int glucose_f_num;
        private int glucose_num;
        private int hdl_f_num;
        private int hdl_num;
        private int height_num;
        private int hip_f_num;
        private int hip_num;
        private int ldl_f_num;
        private int ldl_num;
        private int lsphcrec_num;
        private int nscreen_num;
        private int pamod_f_num;
        private int pamod_num;
        private int pavig_f_num;
        private int pavig_num;
        private int qoleffect_f_num;
        private int qoleffect_num;
        private int qolmh_f_num;
        private int qolmh_num;
        private int qolph_f_num;
        private int qolph_num;
        private int rrcmedadhere_f_num;
        private int rrcmedadhere_num;
        private int rrcnut_f_num;
        private int rrcnut_num;
        private int rrcpa_f_num;
        private int rrcpa_num;
        private int rrcsmoke_f_num;
        private int rrcsmoke_num;
        private int rtc_num;
        private int sechand_f_num;
        private int sechand_num;
        private int sbp2_f_num;
        private int sbp2_num;
        private int sbp1_f_num;
        private int sbp1_num;
        private int totchol_f_num;
        private int totchol_num;
        private int trigly_f_num;
        private int trigly_num;
        private int vegetables_f_num;
        private int vegetables_num;
        private int waist_f_num;
        private int waist_num;
        private int weight_f_num;
        private int weight_num;
        private int zip_num;
        private int rtc_f_num;
        private int hcadhere_num;
        private int hcadhere_f_num;
        private int hbpadhere_num;
        private int hbpadhere_f_num;
        private int dadhere_num;
        private int dadhere_f_num;

        public SPValidation(MDE mdefield, int row, int totrowcount)
        {
            //config = oconfig;
            _row = row;
            _totrowcount = totrowcount;
            submissiondate = toDateTime("07012013");
            mdever = SetUniform(mdefield.mdever);
            stfips = SetUniform(mdefield.stfips);
            hdansi = SetUniform(mdefield.hdansi);
            enrollsiteid = SetUniform(mdefield.enrollsiteid);
            screensiteid = SetUniform(mdefield.screensiteid);
            timeper = SetUniform(mdefield.timeper);
            nscreen = SetUniform(mdefield.nscreen);
            itype = SetUniform(mdefield.type);
            itype_f = SetUniform(mdefield.type_f);
            encodeid = SetUniform(mdefield.encodeid);
            resansi = SetUniform(mdefield.resansi);
            zip = SetUniform(mdefield.zip);
            myb = toDateTime(mdefield.myb); mybstr = SetUniform(mdefield.myb);
            latino = SetUniform(mdefield.latino);
            race1 = SetUniform(mdefield.race1);
            race2 = SetUniform(mdefield.race2);
            education = SetUniform(mdefield.education);
            language = SetUniform(mdefield.language);
            srhc = SetUniform(mdefield.srhc);
            srhc_f = SetUniform(mdefield.srhc_f);
            srhb = SetUniform(mdefield.srhb);
            srhb_f = SetUniform(mdefield.srhb_f);
            srd = SetUniform(mdefield.srd);
            srd_f = SetUniform(mdefield.srd_f);
            srha = SetUniform(mdefield.srha);
            srha_f = SetUniform(mdefield.srha_f);
            hcmeds = SetUniform(mdefield.hcmeds);
            hcmeds_f = SetUniform(mdefield.hcmeds_f);
            hbpmeds = SetUniform(mdefield.hbpmeds);
            hbpmeds_f = SetUniform(mdefield.hbpmeds_f);
            dmeds = SetUniform(mdefield.dmeds);
            dmeds_f = SetUniform(mdefield.dmeds_f);
            hcadhere = SetUniform(mdefield.hcadhere);
            hcadhere_f = SetUniform(mdefield.hcadhere_f);
            hbpadhere = SetUniform(mdefield.hbpadhere);
            hbpadhere_f = SetUniform(mdefield.hbpadhere_f);
            dadhere = SetUniform(mdefield.dadhere);
            dadhere_f = SetUniform(mdefield.dadhere_f);
            bphome = SetUniform(mdefield.bphome);
            bphome_f = SetUniform(mdefield.bphome_f);
            bpfreq = SetUniform(mdefield.bpfreq);
            bpfreq_f = SetUniform(mdefield.bpfreq_f);
            bpsend = SetUniform(mdefield.bpsend);
            bpsend_f = SetUniform(mdefield.bpsend_f);
            fruit = SetUniform(mdefield.fruit);
            fruit_f = SetUniform(mdefield.fruit_f);
            vegetables = SetUniform(mdefield.vegetables);
            vegetables_f = SetUniform(mdefield.vegetables_f);
            fish = SetUniform(mdefield.fish);
            fish_f = SetUniform(mdefield.fish_f);
            grains = SetUniform(mdefield.grains);
            grains_f = SetUniform(mdefield.grains_f);
            sugar = SetUniform(mdefield.sugar);
            sugar_f = SetUniform(mdefield.sugar_f);
            saltwatch = SetUniform(mdefield.saltwatch);
            saltwatch_f = SetUniform(mdefield.saltwatch_f);
            pamod = SetUniform(mdefield.pamod);
            pamod_f = SetUniform(mdefield.pamod_f);
            pavig = SetUniform(mdefield.pavig);
            pavig_f = SetUniform(mdefield.pavig_f);
            smoker = SetUniform(mdefield.smoker);
            smoker_f = SetUniform(mdefield.smoker_f);
            sechand = SetUniform(mdefield.sechand);
            sechand_f = SetUniform(mdefield.sechand_f);
            qolph = SetUniform(mdefield.qolph);
            qolph_f = SetUniform(mdefield.qolph_f);
            qolmh = SetUniform(mdefield.qolmh);
            qolmh_f = SetUniform(mdefield.qolmh_f);
            qoleffect = SetUniform(mdefield.qoleffect);
            qoleffect_f = SetUniform(mdefield.qoleffect_f);
            height = SetUniform(mdefield.height);
            weight = SetUniform(mdefield.weight);
            weight_f = SetUniform(mdefield.weight_f);
            waist = SetUniform(mdefield.waist);
            waist_f = SetUniform(mdefield.waist_f);
            hip = SetUniform(mdefield.hip);
            hip_f = SetUniform(mdefield.hip_f);
            bpdate = toDateTime(mdefield.bpdate); bpdatestr = SetUniform(mdefield.bpdate);
            bpdate_f = toDateTime(mdefield.bpdate_f); bpdate_fstr = SetUniform(mdefield.bpdate_f);
            sbp1 = SetUniform(mdefield.sbp1);
            sbp1_f = SetUniform(mdefield.sbp1_f);
            dbp1 = SetUniform(mdefield.dbp1);
            dbp1_f = SetUniform(mdefield.dbp1_f);
            sbp2 = SetUniform(mdefield.sbp2);
            sbp2_f = SetUniform(mdefield.sbp2_f);
            dbp2 = SetUniform(mdefield.dbp2);
            dbp2_f = SetUniform(mdefield.dbp2_f);
            fast = SetUniform(mdefield.fast);
            fast_f = SetUniform(mdefield.fast_f);
            tcdate = toDateTime(mdefield.tcdate); tcdatestr = SetUniform(mdefield.tcdate);
            tcdate_f = toDateTime(mdefield.tcdate_f); tcdate_fstr = SetUniform(mdefield.tcdate_f);
            totchol = SetUniform(mdefield.totchol);
            totchol_f = SetUniform(mdefield.totchol_f);
            hdl = SetUniform(mdefield.hdl);
            hdl_f = SetUniform(mdefield.hdl_f);
            ldl = SetUniform(mdefield.ldl);
            ldl_f = SetUniform(mdefield.ldl_f);
            trigly = SetUniform(mdefield.trigly);
            trigly_f = SetUniform(mdefield.trigly_f);
            bgdate = toDateTime(mdefield.bgdate); bgdatestr = SetUniform(mdefield.bgdate);
            bgdate_f = toDateTime(mdefield.bgdate_f); bgdate_fstr = SetUniform(mdefield.bgdate_f);
            glucose = SetUniform(mdefield.glucose);
            glucose_f = SetUniform(mdefield.glucose_f);
            a1c = SetUniform(mdefield.a1c);
            a1c_f = SetUniform(mdefield.a1c_f);
            bpalert = SetUniform(mdefield.bpalert);
            bpalert_f = SetUniform(mdefield.bpalert_f);
            bpdidate = toDateTime(mdefield.bpdidate); bpdidatestr = SetUniform(mdefield.bpdidate);
            bpdidate_f = toDateTime(mdefield.bpdidate_f); bpdidate_fstr = SetUniform(mdefield.bpdidate_f);
            bgalert = SetUniform(mdefield.bgalert);
            bgalert_f = SetUniform(mdefield.bgalert_f);
            bgdidate = toDateTime(mdefield.bgdidate); bgdidatestr = SetUniform(mdefield.bgdidate);
            bgdidate_f = toDateTime(mdefield.bgdidate_f); bgdidate_fstr = SetUniform(mdefield.bgdidate_f);
            rrcdate = toDateTime(mdefield.rrcdate); rrcdatestr = SetUniform(mdefield.rrcdate);
            rrcdate_f = toDateTime(mdefield.rrcdate_f); rrcdate_fstr = SetUniform(mdefield.rrcdate_f);
            rrccomplete = toDateTime(mdefield.rrccomplete); rrccompletestr = SetUniform(mdefield.rrccomplete);
            rrccomplete_f = toDateTime(mdefield.rrccomplete_f); rrccomplete_fstr = SetUniform(mdefield.rrccomplete_f);
            rrcnut = SetUniform(mdefield.rrcnut);
            rrcnut_f = SetUniform(mdefield.rrcnut_f);
            rrcpa = SetUniform(mdefield.rrcpa);
            rrcpa_f = SetUniform(mdefield.rrcpa_f);
            rrcsmoke = SetUniform(mdefield.rrcsmoke);
            rrcsmoke_f = SetUniform(mdefield.rrcsmoke_f);
            rrcmedadhere = SetUniform(mdefield.rrcmedadhere);
            rrcmedadhere_f = SetUniform(mdefield.rrcmedadhere_f);
            rtcdate = toDateTime(mdefield.rtcdate); rtcdatestr = SetUniform(mdefield.rtcdate);
            rtcdate_f = toDateTime(mdefield.rtcdate_f); rtcdate_fstr = SetUniform(mdefield.rtcdate_f);
            rtc = SetUniform(mdefield.rtc);
            rtc_f = SetUniform(mdefield.rtc_f);
            refdate = toDateTime(mdefield.refdate); refdatestr = SetUniform(mdefield.refdate);
            refdate2 = toDateTime(mdefield.refdate2); refdate2str = SetUniform(mdefield.refdate2);
            lsphcrec = SetUniform(mdefield.lsphcrec);
            intervention1 = toDateTime(mdefield.intervention); intervention1str = SetUniform(mdefield.intervention);
            intervention2 = toDateTime(mdefield.intervention2); intervention2str = SetUniform(mdefield.intervention2);
            intervention3 = toDateTime(mdefield.intervention3); intervention3str = SetUniform(mdefield.intervention3);
            intervention4 = toDateTime(mdefield.intervention4); intervention4str = SetUniform(mdefield.intervention4);
            intervention5 = toDateTime(mdefield.intervention5); intervention5str = SetUniform(mdefield.intervention5);
            intervention6 = toDateTime(mdefield.intervention6); intervention6str = SetUniform(mdefield.intervention6);
            intervention7 = toDateTime(mdefield.intervention7); intervention7str = SetUniform(mdefield.intervention7);
            intervention8 = toDateTime(mdefield.intervention8); intervention8str = SetUniform(mdefield.intervention8);
            intervention9 = toDateTime(mdefield.intervention9); intervention9str = SetUniform(mdefield.intervention9);
            intervention10 = toDateTime(mdefield.intervention10); intervention10str = SetUniform(mdefield.intervention10);
            intervention11 = toDateTime(mdefield.intervention11); intervention11str = SetUniform(mdefield.intervention11);
            intervention12 = toDateTime(mdefield.intervention12); intervention12str = SetUniform(mdefield.intervention12);
            intervention13 = toDateTime(mdefield.intervention13); intervention13str = SetUniform(mdefield.intervention13);
            intervention14 = toDateTime(mdefield.intervention14); intervention14str = SetUniform(mdefield.intervention14);
            intervention15 = toDateTime(mdefield.intervention15); intervention15str = SetUniform(mdefield.intervention15);
            intervention16 = toDateTime(mdefield.intervention16); intervention16str = SetUniform(mdefield.intervention16);
            lsphcid1 = mdefield.lsphcid;
            lsphcid2 = mdefield.lsphcid2;
            lsphcid3 = mdefield.lsphcid3;
            lsphcid4 = mdefield.lsphcid4;
            lsphcid5 = mdefield.lsphcid5;
            lsphcid6 = mdefield.lsphcid6;
            lsphcid7 = mdefield.lsphcid7;
            lsphcid8 = mdefield.lsphcid8;
            lsphcid9 = mdefield.lsphcid9;
            lsphcid10 = mdefield.lsphcid10;
            lsphcid11 = mdefield.lsphcid11;
            lsphcid12 = mdefield.lsphcid12;
            lsphcid13 = mdefield.lsphcid13;
            lsphcid14 = mdefield.lsphcid14;
            lsphcid15 = mdefield.lsphcid15;
            lsphcid16 = mdefield.lsphcid16;
            slsphctime1 = SetUniform(mdefield.lsphctime);
            slsphctime2 = SetUniform(mdefield.lsphctime2);
            slsphctime3 = SetUniform(mdefield.lsphctime3);
            slsphctime4 = SetUniform(mdefield.lsphctime4);
            slsphctime5 = SetUniform(mdefield.lsphctime5);
            slsphctime6 = SetUniform(mdefield.lsphctime6);
            slsphctime7 = SetUniform(mdefield.lsphctime7);
            slsphctime8 = SetUniform(mdefield.lsphctime8);
            slsphctime9 = SetUniform(mdefield.lsphctime9);
            slsphctime10 = SetUniform(mdefield.lsphctime10);
            slsphctime11 = SetUniform(mdefield.lsphctime11);
            slsphctime12 = SetUniform(mdefield.lsphctime12);
            slsphctime13 = SetUniform(mdefield.lsphctime13);
            slsphctime14 = SetUniform(mdefield.lsphctime14);
            slsphctime15 = SetUniform(mdefield.lsphctime15);
            slsphctime16 = SetUniform(mdefield.lsphctime16);
            lsphctime1 = toInt(mdefield.lsphctime);
            lsphctime2 = toInt(mdefield.lsphctime2);
            lsphctime3 = toInt(mdefield.lsphctime3);
            lsphctime4 = toInt(mdefield.lsphctime4);
            lsphctime5 = toInt(mdefield.lsphctime5);
            lsphctime6 = toInt(mdefield.lsphctime6);
            lsphctime7 = toInt(mdefield.lsphctime7);
            lsphctime8 = toInt(mdefield.lsphctime8);
            lsphctime9 = toInt(mdefield.lsphctime9);
            lsphctime10 = toInt(mdefield.lsphctime10);
            lsphctime11 = toInt(mdefield.lsphctime11);
            lsphctime12 = toInt(mdefield.lsphctime12);
            lsphctime13 = toInt(mdefield.lsphctime13);
            lsphctime14 = toInt(mdefield.lsphctime14);
            lsphctime15 = toInt(mdefield.lsphctime15);
            lsphctime16 = toInt(mdefield.lsphctime16);
            contacttype1 = mdefield.contacttype;
            contacttype2 = mdefield.contacttype2;
            contacttype3 = mdefield.contacttype3;
            contacttype4 = mdefield.contacttype4;
            contacttype5 = mdefield.contacttype5;
            contacttype6 = mdefield.contacttype6;
            contacttype7 = mdefield.contacttype7;
            contacttype8 = mdefield.contacttype8;
            contacttype9 = mdefield.contacttype9;
            contacttype10 = mdefield.contacttype10;
            contacttype11 = mdefield.contacttype11;
            contacttype12 = mdefield.contacttype12;
            contacttype13 = mdefield.contacttype13;
            contacttype14 = mdefield.contacttype14;
            contacttype15 = mdefield.contacttype15;
            contacttype16 = mdefield.contacttype16;
            setting1 = mdefield.setting;
            setting2 = mdefield.setting2;
            setting3 = mdefield.setting3;
            setting4 = mdefield.setting4;
            setting5 = mdefield.setting5;
            setting6 = mdefield.setting6;
            setting7 = mdefield.setting7;
            setting8 = mdefield.setting8;
            setting9 = mdefield.setting9;
            setting10 = mdefield.setting10;
            setting11 = mdefield.setting11;
            setting12 = mdefield.setting12;
            setting13 = mdefield.setting13;
            setting14 = mdefield.setting14;
            setting15 = mdefield.setting15;
            setting16 = mdefield.setting16;
            lsphccomp1 = mdefield.lsphccomp;
            lsphccomp2 = mdefield.lsphccomp2;
            lsphccomp3 = mdefield.lsphccomp3;
            lsphccomp4 = mdefield.lsphccomp4;
            lsphccomp5 = mdefield.lsphccomp5;
            lsphccomp6 = mdefield.lsphccomp6;
            lsphccomp7 = mdefield.lsphccomp7;
            lsphccomp8 = mdefield.lsphccomp8;
            lsphccomp9 = mdefield.lsphccomp9;
            lsphccomp10 = mdefield.lsphccomp10;
            lsphccomp11 = mdefield.lsphccomp11;
            lsphccomp12 = mdefield.lsphccomp12;
            lsphccomp13 = mdefield.lsphccomp13;
            lsphccomp14 = mdefield.lsphccomp14;
            lsphccomp15 = mdefield.lsphccomp15;
            lsphccomp16 = mdefield.lsphccomp16;
            tobresdate = toDateTime(mdefield.tobresdate); tobresdatestr = SetUniform(mdefield.tobresdate);
            tobresdate2 = toDateTime(mdefield.tobresdate2); tobresdate2str = SetUniform(mdefield.tobresdate2);
            tobresdate3 = toDateTime(mdefield.tobresdate3); tobresdate3str = SetUniform(mdefield.tobresdate3);
            tobrestype = mdefield.tobrestype;
            tobrestype2 = mdefield.tobrestype2;
            tobrestype3 = mdefield.tobrestype3;
            trescomp = mdefield.trescomp;
            trescomp2 = mdefield.trescomp2;
            trescomp3 = mdefield.trescomp3;

            /*set numeric values to ease calculations*/
            a1c_f_num = toDouble(a1c_f);
            a1c_num = toDouble(a1c);
            dbp1_f_num = toInt(dbp1_f);
            dbp1_num = toInt(dbp1);
            dbp2_f_num = toInt(dbp2_f);
            dbp2_num = toInt(dbp2);
            fruit_f_num = toInt(fruit_f);
            fruit_num = toInt(fruit);
            glucose_f_num = toInt(glucose_f);
            glucose_num = toInt(glucose);
            hdl_f_num = toInt(hdl_f);
            hdl_num = toInt(hdl);
            height_num = toInt(height);
            hip_f_num = toInt(hip_f);
            hip_num = toInt(hip);
            ldl_f_num = toInt(ldl_f);
            ldl_num = toInt(ldl);
            lsphcrec_num = toInt(lsphcrec);
            nscreen_num = toInt(nscreen);
            pamod_f_num = toInt(pamod_f);
            pamod_num = toInt(pamod);
            pavig_f_num = toInt(pavig_f);
            pavig_num = toInt(pavig);
            qoleffect_f_num = toInt(qoleffect_f);
            qoleffect_num = toInt(qoleffect);
            qolmh_num = toInt(qolmh);
            qolph_f_num = toInt(qolph_f);
            qolph_num = toInt(qolph);
            rrcmedadhere_f_num = toInt(rrcmedadhere_f);
            rrcmedadhere_num = toInt(rrcmedadhere);
            rrcnut_f_num = toInt(rrcnut_f);
            rrcnut_num = toInt(rrcnut);
            rrcpa_f_num = toInt(rrcpa_f);
            rrcpa_num = toInt(rrcpa);
            rrcsmoke_f_num = toInt(rrcsmoke_f);
            rrcsmoke_num = toInt(rrcsmoke);
            rtc_num = toInt(rtc);
            sechand_f_num = toInt(sechand_f);
            sechand_num = toInt(sechand);
            sbp2_f_num = toInt(sbp2_f);
            sbp2_num = toInt(sbp2);
            sbp1_f_num = toInt(sbp1_f);
            sbp1_num = toInt(sbp1);
            totchol_f_num = toInt(totchol_f);
            totchol_num = toInt(totchol);
            trigly_f_num = toInt(trigly_f);
            trigly_num = toInt(trigly);
            vegetables_f_num = toInt(vegetables_f);
            vegetables_num = toInt(vegetables);
            waist_f_num = toInt(waist_f);
            waist_num = toInt(waist);
            weight_f_num = toInt(weight_f);
            weight_num = toInt(weight);
            zip_num = toInt(zip);
            rtc_f_num = toInt(rtc_f);
            hcadhere_num = toInt(hcadhere);
            hcadhere_f_num = toInt(hcadhere_f);
            hbpadhere_num = toInt(hbpadhere);
            hbpadhere_f_num = toInt(hbpadhere_f);
            dadhere_num = toInt(dadhere);
            dadhere_f_num = toInt(dadhere_f);
        }

        public void ValidateMDE(List<SPValidationError> errorCollection)
        {
            int mean_sbp_f;
            int mean_sbp;
            int mean_dbp;
            int mean_dbp_f;
            bool bpdate_valid;
            bool bpdate_f_valid;
            bool bgdate_valid;
            bool bgdate_f_valid;
            bool bpdidate_valid;
            bool bpdidate_f_valid;
            bool bgdidate_valid;
            bool bgdidate_f_valid;
            bool rrcdate_valid;
            bool rrcdate_f_valid;
            bool rtcdate_valid;
            bool rtcdate_f_valid;
            bool rcccomplete_valid;
            bool rcccomplete_f_valid;
            bool refdate_valid;
            bool refdate2_valid;
            int intervention_count;
            DateTime firstbpdate;
            DateTime[] intervention = new DateTime[16];
            string[] interventionstr = new string[16];
            bool[] intervention_valid = new bool[16];
            string[] lsphcid = new string[16];
            string[] contacttype = new string[16];
            string[] setting = new string[16];
            string[] lsphccomp = new string[16];
            int[] lsphctime = new int[16];
            string[] slsphctime = new string[16];
            bool not_diabetic = false;
            bool not_diabetic_f = false;

            //This section eliminates the "invalid" records and sets the boolean value for a invalid row/record. This code was provided by Mathematica
            isvalidrow = ValidateRow();
            iscompleterow = IsCompleteRow();
            isbpplusrow = IsBpPlusRow();

            //******MOVED THIS SECTION OF FUNCTIONALITY INTO SQL
            //      It should not have been exiting out of the validation anyway
            //      When finding a Invalid record - according to Franco & Michaela

            //if (IsMissing(mybstr) && ValidateIn(itype, new string[] { "1", "2" })) { isvalidrow = false; return; } //("3D_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            //if (IsMissing(latino) || latino == "9") { isvalidrow = false; return; } //("3E_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            //if (ValidateIn(race1, new string[] { "9", "", "." }) && latino != "1") { isvalidrow = false; return; } //("3F_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            //if (ValidateIn(srhc, new string[] { "9", "", "." })) { isvalidrow = false; return; } //("4A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            //if (ValidateIn(srhb, new string[] { "9", "", "." })) { isvalidrow = false; return; } //("4B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            //if (ValidateIn(srd, new string[] { "9", "", "." })) { isvalidrow = false; return; } //("4C_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            //if (ValidateIn(srha, new string[] { "9", "", "." })) { isvalidrow = false; return; } //("4D_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            //if (ValidateIn(hcmeds, new string[] { "9", "", "." })) { isvalidrow = false; return; } //("5A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            //if (ValidateIn(hbpmeds, new string[] { "9", "", "." })) { isvalidrow = false; return; } //("5B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            //if (ValidateIn(dmeds, new string[] { "9", "", "." })) { isvalidrow = false; return; } //("5C_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_dmeds == "5C_1,";
            //if (ValidateIn(fruit, new string[] { "99", "", "." })) { isvalidrow = false; return; } //("7A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_fruit == "7A_1,";
            //if (fruit_num > 50 && !ValidateIn(fruit, new string[] { "88", "99" })) { isvalidrow = false; return; } //("7A_3", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_fruit == strip(error_fruit)|| "7A_3,";
            //if (ValidateIn(vegetables, new string[] { "", ".", "99" })) { isvalidrow = false; return; } //("7B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_vegetables == "7B_1,";
            //if (vegetables_num > 15 && !ValidateIn(vegetables, new string[] { "88", "99" })) { isvalidrow = false; return; } //("7B_3", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_vegetables == strip(error_vegetables)|| "7B_3,";
            //if (ValidateIn(fish, new string[] { "9", "", "." })) { isvalidrow = false; return; } //("7C_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_fish == "7C_1,";
            //if (ValidateIn(grains, new string[] { "9", "", "." })) { isvalidrow = false; return; } //("7D_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_grains == "7D_1,";
            //if (ValidateIn(sugar, new string[] { "9", "", "." })) { isvalidrow = false; return; } //("7E_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_sugar == "7E_1,";
            //if (ValidateIn(pamod, new string[] { "999", "", "." })) { isvalidrow = false; return; } //("8A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_pamod == "8A_1,";
            //if (ValidateIn(pavig, new string[] { "999", "", "." })) { isvalidrow = false; return; } //("8B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_pavig == "8B_1,";
            //if (ValidateIn(smoker, new string[] { "9", "", "." })) { isvalidrow = false; return; } //("9A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_smoker == "9A_1,";
            //if (ValidateIn(height, new string[] { "99", "", "." })) { isvalidrow = false; return; } //("11A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_height == strip(error_height)|| "11A_1,";
            //if (((height_num < 52 && height != ".") | height_num > 76) & !ValidateIn(height, new string[] { "77", "88", "99" })) { isvalidrow = false; return; } //("11A_2", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_height == strip(error_height)|| "11A_2,";
            //if (ValidateIn(height, new string[] { "77" })) { isvalidrow = false; return; } //("11A_4", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_height == "11A_4,";
            //if (ValidateIn(height, new string[] { "88" })) { isvalidrow = false; return; } //("11A_5", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_height == strip(error_height)|| "11A_5,";
            //if (ValidateIn(weight, new string[] { "999", "", "." })) { isvalidrow = false; return; } //("11B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_weight == "11B_1,";
            //if (((weight_num < 74 && weight != ".") | weight_num > 460) & !ValidateIn(weight, new string[] { "777", "888", "999" })) { isvalidrow = false; return; } //("11B_2", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_weight == strip(error_weight)|| "11B_2,";
            //if (ValidateIn(bpdatestr, new string[] { "", "." })) { isvalidrow = false; return; } //("12A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_bpdate == strip(error_bpdate)|| "12A_1,";
            //if (ValidateIn(sbp1, new string[] { "999", "", "." })) { isvalidrow = false; return; } //("12B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_spbi1 == "12B_1,";
            //if (sbp1 == "888") { isvalidrow = false; return; } //("12B_3", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_spbi1 == strip(error_spbi1)|| "12B_3,";
            //if (((sbp1_num < 74 && sbp1 != ".") || sbp1_num > 260) && !ValidateIn(sbp1, new string[] { "777", "888", "999" })) { isvalidrow = false; return; } //("12B_2", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_spbi1 == strip(error_spbi1)|| "12B_2,";
            //if (sbp1 == "777") { isvalidrow = false; return; } //("12B_5", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_spbi1 == strip(error_spbi1)|| "12B_5,";
            //if (ValidateIn(totchol, new string[] { "999", "", "." })) { isvalidrow = false; return; } //("14B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_totchol == "14B_1,";
            //else if (((totchol_num < 44 && totchol != ".") | totchol_num > 702) && !ValidateIn(totchol, new string[] { "777", "888", "999" })) { isvalidrow = false; return; } //("14B_2", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_totchol == strip(error_totchol)|| "14B_2,";
            //if (glucose == "999" && a1c == "9999") { isvalidrow = false; return; } //("15B_2", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_glucose == strip(error_glucose)|| "15B_2,";
            //if (ValidateIn(glucose, new string[] { "", "." }) && (ValidateIn(a1c, new string[] { "", "." }) && ValidateIn(itype, new string[] { "1", "2" }))) { isvalidrow = false; return; }
            //if (((glucose_num < 37 && glucose != ".") | glucose_num > 571) && !ValidateIn(glucose, new string[] { "777", "888", "999" }) && (a1c_num < 2.8 && a1c != ".") || (a1c_num > 16.2 && !ValidateIn(a1c, new string[] { "7777", "8888", "9999" }))) { isvalidrow = false; return; }


            //if (IsMissing(mybstr) && ValidateIn(itype, new string[] { "1", "2" })) { isvalidrow = false; } //("3D_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            //if (IsMissing(latino) || latino == "9") { isvalidrow = false; } //("3E_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            //if (ValidateIn(race1, new string[] { "9", "", "." }) && latino != "1") { isvalidrow = false; } //("3F_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            //if (ValidateIn(srhc, new string[] { "9", "", "." })) { isvalidrow = false; } //("4A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            //if (ValidateIn(srhb, new string[] { "9", "", "." })) { isvalidrow = false; } //("4B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            //if (ValidateIn(srd, new string[] { "9", "", "." })) { isvalidrow = false; } //("4C_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            //if (ValidateIn(srha, new string[] { "9", "", "." })) { isvalidrow = false; } //("4D_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            //if (ValidateIn(hcmeds, new string[] { "9", "", "." })) { isvalidrow = false; } //("5A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            //if (ValidateIn(hbpmeds, new string[] { "9", "", "." })) { isvalidrow = false; } //("5B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            //if (ValidateIn(dmeds, new string[] { "9", "", "." })) { isvalidrow = false; } //("5C_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_dmeds == "5C_1,";
            //if (ValidateIn(fruit, new string[] { "99", "", "." })) { isvalidrow = false; } //("7A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_fruit == "7A_1,";
            //if (fruit_num > 50 && !ValidateIn(fruit, new string[] { "88", "99" })) { isvalidrow = false; } //("7A_3", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_fruit == strip(error_fruit)|| "7A_3,";
            //if (ValidateIn(vegetables, new string[] { "", ".", "99" })) { isvalidrow = false; } //("7B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_vegetables == "7B_1,";
            //if (vegetables_num > 15 && !ValidateIn(vegetables, new string[] { "88", "99" })) { isvalidrow = false; } //("7B_3", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_vegetables == strip(error_vegetables)|| "7B_3,";
            //if (ValidateIn(fish, new string[] { "9", "", "." })) { isvalidrow = false; } //("7C_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_fish == "7C_1,";
            //if (ValidateIn(grains, new string[] { "9", "", "." })) { isvalidrow = false; } //("7D_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_grains == "7D_1,";
            //if (ValidateIn(sugar, new string[] { "9", "", "." })) { isvalidrow = false; } //("7E_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_sugar == "7E_1,";
            //if (ValidateIn(pamod, new string[] { "999", "", "." })) { isvalidrow = false; } //("8A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_pamod == "8A_1,";
            //if (ValidateIn(pavig, new string[] { "999", "", "." })) { isvalidrow = false; } //("8B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_pavig == "8B_1,";
            //if (ValidateIn(smoker, new string[] { "9", "", "." })) { isvalidrow = false; } //("9A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_smoker == "9A_1,";
            //if (ValidateIn(height, new string[] { "99", "", "." })) { isvalidrow = false;  } //("11A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_height == strip(error_height)|| "11A_1,";
            //if (((height_num < 52 && height != ".") | height_num > 76) & !ValidateIn(height, new string[] { "77", "88", "99" })) { isvalidrow = false; } //("11A_2", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_height == strip(error_height)|| "11A_2,";
            //if (ValidateIn(height, new string[] { "77" })) { isvalidrow = false; } //("11A_4", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_height == "11A_4,";
            //if (ValidateIn(height, new string[] { "88" })) { isvalidrow = false; } //("11A_5", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_height == strip(error_height)|| "11A_5,";
            //if (ValidateIn(weight, new string[] { "999", "", "." })) { isvalidrow = false; } //("11B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_weight == "11B_1,";
            //if (((weight_num < 74 && weight != ".") | weight_num > 460) & !ValidateIn(weight, new string[] { "777", "888", "999" })) { isvalidrow = false; } //("11B_2", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_weight == strip(error_weight)|| "11B_2,";
            //if (ValidateIn(bpdatestr, new string[] { "", "." })) { isvalidrow = false; } //("12A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_bpdate == strip(error_bpdate)|| "12A_1,";
            //if (ValidateIn(sbp1, new string[] { "999", "", "." })) { isvalidrow = false; } //("12B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_spbi1 == "12B_1,";
            //if (sbp1 == "888") { isvalidrow = false; } //("12B_3", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_spbi1 == strip(error_spbi1)|| "12B_3,";
            //if (((sbp1_num < 74 && sbp1 != ".") || sbp1_num > 260) && !ValidateIn(sbp1, new string[] { "777", "888", "999" })) { isvalidrow = false; } //("12B_2", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_spbi1 == strip(error_spbi1)|| "12B_2,";
            //if (sbp1 == "777") { isvalidrow = false; } //("12B_5", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_spbi1 == strip(error_spbi1)|| "12B_5,";
            //if (ValidateIn(totchol, new string[] { "999", "", "." })) { isvalidrow = false; } //("14B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_totchol == "14B_1,";
            //else if (((totchol_num < 44 && totchol != ".") | totchol_num > 702) && !ValidateIn(totchol, new string[] { "777", "888", "999" })) { isvalidrow = false; } //("14B_2", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_totchol == strip(error_totchol)|| "14B_2,";
            //if (glucose == "999" && a1c == "9999") { isvalidrow = false; } //("15B_2", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_glucose == strip(error_glucose)|| "15B_2,";
            //if (ValidateIn(glucose, new string[] { "", "." }) && (ValidateIn(a1c, new string[] { "", "." }) && ValidateIn(itype, new string[] { "1", "2" }))) { isvalidrow = false;  }
            //if (((glucose_num < 37 && glucose != ".") | glucose_num > 571) && !ValidateIn(glucose, new string[] { "777", "888", "999" }) && (a1c_num < 2.8 && a1c != ".") || (a1c_num > 16.2 && !ValidateIn(a1c, new string[] { "7777", "8888", "9999" }))) { isvalidrow = false; }


            //Set Intervention
            intervention[0] = intervention1;
            intervention[1] = intervention2;
            intervention[2] = intervention3;
            intervention[3] = intervention4;
            intervention[4] = intervention5;
            intervention[5] = intervention6;
            intervention[6] = intervention7;
            intervention[7] = intervention8;
            intervention[8] = intervention9;
            intervention[9] = intervention10;
            intervention[10] = intervention11;
            intervention[11] = intervention12;
            intervention[12] = intervention13;
            intervention[13] = intervention14;
            intervention[14] = intervention15;
            intervention[15] = intervention16;

            interventionstr[0] = intervention1str;
            interventionstr[1] = intervention2str;
            interventionstr[2] = intervention3str;
            interventionstr[3] = intervention4str;
            interventionstr[4] = intervention5str;
            interventionstr[5] = intervention6str;
            interventionstr[6] = intervention7str;
            interventionstr[7] = intervention8str;
            interventionstr[8] = intervention9str;
            interventionstr[9] = intervention10str;
            interventionstr[10] = intervention11str;
            interventionstr[11] = intervention12str;
            interventionstr[12] = intervention13str;
            interventionstr[13] = intervention14str;
            interventionstr[14] = intervention15str;
            interventionstr[15] = intervention16str;

            lsphcid[0] = lsphcid1;
            lsphcid[1] = lsphcid2;
            lsphcid[2] = lsphcid3;
            lsphcid[3] = lsphcid4;
            lsphcid[4] = lsphcid5;
            lsphcid[5] = lsphcid6;
            lsphcid[6] = lsphcid7;
            lsphcid[7] = lsphcid8;
            lsphcid[8] = lsphcid9;
            lsphcid[9] = lsphcid10;
            lsphcid[10] = lsphcid11;
            lsphcid[11] = lsphcid12;
            lsphcid[12] = lsphcid13;
            lsphcid[13] = lsphcid14;
            lsphcid[14] = lsphcid15;
            lsphcid[15] = lsphcid16;

            contacttype[0] = contacttype1;
            contacttype[1] = contacttype2;
            contacttype[2] = contacttype3;
            contacttype[3] = contacttype4;
            contacttype[4] = contacttype5;
            contacttype[5] = contacttype6;
            contacttype[6] = contacttype7;
            contacttype[7] = contacttype8;
            contacttype[8] = contacttype9;
            contacttype[9] = contacttype10;
            contacttype[10] = contacttype11;
            contacttype[11] = contacttype12;
            contacttype[12] = contacttype13;
            contacttype[13] = contacttype14;
            contacttype[14] = contacttype15;
            contacttype[15] = contacttype16;

            setting[0] = setting1;
            setting[1] = setting2;
            setting[2] = setting3;
            setting[3] = setting4;
            setting[4] = setting5;
            setting[5] = setting6;
            setting[6] = setting7;
            setting[7] = setting8;
            setting[8] = setting9;
            setting[9] = setting10;
            setting[10] = setting11;
            setting[11] = setting12;
            setting[12] = setting13;
            setting[13] = setting14;
            setting[14] = setting15;
            setting[15] = setting16;

            lsphctime[0] = lsphctime1;
            lsphctime[1] = lsphctime2;
            lsphctime[2] = lsphctime3;
            lsphctime[3] = lsphctime4;
            lsphctime[4] = lsphctime5;
            lsphctime[5] = lsphctime6;
            lsphctime[6] = lsphctime7;
            lsphctime[7] = lsphctime8;
            lsphctime[8] = lsphctime9;
            lsphctime[9] = lsphctime10;
            lsphctime[10] = lsphctime11;
            lsphctime[11] = lsphctime12;
            lsphctime[12] = lsphctime13;
            lsphctime[13] = lsphctime14;
            lsphctime[14] = lsphctime15;
            lsphctime[15] = lsphctime16;

            slsphctime[0] = slsphctime1;
            slsphctime[1] = slsphctime2;
            slsphctime[2] = slsphctime3;
            slsphctime[3] = slsphctime4;
            slsphctime[4] = slsphctime5;
            slsphctime[5] = slsphctime6;
            slsphctime[6] = slsphctime7;
            slsphctime[7] = slsphctime8;
            slsphctime[8] = slsphctime9;
            slsphctime[9] = slsphctime10;
            slsphctime[10] = slsphctime11;
            slsphctime[11] = slsphctime12;
            slsphctime[12] = slsphctime13;
            slsphctime[13] = slsphctime14;
            slsphctime[14] = slsphctime15;
            slsphctime[15] = slsphctime16;

            lsphccomp[0] = lsphccomp1;
            lsphccomp[1] = lsphccomp2;
            lsphccomp[2] = lsphccomp3;
            lsphccomp[3] = lsphccomp4;
            lsphccomp[4] = lsphccomp5;
            lsphccomp[5] = lsphccomp6;
            lsphccomp[6] = lsphccomp7;
            lsphccomp[7] = lsphccomp8;
            lsphccomp[8] = lsphccomp9;
            lsphccomp[9] = lsphccomp10;
            lsphccomp[10] = lsphccomp11;
            lsphccomp[11] = lsphccomp12;
            lsphccomp[12] = lsphccomp13;
            lsphccomp[13] = lsphccomp14;
            lsphccomp[14] = lsphccomp15;
            lsphccomp[15] = lsphccomp16;

            intervention_count = 0;

            for (int i = 0; i < 16; i++)
            {
                if (!ValidateIn(interventionstr[i], new string[] { ".", "" })) { intervention_count++; }
            }

             
            SPLookup lboogie = new SPLookup();

            bpdate_valid = false;
            if (bpdate < currdate & bpdate >= submissiondate) { bpdate_valid = true; }
            bpdate_f_valid = false;
            if (bpdate_f_valid = bpdate_f < currdate & bpdate_f >= submissiondate) { bpdate_f_valid = false; }

            bgdate_valid = false;
            if (bgdatestr == "." && ValidateIn(glucose, new string[] { "888", "999" }) && ValidateIn(a1c, new string[] { "8888", "9999" })) { bgdate_valid = true; } // Can be missing given certain conditions.;
            else if (bgdate < currdate && bgdate >= submissiondate) { bgdate_valid = true; } // If not missing, needs to be within a certain range;
            else if (bgdatestr == "." && ValidateIn(itype, new string[] { "1", "2" }) && !ValidateIn(glucose, new string[] { "888", "999" }) && !ValidateIn(a1c, new string[] { "8888", "9999" })) { bgdate_valid = false; } // Note that this is written largely for formality, prior conditionals ensure this.;

            bgdate_f_valid = false;
            if (bgdate_fstr == "." && ValidateIn(glucose_f, new string[] { "888", "999" }) && ValidateIn(a1c_f, new string[] { "8888", "9999" })) { bgdate_f_valid = true; } //Can be missing given certain conditions.;
            if (bgdate_f < currdate && bgdate_f >= submissiondate) { bgdate_f_valid = true; } //If not missing, needs to be within a certain range;
            //Note that the conditional of type in 3,4 is not necessary per Michaela Vine;

            bpdidate_valid = false;
            if (bpdidatestr == "." && bpalert == "3") { bpdidate_valid = true; } //Can be missing given certain conditions.;
            else if (bpdidate < currdate && bpdidate >= submissiondate) { bpdidate_valid = true; } //If not missing, needs to be within a certain range;
            else if (bpdidatestr == "." && ValidateIn(bpalert, new string[] { "1", "2", "8", "9" }) && ValidateIn(itype, new string[] { "1", "2" })) { bpdidate_valid = false; } //Note that this is written largely for formality, prior conditionals ensure this.;

            bpdidate_f_valid = false;
            if (bpdidate_fstr == "." && bpalert_f == "3") { bpdidate_f_valid = true; } //Can be missing given certain conditions.;
            else if (bpdidate_f < currdate && bpdidate_f >= submissiondate) { bpdidate_f_valid = true; } //If not missing, needs to be within a certain range;
            //Note that the conditional of type in 3,4 is not necessary per Michaela Vine;

            bgdidate_valid = false;
            if (bgdidatestr == "." && bgalert == "3") { bgdidate_valid = true; } //Can be missing given certain conditions.;
            else if (bgdidate < currdate && bgdidate >= submissiondate) { bgdidate_valid = true; } //If not missing, needs to be within a certain range;
            else if (bgdidatestr == "." && ValidateIn(bgalert, new string[] { "1", "2", "8", "9" }) && ValidateIn(itype, new string[] { "1", "2" })) { bgdidate_valid = false; } //Note that this is written largely for formality, prior conditionals ensure this.;

            bgdidate_f_valid = false;
            if (bgdidate_fstr == "." && bgalert_f == "3") { bgdidate_f_valid = true; } //Can be missing given certain conditions.;
            else if (bgdidate_f < currdate && bgdidate_f >= submissiondate) { bgdidate_f_valid = true; } //If not missing, needs to be within a certain range;
            //Note that the conditional of type in 3,4 is not necessary per Michaela Vine;

            rrcdate_valid = false;
            if (rrcdate < currdate && rrcdate >= submissiondate) { rrcdate_valid = true; } // If not missing, needs to be within a certain range;

            rrcdate_f_valid = false;
            if (rrcdate_f < currdate && rrcdate_f >= submissiondate) { rrcdate_f_valid = true; } // If not missing, needs to be within a certain range;

            rtcdate_valid = false;
            if (rtcdate < currdate && rtcdate >= submissiondate) { rtcdate_valid = true; }

            rtcdate_f_valid = false;
            if (rtcdate_f < currdate && rtcdate_f >= submissiondate) { rtcdate_f_valid = true; }

            rcccomplete_valid = false;
            if ((rrccomplete < currdate && rrccomplete >= submissiondate) || !ValidateIn(rrccompletestr, new string[] { "88888888", "99999999" })) { rcccomplete_valid = true; }

            rcccomplete_f_valid = false;
            if ((rrccomplete_f < currdate && rrccomplete_f >= submissiondate) || !ValidateIn(rrccomplete_fstr, new string[] { "88888888", "99999999" })) { rcccomplete_f_valid = true; }

            refdate_valid = false;
            if (refdate_valid = refdate < currdate && refdate >= submissiondate) { refdate_valid = true; }
            refdate2_valid = false;
            if (refdate2_valid = refdate2 < currdate && refdate2 >= submissiondate) { refdate2_valid = true; }

            //* Required for timeper validation. Data are at the paricipant level. Take the minimum bpdate (should be the bpdate not associated with followup);
            firstbpdate = bpdate;
            //if (bpdatestr == "." && bpdate_fstr != ".") 
            //{ firstbpdate = bpdate; }
            //else if (bpdatestr != "." && bpdate_fstr == ".") 
            //{ firstbpdate = bpdate; }
            //else if (bpdate_fstr != "." && bpdatestr == ".") 
            //{ firstbpdate = bpdate_f; }

            if ((bpdate > bpdate_f) && (bpdate_f.ToShortDateString() != "01/01/0001"))
            {
                firstbpdate = bpdate_f;
            }
            else if (bpdate_f > bpdate)
            {
                firstbpdate = bpdate;
            }

            mean_sbp = 0;
            mean_sbp_f = 0;
            mean_dbp = 0;
            mean_dbp_f = 0;

            //****************************BEGIN ORIGINAL***************************************
            //if (!ValidateIn(sbp1, new string[] { "777", "888", "999", "." }) && !ValidateIn(sbp2, new string[] { "777", "888", "999", "." })) { mean_sbp = SetMean(sbp2_num, sbp1_num); }
            //if (mean_sbp < 0 && !ValidateIn(sbp1, new string[] { "777", "888", "999", "." })) { mean_sbp = sbp1_num; }
            //else if (mean_sbp < 0 && !ValidateIn(sbp2, new string[] { "777", "888", "999", "." })) { mean_sbp = sbp2_num; }

            //if (!ValidateIn(dbp1, new string[] { "777", "888", "999", "." }) && !ValidateIn(dbp2, new string[] { "777", "888", "999", "." })) { mean_dbp = SetMean(dbp1_num, dbp2_num); }
            //if (mean_dbp < 0 && !ValidateIn(dbp1, new string[] { "777", "888", "999", "." })) { mean_dbp = dbp1_num; }
            //else if (mean_dbp < 0 && !ValidateIn(dbp2, new string[] { "777", "888", "999", "." })) { mean_dbp = dbp2_num; }

            //if (!ValidateIn(sbp1_f, new string[] { "777", "888", "999", "." }) && !ValidateIn(sbp2_f, new string[] { "777", "888", "999", "." })) { mean_sbp_f = SetMean(sbp1_f_num, sbp2_f_num); }
            //if (mean_sbp_f < 0 && !ValidateIn(sbp1_f, new string[] { "777", "888", "999", "." })) { mean_sbp_f = sbp1_f_num; }
            //else if (mean_sbp_f < 0 && !ValidateIn(sbp2_f, new string[] { "777", "888", "999", "." })) { mean_sbp_f = sbp2_f_num; }

            //if (!ValidateIn(dbp1_f, new string[] { "777", "888", "999", "." }) && !ValidateIn(dbp2_f, new string[] { "777", "888", "999", "." })) { mean_dbp_f = SetMean(dbp1_f_num, dbp2_f_num); }
            //if (mean_dbp_f < 0 && !ValidateIn(dbp1_f, new string[] { "777", "888", "999", "." })) { mean_dbp_f = dbp1_f_num; }
            //else if (mean_dbp_f < 0 && !ValidateIn(dbp2_f, new string[] { "777", "888", "999", "." })) { mean_dbp_f = dbp2_f_num; }
            //****************************END ORIGINAL***************************************

            //****************************BEGIN CHANGES 02/09/16***************************************
            if (!ValidateIn(sbp1, new string[] { "777", "888", "999", "." }) && !ValidateIn(sbp2, new string[] { "777", "888", "999", "." })) { mean_sbp = SetMean(sbp2_num, sbp1_num); }
            if (mean_sbp == 0 && !ValidateIn(sbp1, new string[] { "777", "888", "999", "." })) { mean_sbp = sbp1_num; }
            else if (mean_sbp == 0 && !ValidateIn(sbp2, new string[] { "777", "888", "999", "." })) { mean_sbp = sbp2_num; }

            if (!ValidateIn(dbp1, new string[] { "777", "888", "999", "." }) && !ValidateIn(dbp2, new string[] { "777", "888", "999", "." })) { mean_dbp = SetMean(dbp1_num, dbp2_num); }
            if (mean_dbp == 0 && !ValidateIn(dbp1, new string[] { "777", "888", "999", "." })) { mean_dbp = dbp1_num; }
            else if (mean_dbp == 0 && !ValidateIn(dbp2, new string[] { "777", "888", "999", "." })) { mean_dbp = dbp2_num; }

            if (!ValidateIn(sbp1_f, new string[] { "777", "888", "999", "." }) && !ValidateIn(sbp2_f, new string[] { "777", "888", "999", "." })) { mean_sbp_f = SetMean(sbp1_f_num, sbp2_f_num); }
            if (mean_sbp_f == 0 && !ValidateIn(sbp1_f, new string[] { "777", "888", "999", "." })) { mean_sbp_f = sbp1_f_num; }
            else if (mean_sbp_f == 0 && !ValidateIn(sbp2_f, new string[] { "777", "888", "999", "." })) { mean_sbp_f = sbp2_f_num; }

            if (!ValidateIn(dbp1_f, new string[] { "777", "888", "999", "." }) && !ValidateIn(dbp2_f, new string[] { "777", "888", "999", "." })) { mean_dbp_f = SetMean(dbp1_f_num, dbp2_f_num); }
            if (mean_dbp_f == 0 && !ValidateIn(dbp1_f, new string[] { "777", "888", "999", "." })) { mean_dbp_f = dbp1_f_num; }
            else if (mean_dbp_f == 0 && !ValidateIn(dbp2_f, new string[] { "777", "888", "999", "." })) { mean_dbp_f = dbp2_f_num; }
            //****************************END CHANGES 02/09/16***************************************

            //***************************BEGIN REGEX CHECKS FOR FORMATTING********************************

            /////////////////////////*****1 CHARACTER NUMERIC CHECKS*******///////////////////////////////
                    //timeper type type_f latino race1 race2 education srhc srhc_f srhb srhb_f srd srd_f srha srha_f hcmeds hcmeds_f hbpmeds hbpmeds_f dmeds dmeds_f
                    // bpfreq bpfreq_f bpsend bpsend_f fish fish_f grains grains_f sugar sugar_f saltwatch saltwatch_f smoker smoker_f fast fast_f
                    //bpalert bpalert_f bgalert bgalert_f rrcnut rrcnut_f rrcpa rrcpa_f rrcsmoke rrcsmoke_f rrcmedadhere rrcmedadhere_f  rtc rtc_f contacttype1
                    //contacttype2 contacttype3 contacttype4 contacttype5 contacttype6 contacttype7 contacttype8 contacttype9 contacttype10 contacttype11 
                    //contacttype12 contacttype13 contacttype14 contacttype15 contacttype16 setting1  setting2 setting3 setting4 setting5 setting6 setting7 setting8
                    //setting9 setting10 setting11 setting12 setting13 setting14 setting15 setting16 lsphccomp1 lsphccomp2 lsphccomp3 lsphccomp4 lsphccomp5 lsphccomp6
                    //lsphccomp7 lsphccomp8 lsphccomp9 lsphccomp10 lsphccomp11 lsphccomp12 lsphccomp13 lsphccomp14 lsphccomp15 lsphccomp16 tobrestype1 tobrestype2 
                    //tobrestype3 trescomp1 trescomp2 trescomp3;

            string sNumeric1Char = "^[0-9]{1}$";

            //Baseline Screening fields
            if (!IsRegExMatch(sNumeric1Char, timeper) && !ValidateIn(timeper, new string[] {"", "."})) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, timeper, "timeper"); }
            if (!IsRegExMatch(sNumeric1Char, itype) && !ValidateIn(itype, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, itype, "type"); }
            if (!IsRegExMatch(sNumeric1Char, latino) && !ValidateIn(latino, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, latino, "latino"); }
            if (!IsRegExMatch(sNumeric1Char, race1) && !ValidateIn(race1, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, race1, "race1"); }
            if (!IsRegExMatch(sNumeric1Char, race2) && !ValidateIn(race2, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, race2, "race2"); }
            if (!IsRegExMatch(sNumeric1Char, education) && !ValidateIn(education, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, education, "education"); }
            if (!IsRegExMatch(sNumeric1Char, srhc) && !ValidateIn(srhc, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, srhc, "srhc"); }
            if (!IsRegExMatch(sNumeric1Char, srhb) && !ValidateIn(srhb, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, srhb, "srhb"); }
            if (!IsRegExMatch(sNumeric1Char, srd) && !ValidateIn(srd, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, srd, "srd"); }
            if (!IsRegExMatch(sNumeric1Char, srha) && !ValidateIn(srha, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, srha, "srha"); }
            if (!IsRegExMatch(sNumeric1Char, hcmeds) && !ValidateIn(hcmeds, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hcmeds, "hcmeds"); }
            if (!IsRegExMatch(sNumeric1Char, hbpmeds) && !ValidateIn(hbpmeds, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hbpmeds, "hbpmeds"); }
            if (!IsRegExMatch(sNumeric1Char, dmeds) && !ValidateIn(dmeds, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dmeds, "dmeds"); }
            if (!IsRegExMatch(sNumeric1Char, bpfreq) && !ValidateIn(bpfreq, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpfreq, "bpfreq"); }
            if (!IsRegExMatch(sNumeric1Char, bpsend) && !ValidateIn(bpsend, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpsend, "bpsend"); }
            if (!IsRegExMatch(sNumeric1Char, fish) && !ValidateIn(fish, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, fish, "fish"); }
            if (!IsRegExMatch(sNumeric1Char, grains) && !ValidateIn(grains, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, grains, "grains"); }
            if (!IsRegExMatch(sNumeric1Char, sugar) && !ValidateIn(sugar, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, sugar, "sugar"); }
            if (!IsRegExMatch(sNumeric1Char, saltwatch) && !ValidateIn(saltwatch, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, saltwatch, "saltwatch"); }
            if (!IsRegExMatch(sNumeric1Char, smoker) && !ValidateIn(smoker, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, smoker, "smoker"); }
            if (!IsRegExMatch(sNumeric1Char, fast) && !ValidateIn(fast, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, fast, "fast"); }
            if (!IsRegExMatch(sNumeric1Char, bpalert) && !ValidateIn(bpalert, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpalert, "bpalert"); }
            if (!IsRegExMatch(sNumeric1Char, bgalert) && !ValidateIn(bgalert, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bgalert, "bgalert"); }
            if (!IsRegExMatch(sNumeric1Char, rrcnut) && !ValidateIn(rrcnut, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rrcnut, "rrcnut"); }
            if (!IsRegExMatch(sNumeric1Char, rrcpa) && !ValidateIn(rrcpa, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rrcpa, "rrcpa"); }
            if (!IsRegExMatch(sNumeric1Char, rrcsmoke) && !ValidateIn(rrcsmoke, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rrcsmoke, "rrcsmoke"); }
            if (!IsRegExMatch(sNumeric1Char, rrcmedadhere) && !ValidateIn(rrcmedadhere, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rrcmedadhere, "rrcmedadhere"); }
            if (!IsRegExMatch(sNumeric1Char, rtc) && !ValidateIn(rtc, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rtc, "rtc"); }

            //Followup Screening Fields
            if (!IsRegExMatch(sNumeric1Char, itype_f) && !ValidateIn(itype_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, itype_f, "type_f"); }
            if (!IsRegExMatch(sNumeric1Char, srhc_f) && !ValidateIn(srhc_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, srhc_f, "srhc_f"); }
            if (!IsRegExMatch(sNumeric1Char, srhb_f) && !ValidateIn(srhb_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, srhb_f, "srhb_f"); }
            if (!IsRegExMatch(sNumeric1Char, srd_f) && !ValidateIn(srd_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, srd_f, "srd_f"); }
            if (!IsRegExMatch(sNumeric1Char, srha_f) && !ValidateIn(srha_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, srha_f, "srha_f"); }
            if (!IsRegExMatch(sNumeric1Char, hcmeds_f) && !ValidateIn(hcmeds_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hcmeds_f, "hcmeds_f"); }
            if (!IsRegExMatch(sNumeric1Char, hbpmeds_f) && !ValidateIn(hbpmeds_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hbpmeds_f, "hbpmeds_f"); }
            if (!IsRegExMatch(sNumeric1Char, dmeds_f) && !ValidateIn(dmeds_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dmeds_f, "dmeds_f"); }
            if (!IsRegExMatch(sNumeric1Char, bpfreq_f) && !ValidateIn(bpfreq_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpfreq_f, "bpfreq_f"); }
            if (!IsRegExMatch(sNumeric1Char, bpsend_f) && !ValidateIn(bpsend_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpsend_f, "bpsend_f"); }
            if (!IsRegExMatch(sNumeric1Char, fish_f) && !ValidateIn(fish_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, fish_f, "fish_f"); }
            if (!IsRegExMatch(sNumeric1Char, grains_f) && !ValidateIn(grains_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, grains_f, "grains_f"); }
            if (!IsRegExMatch(sNumeric1Char, sugar_f) && !ValidateIn(sugar_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, sugar_f, "sugar_f"); }
            if (!IsRegExMatch(sNumeric1Char, saltwatch_f) && !ValidateIn(saltwatch_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, saltwatch_f, "saltwatch_f"); }
            if (!IsRegExMatch(sNumeric1Char, smoker_f) && !ValidateIn(smoker_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, smoker_f, "smoker_f"); }
            if (!IsRegExMatch(sNumeric1Char, fast_f) && !ValidateIn(fast_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, fast_f, "fast_f"); }
            if (!IsRegExMatch(sNumeric1Char, bpalert_f) && !ValidateIn(bpalert_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpalert_f, "bpalert_f"); }
            if (!IsRegExMatch(sNumeric1Char, bgalert_f) && !ValidateIn(bgalert_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bgalert_f, "bgalert_f"); }
            if (!IsRegExMatch(sNumeric1Char, rrcnut_f) && !ValidateIn(rrcnut_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rrcnut_f, "rrcnut_f"); }
            if (!IsRegExMatch(sNumeric1Char, rrcpa_f) && !ValidateIn(rrcpa_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rrcpa_f, "rrcpa_f"); }
            if (!IsRegExMatch(sNumeric1Char, rrcsmoke_f) && !ValidateIn(rrcsmoke_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rrcsmoke_f, "rrcsmoke_f"); }
            if (!IsRegExMatch(sNumeric1Char, rrcmedadhere_f) && !ValidateIn(rrcmedadhere_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rrcmedadhere_f, "rrcmedadhere_f"); }
            if (!IsRegExMatch(sNumeric1Char, rtc_f) && !ValidateIn(rtc_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rtc_f, "rtc_f"); }

            //contactype RegEx Checks
            if (!IsRegExMatch(sNumeric1Char, contacttype1) && !ValidateIn(contacttype1, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, contacttype1, "contacttype1"); }
            if (!IsRegExMatch(sNumeric1Char, contacttype2) && !ValidateIn(contacttype2, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, contacttype2, "contacttype2"); }
            if (!IsRegExMatch(sNumeric1Char, contacttype3) && !ValidateIn(contacttype3, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, contacttype3, "contacttype3"); }
            if (!IsRegExMatch(sNumeric1Char, contacttype4) && !ValidateIn(contacttype4, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, contacttype4, "contacttype4"); }
            if (!IsRegExMatch(sNumeric1Char, contacttype5) && !ValidateIn(contacttype5, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, contacttype5, "contacttype5"); }
            if (!IsRegExMatch(sNumeric1Char, contacttype6) && !ValidateIn(contacttype6, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, contacttype6, "contacttype6"); }
            if (!IsRegExMatch(sNumeric1Char, contacttype7) && !ValidateIn(contacttype7, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, contacttype7, "contacttype7"); }
            if (!IsRegExMatch(sNumeric1Char, contacttype8) && !ValidateIn(contacttype8, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, contacttype8, "contacttype8"); }
            if (!IsRegExMatch(sNumeric1Char, contacttype9) && !ValidateIn(contacttype9, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, contacttype9, "contacttype9"); }
            if (!IsRegExMatch(sNumeric1Char, contacttype10) && !ValidateIn(contacttype10, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, contacttype10, "contacttype10"); }
            if (!IsRegExMatch(sNumeric1Char, contacttype11) && !ValidateIn(contacttype11, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, contacttype11, "contacttype11"); }
            if (!IsRegExMatch(sNumeric1Char, contacttype12) && !ValidateIn(contacttype12, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, contacttype12, "contacttype12"); }
            if (!IsRegExMatch(sNumeric1Char, contacttype13) && !ValidateIn(contacttype13, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, contacttype13, "contacttype13"); }
            if (!IsRegExMatch(sNumeric1Char, contacttype14) && !ValidateIn(contacttype14, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, contacttype14, "contacttype14"); }
            if (!IsRegExMatch(sNumeric1Char, contacttype15) && !ValidateIn(contacttype15, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, contacttype15, "contacttype15"); }
            if (!IsRegExMatch(sNumeric1Char, contacttype16) && !ValidateIn(contacttype16, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, contacttype16, "contacttype16"); }

            //setting RegEx Checks
            if (!IsRegExMatch(sNumeric1Char, setting1) && !ValidateIn(setting1, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, setting1, "setting1"); }
            if (!IsRegExMatch(sNumeric1Char, setting2) && !ValidateIn(setting2, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, setting2, "setting2"); }
            if (!IsRegExMatch(sNumeric1Char, setting3) && !ValidateIn(setting3, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, setting3, "setting3"); }
            if (!IsRegExMatch(sNumeric1Char, setting4) && !ValidateIn(setting4, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, setting4, "setting4"); }
            if (!IsRegExMatch(sNumeric1Char, setting5) && !ValidateIn(setting5, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, setting5, "setting5"); }
            if (!IsRegExMatch(sNumeric1Char, setting6) && !ValidateIn(setting6, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, setting6, "setting6"); }
            if (!IsRegExMatch(sNumeric1Char, setting7) && !ValidateIn(setting7, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, setting7, "setting7"); }
            if (!IsRegExMatch(sNumeric1Char, setting8) && !ValidateIn(setting8, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, setting8, "setting8"); }
            if (!IsRegExMatch(sNumeric1Char, setting9) && !ValidateIn(setting9, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, setting9, "setting9"); }
            if (!IsRegExMatch(sNumeric1Char, setting10) && !ValidateIn(setting10, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, setting10, "setting10"); }
            if (!IsRegExMatch(sNumeric1Char, setting11) && !ValidateIn(setting11, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, setting11, "setting11"); }
            if (!IsRegExMatch(sNumeric1Char, setting12) && !ValidateIn(setting12, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, setting12, "setting12"); }
            if (!IsRegExMatch(sNumeric1Char, setting13) && !ValidateIn(setting13, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, setting13, "setting13"); }
            if (!IsRegExMatch(sNumeric1Char, setting14) && !ValidateIn(setting14, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, setting14, "setting14"); }
            if (!IsRegExMatch(sNumeric1Char, setting15) && !ValidateIn(setting15, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, setting15, "setting15"); }
            if (!IsRegExMatch(sNumeric1Char, setting16) && !ValidateIn(setting16, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, setting16, "setting16"); }

            //lsphccomp RegEx Checks
            if (!IsRegExMatch(sNumeric1Char, lsphccomp1) && !ValidateIn(lsphccomp1, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphccomp1, "lsphccomp1"); }
            if (!IsRegExMatch(sNumeric1Char, lsphccomp2) && !ValidateIn(lsphccomp2, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphccomp2, "lsphccomp2"); }
            if (!IsRegExMatch(sNumeric1Char, lsphccomp3) && !ValidateIn(lsphccomp3, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphccomp3, "lsphccomp3"); }
            if (!IsRegExMatch(sNumeric1Char, lsphccomp4) && !ValidateIn(lsphccomp4, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphccomp4, "lsphccomp4"); }
            if (!IsRegExMatch(sNumeric1Char, lsphccomp5) && !ValidateIn(lsphccomp5, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphccomp5, "lsphccomp5"); }
            if (!IsRegExMatch(sNumeric1Char, lsphccomp6) && !ValidateIn(lsphccomp6, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphccomp6, "lsphccomp6"); }
            if (!IsRegExMatch(sNumeric1Char, lsphccomp7) && !ValidateIn(lsphccomp7, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphccomp7, "lsphccomp7"); }
            if (!IsRegExMatch(sNumeric1Char, lsphccomp8) && !ValidateIn(lsphccomp8, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphccomp8, "lsphccomp8"); }
            if (!IsRegExMatch(sNumeric1Char, lsphccomp9) && !ValidateIn(lsphccomp9, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphccomp9, "lsphccomp9"); }
            if (!IsRegExMatch(sNumeric1Char, lsphccomp10) && !ValidateIn(lsphccomp10, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphccomp10, "lsphccomp10"); }
            if (!IsRegExMatch(sNumeric1Char, lsphccomp11) && !ValidateIn(lsphccomp11, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphccomp11, "lsphccomp11"); }
            if (!IsRegExMatch(sNumeric1Char, lsphccomp12) && !ValidateIn(lsphccomp12, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphccomp12, "lsphccomp12"); }
            if (!IsRegExMatch(sNumeric1Char, lsphccomp13) && !ValidateIn(lsphccomp13, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphccomp13, "lsphccomp13"); }
            if (!IsRegExMatch(sNumeric1Char, lsphccomp14) && !ValidateIn(lsphccomp14, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphccomp14, "lsphccomp14"); }
            if (!IsRegExMatch(sNumeric1Char, lsphccomp15) && !ValidateIn(lsphccomp15, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphccomp15, "lsphccomp15"); }
            if (!IsRegExMatch(sNumeric1Char, lsphccomp16) && !ValidateIn(lsphccomp16, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphccomp16, "lsphccomp16"); }

            //tobrestype RegEx Checks
            if (!IsRegExMatch(sNumeric1Char, tobrestype) && !ValidateIn(tobrestype, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, tobrestype, "tobrestype"); }
            if (!IsRegExMatch(sNumeric1Char, tobrestype2) && !ValidateIn(tobrestype2, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, tobrestype2, "tobrestype2"); }
            if (!IsRegExMatch(sNumeric1Char, tobrestype3) && !ValidateIn(tobrestype3, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, tobrestype3, "tobrestype3"); }

            //trescomp RegEx Checks
            if (!IsRegExMatch(sNumeric1Char, trescomp) && !ValidateIn(trescomp, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, trescomp, "trescomp1"); }
            if (!IsRegExMatch(sNumeric1Char, trescomp2) && !ValidateIn(trescomp2, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, trescomp2, "trescomp2"); }
            if (!IsRegExMatch(sNumeric1Char, trescomp3) && !ValidateIn(trescomp3, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, trescomp3, "trescomp3"); }


            /////////////////////////*****2 CHARACTER NUMERIC CHECKS*******///////////////////////////////
                        //stfips nscreen language hcadhere hcadhere_f hbpadhere hbpadhere_f dadhere dadhere_f fruit fruit_f vegetables vegetables_f sechand sechand_f 
                        //qolph qolph_f qolmh qolmh_f qoleffect qoleffect_f height waist waist_f hip hip_f lsphcrec;

            string sNumeric2Char = "^[0-9]{2}$";

            //Baseline Screening Fields
            if (!IsRegExMatch(sNumeric2Char, stfips) && !ValidateIn(SetUniform(stfips), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, stfips, "stfips"); }
            if (!IsRegExMatch(sNumeric2Char, nscreen) && !ValidateIn(SetUniform(nscreen), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, nscreen, "nscreen"); }
            if (!IsRegExMatch(sNumeric2Char, language) && !ValidateIn(SetUniform(language), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, language, "language"); }
            if (!IsRegExMatch(sNumeric2Char, hcadhere) && !ValidateIn(SetUniform(hcadhere), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hcadhere, "hcadhere"); }
            if (!IsRegExMatch(sNumeric2Char, hbpadhere) && !ValidateIn(SetUniform(hbpadhere), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hbpadhere, "hbpadhere"); }
            if (!IsRegExMatch(sNumeric2Char, dadhere) && !ValidateIn(SetUniform(dadhere), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dadhere, "dadhere"); }
            if (!IsRegExMatch(sNumeric2Char, fruit) && !ValidateIn(SetUniform(fruit), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, fruit, "fruit"); }
            if (!IsRegExMatch(sNumeric2Char, vegetables) && !ValidateIn(SetUniform(vegetables), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, vegetables, "vegetables"); }
            if (!IsRegExMatch(sNumeric2Char, sechand) && !ValidateIn(SetUniform(sechand), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, sechand, "sechand"); }
            if (!IsRegExMatch(sNumeric2Char, qolph) && !ValidateIn(SetUniform(qolph), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, qolph, "qolph"); }
            if (!IsRegExMatch(sNumeric2Char, qolmh) && !ValidateIn(SetUniform(qolmh), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, qolmh, "qolmh"); }
            if (!IsRegExMatch(sNumeric2Char, qoleffect) && !ValidateIn(SetUniform(qoleffect), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, qoleffect, "qoleffect"); }
            if (!IsRegExMatch(sNumeric2Char, waist) && !ValidateIn(SetUniform(waist), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, waist, "waist"); }
            if (!IsRegExMatch(sNumeric2Char, hip) && !ValidateIn(SetUniform(hip), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hip, "hip"); }
            if (!IsRegExMatch(sNumeric2Char, lsphcrec) && !ValidateIn(SetUniform(lsphcrec), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphcrec, "lsphcrec"); }

            //Followup fields
            if (!IsRegExMatch(sNumeric2Char, hcadhere_f) && !ValidateIn(SetUniform(hcadhere_f), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hcadhere_f, "hcadhere_f"); }
            if (!IsRegExMatch(sNumeric2Char, hbpadhere_f) && !ValidateIn(SetUniform(hbpadhere_f), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hbpadhere_f, "hbpadhere_f"); }
            if (!IsRegExMatch(sNumeric2Char, dadhere_f) && !ValidateIn(SetUniform(dadhere_f), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dadhere_f, "dadhere_f"); }
            if (!IsRegExMatch(sNumeric2Char, fruit_f) && !ValidateIn(SetUniform(fruit_f), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, fruit_f, "fruit_f"); }
            if (!IsRegExMatch(sNumeric2Char, vegetables_f) && !ValidateIn(SetUniform(vegetables_f), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, vegetables_f, "vegetables_f"); }
            if (!IsRegExMatch(sNumeric2Char, sechand_f) && !ValidateIn(SetUniform(sechand_f), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, sechand_f, "sechand_f"); }
            if (!IsRegExMatch(sNumeric2Char, qolph_f) && !ValidateIn(SetUniform(qolph_f), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, qolph_f, "qolph_f"); }
            if (!IsRegExMatch(sNumeric2Char, qolmh_f) && !ValidateIn(SetUniform(qolmh_f), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, qolmh_f, "qolmh_f"); }
            if (!IsRegExMatch(sNumeric2Char, qoleffect_f) && !ValidateIn(SetUniform(qoleffect_f), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, qoleffect_f, "qoleffect_f"); }
            if (!IsRegExMatch(sNumeric2Char, waist_f) && !ValidateIn(SetUniform(waist_f), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, waist_f, "waist_f"); }
            if (!IsRegExMatch(sNumeric2Char, hip_f) && !ValidateIn(SetUniform(hip_f), new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hip_f, "hip_f"); }

            /////////////////////////*****3 CHARACTER NUMERIC CHECKS*******///////////////////////////////
                        //%let numeric_3 = bphome bphome_f mdever pamod pamod_f pavig pavig_f weight weight_f spbi1 spb2 spbi1_f spb2_f dbp1 dbp2 dbp1_f dbp2_f totchol totchol_f hdl hdl_f ldl ldl_f
                        //glucose glucose_f lsphctime1 lsphctime2 lsphctime3 lsphctime4 lsphctime5 lsphctime6 lsphctime7 lsphctime8 lsphctime9 lsphctime10 
                        //lsphctime11 lsphctime12 lsphctime13 lsphctime14 lsphctime15 lsphctime16;

            string sNumeric3Char = "^[0-9]{3}$";

            //Baseline screening fields
            if (!IsRegExMatch(sNumeric3Char, mdever) && !ValidateIn(mdever, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, mdever, "mdever"); }
            if (!IsRegExMatch(sNumeric3Char, bphome) && !ValidateIn(bphome, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bphome, "bphome"); }
            if (!IsRegExMatch(sNumeric3Char, pamod) && !ValidateIn(pamod, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, pamod, "pamod"); }
            if (!IsRegExMatch(sNumeric3Char, pavig) && !ValidateIn(pavig, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, pavig, "pavig"); }
            if (!IsRegExMatch(sNumeric3Char, weight) && !ValidateIn(weight, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, weight, "weight"); }
            if (!IsRegExMatch(sNumeric3Char, sbp1) && !ValidateIn(sbp1, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, sbp1, "sbp1"); }
            if (!IsRegExMatch(sNumeric3Char, sbp2) && !ValidateIn(sbp2, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, sbp2, "sbp2"); }
            if (!IsRegExMatch(sNumeric3Char, dbp1) && !ValidateIn(dbp1, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dbp1, "dbp1"); }
            if (!IsRegExMatch(sNumeric3Char, dbp2) && !ValidateIn(dbp2, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dbp2, "dbp2"); }
            if (!IsRegExMatch(sNumeric3Char, totchol) && !ValidateIn(totchol, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, totchol, "totchol"); }
            if (!IsRegExMatch(sNumeric3Char, hdl) && !ValidateIn(hdl, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hdl, "hdl"); }
            if (!IsRegExMatch(sNumeric3Char, ldl) && !ValidateIn(ldl, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, ldl, "ldl"); }
            if (!IsRegExMatch(sNumeric3Char, glucose) && !ValidateIn(glucose, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, glucose, "glucose"); }

            //Followup fields
            if (!IsRegExMatch(sNumeric3Char, bphome_f) && !ValidateIn(bphome_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bphome_f, "bphome_f"); }
            if (!IsRegExMatch(sNumeric3Char, pamod_f) && !ValidateIn(pamod_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, pamod_f, "pamod_f"); }
            if (!IsRegExMatch(sNumeric3Char, pavig_f) && !ValidateIn(pavig_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, pavig_f, "pavig_f"); }
            if (!IsRegExMatch(sNumeric3Char, weight_f) && !ValidateIn(weight_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, weight_f, "weight_f"); }
            if (!IsRegExMatch(sNumeric3Char, sbp1_f) && !ValidateIn(sbp1_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, sbp1_f, "sbp1_f"); }
            if (!IsRegExMatch(sNumeric3Char, sbp2_f) && !ValidateIn(sbp2_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, sbp2_f, "sbp2_f"); }
            if (!IsRegExMatch(sNumeric3Char, dbp1_f) && !ValidateIn(dbp1_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dbp1_f, "dbp1_f"); }
            if (!IsRegExMatch(sNumeric3Char, dbp2_f) && !ValidateIn(dbp2_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dbp2_f, "dbp2_f"); }
            if (!IsRegExMatch(sNumeric3Char, totchol_f) && !ValidateIn(totchol_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, totchol_f, "totchol_f"); }
            if (!IsRegExMatch(sNumeric3Char, hdl_f) && !ValidateIn(hdl_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hdl_f, "hdl_f"); }
            if (!IsRegExMatch(sNumeric3Char, ldl_f) && !ValidateIn(ldl_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, ldl_f, "ldl_f"); }
            if (!IsRegExMatch(sNumeric3Char, glucose_f) && !ValidateIn(glucose_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, glucose_f, "glucose_f"); }

            //slsphctime RegEx Checks
            if (!IsRegExMatch(sNumeric3Char, slsphctime1) && !ValidateIn(slsphctime1, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, slsphctime1, "lsphctime1"); }
            if (!IsRegExMatch(sNumeric3Char, slsphctime2) && !ValidateIn(slsphctime2, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, slsphctime2, "lsphctime2"); }
            if (!IsRegExMatch(sNumeric3Char, slsphctime3) && !ValidateIn(slsphctime3, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, slsphctime3, "lsphctime3"); }
            if (!IsRegExMatch(sNumeric3Char, slsphctime4) && !ValidateIn(slsphctime4, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, slsphctime4, "lsphctime4"); }
            if (!IsRegExMatch(sNumeric3Char, slsphctime5) && !ValidateIn(slsphctime5, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, slsphctime5, "lsphctime5"); }
            if (!IsRegExMatch(sNumeric3Char, slsphctime6) && !ValidateIn(slsphctime6, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, slsphctime6, "lsphctime6"); }
            if (!IsRegExMatch(sNumeric3Char, slsphctime7) && !ValidateIn(slsphctime7, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, slsphctime7, "lsphctime7"); }
            if (!IsRegExMatch(sNumeric3Char, slsphctime8) && !ValidateIn(slsphctime8, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, slsphctime8, "lsphctime8"); }
            if (!IsRegExMatch(sNumeric3Char, slsphctime9) && !ValidateIn(slsphctime9, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, slsphctime9, "lsphctime9"); }
            if (!IsRegExMatch(sNumeric3Char, slsphctime10) && !ValidateIn(slsphctime10, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, slsphctime10, "lsphctime10"); }
            if (!IsRegExMatch(sNumeric3Char, slsphctime11) && !ValidateIn(slsphctime11, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, slsphctime11, "lsphctime11"); }
            if (!IsRegExMatch(sNumeric3Char, slsphctime12) && !ValidateIn(slsphctime12, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, slsphctime12, "lsphctime12"); }
            if (!IsRegExMatch(sNumeric3Char, slsphctime13) && !ValidateIn(slsphctime13, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, slsphctime13, "lsphctime13"); }
            if (!IsRegExMatch(sNumeric3Char, slsphctime14) && !ValidateIn(slsphctime14, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, slsphctime14, "lsphctime14"); }
            if (!IsRegExMatch(sNumeric3Char, slsphctime15) && !ValidateIn(slsphctime15, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, slsphctime15, "lsphctime15"); }
            if (!IsRegExMatch(sNumeric3Char, slsphctime16) && !ValidateIn(slsphctime16, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, slsphctime16, "lsphctime16"); }

            /////////////////////////*****4 CHARACTER NUMERIC CHECKS*******///////////////////////////////
                    //%let numeric_4 = trigly trigly_f a1c a1c_f;

            string sNumeric4Char = @"^[0-9\.]{4}$";

            if (!IsRegExMatch(sNumeric4Char, trigly) && !ValidateIn(trigly, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, trigly, "trigly"); }
            if (!IsRegExMatch(sNumeric4Char, trigly_f) && !ValidateIn(trigly_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, trigly_f, "trigly_f"); }
            if (!IsRegExMatch(sNumeric4Char, a1c) && !ValidateIn(a1c, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, a1c, "a1c"); }
            if (!IsRegExMatch(sNumeric4Char, a1c_f) && !ValidateIn(a1c_f, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, a1c_f, "a1c_f"); }

            /////////////////////////*****5 CHARACTER NUMERIC CHECKS*******///////////////////////////////
                    //%let numeric_5 = hdansi enrollsiteid resansi zip;

            string sNumeric5Char = "^[0-9]{5}$";

            if (!IsRegExMatch(sNumeric5Char, hdansi) && !ValidateIn(hdansi, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hdansi, "hdansi"); }
            if (!IsRegExMatch(sNumeric5Char, enrollsiteid) && !ValidateIn(enrollsiteid, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, enrollsiteid, "enrollsiteid"); }
            if (!IsRegExMatch(sNumeric5Char, resansi) && !ValidateIn(resansi, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, resansi, "resansi"); }
            if (!IsRegExMatch(sNumeric5Char, zip) && !ValidateIn(zip, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, zip, "zip"); }


            /////////////////////////*****6 CHARACTER NUMERIC CHECKS*******///////////////////////////////
                    // Just MYB

            string sNumeric6Char = "^[0-9]{6}$";

            if (!IsRegExMatch(sNumeric6Char, mybstr) && !ValidateIn(mybstr, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, mybstr, "myb"); }


            /////////////////////////*****8 CHARACTER NUMERIC CHECKS*******///////////////////////////////
                    //%let numeric_8 = bpdate bpdate_f tcdate tcdate_f bgdate bgdate_f bpdidate bpdidate_f bgdidate bgdidate_f rrcdate rrcdate_f rrccomplete rrccomplete_f rtcdate 
                    //rtcdate_f refdate refdate2 intervention1 intervention2 intervention3 intervention4 intervention5 intervention6 intervention7 intervention8
                    //intervention9 intervention10 intervention11 intervention12 intervention13 intervention14 intervention15 intervention16 tobresdate1 tobresdate2
                    //tobresdate3;

            string sNumeric8Char = "^[0-9]{8}$";

            //Baseline screening fields
            if (!IsRegExMatch(sNumeric8Char, bpdatestr) && !ValidateIn(bpdatestr, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpdatestr, "bpdatestr"); }
            if (!IsRegExMatch(sNumeric8Char, tcdatestr) && !ValidateIn(tcdatestr, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, tcdatestr, "tcdatestr"); }
            if (!IsRegExMatch(sNumeric8Char, bgdatestr) && !ValidateIn(bgdatestr, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bgdatestr, "bgdatestr"); }
            if (!IsRegExMatch(sNumeric8Char, bpdidatestr) && !ValidateIn(bpdidatestr, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpdidatestr, "bpdidatestr"); }
            if (!IsRegExMatch(sNumeric8Char, bgdidatestr) && !ValidateIn(bgdidatestr, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bgdidatestr, "bgdidatestr"); }
            if (!IsRegExMatch(sNumeric8Char, rrcdatestr) && !ValidateIn(rrcdatestr, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rrcdatestr, "rrcdatestr"); }
            if (!IsRegExMatch(sNumeric8Char, rrccompletestr) && !ValidateIn(rrccompletestr, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rrccompletestr, "rrccompletestr"); }
            if (!IsRegExMatch(sNumeric8Char, rtcdatestr) && !ValidateIn(rtcdatestr, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rtcdatestr, "rtcdatestr"); }

            //Followup fields
            if (!IsRegExMatch(sNumeric8Char, bpdate_fstr) && !ValidateIn(bpdate_fstr, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpdate_fstr, "bpdate_fstr"); }
            if (!IsRegExMatch(sNumeric8Char, tcdate_fstr) && !ValidateIn(tcdate_fstr, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, tcdate_fstr, "tcdate_fstr"); }
            if (!IsRegExMatch(sNumeric8Char, bgdate_fstr) && !ValidateIn(bgdate_fstr, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bgdate_fstr, "bgdate_fstr"); }
            if (!IsRegExMatch(sNumeric8Char, bpdidate_fstr) && !ValidateIn(bpdidate_fstr, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpdidate_fstr, "bpdidate_fstr"); }
            if (!IsRegExMatch(sNumeric8Char, bgdidate_fstr) && !ValidateIn(bgdidate_fstr, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bgdidate_fstr, "bgdidate_fstr"); }
            if (!IsRegExMatch(sNumeric8Char, rrcdate_fstr) && !ValidateIn(rrcdate_fstr, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rrcdate_fstr, "rrcdate_fstr"); }
            if (!IsRegExMatch(sNumeric8Char, rrccomplete_fstr) && !ValidateIn(rrccomplete_fstr, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rrccomplete_fstr, "rrccomplete_fstr"); }
            if (!IsRegExMatch(sNumeric8Char, rtcdate_fstr) && !ValidateIn(rtcdate_fstr, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rtcdate_fstr, "rtcdate_fstr"); }

            //intervention checks
            if (!IsRegExMatch(sNumeric8Char, intervention1str) && !ValidateIn(intervention1str, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, intervention1str, "intervention1"); }
            if (!IsRegExMatch(sNumeric8Char, intervention2str) && !ValidateIn(intervention2str, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, intervention2str, "intervention2"); }
            if (!IsRegExMatch(sNumeric8Char, intervention3str) && !ValidateIn(intervention3str, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, intervention3str, "intervention3"); }
            if (!IsRegExMatch(sNumeric8Char, intervention4str) && !ValidateIn(intervention4str, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, intervention4str, "intervention4"); }
            if (!IsRegExMatch(sNumeric8Char, intervention5str) && !ValidateIn(intervention5str, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, intervention5str, "intervention5"); }
            if (!IsRegExMatch(sNumeric8Char, intervention6str) && !ValidateIn(intervention6str, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, intervention6str, "intervention6"); }
            if (!IsRegExMatch(sNumeric8Char, intervention7str) && !ValidateIn(intervention7str, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, intervention7str, "intervention7"); }
            if (!IsRegExMatch(sNumeric8Char, intervention8str) && !ValidateIn(intervention8str, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, intervention8str, "intervention8"); }
            if (!IsRegExMatch(sNumeric8Char, intervention9str) && !ValidateIn(intervention9str, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, intervention9str, "intervention9"); }
            if (!IsRegExMatch(sNumeric8Char, intervention10str) && !ValidateIn(intervention10str, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, intervention10str, "intervention10"); }
            if (!IsRegExMatch(sNumeric8Char, intervention11str) && !ValidateIn(intervention11str, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, intervention11str, "intervention11"); }
            if (!IsRegExMatch(sNumeric8Char, intervention12str) && !ValidateIn(intervention12str, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, intervention12str, "intervention12"); }
            if (!IsRegExMatch(sNumeric8Char, intervention13str) && !ValidateIn(intervention13str, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, intervention13str, "intervention13"); }
            if (!IsRegExMatch(sNumeric8Char, intervention14str) && !ValidateIn(intervention14str, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, intervention14str, "intervention14"); }
            if (!IsRegExMatch(sNumeric8Char, intervention15str) && !ValidateIn(intervention15str, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, intervention15str, "intervention15"); }
            if (!IsRegExMatch(sNumeric8Char, intervention16str) && !ValidateIn(intervention16str, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, intervention16str, "intervention16"); }


            if (!IsRegExMatch(sNumeric8Char, tobresdatestr) && !ValidateIn(tobresdatestr, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, tobresdatestr, "tobresdate"); }
            if (!IsRegExMatch(sNumeric8Char, tobresdate2str) && !ValidateIn(tobresdate2str, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, tobresdate2str, "tobresdate2"); }
            if (!IsRegExMatch(sNumeric8Char, tobresdate3str) && !ValidateIn(tobresdate3str, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, tobresdate3str, "tobresdate3"); }
            
            /////////////////////////*****10 CHARACTER NUMERIC CHECKS*******///////////////////////////////
                        //Just screensiteid
            string sNumeric10Char = "^[0-9]{10}$";
            if (!IsRegExMatch(sNumeric10Char, screensiteid) && !ValidateIn(screensiteid, new string[] { "", "." })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, screensiteid, "screensiteid"); }
            

            //LSPHCID Checks
            string[] sValidStrings = GetValidLSPHCIDStrings();
            string sCustomErrMsg = "LSPHCID does not correspond with CDC-approved LSPHCIDs for program";
            if (SetUniform(lsphcid1) != "." && !ValidateIn(lsphcid1, sValidStrings)) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphcid1, "lsphcid1", sCustomErrMsg); }
            if (SetUniform(lsphcid2) != "." && !ValidateIn(lsphcid2, sValidStrings)) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphcid2, "lsphcid2", sCustomErrMsg); }
            if (SetUniform(lsphcid3) != "." && !ValidateIn(lsphcid3, sValidStrings)) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphcid3, "lsphcid3", sCustomErrMsg); }
            if (SetUniform(lsphcid4) != "." && !ValidateIn(lsphcid4, sValidStrings)) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphcid4, "lsphcid4", sCustomErrMsg); }
            if (SetUniform(lsphcid5) != "." && !ValidateIn(lsphcid5, sValidStrings)) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphcid5, "lsphcid5", sCustomErrMsg); }
            if (SetUniform(lsphcid6) != "." && !ValidateIn(lsphcid6, sValidStrings)) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphcid6, "lsphcid6", sCustomErrMsg); }
            if (SetUniform(lsphcid7) != "." && !ValidateIn(lsphcid7, sValidStrings)) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphcid7, "lsphcid7", sCustomErrMsg); }
            if (SetUniform(lsphcid8) != "." && !ValidateIn(lsphcid8, sValidStrings)) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphcid8, "lsphcid8", sCustomErrMsg); }
            if (SetUniform(lsphcid9) != "." && !ValidateIn(lsphcid9, sValidStrings)) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphcid9, "lsphcid9", sCustomErrMsg); }
            if (SetUniform(lsphcid10) != "." && !ValidateIn(lsphcid10, sValidStrings)) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphcid10, "lsphcid10", sCustomErrMsg); }
            if (SetUniform(lsphcid11) != "." && !ValidateIn(lsphcid11, sValidStrings)) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphcid11, "lsphcid11", sCustomErrMsg); }
            if (SetUniform(lsphcid12) != "." && !ValidateIn(lsphcid12, sValidStrings)) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphcid12, "lsphcid12", sCustomErrMsg); }
            if (SetUniform(lsphcid13) != "." && !ValidateIn(lsphcid13, sValidStrings)) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphcid13, "lsphcid13", sCustomErrMsg); }
            if (SetUniform(lsphcid14) != "." && !ValidateIn(lsphcid14, sValidStrings)) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphcid14, "lsphcid14", sCustomErrMsg); }
            if (SetUniform(lsphcid15) != "." && !ValidateIn(lsphcid15, sValidStrings)) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphcid15, "lsphcid15", sCustomErrMsg); }
            if (SetUniform(lsphcid16) != "." && !ValidateIn(lsphcid16, sValidStrings)) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphcid16, "lsphcid16", sCustomErrMsg); }
            

            //The following Error Checks are NOT really Formatting checks, but Mathematica GROUPS them into the Formatting checks anyway in their Output file
            //if type in (3,4) then error_type = strip(error_type) || "ERROR: Type is 3 or 4,";
            //if type_f in (1,2) then error_type_f = strip(error_type_f) || "ERROR: Type_f is 1 or 2,";
            //if mdy(1,1,1930) > myb_num or myb_num > mdy(1,1,1993) then error_myb = strip(error_myb) || "ERROR: MYB before 1930 or after 1992,";

            //Type should always be either 1 or 2
            if (ValidateIn(itype, new string[] { "3", "4" })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, itype, "type", "Type is 3 or 4, but should be 1 or 2"); }
            
            //Type_f should always be either 3 or 4
            if (ValidateIn(itype_f, new string[] { "1", "2" })) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, itype_f, "type_f", "Type_f is 1 or 2, but should be 3 or 4"); }
            
            //Participant BirthDate (MYB) should be greater than 1930 but less than 1993
            DateTime dtDateMinValue = DateTime.MinValue;
            DateTime dtMinAllowed = new DateTime(1930, 1, 1);
            DateTime dtMaxAllowed = new DateTime(1993, 1, 1);
            if (myb != dtDateMinValue && (myb < dtMinAllowed || myb > dtMaxAllowed)) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, itype_f, "myb", "MYB before 1930 or after 1992"); }

            //If any Interventions records are found
            if (intervention_count > 0 && !refdate_valid && !refdate2_valid) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, refdatestr, "refdate", "Missing RefDate for participant with at least one Intervention"); }

            if (intervention_count > 0 && !refdate_valid && !refdate2_valid) { CreateFormatError("DQ_FE", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, refdate2str, "refdate2", "Missing RefDate2 for participant with at least one Intervention"); }


            //----NOTE: ** Additional FormatErrors that are NOT truly Formatting Errors 
            //------------------(but Mathematica sorts/groups them as Formatting errors in Validation Ouput xlsx)
            //------------------ Can be found in the MarkInvalidRecords Stored Procedure at the very bottom.**


            //***************************END REGEX CHECKS FOR FORMATTING********************************



            // MDEVER
            //if (!ValidateIn(mdever, new string[] { "901", "900" })) { CreateError("0A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false); }
            //if (mdever != "902" && itype == "1") { CreateError("0A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false); } /*Changed here for MDE Version 902*/
            if (!ValidateIn(mdever, new string[] { "900", "901", "902", "903", "904" })) { CreateError("0A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, mdever); } /* Changed on 10142015 to include 901 and 902 per issue log */
            if (ValidateIn(mdever, new string[] { ".", " " }) && ValidateIn(itype, new string[] { "1", "2" })) { CreateError("0A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, mdever); }

            /* STFIPS */
            /* if (!ValidateIn(stfips, new string[] { "01","05","06","08","09",
            "17","18","19","26","29",
            "31","37","41","42","44",
            "45","49","50","54","55",
            "85","92"})) { CreateError("1A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, stfips); }
           */
           if (IsMissing(stfips) && ValidateIn(itype, new string[] { "1", "2" })) { CreateError("1A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, stfips); }

            /* HDANSI */
            if (IsMissing(hdansi) && ValidateIn(itype, new string[] { "1", "2" })) { CreateError("1B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hdansi); }

            /* ENROLLSITEID */
            if (IsMissing(enrollsiteid) && ValidateIn(itype, new string[] { "1", "2" })) { CreateError("1C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, enrollsiteid); }

            /* SCREENSITEID */
            if (IsMissing(screensiteid) && ValidateIn(itype, new string[] { "1", "2" })) { CreateError("1D_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, screensiteid); }

            /* TIMEPER */
            //if (IsMissing(timeper) && ValidateIn(itype, new string[] { "1", "2" })) { CreateError("2A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false); }
            //else if (IsNotAllowable(timeper, "TimePer")) { CreateError("2A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false); }
            //else if (!ValidateIn(timeper, new string[] { "1", "2", "3", "4", "5", "6", "7", "8" })) { CreateError("2A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false); }

            if (ValidateIn(timeper, new string[] { "", "." }) && itype == "1") { CreateError("2A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, timeper); }
            else if (timeper == "1" & (firstbpdate < toDateTime("07012013") || firstbpdate > toDateTime("12312013")) && itype == "1") { CreateError("2A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, timeper); }
            else if (timeper == "2" & (firstbpdate < toDateTime("01012014") || firstbpdate > toDateTime("06302014")) && itype == "1") { CreateError("2A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, timeper); }
            else if (timeper == "3" & (firstbpdate < toDateTime("07012014") || firstbpdate > toDateTime("12312014")) && itype == "1") { CreateError("2A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, timeper); }
            else if (timeper == "4" & (firstbpdate < toDateTime("01012015") || firstbpdate > toDateTime("06302015")) && itype == "1") { CreateError("2A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, timeper); }
            else if (timeper == "5" & (firstbpdate < toDateTime("07012015") || firstbpdate > toDateTime("12312015")) && itype == "1") { CreateError("2A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, timeper); }
            else if (timeper == "6" & (firstbpdate < toDateTime("01012016") || firstbpdate > toDateTime("06302016")) && itype == "1") { CreateError("2A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, timeper); }
            else if (timeper == "7" & (firstbpdate < toDateTime("07012016") || firstbpdate > toDateTime("12312016")) && itype == "1") { CreateError("2A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, timeper); }
            else if (timeper == "8" & (firstbpdate < toDateTime("01012017") || firstbpdate > toDateTime("06302017")) && itype == "1") { CreateError("2A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, timeper); }
            else if (timeper == "9" & (firstbpdate < toDateTime("07012017") || firstbpdate > toDateTime("12312017")) && itype == "1") { CreateError("2A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, timeper); }
            else if (timeper == "0" & (firstbpdate < toDateTime("01012018") || firstbpdate > toDateTime("06302018")) && itype == "1") { CreateError("2A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, timeper); }
            else if (!ValidateIn(timeper, new string[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" }) && itype == "1") { CreateError("2A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, timeper); }


            /* NSCREEN */
            if (IsMissing(nscreen) && ValidateIn(itype, new string[] { "1", "2" })) { CreateError("2B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false,nscreen); }
            else if (1 > nscreen_num && nscreen_num > 8 && !IsMissing(nscreen)) { CreateError("2B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false,nscreen); }

            /* TYPE */
            if (ValidateIn(itype, new string[] { ".", "9" }) && !IsMissingDate(bpdate)) { CreateError("2C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, itype.ToString()); }  
            else if (!ValidateIn(itype, new string[] { "1", "2" })) { CreateError("2C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false,itype.ToString()); }

            if (ValidateIn(itype_f, new string[] { ".", "9" }) && !IsMissingDate(bpdate_f)) { CreateError("2C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true,itype_f.ToString()); }
            else if (!ValidateIn(itype_f, new string[] { "3", "4" }) && !IsMissingDate(bpdate_f)) { CreateError("2C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true,itype_f.ToString()); }

            /*ENCODEID*/
            if (IsMissing(encodeid)) { CreateError("3A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, encodeid); }

            /*RESANSI*/
            if (IsMissing(resansi) && ValidateIn(itype, new string[] { "1", "2" })) { CreateError("3B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, resansi); }

            /*ZIP*/
            if (IsMissing(zip) || zip == "99999") { CreateError("3C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, zip); }
            if (zip.Length != 5) { CreateError("3C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, zip); }

            /*MYB*/
            /*  Changed here for MDE version 902*/
            //if (mybstr != "." && bpdatestr != "." && (bpdate.Year - (myb.Subtract(new TimeSpan(30, 0, 0, 0)).Year) >= 65)) { CreateError("3D_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false); }
            //if (isDateValid(mybstr) && !IsMissingDate(bpdate) && (bpdate.Year - (myb.AddDays(30).Year) < 40) && (!IsMissingDate(bpdate))) { CreateError("3D_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false); }
            if (IsMissing(mybstr) && ValidateIn(itype, new string[] { "1", "2" })) { CreateError("3D_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, mybstr); }

            /*LATINO*/
            if (IsMissing(latino) || latino == "9") { CreateError("3E_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, latino); }
            else if (!ValidateIn(latino, new string[] { "1", "2", "7", "9" })) { CreateError("3E_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, latino); }

            if (ValidateIn(latino, new string[] { ".", "", "9" }) &&
                ValidateIn(race1, new string[] { ".", "", "9" }) &&
                ValidateIn(race2, new string[] { ".", "", "9" })) { CreateError("3E_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, latino); }

            if (latino == "2" &&
                ValidateIn(race1, new string[] { ".", "", "9" }) &&
                ValidateIn(race2, new string[] { ".", "", "9" })) { CreateError("3E_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, latino); }

            /*RACE1*/
            if (ValidateIn(race1, new string[] { "9", "", "." }) && latino != "1") { CreateError("3F_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, race1); }
            else if (!ValidateIn(race1, new string[] { "1", "2", "3", "4", "5", "7", "9" })) { CreateError("3F_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, race1); }

            if (ValidateIn(race1, new string[] { ".", "", "7", "9" }) && !ValidateIn(race2, new string[] { "7", "9", "", "." })) { CreateError("3F_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, race1); }

            /*RACE2*/
            if (ValidateIn(race2, new string[] { "", "." })) { CreateError("3G_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, race2); }
            else if (!ValidateIn(race2, new string[] { "1", "2", "3", "4", "5", "7", "9" })) { CreateError("3G_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, race2); }

            /*EDUCATION*/
            if (ValidateIn(education, new string[] { "9", "", "." })) { CreateError("3H_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, education); }
            else if (!ValidateIn(education, new string[] { "1", "2", "3", "4", "7", "8", "9" })) { CreateError("3H_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, education); }
            else if (ValidateIn(education, new string[] { "7", "8" })) { CreateError("3H_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, education); }

            /*LANGUAGE*/
            if (ValidateIn(language, new string[] { "99", "", "." })) { CreateError("3I_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, language); }
            else if (!ValidateIn(language, new string[]{ "01","02","03","04","05",
            "06","07","08","09","10",
            "11","12","13","14","15",
            "16","88","99"})) { CreateError("3I_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, language); }
            else if (language == "88") { CreateError("3I_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, language); }

            /*SRHC*/
            if (ValidateIn(srhc, new string[] { "9", "", "." })) { CreateError("4A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, srhc); }
            else if (!ValidateIn(srhc, new string[] { "1", "2", "7", "8", "9" })) { CreateError("4A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, srhc); }
            else if (ValidateIn(srhc, new string[] { "7", "8" })) { CreateError("4A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, srhc); }

            /*SRHB*/
            if (ValidateIn(srhb, new string[] { "9", "", "." })) { CreateError("4B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, srhb); }
            else if (!ValidateIn(srhb, new string[] { "1", "2", "7", "8", "9" })) { CreateError("4B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, srhb); }
            if (ValidateIn(srhb, new string[] { "7", "8" })) { CreateError("4B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, srhb); }

            /*SRD*/
            if (ValidateIn(srd, new string[] { "9", "", "." })) { CreateError("4C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false,srd); }
            else if (!ValidateIn(srd, new string[] { "1", "2", "7", "8", "9" })) { CreateError("4C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, srd); }
            if (ValidateIn(srd, new string[] { "7", "8" })) { CreateError("4C_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, srd); }

            /*SRHA*/
            if (ValidateIn(srha, new string[] { "9", "", "." })) { CreateError("4D_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false,srha); }
            else if (!ValidateIn(srha, new string[] { "1", "2", "7", "8", "9" })) { CreateError("4D_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, srha); }
            if (ValidateIn(srha, new string[] { "7", "8" })) { CreateError("4D_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, srha); }

            /*HCMEDS*/
            if (ValidateIn(hcmeds, new string[] { "9", "", "." })) { CreateError("5A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hcmeds); }
            else if (!ValidateIn(hcmeds, new string[] { "1", "2", "3", "5", "7", "8", "9" })) { CreateError("5A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hcmeds); }
            if (ValidateIn(hcmeds, new string[] { "7", "8" })) { CreateError("5A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hcmeds); }
            if (hcmeds != "5" && srhc != "1" && totchol_num < 240 && totchol != ".") { CreateError("5A_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hcmeds); }
            //07/29/2017 - removed by CDC request - relayed by Katie
            //if ((hcmeds == "5" && (srhc == "1" || totchol_num >= 240)) && !ValidateInInt(totchol_num, new int[] { 777, 888, 999 })) { CreateError("5A_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hcmeds); }/*Changed here for MDE version 902 - from Error to Quality Check*/

            /*HBPMEDS*/
            if (ValidateIn(hbpmeds, new string[] { "9", "", "." })) { CreateError("5B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hbpmeds); }
            else if (!ValidateIn(hbpmeds, new string[] { "1", "2", "3", "5", "7", "8", "9" })) { CreateError("5B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hbpmeds); }
            if (ValidateIn(hbpmeds, new string[] { "7", "8" })) { CreateError("5B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hbpmeds); }
            //if (hbpmeds != "5" && srhb != "1" && ((((sbp1_num + sbp2_num) / 2) < 140 && ((dbp1_num + dbp2_num) / 2) < 90))) { CreateError("5B_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false); }
            if (hbpmeds != "5" && srhb != "1" && mean_sbp < 140 && mean_sbp != 0 && mean_dbp < 90 && mean_dbp != 0) { CreateError("5B_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hbpmeds); }
            //07/29/2017 - removed by CDC request - relayed by Katie
            //if (hbpmeds == "5" && (srhb == "1" || mean_sbp >= 140 || mean_dbp >= 90)) { CreateError("5B_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hbpmeds); }/*Changed here for MDE version 902 - from Error to Quality Check*/

            //*DMEDS;
            if (ValidateIn(dmeds, new string[] { "9", "", "." })) { CreateError("5C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dmeds); } //rror_dmeds == "5C_1,";
            else if (!ValidateIn(dmeds, new string[] { "1", "2", "3", "5", "7", "8", "9" })) { CreateError("5C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dmeds); } //rror_dmeds == strip(error_dmeds)|| "5C_2,";

            if (ValidateIn(dmeds, new string[] { "7", "8" })) { CreateError("5C_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dmeds); } //rror_dmeds == strip(error_dmeds)|| "5C_3,";
            if (dmeds != "5" && srd != "1" &&
                glucose_num < 126 && glucose != "." &&
                a1c_num < 6.5 && a1c != "."
                ) { CreateError("5C_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dmeds); } //rror_dmeds == strip(error_dmeds)|| "5C_4,";
            //07/29/2017 - removed by CDC request - relayed by Katie
            //if (dmeds == "5" &&
            //    (
            //        srd == "1" ||
            //        (glucose_num >= 126 && !ValidateInInt(glucose_num, new int[] { 777, 888, 999 })) ||
            //        (a1c_num >= 6.5 && !ValidateIn(a1c, new string[] { "7777", "8888", "9999" }))
            //    )
            //    ) { CreateError("5C_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dmeds); } /*Changed here for MDE version 902 - from Error to Quality Check*/

            //*HCADHERE;
            if (ValidateIn(hcadhere, new string[] { "99", "", "." })) { CreateError("5D_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hcadhere); } //rror_hcadhere == "5D_1,";
            else if (hcadhere_num > 7 && !ValidateIn(hcadhere, new string[] { "55", "77", "88" })) { CreateError("5D_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hcadhere); } /*Changed here for MDE Version 902*/
            if (ValidateIn(hcadhere, new string[] { "77", "88" })) { CreateError("5D_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hcadhere); } //rror_hcadhere == strip(error_hcadhere)|| "5D_3,";
            if (hcadhere != "55" && hcmeds != "1") { CreateError("5D_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hcadhere); } //rror_hcadhere == strip(error_hcadhere)|| "5D_4,";
            if (hcadhere == "55" && hcmeds == "1") { CreateError("5D_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hcadhere); } //rror_hcadhere == strip(error_hcadhere)|| "5D_5,";

            //*HBPADHERE;
            if (ValidateIn(hbpadhere, new string[] { "99", "", "." })) { CreateError("5E_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hbpadhere); } //rror_hbpadhere == "5E_1,";
            else if (hbpadhere_num > 7 && !ValidateIn(hbpadhere, new string[] { "55", "77", "88" })) { CreateError("5E_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hbpadhere); } /*Changed here for MDE Version 902*/
            if (ValidateIn(hbpadhere, new string[] { "77", "88" })) { CreateError("5E_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hbpadhere); } //rror_hbpadhere == strip(error_hbpadhere)|| "5E_3,";
            if (hbpadhere != "55" && hbpmeds != "1") { CreateError("5E_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hbpadhere); } //rror_hbpadhere == strip(error_hbpadhere)|| "5E_4,";
            if (hbpadhere == "55" && hbpmeds == "1") { CreateError("5E_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hbpadhere); } //rror_hbpadhere == strip(error_hbpadhere)|| "5E_5,";

            //*DADHERE;
            if (ValidateIn(dadhere, new string[] { "99", "", "." })) { CreateError("5F_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dadhere); } //rror_dadhere == "5F_1,";
            else if (dadhere_num > 7 && !ValidateIn(dadhere, new string[] { "55", "77", "88" })) { CreateError("5F_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dadhere); } /*Changed here for MDE Version 902*/
            if (ValidateIn(dadhere, new string[] { "77", "88" })) { CreateError("5F_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dadhere); } //rror_dadhere == strip(error_dadhere)|| "5F_3,";
            if (dadhere != "55" && dmeds != "1") { CreateError("5F_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dadhere); } //rror_dadhere == strip(error_dadhere)|| "5F_4,";
            if (dadhere == "55" && dmeds == "1") { CreateError("5F_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dadhere); } //rror_dadhere == strip(error_dadhere)|| "5F_5,";    

            //BPHOME;
            if (ValidateIn(bphome, new string[] { "009", "", "." })) { CreateError("6A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bphome); ; } //rror_bphome == "6A_1,";
            else if (!ValidateIn(bphome, new string[] { "001", "002", "003", "004", "005", "007", "008", "009", "023", "034", "234" })) { CreateError("6A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bphome); } //rror_bphome == strip(error_bphome)|| "6A_2,";

            if (ValidateIn(bphome, new string[] { "007", "008" })) { CreateError("6A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bphome); } //rror_bphome == strip(error_bphome)|| "6A_3,";
            /*Changed here for MDE Version 902*/
            //if (bphome != "005" && ValidateIn(bphome, new string[] { "001", "002", "003", "004", "007", "008", "009", "023", "034", "234" }) && srhb != "1") { CreateError("6A_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false); } //rror_bphome == strip(error_bphome)|| "6A_4,";
            if (bphome == "005" && (srhb == "1" || hbpmeds == "1")) { CreateError("6A_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bphome); } //rror_bphome == strip(error_bphome)|| "6A_5,";

            //BPFREQ;
            if (ValidateIn(bpfreq, new string[] { "9", ".", "" })) { CreateError("6B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpfreq); } //rror_bpfreq == "6B_1,";
            else if (!ValidateIn(bpfreq, new string[] { "1", "2", "3", "4", "5", "6", "7", "8", "9" })) { CreateError("6B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpfreq); } //rror_bpfreq == strip(error_bpfreq)|| "6B_2,";

            if (ValidateIn(bpfreq, new string[] { "7", "8" })) { CreateError("6B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpfreq); } //rror_bpfreq == strip(error_bpfreq)|| "6B_3,";
            if (bpfreq != "6" && bphome != "001" && ValidateIn(bphome, new string[] { "002", "003", "004", "005", "007", "008", "009", "023", "034", "234" })) { CreateError("6B_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpfreq); } //rror_bpfreq == strip(error_bpfreq)|| "6B_4,";
            if (bpfreq == "6" && bphome == "001") { CreateError("6B_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpfreq); } //rror_bpfreq == strip(error_bpfreq)|| "6B_5,";

            //BPSEND;
            if (ValidateIn(bpsend, new string[] { "9", ".", "" })) { CreateError("6C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpsend); } //rror_bpsend == "6C_1,";
            else if (!ValidateIn(bpsend, new string[] { "1", "2", "5", "7", "8", "9" })) { CreateError("6C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpsend); } //rror_bpsend == strip(error_bpsend)|| "6C_2,";

            if (ValidateIn(bpsend, new string[] { "7", "8" })) { CreateError("6C_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpsend); } //rror_bpsend == strip(error_bpsend)|| "6C_3,";
            if (bpsend != "5" && bphome != "001" && ValidateIn(bphome, new string[] { "002", "003", "004", "005", "007", "008", "009", "023", "034", "234" })) { CreateError("6C_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpsend); } //rror_bpsend == strip(error_bpsend)|| "6C_4,";
            if (bpsend == "5" && bphome == "001") { CreateError("6C_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpsend); } //rror_bpsend == strip(error_bpsend)|| "6C_5,";

            //FRUIT;
            if (ValidateIn(fruit, new string[] { "99", "", "." })) { CreateError("7A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, fruit); } //rror_fruit == "7A_1,";
            if (fruit == "88") { CreateError("7A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, fruit); } //rror_fruit == strip(error_fruit)|| "7A_2,";

            if ((fruit_num < 0 || fruit_num > 50)  && !ValidateIn(fruit, new string[] { "88", "99" })) { CreateError("7A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, fruit); } //rror_fruit == strip(error_fruit)|| "7A_3,";

            //VEGETABLES;
            if (ValidateIn(vegetables, new string[] { "", ".", "99" })) { CreateError("7B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, vegetables); } //rror_vegetables == "7B_1,";
            if (vegetables == "88") { CreateError("7B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, vegetables); } //rror_vegetables == strip(error_vegetables)|| "7B_2,";
            if ((vegetables_num < 0 || vegetables_num > 15) && !ValidateIn(vegetables, new string[] { "88", "99" })) { CreateError("7B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, vegetables); } //rror_vegetables == strip(error_vegetables)|| "7B_3,";

            //FISH; 
            if (ValidateIn(fish, new string[] { "9", "", "." })) { CreateError("7C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, fish); } //rror_fish == "7C_1,";
            else if (!ValidateIn(fish, new string[] { "1", "2", "8", "9" })) { CreateError("7C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, fish); } //rror_fish == strip(error_fish)|| "7C_2,";
            if (fish == "8") { CreateError("7C_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, fish); } //rror_fish == strip(error_fish)|| "7C_3,";

            //GRAIN;
            if (ValidateIn(grains, new string[] { "9", "", "." })) { CreateError("7D_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, grains); } //rror_grains == "7D_1,";
            else if (!ValidateIn(grains, new string[] { "1", "2", "8", "9" })) { CreateError("7D_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, grains); } //rror_grains == strip(error_grains)|| "7D_2,";
            if (grains == "8") { CreateError("7D_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, grains); } //rror_grains == strip(error_grains)|| "7D_3,";

            //SUGAR;
            if (ValidateIn(sugar, new string[] { "9", "", "." })) { CreateError("7E_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, sugar); } //rror_sugar == "7E_1,";
            else if (!ValidateIn(sugar, new string[] { "1", "2", "8", "9" })) { CreateError("7E_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, sugar); } //rror_sugar == strip(error_sugar)|| "7E_2,";
            if (sugar == "8") { CreateError("7E_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, sugar); } //rror_sugar == strip(error_sugar)|| "7E_3,";

            //SALTWATCH;
            if (ValidateIn(saltwatch, new string[] { "9", "", "." })) { CreateError("7F_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, saltwatch); } //rror_saltwatch == "7F_1,";
            if (!ValidateIn(saltwatch, new string[] { "1", "2", "8", "9", "", "." })) { CreateError("7F_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, saltwatch); } //rror_saltwatch == strip(error_saltwatch)|| "7F_2,";
            if (saltwatch == "8") { CreateError("7F_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, saltwatch); } //rror_saltwatch == strip(error_saltwatch)|| "7F_3,";

            //PAMOD;
            if (ValidateIn(pamod, new string[] { "999", "", "." })) { CreateError("8A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, pamod); } //rror_pamod == "8A_1,";
            if (pamod == "888") { CreateError("8A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, pamod); } //rror_pamod == strip(error_pamod)|| "8A_2,";

            if (!ValidateIn(pamod, new string[] { "888", "999", "000" }) &&
                (
                    (pamod_num < 10 && pamod != ".") |
                    pamod_num > 850
                )) { CreateError("8A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, pamod); } /*Changed here for MDE Version 902 - Also changed from error to quality check*/

            //PAVIG;
            if (ValidateIn(pavig, new string[] { "999", "", "." })) { CreateError("8B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, pavig); } //rror_pavig == "8B_1,";

            if (pavig == "888") { CreateError("8B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, pavig); } //rror_pavig == strip(error_pavig)|| "8B_2,";
            if (!ValidateIn(pavig, new string[] { "888", "999", "000" }) &&
                (
                    (pavig_num < 10 && pavig != ".") |
                    (pavig_num > 850)
                )) { CreateError("8B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, pavig); } /*Changed here for MDE Version 902 - Also changed from error to quality check*/

            //SMOKER;
            if (ValidateIn(smoker, new string[] { "9", "", "." })) { CreateError("9A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, smoker); } //rror_smoker == "9A_1,";
            else if (!ValidateIn(smoker, new string[] { "1", "2", "3", "4", "8", "9" })) { CreateError("9A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, smoker); } //rror_smoker == strip(error_smoker)|| "9A_2,";
            if (smoker == "8") { CreateError("9A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, smoker); } //rror_smoker == strip(error_smoker)|| "9A_3,";

            //SECHAND;
            if (ValidateIn(sechand, new string[] { "99", "", "." })) { CreateError("9B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, sechand); } //rror_sechand == "9B_1,";
            if (sechand == "88") { CreateError("9B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, sechand); } //rror_sechand == strip(error_sechand)|| "9B_2,";
            if (sechand_num > 24 && !ValidateIn(sechand, new string[] { "66", "88", "99" })) { CreateError("9B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, sechand); } //rror_sechand == strip(error_sechand)|| "9B_3,";

            //QOLPH;
            if (ValidateIn(qolph, new string[] { "99", "", "." })) { CreateError("10A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, qolph); } //rror_qolph == "10A_1,";
            else if (ValidateIn(qolph, new string[] { "77", "88" })) { CreateError("10A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, qolph); } //rror_qolph == strip(error_qolph)|| "10A_2,";
            if (qolph_num > 30 && !ValidateIn(qolph, new string[] { "77", "88", "99" })) { CreateError("10A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, qolph); } //rror_qolph == strip(error_qolph)|| "10A_3,";

            //QOLMH;
            if (ValidateIn(qolmh, new string[] { "99", "", "." })) { CreateError("10B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, qolmh); } //rror_qolmh == "10B_1,";
            if (ValidateIn(qolmh, new string[] { "77", "88" })) { CreateError("10B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, qolmh); } //rror_qolmh == strip(error_qolmh)|| "10B_2,";
            if (qolmh_num > 30 && !ValidateIn(qolmh, new string[] { "77", "88", "99" })) { CreateError("10B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, qolmh); } //rror_qolmh == strip(error_qolmh)|| "10B_3,";

            //QOLEFFECT;
            if (ValidateIn(qoleffect, new string[] { "99", "", "." })) { CreateError("10C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, qoleffect); } //rror_qoleffect == "10C_1,";
            else if (ValidateIn(qoleffect, new string[] { "77", "88" })) { CreateError("10C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, qoleffect); } //rror_qoleffect == strip(error_qoleffect)|| "10C_2,";
            if (qoleffect_num > 30 && !ValidateIn(qoleffect, new string[] { "77", "88", "99" })) { CreateError("10C_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, qoleffect); } //rror_qoleffect == strip(error_qoleffect)|| "10C_3,";

            //HEIGHT;
            if (ValidateIn(height, new string[] { "77" })) { CreateError("11A_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, height); } //rror_height == "11A_4,";
            if (ValidateIn(height, new string[] { "88" })) { CreateError("11A_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, height); } //rror_height == strip(error_height)|| "11A_5,";
            if (ValidateIn(height, new string[] { "99", "", "." })) { CreateError("11A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, height); } //rror_height == strip(error_height)|| "11A_1,";

            if ((
                (height_num < 48 && height != ".") |
                height_num > 76
                ) &
                !ValidateIn(height, new string[] { "77", "88", "99" })
                    ) { CreateError("11A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, height); } //rror_height == strip(error_height)|| "11A_2,";

            if ((height_num >= 48 && height_num <= 58) |
                (height_num >= 74 && height_num <= 76)) { CreateError("11A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, height); } //rror_height == strip(error_height)|| "11A_3,";

            //WEIGHT;
            if (ValidateIn(weight, new string[] { "999", "", "." })) { CreateError("11B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, weight); } //rror_weight == "11B_1,";
            if (ValidateIn(weight, new string[] { "777" })) { CreateError("11B_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, weight); } //rror_weight == strip(error_weight)|| "11B_4,";
            if (ValidateIn(weight, new string[] { "888" })) { CreateError("11B_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, weight); } //rror_weight == strip(error_weight)|| "11B_5,"; 

            if ((
                (weight_num < 74 && weight != ".") |
                weight_num > 460) &
                !ValidateIn(weight, new string[] { "777", "888", "999" })
                    ) { CreateError("11B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, weight); } //rror_weight == strip(error_weight)|| "11B_2,";

            if ((weight_num >= 74 && weight_num <= 90) |
                (weight_num >= 350 && weight_num <= 460)) { CreateError("11B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, weight); } //rror_weight == strip(error_weight)|| "11B_3,";

            //WAIST;
            if (ValidateIn(waist, new string[] { "", "." })) { CreateError("11C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, waist); } //rror_waist == strip(error_waist)|| "11C_1,";
            if ((
                (waist_num < 16 && waist != ".") |
                waist_num > 71) &
                !ValidateIn(waist, new string[] { "77", "88", "99" })
                    ) { CreateError("11C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, waist); } //rror_waist == strip(error_waist)||"11C_2,";

            //HIP;
            if (ValidateIn(hip, new string[] { "", "." }) && ValidateIn(itype, new string[] { "1", "2" })) { CreateError("11D_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hip); } //rror_hip == strip(error_hip)|| "11D_1,";
            if (((hip_num < 26 && hip != ".") | hip_num > 75) &
                !ValidateIn(hip, new string[] { "77", "88", "99" })) { CreateError("11D_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hip); } //rror_hip == strip(error_hip)||"11D_2,";

            //BPDATE;
            if (ValidateIn(bpdatestr, new string[] { "", "." })) { CreateError("12A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpdatestr); } //rror_bpdate == strip(error_bpdate)|| "12A_1,";
            if (bpdate < submissiondate && bpdatestr != "." && mdever == "901") { CreateError("12A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpdatestr); } //rror_bpdate == strip(error_bpdate)|| "12A_2,";
            if (bpdate > currdate) { CreateError("12A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpdatestr); } //rror_bpdate == strip(error_bpdate)|| "12A_3,";

            //sbp1 (should be SBP1?);
            if (ValidateIn(sbp1, new string[] { "999", "", "." })) { CreateError("12B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, sbp1); } //rror_spbi1 == "12B_1,";
            if (sbp1 == "888") { CreateError("12B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, sbp1); } //rror_spbi1 == strip(error_spbi1)|| "12B_3,";
            if (((sbp1_num < 74 && sbp1 != ".") || sbp1_num > 260) && !ValidateIn(sbp1, new string[] { "777", "888", "999" })) { CreateError("12B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, sbp1); } //rror_spbi1 == strip(error_spbi1)|| "12B_2,";
            if ((sbp1_num >= 230 && sbp1_num <= 260)) { CreateError("12B_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, sbp1); } //rror_spbi1 == strip(error_spbi1)|| "12B_4,";
            if (sbp1 == "777") { CreateError("12B_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, sbp1); } //rror_spbi1 == strip(error_spbi1)|| "12B_5,";
            if (ValidateIn(sbp1, new string[] { "777", "888", "999" }) && !ValidateIn(sbp2, new string[] { "777", "888", "999" })) { CreateError("12B_6", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, sbp1); } //rror_spbi1 == strip(error_spbi1)|| "12B_6,";

            //DBP1;
            if (ValidateIn(dbp1, new string[] { "999", "", "." })) { CreateError("12C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dbp1); } //rror_dbp1 == "12C_1,";
            if (dbp1 == "888") { CreateError("12C_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dbp1); } //rror_dbp1 == strip(error_dbp1)|| "12C_3,";

            if (((dbp1_num < 2 && dbp1 != ".") || dbp1_num > 156) && !ValidateIn(dbp1, new string[] { "777", "888", "999" })) { CreateError("12C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dbp1); } //rror_dbp1 == strip(error_dbp1)|| "12C_2,";
            if ((dbp1_num >= 2 && dbp1_num <= 12) || (dbp1_num >= 122 && dbp1_num <= 156)) { CreateError("12C_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dbp1); } //rror_dbp1 == strip(error_dbp1)|| "12C_4,";
            if (dbp1 == "777") { CreateError("12C_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dbp1); } //rror_dbp1 == strip(error_dbp1)|| "12C_5,";
            if (ValidateIn(dbp1, new string[] { "777", "888", "999" }) && !ValidateIn(dbp2, new string[] { "777", "888", "999" })) { CreateError("12C_6", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dbp1); } //rror_dbp1 == strip(error_dbp1)|| "12C_6,";
            if ((sbp1 == "777" && dbp1 != "777") || (sbp1 != "777" && dbp1 == "777")) { CreateError("12C_7", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dbp1); } //rror_dbp1 == strip(error_dbp1)|| "12C_7,";

            //SPB2 (Should be SBP2?);
            if (ValidateIn(sbp2, new string[] { "999", "", "." })) { CreateError("12D_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, sbp2); } //rror_sbp2 == "12D_1,";
            if (sbp2 == "888") { CreateError("12D_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, sbp2); } //rror_sbp2 == strip(error_sbp2)|| "12D_3,";

            if (((sbp2_num < 74 && sbp2 != ".") || sbp2_num > 260) && !ValidateIn(sbp2, new string[] { "777", "888", "999" })) { CreateError("12D_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, sbp2); } //rror_sbp2 == strip(error_sbp2)|| "12D_2,";
            if ((sbp2_num >= 230 && sbp2_num <= 260)) { CreateError("12D_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, sbp2); } //rror_sbp2 == strip(error_sbp2)|| "12D_4,";
            if (sbp2 == "777") { CreateError("12D_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, sbp2); } //rror_sbp2 == strip(error_sbp2)|| "12D_5,";

            //DBP2;
            if (ValidateIn(dbp2, new string[] { "999", "", "." })) { CreateError("12E_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dbp2); } //rror_dbp2 == "12E_1,";
            if (dbp2 == "888") { CreateError("12E_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dbp2); } //rror_dbp2 == strip(error_dbp2)|| "12E_3,";

            if (((dbp2_num < 2 && dbp2 != ".") || dbp2_num > 156) && !ValidateIn(dbp2, new string[] { "777", "888", "999" })) { CreateError("12E_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dbp2); } //rror_dbp2 == strip(error_dbp2)|| "12E_2,";
            if ((dbp2_num >= 2 && dbp2_num <= 12) || (dbp2_num >= 122 && dbp2_num <= 156)) { CreateError("12E_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dbp2); } //rror_dbp2 == strip(error_dbp2)|| "12E_4,";
            if (dbp2 == "777") { CreateError("12E_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dbp2); } //rror_dbp2 == strip(error_dbp2)|| "12E_5,";
            if ((sbp2 == "777" && dbp2 != "777") || (sbp2 != "777" && dbp2 == "777")) { CreateError("12E_6", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dbp2); } //rror_dbp2 == strip(error_dbp2)|| "12E_6,";

            //FAST;
            if (ValidateIn(fast, new string[] { "9", "", "." })) { CreateError("13A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, fast); } //rror_fast == "13A_1,";
            else if (!ValidateIn(fast, new string[] { "1", "2", "9" })) { CreateError("13A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, fast); } //rror_fast == strip(error_fast)|| "13A_2,";

            if (fast != "9" && totchol == "777" && hdl == "777" && ldl == "777" && trigly == "7777" && glucose == "777") { CreateError("13A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, fast); } //rror_fast == strip(error_fast)|| "13A_3,";

            if (fast != "9" && totchol == "888" && hdl == "888" && ldl == "888" && trigly == "8888" && glucose == "888") { CreateError("13A_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, fast); } //rror_fast == strip(error_fast)|| "13A_4,";

            if (fast != "9" && totchol == "999" && hdl == "999" && ldl == "999" && trigly == "9999" && glucose == "999") { CreateError("13A_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, fast); } //rror_fast == strip(error_fast)|| "13A_5,";

            if (_row == 990)
            {
               string sTesting = "do something here.";
            }
            //TCDATE;
            if (ValidateIn(tcdatestr, new string[] { "", "." }) && (!ValidateIn(totchol, new string[] { "888", "999", "", "." }) || !ValidateIn(hdl, new string[] { "888", "999", "", "." }) || !ValidateIn(ldl, new string[] { "888", "999", "", "." }) || !ValidateIn(trigly, new string[] { "8888", "9999", "", "." }))) { CreateError("14A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, tcdatestr); } //rror_tcdate == "14A_1,";
            if (tcdate > currdate) { CreateError("14A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, tcdatestr); } //rror_tcdate == strip(error_tcdate)|| "14A_2,";

            //TOTCHOL;
            if (ValidateIn(totchol, new string[] { "999", "", "." })) { CreateError("14B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, totchol); } //rror_totchol == "14B_1,";
            else if (((totchol_num < 44 && totchol != ".") || totchol_num > 702) && !ValidateIn(totchol, new string[] { "777", "888", "999" })) { CreateError("14B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, totchol); } //rror_totchol == strip(error_totchol)|| "14B_2,";

            if ((totchol_num >= 44 && totchol_num <= 60) || (totchol_num >= 400 && totchol_num <= 702)) { CreateError("14B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, totchol); } //rror_totchol == strip(error_totchol)|| "14B_3,";
            if (ValidateIn(totchol, new string[] { "777", "888" })) { CreateError("14B_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, totchol); } //rror_totchol == strip(error_totchol)|| "14B_4,";

            //HDL;
            if (ValidateIn(hdl, new string[] { "", ".", "999" })) { CreateError("14C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hdl); } //rror_hdl == "14C_1,";
            else if (((hdl_num < 7 && hdl != ".") || hdl_num > 196) && !ValidateIn(hdl, new string[] { "777", "888", "999" })) { CreateError("14C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hdl); } //rror_hdl == strip(error_hdl)|| "14C_2,";
            if ((hdl_num >= 155 && hdl_num <= 196)) { CreateError("14C_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hdl); } //rror_hdl == strip(error_hdl)|| "14C_3,";
            if (ValidateIn(hdl, new string[] { "777", "888" })) { CreateError("14C_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hdl); } //rror_hdl == strip(error_hdl)|| "14C_4,";

            //LDL;
            if (ValidateIn(ldl, new string[] { "", "." })) { CreateError("14D_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, ldl); } //rror_ldl == strip(error_ldl)|| "14D_1,";
            if (((ldl_num < 20 && ldl != ".") | ldl_num > 380) && !ValidateIn(ldl, new string[] { "777", "888", "999" })) { CreateError("14D_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, ldl); } //rror_ldl == strip(error_ldl)||"14D_2,";
            if (ValidateIn(fast, new string[] { "2" }) && ldl != "999") { CreateError("14D_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, ldl); } //rror_ldl == strip(error_ldl)|| "14D_3,";
            if ((ldl_num >= 344 && ldl_num <= 380)) { CreateError("14D_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, ldl); } //rror_ldl == strip(error_ldl)|| "14D_4,";

            //TRIGLY;
            if (ValidateIn(trigly, new string[] { "", "." })) { CreateError("14E_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, trigly); } //rror_trigly == strip(error_trigly)|| "14E_1,";
            if (((trigly_num < 12 && trigly != ".") | trigly_num > 3000) && !ValidateIn(trigly, new string[] { "7777", "8888", "9999" })) { CreateError("14E_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, trigly); } //rror_trigly ==  strip(error_trigly) || "14E_2,";
            if (ValidateIn(fast, new string[] { "2" }) && trigly != "9999") { CreateError("14E_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, trigly); } //rror_trigly == strip(error_trigly)|| "14E_3,";
            if ((trigly_num >= 1000 && trigly_num <= 3000)) { CreateError("14E_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, trigly); } //rror_trigly == strip(error_trigly)|| "14E_4,";

            //BGDATE;
            if (bgdatestr == "." && (!ValidateIn(glucose, new string[] { "888", "999" }) || !ValidateIn(a1c, new string[] { "8888", "9999" }))) { CreateError("15A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bgdatestr); } //rror_bgdate == "15A_1,";
            if (bgdate > currdate) { CreateError("15A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bgdatestr); } //rror_bgdate == strip(error_bgdate)|| "15A_2,";

            //GLUCOSE;
            if (ValidateIn(glucose, new string[] { "", "." })) { CreateError("15B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, glucose); } //rror_glucose == strip(error_glucose)|| "15B_1,";
            if (glucose == "999" && a1c == "9999") { CreateError("15B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, glucose); } //rror_glucose == strip(error_glucose)|| "15B_2,";
            if (((glucose_num < 37 && glucose != ".") | glucose_num > 571) && !ValidateIn(glucose, new string[] { "777", "888", "999" })) { CreateError("15B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, glucose); } //rror_glucose == strip(error_glucose)|| "15B_3,";
            if ((glucose_num >= 37 && glucose_num <= 50) || (glucose_num >= 275 && glucose_num <= 571)) { CreateError("15B_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, glucose); } //rror_glucose == strip(error_glucose)|| "15B_4,";
            if (glucose == "777") { CreateError("15B_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, glucose); } //rror_glucose == strip(error_glucose)|| "15B_5,";
            if (glucose == "888" && a1c == "8888") { CreateError("15B_6", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, glucose); } //rror_glucose == strip(error_glucose)|| "15B_6,";
            if (fast == "2" && glucose != "999" && bpdate > toDateTime("01012015")) { CreateError("15B_7", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, glucose); } /*Changed here for MDE Version 902*/

            //A1C; 
            if (ValidateIn(a1c, new string[] { "", "." }) && ValidateIn(itype, new string[] { "1", "2" })) { CreateError("15C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, a1c); } //rror_a1c == strip(error_a1c)|| "15C_1,";
            if ((a1c_num < 2.8 && a1c != ".") || (a1c_num > 16.2 && !ValidateIn(a1c, new string[] { "7777", "8888", "9999" }))) { CreateError("15C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, a1c); } //rror_a1c == strip(error_a1c)|| "15C_2,";
            if ((a1c_num >= 2.8 && a1c_num <= 4) || (a1c_num >= 13 && a1c_num <= 16.2)) { CreateError("15C_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, a1c); } //rror_a1c == strip(error_a1c)|| "15C_3,";
            if (a1c == "7777") { CreateError("15C_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, a1c); } //rror_a1c == strip(error_a1c)|| "15C_4,";

            //BPALERT;
            if (ValidateIn(bpalert, new string[] { "9", "", "." })) { CreateError("16A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpalert); } //rror_bpalert == "16A_1,";
            else if (!ValidateIn(bpalert, new string[] { "1", "2", "3", "8", "9" })) { CreateError("16A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpalert); } //rror_bpalert == strip(error_bpalert)|| "16A_2,";
            if ((
                    (mean_sbp > 180) ||
                    (mean_dbp > 110)
                ) &&
                !ValidateIn(bpalert, new string[] { "1", "2", "8", "9" })
                ) { CreateError("16A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpalert); } //rror_bpalert == strip(error_bpalert)|| "16A_3,";
            if (((mean_sbp <= 180 && mean_sbp != 0) &&
                (mean_dbp <= 110 && mean_dbp != 0)) &&
                !ValidateIn(bpalert, new string[] { "3", "", "." })
                ) { CreateError("16A_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpalert); } //rror_bpalert == strip(error_bpalert)|| "16A_4,";
            if (ValidateIn(sbp1, new string[] { "777", "888", "999" }) && ValidateIn(dbp1, new string[] { "777", "888", "999" }) && bpalert != "3") { CreateError("16A_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpalert); } //rror_bpalert == strip(error_bpalert)|| "16A_5,";
            if ((
                    (mean_sbp > 180) ||
                    (mean_dbp > 110)
                ) &
                ValidateIn(bpalert, new string[] { "8", "9", "", "." }) &&
                bpdidate_valid
                ) { CreateError("16A_6", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpalert); } //rror_bpalert == strip(error_bpalert)|| "16A_6,";

            //BPDIDATE;
            if ((
                    (mean_sbp > 180) ||
                    (mean_dbp > 110)
                ) &&
                bpalert == "1" &&
                bpdidate_valid &&
                bpdate_valid &
                bpdidate < bpdate
                ) { CreateError("16B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpdidatestr); } //rror_bpdidate == "16B_1,";
            if ((
                    (mean_sbp <= 180 && mean_sbp > 0) &
                    (mean_dbp <= 110 && mean_dbp > 0)
                ) &&
                bpdidate_valid && bpdidatestr != "."
                ) { CreateError("16B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpdidatestr); } //rror_bpdidate == strip(error_bpdidate)|| "16B_2,";

            if (ValidateIn(sbp1, new string[] { "777", "888", "999" }) &&
                ValidateIn(dbp1, new string[] { "777", "888", "999" }) &&
                bpdidate_valid
                ) { CreateError("16B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpdidatestr); } //rror_bpdidate == strip(error_bpdidate)|| "16B_3,";

            if ((
                    (mean_sbp > 180) ||
                    (mean_dbp > 110)
                ) &&
                bpalert == "1" &&
                (
                    (bpdidate_valid && bpdate_valid && bpdidate - bpdate > new TimeSpan(7, 0, 0, 0)) ||
                    bpdidatestr == "."
                )
                ) { CreateError("16B_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpdidatestr); } //rror_bpdidate == strip(error_bpdidate)|| "16B_4,";
            if ((
                    (mean_sbp > 180) ||
                    (mean_dbp > 110)
                ) &&
                bpalert == "2" &&
                (
                    (bpdidate_valid && bpdate_valid && bpdidate - bpdate > new TimeSpan(7, 0, 0, 0)) ||
                    bpdidatestr == "."
                )
                ) { CreateError("16B_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpdidatestr); } //rror_bpdidate == strip(error_bpdidate)|| "16B_5,";
            if ((
                    (mean_sbp > 180) ||
                    (mean_dbp > 110)
                ) &&
                bpalert == "8" && bpdidatestr == ".") { CreateError("16B_6", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpdidatestr); } //rror_bpdidate == strip(error_bpdidate)|| "16B_6,";
            if ((
                    (mean_sbp > 180) ||
                    (mean_dbp > 110)
                ) &&
                bpalert == "9" && bpdidatestr == ".") { CreateError("16B_7", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bpdidatestr); } //rror_bpdidate == strip(error_bpdidate)|| "16B_7,";

            /*Not Diabetic (for use later);*/
            if ((srd != "1" & a1c_num < 6.5 & a1c != ".") || (ValidateIn(a1c, new string[] { "7777", "8888", "9999", "", "." }))) { not_diabetic = true; }/*Changed here for MDE Version 902*/


            //BGALERT;
            if (ValidateIn(bgalert, new string[] { "9", "", "." })) { CreateError("16C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bgalert); } //rror_bgalert == "16C_1,";
            else if (!ValidateIn(bgalert, new string[] { "1", "2", "3", "8", "9" })) { CreateError("16C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bgalert); } //rror_bgalert == strip(error_bgalert)|| "16C_2,";

            if (((glucose_num <= 50 && glucose != ".") || (glucose_num >= 250 && !ValidateIn(glucose, new string[] { "777", "888", "999" }))) && not_diabetic && !ValidateIn(bgalert, new string[] { "1", "2", "8", "9", "", "." })) { CreateError("16C_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bgalert); } /*Changed here for MDE Version 902*/
            if ((glucose_num > 50 && glucose_num < 250) && bgalert != "3") { CreateError("16C_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bgalert); } //rror_bgalert == strip(error_bgalert)|| "16C_4,";
            if (ValidateIn(glucose, new string[] { "777", "888", "999" }) && bgalert != "3") { CreateError("16C_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bgalert); } //rror_bgalert == strip(error_bgalert)|| "16C_5,";
            if ((
                (glucose_num <= 50 && glucose != ".") ||
                (glucose_num >= 250 && !ValidateIn(glucose, new string[] { "777", "888", "999" }))
                ) &&
                not_diabetic &&
                ValidateIn(bgalert, new string[] { "8", "9" }) &&
                bgdidate_valid
                ) { CreateError("16C_6", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bgalert); } /*Changed here for MDE Version 902*/

            //BGDIDATE;
            if ((
                    (glucose_num <= 50 && glucose != ".") ||
                    (glucose_num >= 250 && !ValidateIn(glucose, new string[] { "777", "888", "999" }))
                ) &&
                not_diabetic &&
                bgdidate_valid &&
                bgdate_valid &&
                bgdidate < bgdate &&
                bgalert == "1"
                ) { CreateError("16D_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bgdidatestr); } //rror_bgdidate == "16D_1,";
            if ((glucose_num > 50 && glucose_num < 250) && bgdidate_valid && bgdidatestr != ".") { CreateError("16D_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bgdidatestr); } //rror_bgdidate == strip(error_bgdidate)|| "16D_2,";
            if (ValidateIn(glucose, new string[] { "777", "888", "999" }) && bgdidate_valid && bgdidatestr != ".") { CreateError("16D_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bgdidatestr); } //rror_bgdidate == strip(error_bgdidate)|| "16D_3,";
            if ((
                    (glucose_num <= 50 && glucose != ".") ||
                    (glucose_num >= 250 && !ValidateIn(glucose, new string[] { "777", "888", "999" }))
                ) &&
                not_diabetic &&
               bgalert == "1" &&
               ((bgdidatestr != "." && bgdatestr != "." && bgdidate - bgdate > new TimeSpan(7, 0, 0, 0) && bgdidate_valid && bgdate_valid) || bgdidatestr == ".")
               ) { CreateError("16D_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bgdidatestr); } /*Changed here for MDE Version 902*/
            if ((
                    (glucose_num <= 50 && glucose != ".") ||
                    (glucose_num >= 250 && !ValidateIn(glucose, new string[] { "777", "888", "999" }))
                ) &&
                not_diabetic &&
               bgalert == "2" &&
               ((bgdidatestr != "." && bgdatestr != "." && bgdidate_valid && bgdate_valid && bgdidate - bgdate > new TimeSpan(7, 0, 0, 0)) || bgdidatestr == ".")
               ) { CreateError("16D_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bgdidatestr); } /*Changed here for MDE Version 902*/
            if ((
                    (glucose_num <= 50 && glucose != ".") ||
                    (glucose_num >= 250 && !ValidateIn(glucose, new string[] { "777", "888", "999" }))
                ) &&
                not_diabetic &&
                bgalert == "8" && bgdidatestr == ".") { CreateError("16D_6", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bgdidatestr); } /*Changed here for MDE Version 902*/
            if ((
                    (glucose_num <= 50 && glucose != ".") ||
                    (glucose_num >= 250 && !ValidateIn(glucose, new string[] { "777", "888", "999" }))
                ) &&
                not_diabetic &&
                bgalert == "9" && bgdidatestr == ".") { CreateError("16D_7", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, bgdidatestr); } /*Changed here for MDE Version 902*/

            //RRCDATE;
            if (rrcdatestr == "." && ValidateIn(itype, new string[] { "1", "2" })) { CreateError("17A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rrcdatestr); } //rror_rrcdate == "17A_1,";
            if (rrcdate > currdate) { CreateError("17A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rrcdatestr); } //rror_rrcdate == strip(error_rrcdate)|| "17A_3,";

            //RRCCOMPLETE;
            if (rrcdate_valid && SetUniform(rrccompletestr) == ".") { CreateError("17B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rrccompletestr); } //rror_rrccomplete == "17B_1,";

            if (rrccomplete < rrcdate &&
                    rrcdate_valid &&
                    rcccomplete_valid &&
                    rrccompletestr != "." &&
                    rrcdatestr != "."
                ) { CreateError("17B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rrccompletestr); } //rror_rrccomplete == strip(error_rrccomplete)|| "17B_2,";

            if (rrccomplete > currdate) { CreateError("17B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rrccompletestr); } //rror_rrccomplete == strip(error_rrccomplete)|| "17B_3,";
            if (ValidateIn(rrccompletestr, new string[] { "88888888", "99999999" })) { CreateError("17B_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rrccompletestr); } //rror_rrccomplete == strip(error_rrccomplete)|| "17B_4,";
            if (bpdatestr != "." && rrccompletestr != "." && rrccomplete > bpdate.AddDays(60) && rrcdate_valid && rcccomplete_valid) { CreateError("17B_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rrccompletestr); } //rror_rrccomplete == strip(error_rrccomplete)|| "17B_5,";

            //RRCNUT;
            if (rrcdate_valid && rrcnut == ".") { CreateError("17C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rrcnut); } //rror_rrcnut == "17C_1,";
            else if (rrcdate_valid && !ValidateIn(rrcnut, new string[] { "1", "2", "7" })) { CreateError("17C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rrcnut); } //rror_rrcnut == strip(error_rrcnut)|| "17C_2,";
            if (rrcnut_num == 7) { CreateError("17C_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rrcnut); } //rror_rrcnut == strip(error_rrcnut)|| "17C_3,";

            //RRCPA;
            if (rrcdate_valid && rrcpa == ".") { CreateError("17D_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rrcpa); } //rror_rrcpa == "17D_1,";
            else if (rrcdate_valid && !ValidateIn(rrcpa, new string[] { "1", "2", "7" })) { CreateError("17D_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rrcpa); } //rror_rrcpa == strip(error_rrcpa)|| "17D_2,";
            if (rrcpa_num == 7) { CreateError("17D_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rrcpa); } //rror_rrcpa == strip(error_rrcpa)|| "17D_3,";

            //RRCSMOKE;
            if (rrcdate_valid && smoker == "1" && rrcsmoke == ".") { CreateError("17E_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rrcsmoke); } //rror_rrcsmoke == "17E_1,";
            else if (rrcdate_valid && smoker == "1" && !ValidateIn(rrcsmoke, new string[] { "1", "2", "7" })) { CreateError("17E_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rrcsmoke); } //rror_rrcsmoke == strip(error_rrcsmoke)|| "17E_2,";
            if (rrcsmoke_num == 7) { CreateError("17E_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rrcsmoke); } //rror_rrcsmoke == strip(error_rrcsmoke)|| "17E_3,";

            //RRCMADHERE;
            if (rrcdate_valid && hbpmeds == "1" && rrcmedadhere == ".") { CreateError("17F_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rrcmedadhere); } //rror_rrcmedadhere == "17F_1,";
            else if (rrcdate_valid && hbpmeds == "1" && !ValidateIn(rrcmedadhere, new string[] { "1", "2", "7" })) { CreateError("17F_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rrcmedadhere); } //rror_rrcmedadhere == strip(error_rrcmedadhere)|| "17F_2,";
            if (rrcmedadhere_num == 7) { CreateError("17F_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rrcmedadhere); } //rror_rrcmedadhere == strip(error_rrcmedadhere)|| "17F_3,";

            //RTCDATE;
            if (rrcdate_valid && rtcdatestr == ".") { CreateError("18A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rtcdatestr); } //rror_rtcdate == "18A_1,";
            if (rtcdate > currdate) { CreateError("18A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rtcdatestr); } //rror_rtcdate == strip(error_rtcdate)|| "18A_2,";

            //RTC;
            if (rtcdate_valid && ValidateIn(rtc, new string[] { ".", "9" })) { CreateError("18B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rtc); } //rror_rtc == "18B_1,";
            else if (rtcdate_valid && !ValidateIn(rtc, new string[] { "1", "2", "3", "4", "5", "8", "9" })) { CreateError("18B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rtc); } //rror_rtc == strip(error_rtc)|| "18B_2,";
            if (rtc_num == 8) { CreateError("18B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, rtc); } //rror_rtc == strip(error_rtc)|| "18B_3,";

            //REFDATE;
            if (refdate > currdate) { CreateError("19A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, refdatestr); } //rror_refdate == "19A_1,";
            if (refdate2 > currdate) { CreateError("19A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, refdatestr); } //rror_refdate2 == "19A_1,";

            //LSPHREC;
            if (refdate_valid && lsphcrec == ".") { CreateError("20A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphcrec); } //rror_lsphcrec == "20A_1,";
            if (lsphcrec == ".") { lsphcrec = "0"; }
            if (lsphcrec_num != intervention_count) { CreateError("20A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphcrec); } //rror_lsphcrec == strip(error_lsphcrec)|| "20A_2,";

            /*TOBRESDATE*/
            if (rrcsmoke == "1" && tobresdatestr == ".") { CreateError("21A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, tobresdatestr); }  // then error_lsphccomp&i. = "21A_1,";
            if (tobresdate2 != DateTime.MinValue && tobresdate2 > DateTime.Now) { CreateError("21A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, tobresdatestr); }  // then error_lsphccomp&i. = "21A_2,";

            /*TOBRESTYPE*/
            if (tobresdate != DateTime.MinValue && ValidateIn(tobrestype, new string[] { ".", "9" })) { CreateError("21B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, tobrestype); }
            else if (tobresdate != DateTime.MinValue && !ValidateIn(tobrestype, new string[] { "1", "2", "3", "4", "9" })) { CreateError("21B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, tobrestype); }
            if (tobresdate2 != DateTime.MinValue && ValidateIn(tobrestype2, new string[] { ".", "9" })) { CreateError("21B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, tobrestype2); }
            else if (tobresdate2 != DateTime.MinValue && !ValidateIn(tobrestype2, new string[] { "1", "2", "3", "4", "9" })) { CreateError("21B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, tobrestype2); }
            if (tobresdate3 != DateTime.MinValue && ValidateIn(tobrestype3, new string[] { ".", "9" })) { CreateError("21B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, tobrestype3); }
            else if (tobresdate3 != DateTime.MinValue && !ValidateIn(tobrestype3, new string[] { "1", "2", "3", "4", "9" })) { CreateError("21B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, tobrestype3); }

            /*TOBRESCOMP*/
            if (tobresdate != DateTime.MinValue && ValidateIn(trescomp, new string[] { ".", "9"})) { CreateError("21C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, trescomp); }
            else if (tobresdate != DateTime.MinValue && !ValidateIn(trescomp, new string[] { "1", "2", "3", "4", "9" })) { CreateError("21C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, trescomp); }
            if (tobresdate2 != DateTime.MinValue && ValidateIn(trescomp2, new string[] { ".", "9" })) { CreateError("21C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, trescomp2); }
            else if (tobresdate2 != DateTime.MinValue && !ValidateIn(trescomp2, new string[] { "1", "2", "3", "4", "9" })) { CreateError("21C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, trescomp2); }
            if (tobresdate3 != DateTime.MinValue && ValidateIn(trescomp3, new string[] { ".", "9" })) { CreateError("21C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, trescomp3); }
            else if (tobresdate3 != DateTime.MinValue && !ValidateIn(trescomp3, new string[] { "1", "2", "3", "4", "9" })) { CreateError("21C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, trescomp3); }


            for (int i = 0; i < 16; i++)
            {
                //INTERVENTION
                if (intervention[i] < currdate & intervention[i] >= submissiondate) { intervention_valid[i] = true; } else { intervention_valid[i] = false; }
                if (i == 0)
                {
                    if (refdate_valid && SetUniform(interventionstr[i]) == ".") { CreateError("20B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, intervention[i].ToString()); }// error_intervention&i. = "20B_1,";
                    if (intervention_valid[i] && refdatestr == "." & refdate2str == ".") { CreateError("20B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, intervention[i].ToString()); } //then error_intervention&i. = strip(error_intervention&i.)|| "20B_3,";
                }
                /* we won't be considering this an error if not first intervention because only one intervention is required for a refdate.*/
                if (intervention_valid[i] && intervention[i] > currdate) { CreateError("20B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, intervention[i].ToString()); } //then error_intervention&i.= strip(error_intervention&i.)|| "20B_2,";

                /*LSPHCID*/
                if (intervention_valid[i] && ValidateIn(SetUniform(lsphcid[i]), new string[] { "." })) { CreateError("20C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphcid[i].ToString()); } //then error_lsphcid&i.= "20C_1,";

                /*LSPHCTIME*/
                if (intervention_valid[i] && SetUniform(slsphctime[i]) == ".") { CreateError("20D_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphctime[i].ToString()); }  //error_lsphctime&i.= "20D_1,";
                if (intervention_valid[i] && lsphctime[i] > 120) { CreateError("20D_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphctime[i].ToString()); } //"20D_2,";

                /*CONTACTTYPE*/
                if (intervention_valid[i] && ValidateIn(contacttype[i], new string[] { ".", "9" })) { CreateError("20E_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, contacttype[i].ToString()); } //then error_contacttype&i. = "20E_1,";
                else if (intervention_valid[i] && !ValidateIn(contacttype[i], new string[] { "1", "2", "3", "4", "5", "6", "7", "0", "9" })) { CreateError("20E_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, contacttype[i].ToString()); } //then error_contacttype&i. = "20E_2,";        

                /*SETTING*/
                if (intervention_valid[i] && ValidateIn(setting[i], new string[] { ".", "9" })) { CreateError("20F_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, setting[i].ToString()); } // then error_setting&i. = "20F_1,";
                else if (intervention_valid[i] && !ValidateIn(setting[i], new string[] { "1", "2", "3", "9" })) { CreateError("20F_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, setting[i].ToString()); }  //then error_setting&i. = "20F_2,";

                /*LSPHCCOMP*/
                if (intervention_valid[i] && ValidateIn(lsphccomp[i], new string[] { ".", "9" })) { CreateError("20G_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, lsphccomp[i].ToString()); }  // then error_lsphccomp&i. = "20G_1,";
                else if (intervention_valid[i] && !ValidateIn(lsphccomp[i], new string[] { "1", "2", "3", "9" })) { CreateError("20G_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false,lsphccomp[i].ToString()); }  //then error_lsphccomp&i. = "20G_2,";

            }

            
            if (!ValidateIn(itype_f, new string[] { "", ".", "9" }))
            {
                /*SRHC_F*/
                if (srhc_f == "9") { CreateError("4A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, srhc_f); }
                else if (!ValidateIn(srhc_f, new string[] { "1", "2", "7", "8", "9", ".", "" })) { CreateError("4A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, srhc_f); }
                else if (ValidateIn(srhc_f, new string[] { "7", "8" })) { CreateError("4A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, srhc_f); }

                /*SRHB_F*/
                if (srhb_f == "9") { CreateError("4B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, srhb_f); }
                else if (!ValidateIn(srhb_f, new string[] { "1", "2", "7", "8", "9", ".", "" })) { CreateError("4B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, srhb_f); }
                if (ValidateIn(srhb_f, new string[] { "7", "8" })) { CreateError("4B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, srhb_f); }

                /*SRD_F*/
                if (srd_f == "9") { CreateError("4C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, srd_f); }
                else if (!ValidateIn(srd_f, new string[] { "1", "2", "7", "8", "9", "", "." })) { CreateError("4C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, srd_f); }
                else if (ValidateIn(srd_f, new string[] { "7", "8" })) { CreateError("4C_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, srd_f); }

                /*SRHA_F*/
                if (srha_f == "9") { CreateError("4D_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, srha_f); }
                else if (!ValidateIn(srha_f, new string[] { "1", "2", "7", "8", "9", "", "." })) { CreateError("4D_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, srha_f); }
                else if (ValidateIn(srha_f, new string[] { "7", "8" })) { CreateError("4D_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, srha_f); }

                /*HCMEDS_F*/
                if (ValidateIn(hcmeds_f, new string[] { "", "." }) && !IsMissingDate(bpdate_f)) { CreateError("5A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hcmeds_f); }
                else if (hcmeds_f == "9") { CreateError("5A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hcmeds_f); }
                else if (!ValidateIn(hcmeds_f, new string[] { "1", "2", "3", "5", "7", "8", "", "." })) { CreateError("5A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hcmeds_f); }
                if (ValidateIn(hcmeds_f, new string[] { "7", "8" })) { CreateError("5A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hcmeds_f); }
                if (hcmeds_f != "5" && srhc_f != "1" && totchol_f_num < 240 && totchol_f != ".") { CreateError("5A_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hcmeds_f); }
                //07/29/2017 - removed by CDC request - relayed by Katie
                //if ((hcmeds_f == "5" && (srhc_f == "1" || totchol_f_num >= 240)) && !ValidateInInt(totchol_f_num, new int[] { 777, 888, 999 })) { CreateError("5A_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hcmeds_f); }

                //*HBPMEDS_F;
                if (ValidateIn(hbpmeds_f, new string[] { "", "." }) && bpdate_fstr != ".") { CreateError("5B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hbpmeds_f); }
                else if (ValidateIn(hbpmeds_f, new string[] { "9" })) { CreateError("5B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hbpmeds_f); }
                else if (!ValidateIn(hbpmeds_f, new string[] { "1", "2", "3", "5", "7", "8", "9", "", "." })) { CreateError("5B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hbpmeds_f); }

                if (ValidateIn(hbpmeds_f, new string[] { "7", "8" })) { CreateError("5B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hbpmeds_f); }
                if (hbpmeds_f != "5" && srhb_f != "1" &&
                    mean_sbp_f < 140 && mean_sbp_f != 0 &&
                    mean_dbp_f < 90 && mean_dbp_f != 0
                    ) { CreateError("5B_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hbpmeds_f); }
                //07/29/2017 - removed by CDC request - relayed by Katie
                //if (hbpmeds_f == "5" &&
                //    (
                //        srhb_f == "1" ||
                //        mean_sbp_f >= 140 ||
                //        mean_dbp_f >= 90
                //    )
                //    ) { CreateError("5B_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hbpmeds_f); }

                //*DMEDS_F;
                if (ValidateIn(dmeds_f, new string[] { "", "." }) && bpdate_fstr != ".") { CreateError("5C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dmeds_f); } //rror_dmeds_f == "5C_1,";
                else if (dmeds_f == "9") { CreateError("5C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dmeds_f); } //rror_dmeds_f == "5C_1,";
                else if (!ValidateIn(dmeds_f, new string[] { "1", "2", "3", "5", "7", "8", "9", "", "." })) { CreateError("5C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dmeds_f); } //rror_dmeds_f == strip(error_dmeds_f)|| "5C_2,";

                if (ValidateIn(dmeds_f, new string[] { "7", "8" })) { CreateError("5C_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dmeds_f); } //rror_dmeds_f == strip(error_dmeds_f)|| "5C_3,";
                if (dmeds_f != "5" && srd_f != "1" &&
                    glucose_f_num < 126 && glucose_f != "." &&
                    a1c_f_num < 6.5 && a1c_f != "."
                    ) { CreateError("5C_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dmeds_f); } //rror_dmeds_f == strip(error_dmeds_f)|| "5C_4,";
                //07/29/2017 - removed by CDC request - relayed by Katie
                //if (dmeds_f == "5" &&
                    //(
                    //    srd_f == "1" ||
                    //    (glucose_f_num >= 126 && !ValidateInInt(glucose_f_num, new int[] { 777, 888, 999 })) ||
                    //    (a1c_f_num >= 6.5 && !ValidateIn(a1c, new string[] { "7777", "8888", "9999" }))
                    //)
                    //) { CreateError("5C_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dmeds_f); } //rror_dmeds_f == strip(error_dmeds_f)|| "5C_5,";

                //*HCADHERE_F; 
                if (ValidateIn(hcadhere_f, new string[] { "", "." }) && bpdate_fstr != ".") { CreateError("5D_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hcadhere_f); } //rror_hcadhere_f == "5D_1,";
                else if (hcadhere_f == "99") { CreateError("5D_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hcadhere_f); } //rror_hcadhere_f == "5D_1,";
                else if (hcadhere_f_num > 7 && !ValidateIn(hcadhere_f, new string[] { "55", "77", "88" })) { CreateError("5D_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hcadhere_f); } /*Changed here for MDE Version 902*/
                if (ValidateIn(hcadhere_f, new string[] { "77", "88" })) { CreateError("5D_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hcadhere_f); } //rror_hcadhere_f == strip(error_hcadhere_f)|| "5D_3,";
                if (hcadhere_f != "55" && hcmeds_f != "1") { CreateError("5D_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hcadhere_f); } //rror_hcadhere_f == strip(error_hcadhere_f)|| "5D_4,";
                if (hcadhere_f == "55" && hcmeds_f == "1") { CreateError("5D_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hcadhere_f); } //rror_hcadhere_f == strip(error_hcadhere_f)|| "5D_5,";

                //*HBPADHERE_F;
                if (ValidateIn(hbpadhere_f, new string[] { "", "." }) && bpdate_fstr != ".") { CreateError("5E_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hbpadhere_f); } //rror_hbpadhere_f == "5E_1,";
                else if (hbpadhere_f == "99") { CreateError("5E_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hbpadhere_f); } //rror_hbpadhere_f == "5E_1,";
                else if (hbpadhere_f_num > 7 && !ValidateIn(hbpadhere_f, new string[] { "55", "77", "88" })) { CreateError("5E_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, hbpadhere_f); } /*Changed here for MDE Version 902*/
                if (ValidateIn(hbpadhere_f, new string[] { "77", "88" })) { CreateError("5E_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hbpadhere_f); } //rror_hbpadhere_f == strip(error_hbpadhere_f)|| "5E_3,";
                if (hbpadhere_f != "55" && hbpmeds_f != "1") { CreateError("5E_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hbpadhere_f); } //rror_hbpadhere_f == strip(error_hbpadhere_f)|| "5E_4,";
                if (hbpadhere_f == "55" && hbpmeds_f == "1") { CreateError("5E_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hbpadhere_f); } //rror_hbpadhere_f == strip(error_hbpadhere_f)|| "5E_5,";

                //*DADHERE_F;
                if (ValidateIn(dadhere_f, new string[] { "", "." }) && bpdate_fstr != ".") { CreateError("5F_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dadhere_f); } //rror_dadhere_f == "5F_1,";
                else if (dadhere_f == "99") { CreateError("5F_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dadhere_f); } //rror_dadhere_f == "5F_1,";
                else if (dadhere_f_num > 7 && !ValidateIn(dadhere_f, new string[] { "55", "77", "88" })) { CreateError("5F_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, false, dadhere_f); } /*Changed here for MDE Version 902*/
                if (ValidateIn(dadhere_f, new string[] { "77", "88" })) { CreateError("5F_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dadhere_f); } //rror_dadhere_f == strip(error_dadhere_f)|| "5F_3,";
                if (dadhere_f != "55" && dmeds_f != "1") { CreateError("5F_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dadhere_f); } //rror_dadhere_f == strip(error_dadhere_f)|| "5F_4,";
                if (dadhere_f == "55" && dmeds_f == "1") { CreateError("5F_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dadhere_f); } //rror_dadhere_f == strip(error_dadhere_f)|| "5F_5,";

                //BPHOME_F;
                if (bphome_f == "009") { CreateError("6A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bphome_f); } //rror_bphome_f == "6A_1,";
                else if (!ValidateIn(bphome_f, new string[] { "001", "002", "003", "004", "005", "007", "008", "009", "023", "034", "234", ".", "" })) { CreateError("6A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bphome_f); } //rror_bphome_f == strip(error_bphome_f)|| "6A_2,";

                if (ValidateIn(bphome_f, new string[] { "007", "008" })) { CreateError("6A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bphome_f); } //rror_bphome_f == strip(error_bphome_f)|| "6A_3,";
                //if (bphome_f != "005" && ValidateIn(bphome_f, new string[] { "001", "002", "003", "004", "007", "008", "009", "023", "034", "234", ".", "" }) && srhb_f != "1") { CreateError("6A_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true); } /*Changed here for MDE Version 902*/
                if (bphome_f == "005" && (srhb_f == "1" || hbpmeds_f == "1")) { CreateError("6A_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bphome_f); } //rror_bphome_f == strip(error_bphome_f)|| "6A_5,";

                //BPFREQ_F;
                if (ValidateIn(bpfreq_f, new string[] { "", "." }) && bpdate_fstr != ".") { CreateError("6B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpfreq_f); } //rror_bpfreq_f == "6B_1,";
                else if (bpfreq_f == "9") { CreateError("6B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpfreq_f); } //rror_bpfreq_f == "6B_1,";
                else if (!ValidateIn(bpfreq_f, new string[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", ".", "" })) { CreateError("6B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpfreq_f); } //rror_bpfreq_f == strip(error_bpfreq_f)|| "6B_2,";

                if (ValidateIn(bpfreq_f, new string[] { "7", "8" })) { CreateError("6B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpfreq_f); } //rror_bpfreq_f == strip(error_bpfreq_f)|| "6B_3,";
                if (bpfreq_f != "6" && bphome_f != "001" && ValidateIn(bphome_f, new string[] { "002", "003", "004", "005", "007", "008", "009", "023", "034", "234", "", "." })) { CreateError("6B_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpfreq_f); } //rror_bpfreq_f == strip(error_bpfreq_f)|| "6B_4,";
                if (bpfreq_f == "6" && bphome_f == "001") { CreateError("6B_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpfreq_f); } //rror_bpfreq_f == strip(error_bpfreq_f)|| "6B_5,";

                //BPSEND_F;
                if (ValidateIn(bpsend_f, new string[] { "", "." }) && bpdate_fstr != ".") { CreateError("6C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpsend_f); } //rror_bpsend_f == "6C_1,";
                else if (bpsend_f == "9") { CreateError("6C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpsend_f); } //rror_bpsend_f == "6C_1,";
                if (!ValidateIn(bpsend_f, new string[] { "1", "2", "5", "7", "8", "9", ".", "" })) { CreateError("6C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpsend_f); } //rror_bpsend_f == strip(error_bpsend_f)|| "6C_2,";

                if (ValidateIn(bpsend_f, new string[] { "7", "8" })) { CreateError("6C_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpsend_f); } //rror_bpsend_f == strip(error_bpsend_f)|| "6C_3,";
                if (bpsend_f != "5" && bphome_f != "001" && ValidateIn(bphome_f, new string[] { "002", "003", "004", "005", "007", "008", "009", "023", "034", "234", "", "." })) { CreateError("6C_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpsend_f); } //rror_bpsend_f == strip(error_bpsend_f)|| "6C_4,";
                if (bpsend_f == "5" && bphome_f == "001") { CreateError("6C_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpsend_f); } //rror_bpsend_f == strip(error_bpsend_f)|| "6C_5,";

                //FRUIT_F;
                if (ValidateIn(fruit_f, new string[] { "", "." }) && bpdate_fstr != ".") { CreateError("7A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, fruit_f); } //rror_fruit_f == "7A_1,";
                else if (fruit_f == "99") { CreateError("7A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, fruit_f); } //rror_fruit_f == "7A_1,";
                if (fruit_f == "88") { CreateError("7A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, fruit_f); } //rror_fruit_f == strip(error_fruit_f)|| "7A_2,";
                if ((fruit_f_num < 0 || fruit_f_num > 50) && !ValidateIn(fruit_f, new string[] { "88", "99" })) { CreateError("7A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, fruit_f); } //rror_fruit_f == strip(error_fruit_f)|| "7A_3,";

                //VEGETABLES_F;
                if (ValidateIn(vegetables_f, new string[] { "", "." }) && bpdate_fstr != ".") { CreateError("7B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, vegetables_f); } //rror_vegetables_f == "7B_1,";
                else if (vegetables_f == "99") { CreateError("7B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, vegetables_f); } //rror_vegetables_f == "7B_1,";
                if (vegetables_f == "88") { CreateError("7B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, vegetables_f); } //rror_vegetables_f == strip(error_vegetables_f)|| "7B_2,";
                if ((vegetables_f_num < 0 || vegetables_f_num > 15) && !ValidateIn(vegetables_f, new string[] { "88", "99" })) { CreateError("7B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, vegetables_f); } //rror_vegetables_f == strip(error_vegetables_f)|| "7B_3,";

                //FISH_F;
                if (ValidateIn(fish_f, new string[] { "", "." }) && bpdate_fstr != ".") { CreateError("7C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, fish_f); } //rror_fish_f == "7C_1,";
                else if (fish_f == "9") { CreateError("7C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, fish_f); } //rror_fish_f == "7C_1,";
                else if (!ValidateIn(fish_f, new string[] { "1", "2", "8", "9", "", "." })) { CreateError("7C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, fish_f); } //rror_fish_f == strip(error_fish_f)|| "7C_2,";

                if (fish_f == "8") { CreateError("7C_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, fish_f); } //rror_fish_f == strip(error_fish_f)|| "7C_3,";

                //GRAIN_F;
                if (ValidateIn(grains_f, new string[] { "", "." }) && bpdate_fstr != ".") { CreateError("7D_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, grains_f); } //rror_grains_f == "7D_1,";
                else if (grains_f == "9") { CreateError("7D_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, grains_f); } //rror_grains_f == "7D_1,";
                if (!ValidateIn(grains_f, new string[] { "1", "2", "8", "9", "", "." })) { CreateError("7D_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, grains_f); } //rror_grains_f == strip(error_grains_f)|| "7D_2,";
                if (grains_f == "8") { CreateError("7D_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, grains_f); } //rror_grains_f == strip(error_grains_f)|| "7D_3,";

                //SUGAR_F;
                if (ValidateIn(sugar_f, new string[] { "", "." }) && bpdate_fstr != ".") { CreateError("7E_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, sugar_f); } //rror_sugar_f == "7E_1,";
                else if (sugar_f == "9") { CreateError("7E_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, sugar_f); } //rror_sugar_f == "7E_1,";
                else if (!ValidateIn(sugar_f, new string[] { "1", "2", "8", "9", "", "." })) { CreateError("7E_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, sugar_f); } //rror_sugar_f == strip(error_sugar_f)|| "7E_2,";
                if (sugar_f == "8") { CreateError("7E_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, sugar_f); } //rror_sugar_f == strip(error_sugar_f)|| "7E_3,";

                //SALTWATCH_F;
                if (ValidateIn(saltwatch_f, new string[] { "", "." }) && bpdate_fstr != ".") { CreateError("7F_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, saltwatch_f); } //rror_saltwatch_f == "7F_1,";
                else if (ValidateIn(saltwatch_f, new string[] { "9" })) { CreateError("7F_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, saltwatch_f); } //rror_saltwatch_f == "7F_1,";
                else if (!ValidateIn(saltwatch_f, new string[] { "1", "2", "8", "9" })) { CreateError("7F_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, saltwatch_f); } //rror_saltwatch_f == strip(error_saltwatch_f)|| "7F_2,";
                if (saltwatch_f == "8") { CreateError("7F_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, saltwatch_f); } //rror_saltwatch_f == strip(error_saltwatch_f)|| "7F_3,";

                //PAMOD_F;
                if (ValidateIn(pamod_f, new string[] { "", "." }) && bpdate_fstr != ".") { CreateError("8A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, pamod_f); } //rror_pamod_f == "8A_1,";
                else if (pamod_f == "999") { CreateError("8A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, pamod_f); } //rror_pamod_f == "8A_1,";

                if (pamod_f == "888") { CreateError("8A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, pamod_f); } //rror_pamod_f == strip(error_pamod_f)|| "8A_2,";
                if (!ValidateIn(pamod_f, new string[] { "888", "999", "000" }) &&
                    (
                        (pamod_f_num < 10 && pamod_f != ".") |
                        pamod_f_num > 850
                    )) { CreateError("8A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, pamod_f); } /*Changed here for MDE Version 902 - Also changed from error to quality check*/

                //PAVIG_F;
                if (ValidateIn(pavig_f, new string[] { "", "." }) && bpdate_fstr != ".") { CreateError("8B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, pavig_f); } //rror_pavig_f == "8B_1,";
                else if (ValidateIn(pavig_f, new string[] { "999" })) { CreateError("8B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, pavig_f); } //rror_pavig_f == "8B_1,";

                if (pavig_f == "888") { CreateError("8B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, pavig_f); } //rror_pavig_f == strip(error_pavig_f)|| "8B_2,";
                if (!ValidateIn(pavig_f, new string[] { "888", "999", "000" }) &&
                    (
                        (pavig_f_num < 10 && pavig_f != ".") |
                        (pavig_f_num > 850)
                    )) { CreateError("8B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, pavig_f); } //rror_pavig_f == strip(error_pavig_f)|| "8B_3,"; 

                //SMOKER_F;
                if (ValidateIn(smoker_f, new string[] { "", "." }) && bpdate_fstr != ".") { CreateError("9A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, smoker_f); } //rror_smoker_f == "9A_1,";
                else if (smoker_f == "9") { CreateError("9A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, smoker_f); } //rror_smoker_f == "9A_1,";
                else if (!ValidateIn(smoker_f, new string[] { "1", "2", "3", "4", "8", "9", "", "." })) { CreateError("9A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, smoker_f); } //rror_smoker_f == strip(error_smoker_f)|| "9A_2,";

                if (smoker_f == "8") { CreateError("9A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, smoker_f); } //rror_smoker_f == strip(error_smoker_f)|| "9A_3,";

                //SECHAND_F;
                if (ValidateIn(sechand_f, new string[] { "", "." }) && bpdate_fstr != ".") { CreateError("9B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, sechand_f); } //rror_sechand_f == "9B_1,";
                else if (sechand_f == "99") { CreateError("9B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, sechand_f); } //rror_sechand_f == "9B_1,";

                if (sechand_f == "88") { CreateError("9B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, sechand_f); } //rror_sechand_f == strip(error_sechand_f)|| "9B_2,";
                if (sechand_f_num > 24 && !ValidateIn(sechand_f, new string[] { "66", "88", "99" })) { CreateError("9B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, sechand_f); } //rror_sechand_f == strip(error_sechand_f)|| "9B_3,";

                //QOLPH_F;
                if (ValidateIn(qolph_f, new string[] { "", "." }) && bpdate_fstr != ".") { CreateError("10A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, qolph_f); } //rror_qolph_f == "10A_1,";
                else if (qolph_f == "99") { CreateError("10A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, qolph_f); } //rror_qolph_f == "10A_1,";
                else if (ValidateIn(qolph_f, new string[] { "77", "88" })) { CreateError("10A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, qolph_f); } //rror_qolph_f == strip(error_qolph_f)|| "10A_2,";
                if (qolph_f_num > 30 && !ValidateIn(qolph_f, new string[] { "77", "88", "99" })) { CreateError("10A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, qolph_f); } //rror_qolph_f == strip(error_qolph_f)|| "10A_3,";

                //QOLMH_F;
                if (ValidateIn(qolmh_f, new string[] { "", "." }) && bpdate_fstr != ".") { CreateError("10B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, qolmh_f); } //rror_qolmh_f == "10B_1,";
                else if (qolmh_f == "99") { CreateError("10B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, qolmh_f); } //rror_qolmh_f == "10B_1,";

                if (ValidateIn(qolmh_f, new string[] { "77", "88" })) { CreateError("10B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, qolmh_f); } //rror_qolmh_f == strip(error_qolmh_f)|| "10B_2,";
                if (qolmh_f_num > 30 && !ValidateIn(qolmh_f, new string[] { "77", "88", "99" })) { CreateError("10B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, qolmh_f); } //rror_qolmh_f == strip(error_qolmh_f)|| "10B_3,";

                //QOLEFFECT_F;
                if (ValidateIn(qoleffect_f, new string[] { "", "." }) && bpdate_fstr != ".") { CreateError("10C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, qoleffect_f); } //rror_qoleffect_f == "10C_1,";
                else if (qoleffect_f == "99") { CreateError("10C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, qoleffect_f); } //rror_qoleffect_f == "10C_1,";
                if (ValidateIn(qoleffect_f, new string[] { "77", "88" })) { CreateError("10C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, qoleffect_f); } //rror_qoleffect_f == strip(error_qoleffect_f)|| "10C_2,";
                if (qoleffect_f_num > 30 && !ValidateIn(qoleffect_f, new string[] { "77", "88", "99" })) { CreateError("10C_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, qoleffect_f); } //rror_qoleffect_f == strip(error_qoleffect_f)|| "10C_3,";

                //WEIGHT_F;
                if (ValidateIn(weight_f, new string[] { "999" })) { CreateError("11B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, weight_f); } //rror_weight_f == "11B_1,";
                if (ValidateIn(weight_f, new string[] { "777" })) { CreateError("11B_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, weight_f); } //rror_weight_f == strip(error_weight_f)|| "11B_4,";
                if (ValidateIn(weight_f, new string[] { "888" })) { CreateError("11B_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, weight_f); } //rror_weight_f == strip(error_weight_f)|| "11B_5,";

                if ((
                    (weight_f_num < 74 && weight_f != ".") ||
                    weight_f_num > 460
                    ) &
                    !ValidateIn(weight_f, new string[] { "777", "888", "999" })
                        ) { CreateError("11B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, weight_f); } //rror_weight_f == strip(error_weight_f)|| "11B_2,";
                if ((weight_f_num >= 74 && weight_f_num <= 90) |
                    (weight_f_num >= 350 && weight_f_num <= 460)) { CreateError("11B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, weight_f); } //rror_weight_f == strip(error_weight_f)|| "11B_3,";

                //WAIST_F;
                if (((waist_f_num < 16 && waist_f != ".") | waist_f_num > 71) &
                    !ValidateIn(waist_f, new string[] { "77", "88", "99" })) { CreateError("11C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, waist_f); } //rror_waist_f == "11C_2,";

                //HIP_F;
                if (((hip_f_num < 26 && hip_f != ".") | hip_f_num > 75) &
                    !ValidateIn(hip_f, new string[] { "77", "88", "99" })) { CreateError("11D_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hip_f); } //rror_hip_f == "11D_2,";

                //BPDATE_F;
                if ((bpdatestr == "." && bpdate_fstr != ".") || bpdate_fstr == ".") { CreateError("12A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpdate_fstr); } /*Changed here for MDE Version 902*/
                if (bpdate_f < submissiondate && bpdate_fstr != "." && mdever == "901") { CreateError("12A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpdate_fstr); } //rror_bpdate_f == strip(error_bpdate_f)||"12A_2,";
                if (bpdate_f > currdate) { CreateError("12A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpdate_fstr); } //rror_bpdate_f == strip(error_bpdate_f)|| "12A_3,";

                //SPB1_F;
                if (sbp1_f == "999") { CreateError("12B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, sbp1_f); } //rror_spbi1_f == "12B_1,";
                if (sbp1_f == "888") { CreateError("12B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, sbp1_f); } //rror_spbi1_f == strip(error_spbi1_f)|| "12B_3,";

                if (((sbp1_f_num < 74 && sbp1_f != ".") || sbp1_f_num > 260) && !ValidateIn(sbp1_f, new string[] { "777", "888", "999" })) { CreateError("12B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true,sbp1_f); } //rror_spbi1_f == strip(error_spbi1_f)|| "12B_2,";
                if ((sbp1_f_num >= 230 && sbp1_f_num <= 260)) { CreateError("12B_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true,sbp1_f); } //rror_spbi1_f == strip(error_spbi1_f)|| "12B_4,";
                if (sbp1_f == "777") { CreateError("12B_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true,sbp1_f); } //rror_spbi1_f == strip(error_spbi1_f)|| "12B_5,";
                if (ValidateIn(sbp1_f, new string[] { "777", "888", "999" }) && !ValidateIn(sbp2_f, new string[] { "777", "888", "999" })) { CreateError("12B_6", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true,sbp1_f); } //rror_spbi1_f == strip(error_spbi1_f)|| "12B_6,";

                //DBP1_F;
                if (dbp1_f == "999") { CreateError("12C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true,dbp1_f); } //rror_dbp1_f == "12C_1,";
                if (dbp1_f == "888") { CreateError("12C_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true,dbp1_f); } //rror_dbp1_f == strip(error_dbp1_f)|| "12C_3,";

                if (((dbp1_f_num < 2 && dbp1_f != ".") || dbp1_f_num > 156) && !ValidateIn(dbp1_f, new string[] { "777", "888", "999" })) { CreateError("12C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dbp1_f); } //rror_dbp1_f == strip(error_dbp1_f)|| "12C_2,";
                if ((dbp1_f_num >= 2 && dbp1_f_num <= 12) || (dbp1_f_num >= 122 && dbp1_f_num <= 156)) { CreateError("12C_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dbp1_f); } //rror_dbp1_f == strip(error_dbp1_f)|| "12C_4,";
                if (dbp1_f == "777") { CreateError("12C_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dbp1_f); } //rror_dbp1_f == strip(error_dbp1_f)|| "12C_5,";
                if (ValidateIn(dbp1_f, new string[] { "777", "888", "999" }) && !ValidateIn(dbp2_f, new string[] { "777", "888", "999" })) { CreateError("12C_6", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dbp1_f); } //rror_dbp1_f == strip(error_dbp1_f)|| "12C_6,";
                if ((sbp1_f == "777" && dbp1_f != "777") || (sbp1_f != "777" && dbp1_f == "777")) { CreateError("12C_7", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dbp1_f); } //rror_dbp1_f == strip(error_dbp1_f)|| "12C_7,";

                //SPB2_F (Should be SBP2?);
                if (sbp2_f == "999") { CreateError("12D_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true,sbp2_f); } //rror_sbp2_f == "12D_1,";
                if (sbp2_f == "888") { CreateError("12D_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true,sbp2_f); } //rror_sbp2_f == strip(error_sbp2_f)|| "12D_3,";

                if (((sbp2_f_num < 74 && sbp2_f != ".") || sbp2_f_num > 260) && !ValidateIn(sbp2_f, new string[] { "777", "888", "999" })) { CreateError("12D_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, sbp2_f); } //rror_sbp2_f == strip(error_sbp2_f)|| "12D_2,";
                if ((sbp2_f_num >= 230 && sbp2_f_num <= 260)) { CreateError("12D_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, sbp2_f); } //rror_sbp2_f == strip(error_sbp2_f)|| "12D_4,";
                if (sbp2_f == "777") { CreateError("12D_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, sbp2_f); } //rror_sbp2_f == strip(error_sbp2_f)|| "12D_5,";

                //DBP2_F;
                if (dbp2_f == "999") { CreateError("12E_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dbp2_f); } //rror_dbp2_f == "12E_1,";
                if (dbp2_f == "888") { CreateError("12E_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dbp2_f); } //rror_dbp2_f == strip(error_dbp2_f)|| "12E_3,";

                if (((dbp2_f_num < 2 && dbp2_f != ".") || dbp2_f_num > 156) && !ValidateIn(dbp2_f, new string[] { "777", "888", "999" })) { CreateError("12E_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dbp2_f); } //rror_dbp2_f == strip(error_dbp2_f)|| "12E_2,";
                if ((dbp2_f_num >= 2 && dbp2_f_num <= 12) || (dbp2_f_num >= 122 && dbp2_f_num <= 156)) { CreateError("12E_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dbp2_f); } //rror_dbp2_f == strip(error_dbp2_f)|| "12E_4,";
                if (dbp2_f == "777") { CreateError("12E_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dbp2_f); } //rror_dbp2_f == strip(error_dbp2_f)|| "12E_5,";
                if ((sbp2_f == "777" && dbp2_f != "777") || (sbp2_f != "777" && dbp2_f == "777")) { CreateError("12E_6", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, dbp2_f); } //rror_dbp2_f == strip(error_dbp2_f)|| "12E_6,";

                //FAST_F;
                if (fast_f == "9") { CreateError("13A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, fast_f); } //rror_fast_f == "13A_1,";
                else if (!ValidateIn(fast_f, new string[] { "1", "2", "9", "", "." })) { CreateError("13A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, fast_f); } //rror_fast_f == strip(error_fast_f)|| "13A_2,";

                if (fast_f != "9" && totchol_f == "777" && hdl_f == "777" && ldl_f == "777" && trigly_f == "7777" && glucose_f == "777") { CreateError("13A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, fast_f); } //rror_fast_f == strip(error_fast_f)|| "13A_3,";

                if (fast_f != "9" && totchol_f == "888" && hdl_f == "888" && ldl_f == "888" && trigly_f == "8888" && glucose_f == "888") { CreateError("13A_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, fast_f); } //rror_fast_f == strip(error_fast_f)|| "13A_4,";

                if (fast_f != "9" && totchol_f == "999" && hdl_f == "999" && ldl_f == "999" && trigly_f == "9999" && glucose_f == "999") { CreateError("13A_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, fast_f); } //rror_fast_f == strip(error_fast_f)|| "13A_5,";

                //TCDATE_F;
                if (ValidateIn(tcdate_fstr, new string[] { "", "." }) && (!ValidateIn(totchol_f, new string[] { "888", "999", "", "." }) || !ValidateIn(hdl_f, new string[] { "888", "999", "", "." }) || !ValidateIn(ldl_f, new string[] { "888", "999", "", "." }) || !ValidateIn(trigly_f, new string[] { "8888", "9999", "", "." }))) { CreateError("14A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, tcdate_fstr); } //rror_tcdate_f == "14A_1,";
                if (tcdate_f > currdate) { CreateError("14A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, tcdate_fstr); } //rror_tcdate_f == strip(error_tcdate_f)|| "14A_2,";

                //TOTCHOL_F;
                if (totchol_f == "999") { CreateError("14B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, totchol_f); } //rror_totchol_f == "14B_1,";
                else if (((totchol_f_num < 44 && totchol_f != ".") || totchol_f_num > 702) && !ValidateIn(totchol_f, new string[] { "777", "888", "999" })) { CreateError("14B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, totchol_f); } //rror_totchol_f == strip(error_totchol_f)|| "14B_2,"; *Are period/missing valid?;

                if ((totchol_f_num >= 44 && totchol_f_num <= 60) || (totchol_f_num >= 400 && totchol_f_num <= 702)) { CreateError("14B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, totchol_f); } //rror_totchol_f == strip(error_totchol_f)|| "14B_3,";
                if (ValidateIn(totchol_f, new string[] { "777", "888" })) { CreateError("14B_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, totchol_f); } //rror_totchol_f == strip(error_totchol_f)|| "14B_4,";

                //HDL_F;
                if (hdl_f == "999") { CreateError("14C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hdl_f); } //rror_hdl_f == "14C_1,";
                if (((hdl_f_num < 7 && hdl_f != ".") || hdl_f_num > 196) && !ValidateIn(hdl_f, new string[] { "777", "888", "999" })) { CreateError("14C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hdl_f); } //rror_hdl_f == strip(error_hdl_f)|| "14C_2,";
                if ((hdl_f_num >= 155 && hdl_f_num <= 196)) { CreateError("14C_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hdl_f); } //rror_hdl_f == strip(error_hdl_f)|| "14C_3,";
                if (ValidateIn(hdl_f, new string[] { "777", "888" })) { CreateError("14C_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, hdl_f); } //rror_hdl_f == strip(error_hdl_f)|| "14C_4,";

                //LDL_F;
                if (((ldl_f_num < 20 && ldl_f != ".") || ldl_f_num > 380) && !ValidateIn(ldl_f, new string[] { "777", "888", "999" })) { CreateError("14D_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, ldl_f); } //rror_ldl_f == "14D_2,";
                if (ValidateIn(fast_f, new string[] { "2" }) && ldl_f != "999") { CreateError("14D_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, ldl_f); } //rror_ldl_f == strip(error_ldl_f)|| "14D_3,";
                if ((ldl_f_num >= 344 && ldl_f_num <= 380)) { CreateError("14D_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, ldl_f); } //rror_ldl_f == strip(error_ldl_f)|| "14D_4,";

                //TRIGLY_F;
                if (((trigly_f_num < 12 && trigly_f != ".") || trigly_f_num > 3000) && !ValidateIn(trigly_f, new string[] { "7777", "8888", "9999" })) { CreateError("14E_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, trigly_f); } //rror_trigly_f == "14E_2,";
                if (ValidateIn(fast_f, new string[] { "2" }) && trigly_f != "9999") { CreateError("14E_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, trigly_f); } //rror_trigly_f == strip(error_trigly_f)|| "14E_3,";
                if ((trigly_f_num >= 1000 && trigly_f_num <= 3000)) { CreateError("14E_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, trigly_f); } //rror_trigly_f == strip(error_trigly_f)|| "14E_4,";

                //BGDATE_F;
                if (bgdate_fstr == "." && (!ValidateIn(glucose_f, new string[] { "888", "999", "", "." }) || !ValidateIn(a1c_f, new string[] { "8888", "9999", "", "." }))) { CreateError("15A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bgdate_fstr); } //rror_bgdate_f == "15A_1,";
                if (bgdate_f > currdate) { CreateError("15A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true,bgdate_fstr); } //rror_bgdate_f == strip(error_bgdate_f)|| "15A_2,";

                //GLUCOSE_F;
                if (glucose_f == "999" && a1c == "9999") { CreateError("15B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, glucose_f); } //rror_glucose_f == "15B_2,";
                if (((glucose_f_num < 37 && glucose_f != ".") | glucose_f_num > 571) && !ValidateIn(glucose_f, new string[] { "777", "888", "999" })) { CreateError("15B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, glucose_f); } //rror_glucose_f == strip(error_glucose_f)|| "15B_3,";
                if ((glucose_f_num >= 37 && glucose_f_num <= 50) || (glucose_f_num >= 275 && glucose_f_num <= 571)) { CreateError("15B_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, glucose_f); } //rror_glucose_f == strip(error_glucose_f)|| "15B_4,";
                if (glucose_f == "777") { CreateError("15B_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, glucose_f); } //rror_glucose_f == strip(error_glucose_f)|| "15B_5,";
                if (glucose_f == "888" && a1c_f == "8888") { CreateError("15B_6", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, glucose_f); } //rror_glucose_f == strip(error_glucose_f)|| "15B_6,";
                if (fast_f == "2" && glucose_f != "999" && bpdate > toDateTime("01012015")) { CreateError("15B_7", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, glucose_f); }/*Changed here for MDE Version 902*/

                //A1C_F;
                if ((a1c_f_num < 2.8 && a1c_f != ".") || (a1c_f_num > 16.2 && !ValidateIn(a1c_f, new string[] { "7777", "8888", "9999" }))) { CreateError("15C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true,a1c_f); } //rror_a1c_f == "15C_2,";
                if ((a1c_f_num >= 2.8 && a1c_f_num <= 4) || (a1c_f_num >= 13 && a1c_f_num <= 16.2)) { CreateError("15C_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true,a1c_f); } //rror_a1c_f == strip(error_a1c_f)|| "15C_3,";
                if (a1c_f == "7777") { CreateError("15C_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true,a1c_f); } //rror_a1c_f == strip(error_a1c_f)|| "15C_4,";
                //there is no need for the "#" error for a1c_f;

                //BPALERT_F;
                if (bpalert_f == "9") { CreateError("16A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpalert_f); } //rror_bpalert_f == "16A_1,";
                else if (!ValidateIn(bpalert_f, new string[] { "1", "2", "3", "8", "9", "", "." })) { CreateError("16A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpalert_f); } //rror_bpalert_f == strip(error_bpalert_f)|| "16A_2,";


                if ((
                        (mean_sbp_f > 180) ||
                        (mean_dbp_f > 110)
                    ) &&
                    !ValidateIn(bpalert_f, new string[] { "1", "2", "8", "9" })
                    ) { CreateError("16A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpalert_f); } //rror_bpalert_f == strip(error_bpalert_f)|| "16A_3,";
                if (((mean_sbp_f <= 180 && mean_sbp_f != 0) &&
                    (mean_dbp_f <= 110 && mean_dbp_f != 0)) &&
                    !ValidateIn(bpalert, new string[] { "3", "", "." })
                    ) { CreateError("16A_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpalert_f); } //rror_bpalert_f == strip(error_bpalert_f)|| "16A_4,";
                if (ValidateIn(sbp1_f, new string[] { "777", "888", "999" }) && ValidateIn(dbp1_f, new string[] { "777", "888", "999" }) && bpalert_f != "3") { CreateError("16A_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpalert_f); } //rror_bpalert_f == strip(error_bpalert_f)|| "16A_5,";
                if ((
                        (mean_sbp_f > 180) ||
                        (mean_dbp_f > 110)
                    ) &
                    ValidateIn(bpalert_f, new string[] { "8", "9", "", "." }) &&
                    bpdidate_f_valid
                    ) { CreateError("16A_6", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpalert_f); } //rror_bpalert_f == strip(error_bpalert_f)|| "16A_6,";

                //BPDIDATE_F;
                if ((
                        (mean_sbp_f > 180) ||
                        (mean_dbp_f > 110)
                    ) &&
                    bpalert_f == "1" &&
                    bpdidate_f_valid &&
                    bpdidate_f < bpdate_f
                    ) { CreateError("16B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpdidatestr); } //rror_bpdidate_f == "16B_1,";
                if ((
                        (mean_sbp_f <= 180 && mean_sbp_f > 0) &
                        (mean_dbp_f <= 110 && mean_dbp_f > 0)
                    ) &&
                    bpdidate_f_valid && bpdidate_fstr != "."
                    ) { CreateError("16B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpdidatestr); } //rror_bpdidate_f == strip(error_bpdidate_f)|| "16B_2,";
                if (ValidateIn(sbp1_f, new string[] { "777", "888", "999" }) &&
                    ValidateIn(dbp1_f, new string[] { "777", "888", "999" }) &&
                    bpdidate_f_valid
                    ) { CreateError("16B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpdidatestr); } //rror_bpdidate_f == strip(error_bpdidate_f)|| "16B_3,";
                if ((
                        (mean_sbp_f > 180) ||
                        (mean_dbp_f > 110)
                    ) &&
                    bpalert_f == "1" &&
                    (
                        (bpdidate_f_valid && bpdate_f_valid && bpdidate_f - bpdate_f > new TimeSpan(7, 0, 0, 0)) ||
                        bpdidate_fstr == "."
                    )
                    ) { CreateError("16B_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpdidatestr); } //rror_bpdidate_f == strip(error_bpdidate_f)|| "16B_4,";
                if ((
                        (mean_sbp_f > 180) ||
                        (mean_dbp_f > 110)
                    ) &&
                    bpalert_f == "2" &&
                    (
                        (bpdidate_f_valid && bpdate_f_valid && bpdidate_f - bpdate_f > new TimeSpan(7, 0, 0, 0)) ||
                        bpdidate_fstr == "."
                    )
                    ) { CreateError("16B_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpdidatestr); } //rror_bpdidate_f == strip(error_bpdidate_f)|| "16B_5,";
                if ((
                        (mean_sbp_f > 180) ||
                        (mean_dbp_f > 110)
                    ) &&
                    bpalert_f == "8" && bpdidate_fstr == ".") { CreateError("16B_6", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpdidatestr); } //rror_bpdidate_f == strip(error_bpdidate_f)|| "16B_6,";
                if ((
                        (mean_sbp_f > 180) ||
                        (mean_dbp_f > 110)
                    ) &&
                    bpalert_f == "9" && bpdidate_fstr == ".") { CreateError("16B_7", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bpdidatestr); } //rror_bpdidate_f == strip(error_bpdidate_f)|| "16B_7,";

                /*Not Diabetic for later use;*/
                if ((srd_f != "1" && a1c_f_num < 6.5 && a1c_f != ".") || (ValidateIn(a1c_f, new string[] { "7777", "8888", "9999", "", "." }))) { not_diabetic_f = true; } /*Changed here for MDE Version 902*/

                //BGALERT_F;
                if (bgalert_f == "9") { CreateError("16C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bgalert_f); } //rror_bgalert_f == "16C_1,";
                else if (!ValidateIn(bgalert_f, new string[] { "1", "2", "3", "8", "9", "", "." })) { CreateError("16C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bgalert_f); } //rror_bgalert_f == strip(error_bgalert_f)|| "16C_2,";

                if (((glucose_f_num <= 50 && glucose_f != ".") || glucose_f_num >= 250 && !ValidateIn(glucose_f, new string[] { "777", "888", "999" })) && not_diabetic_f && ValidateIn(bgalert_f, new string[] { "1", "2", "8", "9", "", "." })) { CreateError("16C_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bgalert_f); } /*Changed here for MDE Version 902*/
                if ((glucose_f_num > 50 && glucose_f_num < 250) && bgalert_f != "3") { CreateError("16C_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bgalert_f); } //rror_bgalert_f == strip(error_bgalert_f)|| "16C_4,";
                if (ValidateIn(glucose_f, new string[] { "777", "888", "999" }) && bgalert_f != "3") { CreateError("16C_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bgalert_f); } //rror_bgalert_f == strip(error_bgalert_f)|| "16C_5,";
                if ((
                    (glucose_f_num <= 50 && glucose_f != ".") ||
                    (glucose_f_num >= 250 && !ValidateIn(glucose_f, new string[] { "777", "888", "999" }))
                    ) &&
                    not_diabetic_f &&
                    ValidateIn(bgalert_f, new string[] { "8", "9" }) &&
                    bgdidate_f_valid
                    ) { CreateError("16C_6", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bgalert_f); } /*Changed here for MDE Version 902*/

                //BGDIDATE_F;
                if ((
                        (glucose_f_num <= 50 && glucose_f != ".") ||
                        (glucose_f_num >= 250 && !ValidateIn(glucose_f, new string[] { "777", "888", "999" }))
                    ) &&
                    not_diabetic_f &&
                    bgdidate_f_valid &&
                    bgdate_f_valid &&
                    bgdidate_f < bgdate_f &&
                    bgalert_f == "1"
                    ) { CreateError("16D_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bgdidate_fstr); } /*Changed here for MDE Version 902*/
                if ((glucose_f_num > 50 && glucose_f_num < 250) && bgdidate_f_valid && bgdidate_fstr != ".") { CreateError("16D_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bgdidate_fstr); } //rror_bgdidate_f == strip(error_bgdidate_f)|| "16D_2,";
                if (ValidateIn(glucose_f, new string[] { "777", "888", "999" }) && bgdidate_f_valid && bgdidate_fstr != ".") { CreateError("16D_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bgdidate_fstr); } //rror_bgdidate_f == strip(error_bgdidate_f)|| "16D_3,";
                if ((
                        (glucose_f_num <= 50 && glucose_f != ".") ||
                        (glucose_f_num >= 250 && !ValidateIn(glucose_f, new string[] { "777", "888", "999" }))
                    ) &&
                    not_diabetic_f &&
                   bgalert_f == "1" &&
                   ((bgdidate_fstr != "." && bgdate_fstr != "." && bgdidate_f_valid && bgdate_f_valid && bgdidate_f - bgdate_f > new TimeSpan(7, 0, 0, 0) || bgdidate_fstr == ".")
                   )) { CreateError("16D_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bgdidate_fstr); } /*Changed here for MDE Version 902*/
                if ((
                        (glucose_f_num <= 50 && SetUniform(glucose_f) != ".") ||
                        (glucose_f_num >= 250 && !ValidateIn(glucose_f, new string[] { "777", "888", "999" }))
                    ) &&
                    not_diabetic_f &&
                   bgalert_f == "2" &&
                   ((SetUniform(bgdidate_fstr) != "." && SetUniform(bgdate_fstr) != "." && bgdidate_f_valid && bgdate_f_valid && bgdidate_f - bgdate_f > new TimeSpan(7, 0, 0, 0) || bgdidate_fstr == ".")
                   )) { CreateError("16D_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bgdidate_fstr); } /*Changed here for MDE Version 902*/
                if ((
                        (glucose_f_num <= 50 && SetUniform(glucose_f) != ".") ||
                        (glucose_f_num >= 250 && !ValidateIn(glucose_f, new string[] { "777", "888", "999" }))
                    ) &&
                    not_diabetic_f &&
                    bgalert_f == "8" && SetUniform(bgdidate_fstr) == ".") { CreateError("16D_6", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bgdidate_fstr); } /*Changed here for MDE Version 902*/
                if ((
                        (glucose_f_num <= 50 && SetUniform(glucose_f) != ".") ||
                        (glucose_f_num >= 250 && !ValidateIn(glucose_f, new string[] { "777", "888", "999" }))
                    ) &&
                    not_diabetic_f &&
                    bgalert_f == "9" && SetUniform(bgdidate_fstr) == ".") { CreateError("16D_7", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, bgdidate_fstr); } /*Changed here for MDE Version 902*/

                //RRCDATE_F;
                //if (rrcdate_fstr == "." && bpdate_fstr != ".") { CreateError("17A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true); } //rror_rrcdate_f == "17A_1,";
                if (rrcdate_f > currdate) { CreateError("17A_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rrcdate_fstr); } //rror_rrcdate_f == strip(error_rrcdate_f)|| "17A_3,";

                //RRCCOMPLETE_F;
                if (rrcdate_f_valid && SetUniform(rrccomplete_fstr) == ".") { CreateError("17B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rrccomplete_fstr); } //rror_rrccomplete_f == "17B_1,";
                if (rrccomplete_f < rrcdate_f &&
                    rrcdate_f_valid &&
                    rcccomplete_f_valid &&
                    rrccomplete_fstr != "." &&
                    rrcdate_fstr != ".") { CreateError("17B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rrccomplete_fstr); } //rror_rrccomplete_f == strip(error_rrccomplete_f)|| "17B_2,";
                if (rrccomplete_f > currdate) { CreateError("17B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rrccomplete_fstr); } //rror_rrccomplete_f == strip(error_rrccomplete_f)|| "17B_3,";
                if (ValidateIn(rrccomplete_fstr, new string[] { "88888888", "99999999" })) { CreateError("17B_4", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rrccomplete_fstr); } //rror_rrccomplete_f == strip(error_rrccomplete_f)|| "17B_4,";
                if (bpdate_fstr != "." && rrccomplete_fstr != "." && rrccomplete_f > bpdate_f.AddDays(60) && rrcdate_f_valid && rcccomplete_f_valid && rrccomplete_fstr!="." && rrcdate_fstr !=".") { CreateError("17B_5", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rrccomplete_fstr); } //rror_rrccomplete_f == strip(error_rrccomplete_f)|| "17B_5,";

                //RRCNUT_F;
                if (rrcdate_f_valid && rrcnut_f == ".") { CreateError("17C_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rrcnut_f); } //rror_rrcnut_f == "17C_1,";
                else if (rrcdate_f_valid && !ValidateIn(rrcnut_f, new string[] { "1", "2", "7" }) && rrcdate_f_valid) { CreateError("17C_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rrcnut_f); } //rror_rrcnut_f == strip(error_rrcnut_f)|| "17C_2,";
                if (rrcnut_f_num == 7) { CreateError("17C_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rrcnut_f); } //rror_rrcnut_f == strip(error_rrcnut_f)|| "17C_3,";

                //RRCPA_F;
                if (rrcdate_f_valid && rrcpa_f == ".") { CreateError("17D_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rrcpa_f); } //rror_rrcpa_f == "17D_1,";
                else if (rrcdate_f_valid && !ValidateIn(rrcpa_f, new string[] { "1", "2", "7" })) { CreateError("17D_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rrcpa_f); } //rror_rrcpa_f == strip(error_rrcpa_f)|| "17D_2,";
                if (rrcpa_f_num == 7) { CreateError("17D_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rrcpa_f); } //rror_rrcpa_f == strip(error_rrcpa_f)|| "17D_3,";

                //RRCSMOKE_F;
                if (rrcdate_f_valid && smoker_f == "1" && rrcsmoke_f == ".") { CreateError("17E_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rrcsmoke_f); } //rror_rrcsmoke_f == "17E_1,";
                else if (rrcdate_f_valid && smoker_f == "1" && !ValidateIn(rrcsmoke_f, new string[] { "1", "2", "7" })) { CreateError("17E_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rrcsmoke_f); } //rror_rrcsmoke_f == strip(error_rrcsmoke_f)|| "17E_2,";
                if (rrcsmoke_f_num == 7) { CreateError("17E_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rrcsmoke_f); } //rror_rrcsmoke_f == strip(error_rrcsmoke_f)|| "17E_3,";

                //RRCMADHERE_F;
                if (rrcdate_f_valid && hbpmeds_f == "1" && rrcmedadhere_f == ".") { CreateError("17F_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rrcmedadhere_f); } //rror_rrcmedadhere_f == "17F_1,";
                else if (rrcdate_f_valid && hbpmeds_f == "1" && !ValidateIn(rrcmedadhere_f, new string[] { "1", "2", "7" })) { CreateError("17F_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rrcmedadhere_f); } //rror_rrcmedadhere_f == strip(error_rrcmedadhere_f)|| "17F_2,";
                if (rrcmedadhere_f_num == 7) { CreateError("17F_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rrcmedadhere_f); } //rror_rrcmedadhere_f == strip(error_rrcmedadhere_f)|| "17F_3,";

                //RTCDATE_F;
                if (rrcdate_f_valid && rtcdate_fstr == ".") { CreateError("18A_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rtcdate_fstr); } //rror_rtcdate_f == "18A_1,";
                if (rtcdate_f > currdate) { CreateError("18A_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rtcdate_fstr); } //rror_rtcdate_f == strip(error_rtcdate_f)|| "18A_2,";

                //RTC_F;
                if (rtcdate_f_valid && ValidateIn(rtc_f, new string[] { ",", "9" })) { CreateError("18B_1", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rtcdate_fstr); } //rror_rtc_f == "18B_1,";
                else if (rtcdate_f_valid && !ValidateIn(rtc_f, new string[] { "1", "2", "3", "4", "5", "8", "9" })) { CreateError("18B_2", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rtcdate_fstr); } //rror_rtc_f == strip(error_rtc_f)|| "18B_2,";
                if (rtc_f_num == 8) { CreateError("18B_3", stfips, _row, bpdatestr, encodeid, screensiteid, errorCollection, true, rtcdate_fstr); } //rror_rtc_f == strip(error_rtc_f)|| "18B_3,";

            }
        }



        /***********************/

        //CreateError(IsMissing(_MDEVer), "00A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_StFIPS), "01A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_StFIPS, "StFIPS"), "01A_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_HdANSI), "01B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_EnrollSiteID), "01C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_ScreenSiteID), "01D_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_TimePer), "02A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_TimePer, "TimePer"), "02A_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_Type, "Type"), "02C_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_NScreen), "02B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_EncodeID), "03A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);

        //    /* Screening and Assessment */
        //    CreateError(IsMissing(_ResANSI), "03B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_Zip), "03C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingDate(_MYB), "03D_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_Latino), "03E_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_Latino.ToString(), "Latino"), "03E_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_Race1), "03F_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_Race1.ToString(), "Race1or2"), "03F_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_Race2), "03G_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_Race2.ToString(), "Race1or2"), "03G_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_Education), "03H_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_Education.ToString(), "Education"), "03H_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_Language), "03I_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_Language, "Language"), "03I_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_SRHC), "04A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_SRHC.ToString(), "YesNoGeneric"), "04A_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_SRHB), "04B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_SRHB.ToString(), "YesNoGeneric"), "04B_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_SRD), "04C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_SRD.ToString(), "YesNoGeneric"), "04C_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_SRHA), "04D_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_SRHA.ToString(), "YesNoGeneric"), "04D_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_HCMeds), "05A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_HCMeds.ToString(), "MedsGeneric"), "05A_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_HBPMeds), "05B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_HBPMeds.ToString(), "MedsGeneric"), "05B_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_DMeds), "05C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_DMeds.ToString(), "MedsGeneric"), "05C_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_HCAdherestr), "05D_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_HCAdherestr, "AdhereGeneric"), "05D_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); 
        //    CreateError(IsMissing(_HBPAdherestr), "05E_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_HBPAdherestr, "AdhereGeneric"), "05E_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_DAdherestr), "05F_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_DAdherestr, "AdhereGeneric"), "05F_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_BPHome), "06A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_BPHome.ToString(), "BPHome"), "06A_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_BPFreq), "06B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_BPFreq.ToString(), "BPFreq"), "06B_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_BPSend), "06C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_BPSend.ToString(), "BPSend"), "06C_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_Fruitstr), "07A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); //Can't safely do this check since the field accepts string and int types. If user enters 00 to represent none, that is a valid response; however, this will throw a false positive because of check for 0
        //    CreateError(IsNotAllowable(_Fruitstr, "Fruit"), "07A_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); 
        //    CreateError(IsMissing(_Vegetablesstr), "07B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_Vegetablesstr, "Vegetables"), "07B_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_Fish), "07C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_Fish.ToString(), "FishGrains"), "07C_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_Grains), "07D_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_Grains.ToString(), "FishGrains"), "07D_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_Sugar), "07E_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_Sugar.ToString(), "YesNoGeneric"), "07E_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_SaltWatch.ToString(), "YesNoGeneric"), "07F_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    //CreateError(IsMissingInt(_PAMod), "08A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    //CreateError(IsMissingInt(_PAVig), "08B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_Smoker), "09A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_Smoker.ToString(), "Smoker"), "09A_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_Sechandstr), "09B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_QOLPHstr), "10A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_QOLMHstr), "10B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_QOLEffectstr), "10C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_Height), "11A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_Weight), "11B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_Waist), "11C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_Hip), "11D_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingDate(_BPDate), "12A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_SPB1), "12D_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_DBP1), "12C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_SPB2), "12D_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_DBP2), "12E_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_Fast), "13A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingDate(_TCDate), "14A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_TotChol), "14B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_HDL), "14C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_LDL), "14D_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_Trigly), "14E_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_Glucosestr), "15B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_A1Cstr), "15C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingInt(_BPAlert), "16A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_BPAlertstr, "AlertGeneric"), "16A_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsNotAllowable(_BGAlertstr, "AlertGeneric"), "16C_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //}
        //else if (_Type == "3" || _Type == "4")
        //{
        /* Administrative */

        /* Screening and Assessment */
        //    CreateError(IsMissing(_HCMeds_fstr), "05A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_HBPMeds_fstr), "05B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_DMeds_fstr), "05C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_HCAdhere_fstr), "05D_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_HBPAdhere_fstr), "05E_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_DAdhere_fstr), "05F_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_BPHome_fstr), "06A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_BPFreq_fstr), "06B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_BPSend_fstr), "06C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_Fruit_fstr), "07A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_Vegetables_fstr), "07B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_Fish_fstr), "07C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_Grains_fstr), "07D_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_Sugar_fstr), "07E_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_PAMod_fstr), "08A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_PAVig_fstr), "08B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_Smoker_fstr), "09A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_Sechand_fstr), "09B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_QOLPH_fstr), "10A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_QOLMH_fstr), "10B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissing(_QOLEffect_fstr), "10C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //    CreateError(IsMissingDate(_BPDate_f), "12A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        ////}

        ///* Administrative */
        //CreateError((_MDEVer != "900" && _MDEVer !="901"), "00A_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //CreateError((IsMissing(_TimePer) && (_Type == "1")), "02A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //CreateError(!isTimePerValid(_TimePer, _BPDate), "02A_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //CreateError(1 > _NScreen && _NScreen > 8, "2B_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //CreateError(IsMissing(_Type), "2C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);

        ///* Screening and Assessment */
        //CreateError((_Zip == "99999"), "03C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        // Added a check for a valid MYB date type as the CreateError code will generate an out of range error. 
        //if (isDateValid(_MYBstr))  
        //{
        //    CreateError((_BPDate.Year - (_MYB.Subtract(new TimeSpan(30, 0, 0, 0)).Year) > 64) && (!IsMissingDate(_BPDate)) { CreateError( "03D_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror);
        //}
        //if((_BPDate.Year - (_MYB.AddDays(30).Year) < 40) && (!IsMissingDate(_BPDate)) { CreateError( "03D_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Latino == 9, "03E_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((IsMissingInt(_Latino) && IsMissingInt(_Race1) && IsMissingInt(_Race2)) || (_Latino == 7 || _Latino == 9) { CreateError( "03E_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Latino == 2 && IsMissingInt(_Race1) && IsMissingInt(_Race2) || (_Latino == 7 || _Latino == 9) { CreateError( "03E_4", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_Race1 == 9 && _Latino != 1) { CreateError( "03F_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_Race1, true, new int[] { 7, 9 }) && ValidateInInt(_Race2, false, new int[] { 7, 9 }) { CreateError( "03F_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_Education == 9) { CreateError( "03H_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_Education == 7) || (_Education == 8) { CreateError( "03H_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Language == "99", "03I_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Language == "88", "03I_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_SRHC == 9, "04A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_SRHC, true, new int[] { 7, 9 }) { CreateError( "04A_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_SRHB == 9, "04B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_SRHB, true, new int[] { 7, 8 }) { CreateError( "04B_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_SRD == 9, "04C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_SRD, true, new int[] { 7, 8 }) { CreateError( "04C_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_SRHA == 9, "04D_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_SRHA, true, new int[] { 7, 8 }) { CreateError( "04D_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_HCMeds == 9, "05A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_HCMeds, true, new int[] { 7, 8 }) { CreateError( "05A_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_HBPMeds == 9, "05B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_HBPMeds, true, new int[] { 7, 8 }) { CreateError( "05B_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_DMeds == 9, "05C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_DMeds, true, new int[] { 7, 8 }) { CreateError( "05C_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_HCAdhere == 99, "05D_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_HCAdhere, true, new int[] { 77, 88 }) { CreateError( "05D_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_HCMeds != 1 && _HCAdhere != 55, "05D_4", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_HCMeds == 1 && _HCAdhere == 55, "05D_5", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_HCMeds != 5 && _SRHC != 1 && (_TotChol) < 240, "05A_4", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_HCMeds == 5 && (_SRHC == 1 || _TotChol >= 240)) && ! ValidateInInt(_TotChol, true, new int[] {777, 888, 999}) { CreateError( "05A_5", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_HBPMeds != 5 && _SRHB != 1 && ((((_SPB1 + _SPB2)/2) < 140 && ((_DBP1 + _DBP2)/2) < 90)) { CreateError( "05B_4", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_HBPMeds == 5 && (_SRHB == 1 || (((_SPB1 + _SPB2)/2) >= 140) || (((_DBP1 + _DBP2)/2) >= 90)) { CreateError( "05B_5", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_DMeds != 5 && _SRD != 1 && ((_Glucose) < 126) && (_A1C) < 6.5, "05C_4", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_DMeds == 5 && (_SRD == 1 || (_Glucose) >= 126 || (_A1C) >= 6.5 )) && (! ValidateInInt(_Glucose, true, new int[] { 777, 888, 999 }) && _A1C!=9999.0) { CreateError( "05C_5", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_HBPAdhere == 99, "05E_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_HBPAdhere, true, new int[] { 77, 88 }) { CreateError( "05E_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_HBPMeds != 1 && _HBPAdhere != 55, "05E_4", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_HBPMeds == 1 && _HBPAdhere == 55, "05E_5", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_DAdhere == 99, "05F_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_DAdhere, true, new int[] { 77, 88 }) { CreateError( "05F_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_DMeds != 1 && _DAdhere != 55, "05F_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_BPHome == 9, "06A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_BPHome, true, new int[] { 7, 8 }) { CreateError( "06A_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_SRHB != 1 && _BPHome != 5, "06A_4", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); } 
        //if((_SRHB == 1 || _HBPMeds == 1) && _BPHome == 5, "06A_5", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_BPFreq == 9, "06B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_BPFreq, true, new int[] { 7, 8 }) { CreateError( "06B_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_BPHome != 1 && _BPFreq != 6, "06B_4", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_BPHome == 1 && _BPFreq == 6, "06B_5", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_BPSend == 9, "06C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_BPSend, true, new int[] { 7, 8 }) { CreateError( "06C_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_BPHome != 1 && _BPSend != 5, "06C_4", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_BPHome == 1 && _BPSend == 5, "06C_5", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Fruit == 99, "07A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Fruit == 88, "07A_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Fruit > 50 && ! ValidateInInt(_Fruit, true, new int[] { 88, 99 }) { CreateError( "07A_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Vegetables == 99, "07B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Vegetables == 88, "07B_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Vegetables > 15 && ! ValidateInInt(_Vegetables, true, new int[] { 88, 99 }) { CreateError( "07B_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Fish == 9, "07C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Fish == 8, "07C_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Grains == 9, "07D_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Grains == 8, "07D_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Sugar == 9, "07E_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Sugar == 8, "07E_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_SaltWatch == 9, "07F_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_SaltWatch == 8, "07F_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_PAMod == 999, "08A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_PAMod == 888, "08A_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_PAMod < 10 || _PAMod > 600) && ! ValidateIn(_PAModstr, true, new string[] { "888", "999", "000" }) { CreateError( "08A_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_PAVig == 999, "08B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_PAVig == 888, "08B_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_PAVig < 10 || _PAVig > 480) && ValidateIn(_PAVigstr, false, new string[] { "888", "999", "000" }) { CreateError( "08B_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Smoker == 9, "09A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Smoker == 8, "09A_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Sechand == 99, "09B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Sechand == 88, "09B_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Sechand > 24 && ValidateInInt(_Sechand, false, new int[] { 66, 88, 99 }) { CreateError( "09B_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_QOLPH == 99, "10A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_QOLPH, true, new int[] { 77, 88 }) { CreateError( "10A_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_QOLPH > 30 && ! ValidateInInt(_QOLPH, true, new int[] { 77, 88, 99 }) { CreateError( "10A_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_QOLMH == 99, "10B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_QOLMH, true, new int[] { 77, 88 }) { CreateError( "10B_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_QOLMH > 30 && ValidateInInt(_QOLMH, false, new int[] { 77, 88, 99 }) { CreateError( "10B_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_QOLEffect == 99, "10C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_QOLEffect, true, new int[] { 77, 88 }) { CreateError( "10C_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_QOLEffect > 30 && ! ValidateInInt(_QOLEffect, true, new int[] { 77, 88 }) { CreateError( "10C_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_Height, true, new int[] { 77, 88, 99 }) { CreateError( "11A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_Height < 52 || _Height > 76) && ValidateInInt(_Height, false, new int[] { 77, 88, 99 }) { CreateError( "11A_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((52 <= _Height && _Height <= 58) || (74 <= _Height && _Height <= 76) { CreateError( "11A_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Weight == 999, "11B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_Weight < 074 || _Weight > 460) && ValidateInInt(_Weight, false, new int[] { 777, 888, 999 }) { CreateError( "11B_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((074 <= _Weight && _Weight <= 090) || (350 <= _Weight && _Weight <= 460) { CreateError( "11B_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_Weight, true, new int[] { 777, 888 }) { CreateError( "11B_4", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_Waist < 16 || _Waist > 71) && ValidateInInt(_Waist, false, new int[] { 77, 88, 99 }) { CreateError( "11C_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_Hip < 26 || _Hip > 75) && ValidateInInt(_Waist, false, new int[] { 77, 88, 99 }) { CreateError( "11D_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_BPDate < toDateTime("06 / 01 / 2013") && _MDEVer == "900", "12A_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_BPDate > currdate, "12A_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_SPB1, true, new int[] { 888, 999 }) { CreateError( "12B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_SPB1 < 074 || _SPB1 > 260) && ! ValidateInInt(_SPB1, false, new int[] { 777, 888, 999 }) { CreateError( "12B_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(230 <= _SPB1 && _SPB1 <= 260, "12B_4", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_SPB1 == 777, "12B_5", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_SPB1, true, new int[] { 777, 888, 999 }) && ValidateInInt(_SPB2, false, new int[] { 777, 888, 999 }) { CreateError( "12B_6", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_DBP1, true, new int[] { 888, 999 }) { CreateError( "12C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_DBP1 < 2 || _DBP1 > 156) && ValidateInInt(_DBP1, false, new int[] { 777, 888, 999 }) { CreateError( "12C_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((2 <= _DBP1 && _DBP1 <= 12) || (122 <= _DBP1 && _DBP1 <= 156) { CreateError( "12C_4", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_DBP1 == 777, "12C_5", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_DBP1, true, new int[] { 777, 888, 999 }) && ValidateInInt(_DBP2, false, new int[] { 777, 888, 999 }) { CreateError( "12C_6", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_SPB1 == 777 && _DBP1 != 777) && (_SPB1 != 777 && _DBP1 == 777) { CreateError( "12C_7", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_SPB2, true, new int[] { 888, 999 }) { CreateError( "12D_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_SPB2 < 074 || _SPB2 > 260) && ValidateInInt(_SPB2, false, new int[] { 777, 888, 999 }) { CreateError( "12D_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_SPB2 == 777, "12D_5", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_DBP2, true, new int[] { 888, 999 }) { CreateError( "12E_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_DBP2 < 2 || _DBP2 > 156) && ValidateInInt(_DBP2, false, new int[] { 777, 888, 999 }) { CreateError( "12E_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((2 <= _DBP1 && _DBP1 <= 012) || (122 <= _DBP1 && _DBP1 <= 156) { CreateError( "12E_4", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_SPB2 == 777 && _DBP2 != 777) || (_SPB2 != 777 && _DBP2 == 777) { CreateError( "12E_6", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Fast == 9, "13A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_Fast != 9 && (_TotChol == 777 && _HDL == 777 && _LDL == 777 && _Trigly == 7777 && _Glucose == 777)) || (_Fast == 9 && (_TotChol != 777 && _HDL != 777 && _LDL != 777 && _Trigly != 7777 && _Glucose != 777)) { CreateError( "13A_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_Fast != 9 && (_TotChol == 888 && _HDL == 888 && _LDL == 888 && _Trigly == 8888 && _Glucose == 888)) || (_Fast == 9 && (_TotChol != 888 && _HDL != 888 && _LDL != 888 && _Trigly != 8888 && _Glucose != 888)) { CreateError( "13A_4", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_Fast != 9 && (_TotChol == 999 && _HDL == 999 && _LDL == 999 && _Trigly == 9999 && _Glucose == 999)) || (_Fast == 9 && (_TotChol !=999 && _HDL !=999 && _LDL !=999 && _Trigly !=9999 && _Glucose !=999)) { CreateError( "13A_5", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(IsMissingDate(_TCDate) && (_TotChol != 888 || _TotChol != 8888 || _HDL != 888 || _HDL != 8888 || _LDL != 888 || _LDL != 8888 || _Trigly != 888 || _Trigly != 8888 || _TotChol != 999 || _TotChol != 9999 || _HDL != 999 || _HDL != 9999 || _LDL != 999 || _LDL != 9999 || _Trigly != 999 || _Trigly != 9999) { CreateError( "14A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_TCDate > currdate, "14A_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_TotChol == 999, "14B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_TotChol < 044 || _TotChol > 702) && ValidateInInt(_TotChol, false, new int[] { 777, 888, 999 }) { CreateError( "14B_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((44 <= _TotChol && _TotChol <= 60) || (400 <= _TotChol && _TotChol <= 702) { CreateError( "14B_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_TotChol, true, new int[] { 777, 888 }) { CreateError( "14B_4", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_HDL == 999, "14C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_HDL < 7 || _HDL > 196) && ! ValidateInInt(_HDL, true, new int[] { 777, 888, 999 }) { CreateError( "14C_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(155 <= _HDL && _HDL <= 196, "14C_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_HDL, true, new int[] { 777, 888 }) { CreateError( "14C_4", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_LDL < 020 || _LDL > 380) && ValidateInInt(_LDL, false, new int[] { 777, 888, 999 }) { CreateError( "14D_2",  _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        ////if((_Fast != 1 || _Fast != 9) && _LDL != 999, "14D_3",  _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_Fast == 2) && (_LDL != 999) { CreateError( "14D_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); } //Updated on 100914 based on email sent from Michaela Vine
        //if(344 <= _LDL && _LDL <= 380, "14D_4",  _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_Trigly < 12 || _Trigly > 3000) && ! ValidateInInt(_Trigly, true, new int[] { 7777, 8888, 9999 }) { CreateError( "14E_2",  _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        ////if((_Fast != 1 || _Fast != 9) && _Trigly != 9999, "14E_3",  _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_Fast == 2) && (_Trigly != 9999) { CreateError( "14E_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); } //Updated on 100914 based on email sent from Michaela Vine
        //if(1000 <= _Trigly && _Trigly <= 3000, "14E_4",  _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(IsMissing(_BGDatestr) && (ValidateIn(_Glucosestr, false, new string[] { "888", "999" }) || ValidateIn(_A1Cstr, false, new string[] { "8888", "9999" })) { CreateError( "15A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_BGDate > currdate, "15A_2",  _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Glucose == 999 && _A1Cstr == "9999", "15B_2",  _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(((_Glucose < 37 || _Glucose > 571) && ValidateInInt(_Glucose, false, new int[] { 777, 888, 999 }) && ! IsMissing(_Glucosestr)) , "15B_3" ,  _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((37 <= _Glucose && _Glucose <= 50) || (275 <= _Glucose && _Glucose <= 571) { CreateError( "15B_4",  _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Glucose == 777, "15B_5", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Glucose == 888 && _A1C == 8888, "15B_6", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        ////if((_Fast != 1 || _Fast != 9) && _Glucose != 999, "15B_7", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); } Deactivated based on email sent from M. Vine on 100914
        //if((_A1C < 02.8 || _A1C > 16.2) && ValidateIn(_A1Cstr, false, new string[] { "7777", "8888", "9999" }) { CreateError( "15C_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((2.8 <= _A1C && _A1C <= 4.0) || (13.0 <= _A1C && _A1C <= 16.2) { CreateError( "15C_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_A1C == 7777, "15C_4", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_BPAlert == 9, "16A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(((((_SPB1 + _SPB2) / 2) > 180) || ((_DBP1 + _DBP2) / 2) > 110) && ! ValidateIn(_BPAlertstr, true, new string[] { "1", "2", "8", "9" }) && (!ValidateInInt(_SPB2, true, new int[] { 777, 888, 999 }) || ! ValidateInInt(_DBP2, true, new int[] { 777, 888, 999 })) { CreateError( "16A_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(((((_SPB1 + _SPB2) / 2) <= 180 || ((_DBP1 + _DBP2) / 2) <= 110) && _BPAlertstr != "3")&& (!ValidateInInt(_SPB2, true, new int[] { 777, 888, 999 }) || ! ValidateInInt(_DBP2, true, new int[] { 777, 888, 999 })) { CreateError( "16A_4", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_SPB1, true, new int[] { 777, 888, 999 }) && ValidateInInt(_DBP1, true, new int[] { 777, 888, 999 }) && _BPAlert != 3, "16A_5", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((((_SPB1 + _SPB2) / 2) > 180 || ((_DBP1 + _DBP2) / 2) > 110) && (_BPAlert == 8 || _BPAlert == 9) && isDateValid(_BPDiDatestr) { CreateError( "16A_6", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((((_SPB1 + _SPB2) / 2) > 180 || ((_DBP1 + _DBP2) / 2) > 110) && _BPAlert == 1 && isDateValid(_BPDiDatestr) && isDateValid(_BPDatestr) && _BPDiDate < _BPDate, "16B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((((_SPB1 + _SPB2) / 2) <= 180 && ((_DBP1 + _DBP2) / 2) <= 110) && isDateValid(_BPDiDatestr) { CreateError( "16B_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_SPB1, true, new int[] { 777, 888, 999 }) && ValidateInInt(_DBP1, true, new int[] { 777, 888, 999 }) && isDateValid(_BPDiDatestr) { CreateError( "16B_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(((((_SPB1 + _SPB2) / 2) > 180) || ((_DBP1 + _DBP2) / 2) > 110) && _BPAlert == 1 && ((isDateValid(_BPDiDatestr) && isDateValid(_BPDatestr) && _BPDiDate - _BPDate > new TimeSpan(7, 0, 0, 0)) || IsMissingDate(_BPDiDate)) { CreateError( "16B_4", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        ////if((((_SPB1 + _SPB2) / 2) > 180 || ((_DBP1 + _DBP2) / 2) > 110) && _BPAlert == 2 && (isDateValid(_BPDiDatestr) && isDateValid(_BPDatestr) && _BPDiDate - _BPDate > new TimeSpan(7, 0, 0, 0)) || IsMissingDate(_BPDiDate) { CreateError( "16B_5", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); } Updated this formula based on email sent from M.Vine on 100914
        //if((((_SPB1 + _SPB2) / 2) > 180 || ((_DBP1 + _DBP2) / 2) > 110) && _BPAlert == 2 && ((isDateValid(_BPDiDatestr) && isDateValid(_BPDatestr) && _BPDiDate - _BPDate > new TimeSpan(7, 0, 0, 0)) || IsMissingDate(_BPDiDate)) { CreateError( "16B_5", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((((_SPB1 + _SPB2) / 2) > 180 || ((_DBP1 + _DBP2) / 2) > 110) && _BPAlert == 8 && IsMissingDate(_BPDiDate) { CreateError( "16B_6", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((((_SPB1 + _SPB2) / 2) > 180 || ((_DBP1 + _DBP2) / 2) > 110) && _BPAlert == 9 && IsMissingDate(_BPDiDate) { CreateError( "16B_7", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }

        ////BGAlert
        //if(_BGAlert == 9 && IsMissing(_BGAlertstr) { CreateError( "16C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_Glucose <= 50 || _Glucose >= 250) && ! ValidateInInt(_BGAlert, true, new int[] { 1, 2, 8, 9 }) && (!IsMissing(_Glucosestr) || !IsMissing(_BGAlertstr) && !ValidateIn(_Glucosestr, true, new string[]{"777","888","999"})) { CreateError( "16C_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((50 < _Glucose && _Glucose < 250) && _BGAlert != 3, "16C_4", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_Glucose, true, new int[] { 777, 888, 999 }) && _BGAlert != 3, "16C_5", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_Glucose <= 50 || _Glucose >= 250) && ValidateInInt(_Glucose, true, new int[] { 8, 9 }) && isDateValid(_BGDiDatestr) { CreateError( "16C_6", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_Glucose <= 50 || _Glucose >= 250) && isDateValid(_BGDiDatestr) && isDateValid(_BGDatestr) && _BGDiDate < _BGDate && _BGAlert == 1, "16D_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((50 < _Glucose && _Glucose < 250) && isDateValid(_BGDiDatestr) { CreateError( "16D_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(ValidateInInt(_Glucose, true, new int[] { 777, 888, 999 }) && isDateValid(_BGDiDatestr) { CreateError( "16D_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        ////if((_Glucose <= 50 || _Glucose >= 250) && _BGAlert == 1 && (isDateValid(_BGDiDatestr) && isDateValid(_BGDatestr) && _BGDiDate - _BGDate > new TimeSpan(7, 0, 0, 0)) || IsMissingDate(_BGDiDate) { CreateError( "16D_4", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); } Updated this formula based on email sent from M.Vine on 100914
        //if((_Glucose <= 50 || _Glucose >= 250) && _BGAlert == 1 && ((isDateValid(_BGDiDatestr) && isDateValid(_BGDatestr) && _BGDiDate - _BGDate > new TimeSpan(7, 0, 0, 0)) || IsMissingDate(_BGDiDate)) { CreateError( "16D_4", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        ////if((_Glucose <= 50 || _Glucose >= 250) && _BGAlert == 2 && (isDateValid(_BGDiDatestr) && isDateValid(_BGDatestr) && _BGDiDate - _BGDate > new TimeSpan(7, 0, 0, 0)) || IsMissingDate(_BGDiDate) { CreateError( "16D_5", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_Glucose <= 50 || _Glucose >= 250) && _BGAlert == 2 && ((isDateValid(_BGDiDatestr) && isDateValid(_BGDatestr) && _BGDiDate - _BGDate > new TimeSpan(7, 0, 0, 0)) || IsMissingDate(_BGDiDate)) { CreateError( "16D_5", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_Glucose <= 50 || _Glucose >= 250) && _BGAlert == 8 && IsMissingDate(_BGDiDate) { CreateError( "16D_6", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if((_Glucose <= 50 || _Glucose >= 250) && _BGAlert == 9 && IsMissingDate(_BGDiDate) { CreateError( "16D_7", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }

        ///* Risk Reduction Counseling */
        //if(IsMissingDate(_RRCDate) && ValidateInInt(_BGAlert, true, new int[] { 1, 2 }) { CreateError( "17A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_RRCDate > currdate, "17A_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(isDateValid(_RRCDatestr) && !IsMissingDate(_RRCComplete) { CreateError( "17B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(isDateValid(_RRCCompletestr) && _RRCComplete <= _RRCDate, "17B_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_RRCComplete > _CDate, "17B_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_RRCComplete >= _BPDate.AddDays(60) { CreateError( "17B_5", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(isDateValid(_RRCDatestr) && IsMissingInt(_RRCNut) { CreateError( "17C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_RRCNut == 7, "17C_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(isDateValid(_RRCDatestr) && IsMissingInt(_RRCPA) { CreateError( "17D_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_RRCPA == 7, "17D_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(isDateValid(_RRCDatestr) && _Smoker == 1 && IsMissingInt(_RRCSmoke) { CreateError( "17E_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_RRCSmoke == 7, "17E_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(isDateValid(_RRCDatestr) && _HBPMeds == 1 && IsMissingInt(_RRCMedAdhere) { CreateError( "17F_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_RRCMedAdhere == 7, "17F_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(isDateValid(_RRCDatestr) && IsMissingDate(_RTCDate) { CreateError( "18A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_RTCDate > _CDate, "18A_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(isDateValid(_RTCDatestr) && (IsMissingInt(_RTC) || _RTC == 9) { CreateError( "18B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_RTC == 8, "18B_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }

        ///*Healthy Behavior Support Option*/
        //if(_RefDate > _CDate, "19A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(isDateValid(_RefDatestr) && IsMissing(_LSPHCRecstr) { CreateError( "20A_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        ////Missing 20A_2
        //if(isDateValid(_RefDatestr) && IsMissingDate(_Intervention) { CreateError( "20B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_Intervention > _CDate, "20B_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(isDateValid( _Interventionstr) && IsMissingDate(_RefDate) { CreateError( "20B_3", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(isDateValid( _Interventionstr) && IsMissingInt(_LSPHCID) { CreateError( "20C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(isDateValid( _Interventionstr) && IsMissingInt(_LSPHCTime) { CreateError( "20D_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(_LSPHCTime > 120, "20D_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(isDateValid( _Interventionstr) && (IsMissingInt(_ContactType) || _ContactType == 9) { CreateError( "20E_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(isDateValid( _Interventionstr) && (IsMissingInt(_Setting) || _Setting == 9) { CreateError( "20F_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(isDateValid( _Interventionstr) && (IsMissingInt(_LSPHCComp) || _LSPHCComp == 9) { CreateError( "20G_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        ////Missing 21A_1
        //if(_TobResDate > _CDate, "21A_2", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(isDateValid( _TobResDatestr) && (IsMissingInt(_TobResType) || _TobResType == 9) { CreateError( "21B_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //if(isDateValid( _TobResDatestr) && (IsMissingInt(_TResComp) || _TResComp == 9) { CreateError( "21C_1", _StFIPS, _row, _BPDatestr, _EncodeID, _ScreenSiteID, verror); }
        //}

        private int GetRoundForFieldName(string fieldName)
        {
            int round = -1;
            if (ValueIndexLookup.Dictionary.ContainsKey(fieldName.ToLower()))
                round = ValueIndexLookup.Dictionary[fieldName.ToLower()] + 1;
            return round;
        }

        private void CreateError(string errorKey, string state, int row, string bpdate, string encodeid, string screensiteid, List<SPValidationError> errorCollection, bool isfollowup, string mdeFieldVal)
        {
            SPValidationMessage vMessage;
            SPValidationLookup vLookup = new SPValidationLookup();
            SPValidationError vError = new SPValidationError();
            vMessage = vLookup.ErrorDictionary[errorKey];

            string sFieldName = vMessage.MDEName;
            if (isfollowup) { sFieldName += "_f"; }

            //string description = "<tr><td>" + vMessage.MDEName + "</td><td>" + vMessage.MDEType + "</td><td>" + errorKey + "</td><td>" + vMessage.Message + "</td></tr>";
            //vError.Description += description;
            vError.BPPlus = isbpplusrow;
            vError.InvalidRow = !isvalidrow;
            vError.State = state;
            vError.Row = row;
            vError.Round = GetRoundForFieldName(sFieldName);
            vError.BPDate = bpdate;
            vError.EncodeID = encodeid;
            vError.ScreenSiteID = screensiteid;
            vError.MDEFieldName = sFieldName;
            vError.MDEValue = mdeFieldVal;
            vError.ValidationType = vMessage.MDEType;
            vError.ErrorKey = errorKey;
            vError.Message = vMessage.Message;
            vError.Type = itype;
            if (vMessage.MDEType == "Error" && !isfollowup) //we do not want to include follow up errors in the count
            {
                vError.ErrorCnt++;
            }
            errorCollection.Add(vError);
        }

        private void CreateFormatError(string errorKey, string state, int row, string bpdate, string encodeid, string screensiteid, List<SPValidationError> errorCollection, bool isfollowup, string mdeFieldVal, string mdeFieldName, string sCustomErrMsg = "")
        {
            SPValidationMessage vMessage;
            SPValidationLookup vLookup = new SPValidationLookup();
            SPValidationError vError = new SPValidationError();
            vMessage = vLookup.ErrorDictionary[errorKey];

            string sFieldName = mdeFieldName;

            //string description = "<tr><td>" + vMessage.MDEName + "</td><td>" + vMessage.MDEType + "</td><td>" + errorKey + "</td><td>" + vMessage.Message + "</td></tr>";
            //vError.Description += description;
            vError.BPPlus = isbpplusrow;
            vError.InvalidRow = !isvalidrow;
            vError.State = state;
            vError.Row = row;
            vError.Round = GetRoundForFieldName(sFieldName);
            vError.BPDate = bpdate;
            vError.EncodeID = encodeid;
            vError.ScreenSiteID = screensiteid;
            vError.MDEFieldName = sFieldName;
            vError.MDEValue = mdeFieldVal;
            vError.ValidationType = vMessage.MDEType;
            vError.ErrorKey = errorKey;
            vError.Type = itype;

            if (sCustomErrMsg == "")
            {
                sCustomErrMsg = sFieldName + " has the wrong number of digits.";
            }

            vError.Message = sCustomErrMsg;

            if (vMessage.MDEType == "Error" && !isfollowup) //we do not want to include follow up errors in the count
            {
                vError.ErrorCnt++;
            }
            errorCollection.Add(vError);
        }

        //private void CreateFormatError(string errorKey, string state, int row, string bpdate, string encodeid, string screensiteid, List<SPValidationError> errorCollection, bool isfollowup, string mdeFieldVal, string mdeFieldName)
        //{
        //    SPValidationMessage vMessage;
        //    SPValidationLookup vLookup = new SPValidationLookup();
        //    SPValidationError vError = new SPValidationError();
        //    vMessage = vLookup.ErrorDictionary[errorKey];

        //    string sFieldName = mdeFieldName;

        //    //string description = "<tr><td>" + vMessage.MDEName + "</td><td>" + vMessage.MDEType + "</td><td>" + errorKey + "</td><td>" + vMessage.Message + "</td></tr>";
        //    //vError.Description += description;
        //    vError.State = state;
        //    vError.Row = row;
        //    vError.BPDate = bpdate;
        //    vError.EncodeID = encodeid;
        //    vError.ScreenSiteID = screensiteid;
        //    vError.MDEFieldName = sFieldName;
        //    vError.MDEValue = mdeFieldVal;
        //    vError.ValidationType = vMessage.MDEType;
        //    vError.ErrorKey = errorKey;
        //    vError.Message = sFieldName + " has the wrong number of digits.";
        //    if (vMessage.MDEType == "Error" && !isfollowup) //we do not want to include follow up errors in the count
        //    {
        //        vError.ErrorCnt++;
        //    }
        //    errorCollection.Add(vError);
        //}

        //private void CreateFormatError(string errorKey, string state, int row, string bpdate, string encodeid, string screensiteid, List<SPValidationError> errorCollection, bool isfollowup, string mdeFieldVal, string mdeFieldName, string CustomErrDescription)
        //{
        //    SPValidationMessage vMessage;
        //    SPValidationLookup vLookup = new SPValidationLookup();
        //    SPValidationError vError = new SPValidationError();
        //    vMessage = vLookup.ErrorDictionary[errorKey];



        //    string sFieldName = mdeFieldName;

        //    //string description = "<tr><td>" + vMessage.MDEName + "</td><td>" + vMessage.MDEType + "</td><td>" + errorKey + "</td><td>" + vMessage.Message + "</td></tr>";
        //    //vError.Description += description;
        //    vError.State = state;
        //    vError.Row = row;
        //    vError.BPDate = bpdate;
        //    vError.EncodeID = encodeid;
        //    vError.ScreenSiteID = screensiteid;
        //    vError.MDEFieldName = sFieldName;
        //    vError.MDEValue = mdeFieldVal;
        //    vError.ValidationType = vMessage.MDEType;
        //    vError.ErrorKey = errorKey;
        //    vError.Message = CustomErrDescription;
        //    if (vMessage.MDEType == "Error" && !isfollowup) //we do not want to include follow up errors in the count
        //    {
        //        vError.ErrorCnt++;
        //    }
        //    errorCollection.Add(vError);
        //}

        private bool IsMissing(string mdeElement)
        {
            //Validates whether an element is missing
            if (IsAllDups(mdeElement, ".") || IsAllDups(mdeElement, " "))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private string SetUniform(string element)
        {
            //Sets to a period when value is missing
            if (IsAllDups(element, ".") || IsAllDups(element, " "))
            {
                return ".";
            }
            else
            {
                return element;
            }
        }

        private bool IsRegExMatch(string sRegex, string sField)
        {
            bool bRet = false;

            Regex regex = new Regex(@sRegex);
            Match match = regex.Match(sField);
            bRet = match.Success;

            return bRet;
        }

        private int SetMean(int element1, int element2)
        {
            return (element1 + element2) / 2;
        }

        private bool IsAllDups(string element, string arg)
        {
            int dupcount = 0;
            string[] elements = new string[element.Length];
            for (int i = 0; i < elements.Length; i++)
            {
                elements[i] = element[i].ToString();
            }

            for (int i = 0; i < elements.Length; i++)
            {
                if (elements[i] == arg) { dupcount++; }
            }
            if (dupcount == elements.Length)
            //if (dupcount == elements.Length + 1)
            { return true; }
            else
            { return false; }
        }

        private bool IsMissingDecimalField(string mdeElement)
        {
            //Validates whether an element is missing. Had to write a specific check to eliminate a possible decimal (A1C)
            if (mdeElement == "" || mdeElement == "...." || mdeElement.Contains(" "))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsMissingInt(int mdeElement)
        {
            //Validates whether an element is missing
            if (mdeElement == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsMissingDate(DateTime mdeElement)
        {
            //Validates whether a date element is missing
            if (mdeElement.Equals(DateTime.MinValue))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool IsNotAllowable(string mdeElement, string dictName)
        {
            SPLookup lboogie = new SPLookup();
            //Validates whether an MDE element is an allowable value in the provided table
            if (!lboogie.MasterDictionary[dictName].ContainsKey(mdeElement))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool ValidateInInt(int icomp, int[] ival)
        {

            if (ival.Contains(icomp)) { return true; } else { return false; }
        }

        private bool ValidateIn(string scomp, params string[] sval)
        {
            if (sval.Contains(scomp)) { return true; } else { return false; }
        }

        private bool ValidateAnd(string scomp, bool typeisequal, params string[] sval)
        {
            bool retVal = true;

            if (typeisequal)
            {
                foreach (string val in sval)
                {
                    retVal = (retVal && val == scomp);
                }
            }
            else
            {
                foreach (string val in sval)
                {
                    retVal = (retVal && val != scomp);
                }
            }
            return retVal;
        }

        private int toInt(string sVal)
        {
            sVal = sVal.Replace(".", "");  //Strip decimals/periods out, they force a conversion to 0 since it is to an INT type.
            int myInt;
            bool isValid = int.TryParse(sVal, out myInt);
            return myInt;
        }

        private double toDouble(string sval)
        {
            double myDec;
            bool isValid = double.TryParse(sval, out myDec);
            return myDec;
        }

        private bool isNumeric(string mdeElement)
        {
            int myInt;
            bool isValid = int.TryParse(mdeElement, out myInt);
            return isValid;
        }

        private bool isDate(string sdate)
        {
            DateTime myDt;
            bool isValid = DateTime.TryParse(sdate, out myDt); //returns Date.MinValue if not valid date
            return isValid;
        }

        private bool isDateValid(string sdate)
        {
            DateTime myDt;
            bool isValid = DateTime.TryParse(sdate, out myDt); //returns Date.MinValue if not valid date
            return isValid;
        }

        private bool isTimePerValid(string timeper, DateTime dtdate)
        {
            bool isValid = false;
            if (!IsMissing(timeper))
            {
                switch (timeper)
                {
                    case "1":
                        {
                            if (dtdate > toDateTime("07/01/13") || dtdate < toDateTime("12/31/13")) { isValid = true; }
                            break;
                        }
                    case "2":
                        {
                            if (dtdate > toDateTime("01/01/14") || dtdate < toDateTime("06/30/14")) { isValid = true; }
                            break;
                        }
                    case "3":
                        {
                            if (dtdate > toDateTime("06/01/14") || dtdate < toDateTime("12/31/14")) { isValid = true; }
                            break;
                        }
                    case "4":
                        {
                            if (dtdate > toDateTime("01/01/15") || dtdate < toDateTime("06/30/15")) { isValid = true; }
                            break;
                        }
                    case "5":
                        {
                            if (dtdate > toDateTime("07/01/15") || dtdate < toDateTime("12/31/15")) { isValid = true; }
                            break;
                        }
                    case "6":
                        {
                            if (dtdate > toDateTime("01/01/16") || dtdate < toDateTime("06/30/16")) { isValid = true; }
                            break;
                        }
                    case "7":
                        {
                            if (dtdate > toDateTime("07/01/16") || dtdate < toDateTime("12/31/16")) { isValid = true; }
                            break;
                        }
                    case "8":
                        {
                            if (dtdate > toDateTime("01/01/17") || dtdate < toDateTime("06/30/17")) { isValid = true; }
                            break;
                        }
                    default:
                        isValid = false;
                        break;
                }
            }
            else
            {
                isValid = false;
            }
            return isValid;
        }

        private DateTime toDateTime(string mdeElement)
        {
            DateTime myDt;
            string[] formats = new[] { "MMddyyyy", "MMyyyy" };
            //bool isValid = DateTime.TryParse(mdeElement, out myDt); //returns Date.MinValue if not valid date
            DateTime.TryParseExact(mdeElement, formats, new CultureInfo("en-US"), DateTimeStyles.None, out myDt);
            return myDt;
        }

        private string[] GetValidLSPHCIDStrings()
        {
            //Mathematica is maintaining these values in a spreadsheet.
            //TODO: Move this into the DB "LookupLists" table.
            string[] sValidStrings = { 
                        "AL13HCG001",
                        "AL14LSPEFX",
                        "AL16LSPSNP",
                        "AL16LSPCMX",
                        "AR13LSPTOP",
                        "AR14HCG00X ",
                        "AR14LSPALX",
                        "AR14LSPCMX",
                        "AR14LSPEFX",
                        "CA13HCG001",
                        "CA13HCG002",
                        "CA14LSPEFN",
                        "CA15LSPDPP",
                        "CA16HCG00X",
                        "CO13LSPDPP",
                        "CO14HCG001",
                        "CO15LSPCMS",
                        "CO17LSPASP",
                        "CT15HCG001",
                        "CT15HCG002",
                        "CT15LSPDPP",
                        "CT15LSPTOP",
                        "CT15LSPWWS",
                        "CT16HCG00X",
                        "CT17HCGMTM",
                        "IA14HCG001",
                        "IA14LSPWWS",
                        "IA16HCGBPX",
                        "IA16HCGMTX",
                        "IL14HCG001",
                        "IL14LSPEFX",
                        "IL14LSPTOP",
                        "IL15HCG002",
                        "IL15LSPDPP",
                        "IL15LSPWWS",
                        "IL16HCG00X",
                        "IN13HCG001",
                        "IN13LSPDPP",
                        "IN13LSPTOP",
                        "IN15LSPEFX",
                        "MI13HCG001",
                        "MI13LSPDPP",
                        "MI13LSPTOP",
                        "MO13HCG001",
                        "MO13LSPDPP",
                        //"MO13LSPESB",   //removed 08/17/17 - renamed to MO13LSPESX seen below
                        "MO13LSPESX",
                        "MO16HCG00X",
                        "NC13HCG001",
                        //"NC13LSPEMW",   //removed 08/17/17 - renamed to NC13LSPEMX 
                        "NC13LSPEMX",
                        "NC13LSPTOP",
                        "NC13LSPWWS",
                        "NC14LSPDPP",
                        "NC16HCG002",
                        "NE13LSPDPP",
                        "NE14HCG001",
                        "NE15HCG002",
                        "NE15HCG003",
                        "NE15LSPDPO",
                        "NE15LSPTOP",
                        "OR13HCG001",
                        "OR13HCG101",
                        "OR14HCG001",
                        "OR14LSPTOP",
                        "OR15HCG001",
                        "OR15LSPDPP",
                        "PA13EHCWLK",
                        "PA13HCG001",
                        "PA13LSPWWS",
                        "PA14HCG00X",
                        "PA14LSPCCX",
                        "PA14LSPTOP",
                        "RI14HCG001",
                        "RI14HCG002",
                        "RI14LSPDPP",
                        "RI14LSPTOP",
                        "RI14LSPWWS",
                        "RI15LSPCCX",
                        "RI15LSPVSX",
                        "SC14HCG001",
                        "SC14HCG002",
                        "SC15LSPTOP",   //changed from 14 to 15 - 08/17/17
                        "SC16LSPDPP",
                        "SE13HCG001",
                        "SE14LSPIDP",
                        "SE15HCG002",
                        "SE15LSPWWS",
                        "SE16LSPWWX",
                        "SO13HCG001",
                        "SO13LSPLTW",
                        "UT13HCG001",
                        "UT14LSPEFN",
                        "UT16HCG00X",
                        "UT16LSPWWS",
                        "VT13LSPCCX",
                        "VT13LSPDPP",
                        "VT13LSPVTX",
                        "VT13LSPWWS",
                        "VT14HCG001",
                        "VT14HCG101",
                        "VT15LSPFIT",
                        "VT15LSPTOP",
                        "WI13HCG001",
                        "WI13LSPDPP",
                        "WI13LSPEFN",
                        "WI13LSPTOP",
                        "WI15HCG101",
                        "WI15LSPSNP",
                        "WV13HCG001",
                        "WV13LSPTOP",
                        "WV14HCG101",
                        "WV14LSPDPP",
                        "WV16LSPEFN",
                        "WI17LSPVEX",   //added 08/17/17
                        "CO17HCGMTX",    //added 01/28/18
                        "SE17LSPTOP",    //added 01/28/18
                        "SE17EHC00X",    //added 01/28/18
                        "VT17LSPSTW",    //added 01/28/18
                        "VT17LSPWWO",    //added 01/28/18
                        "RI18HCG00X",    //added 03/07/2018 - requested by Katie (via email)
                        "PA16HCYMCA"    //added 04/23/2018 - requested by Katie (via email)         
                        };
            return sValidStrings;
        }

        private bool ValidateRow()
        {
            if (IsMissing(mybstr) && ValidateIn(itype, new string[] { "1", "2" })) { return false; } //("3D_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            if (IsMissing(latino) || latino == "9") { return false; } //("3E_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            if (ValidateIn(race1, new string[] { "9", "", "." }) && latino != "1") { return false; } //("3F_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            if (ValidateIn(srhc, new string[] { "9", "", "." })) { return false; } //("4A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            if (ValidateIn(srhb, new string[] { "9", "", "." })) { return false; } //("4B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            if (ValidateIn(srd, new string[] { "9", "", "." })) { return false; } //("4C_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            if (ValidateIn(srha, new string[] { "9", "", "." })) { return false; } //("4D_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            if (ValidateIn(hcmeds, new string[] { "9", "", "." })) { return false; } //("5A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            if (ValidateIn(hbpmeds, new string[] { "9", "", "." })) { return false; } //("5B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            if (ValidateIn(dmeds, new string[] { "9", "", "." })) { return false; } //("5C_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_dmeds == "5C_1,";
            if (ValidateIn(fruit, new string[] { "99", "", "." })) { return false; } //("7A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_fruit == "7A_1,";
            if (fruit_num > 50 && !ValidateIn(fruit, new string[] { "88", "99" })) { return false; } //("7A_3", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_fruit == strip(error_fruit)|| "7A_3,";
            if (ValidateIn(vegetables, new string[] { "", ".", "99" })) { return false; } //("7B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_vegetables == "7B_1,";
            if (vegetables_num > 15 && !ValidateIn(vegetables, new string[] { "88", "99" })) { return false; } //("7B_3", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_vegetables == strip(error_vegetables)|| "7B_3,";
            if (ValidateIn(fish, new string[] { "9", "", "." })) { return false; } //("7C_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_fish == "7C_1,";
            if (ValidateIn(grains, new string[] { "9", "", "." })) { return false; } //("7D_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_grains == "7D_1,";
            if (ValidateIn(sugar, new string[] { "9", "", "." })) { return false; } //("7E_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_sugar == "7E_1,";
            if (ValidateIn(pamod, new string[] { "999", "", "." })) { return false; } //("8A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_pamod == "8A_1,";
            if (ValidateIn(pavig, new string[] { "999", "", "." })) { return false; } //("8B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_pavig == "8B_1,";
            if (ValidateIn(smoker, new string[] { "9", "", "." })) { return false; } //("9A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_smoker == "9A_1,";
            if (ValidateIn(height, new string[] { "99", "", "." })) { return false; } //("11A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_height == strip(error_height)|| "11A_1,";
            if (((height_num < 48 && height != ".") || height_num > 76) && !ValidateIn(height, new string[] { "77", "88", "99" })) { return false; } //("11A_2", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_height == strip(error_height)|| "11A_2,";
            if (ValidateIn(height, new string[] { "77" })) { return false; } //("11A_4", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_height == "11A_4,";
            if (ValidateIn(height, new string[] { "88" })) { return false; } //("11A_5", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_height == strip(error_height)|| "11A_5,";
            if (ValidateIn(weight, new string[] { "999", "", "." })) { return false; } //("11B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_weight == "11B_1,";
            if (((weight_num < 74 && weight != ".") | weight_num > 460) & !ValidateIn(weight, new string[] { "777", "888", "999" })) { return false; } //("11B_2", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_weight == strip(error_weight)|| "11B_2,";
            if (ValidateIn(bpdatestr, new string[] { "", "." }) | bpdatestr == ".") { return false; } //("12A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_bpdate == strip(error_bpdate)|| "12A_1,";
            if (ValidateIn(sbp1, new string[] { "999", "", "." })) { return false; } //("12B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_spbi1 == "12B_1,";
            if (sbp1 == "888") { return false; } //("12B_3", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_spbi1 == strip(error_spbi1)|| "12B_3,";
            if (((sbp1_num < 74 && sbp1 != ".") || sbp1_num > 260) && !ValidateIn(sbp1, new string[] { "777", "888", "999" })) { return false; } //("12B_2", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_spbi1 == strip(error_spbi1)|| "12B_2,";
            if (sbp1 == "777") { return false; } //("12B_5", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_spbi1 == strip(error_spbi1)|| "12B_5,";
            if (ValidateIn(totchol, new string[] { "999", "", "." })) { return false; } //("14B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_totchol == "14B_1,";
            else if (((totchol_num < 44 && totchol != ".") | totchol_num > 702) && !ValidateIn(totchol, new string[] { "777", "888", "999" })) { return false; } //("14B_2", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_totchol == strip(error_totchol)|| "14B_2,";
            return true;
        }

        private bool IsBpPlusRow()
        {
            // All of the following must be present and valid in order for this record to be a bpplus row
            if (IsMissing(mybstr) && ValidateIn(itype, new string[] { "1", "2" })) 
            { 
                return false; 
            } //("3D_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            if (ValidateIn(srhc, new string[] { "9", "", "." })) 
            { 
                return false; 
            } //("4A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            if (ValidateIn(srhb, new string[] { "9", "", "." })) 
            { 
                return false; 
            } //("4B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            if (ValidateIn(srd, new string[] { "9", "", "." })) 
            { 
                return false; 
            } //("4C_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            if (ValidateIn(hcmeds, new string[] { "9", "", "." })) 
            { 
                return false; 
            } //("5A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            if (ValidateIn(hbpmeds, new string[] { "9", "", "." })) 
            { 
                return false; 
            } //("5B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            if (ValidateIn(dmeds, new string[] { "9", "", "." })) 
            { 
                return false; 
            } //("5C_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_dmeds == "5C_1,";
            if (ValidateIn(smoker, new string[] { "9", "", "." })) 
            { 
                return false; 
            } //("9A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_smoker == "9A_1,";
            if (ValidateIn(height, new string[] { "99", "", "." })) 
            { 
                return false; 
            } //("11A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_height == strip(error_height)|| "11A_1,";
            if (((height_num < 48 && height != ".") || height_num > 76) && !ValidateIn(height, new string[] { "77", "88", "99" })) 
            { 
                return false; 
            } //("11A_2", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_height == strip(error_height)|| "11A_2,";
            if (ValidateIn(height, new string[] { "77" })) 
            { 
                return false; 
            } //("11A_4", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_height == "11A_4,";
            if (ValidateIn(height, new string[] { "88" })) 
            { 
                return false; 
            } //("11A_5", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_height == strip(error_height)|| "11A_5,";
            if (ValidateIn(weight, new string[] { "999", "", "." })) 
            { 
                return false; 
            } //("11B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_weight == "11B_1,";
            if (((weight_num < 74 && weight != ".") | weight_num > 460) & !ValidateIn(weight, new string[] { "777", "888", "999" })) 
            { 
                return false; 
            } //("11B_2", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_weight == strip(error_weight)|| "11B_2,";
            if (ValidateIn(sbp1, new string[] { "999", "", "." })) 
            { 
                return false; 
            } //("12B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_spbi1 == "12B_1,";
            if (sbp1 == "888") 
            { 
                return false; 
            } //("12B_3", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_spbi1 == strip(error_spbi1)|| "12B_3,";
            if (((sbp1_num < 74 && sbp1 != ".") || sbp1_num > 260) && !ValidateIn(sbp1, new string[] { "777", "888", "999" })) 
            { 
                return false; 
            } //("12B_2", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_spbi1 == strip(error_spbi1)|| "12B_2,";
            if (sbp1 == "777") 
            { 
                return false; 
            } //("12B_5", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_spbi1 == strip(error_spbi1)|| "12B_5,";
            if (ValidateIn(dbp1, new string[] { "999", "", "." })) 
            { 
                return false; 
            } //rror_dbp1 == "12C_1,";
            if (dbp1 == "888") 
            { 
                return false; 
            } //rror_dbp1 == strip(error_dbp1)|| "12C_3,";
            if (((dbp1_num < 2 && dbp1 != ".") || dbp1_num > 156) && !ValidateIn(dbp1, new string[] { "777", "888", "999" })) 
            { 
                return false; 
            } //rror_dbp1 == strip(error_dbp1)|| "12C_2,";
            if ((dbp1_num >= 2 && dbp1_num <= 12) || (dbp1_num >= 122 && dbp1_num <= 156)) 
            { 
                return false; 
            } //rror_dbp1 == strip(error_dbp1)|| "12C_4,";
            if (dbp1 == "777") 
            { 
                return false; 
            } //rror_dbp1 == strip(error_dbp1)|| "12C_5,";
            if (ValidateIn(dbp1, new string[] { "777", "888", "999" }) && !ValidateIn(dbp2, new string[] { "777", "888", "999" })) 
            { 
                return false; 
            } //rror_dbp1 == strip(error_dbp1)|| "12C_6,";
            if ((sbp1 == "777" && dbp1 != "777") || (sbp1 != "777" && dbp1 == "777")) 
            { 
                return false; 
            } //rror_dbp1 == strip(error_dbp1)|| "12C_7,";

            return true;
        }

        private bool IsCompleteRow()
        {
            // All of the following must be present and valid in order for this record to be a complete row
            if (IsMissing(mybstr) && ValidateIn(itype, new string[] { "1", "2" }))
            {
                return false;
            } //("3D_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            if (ValidateIn(srhc, new string[] { "9", "", "." })) 
            { 
                return false; 
            } //("4A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            if (ValidateIn(srhb, new string[] { "9", "", "." })) 
            { 
                return false; 
            } //("4B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            if (ValidateIn(srd, new string[] { "9", "", "." })) 
            { 
                return false; 
            } //("4C_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            if (ValidateIn(srha, new string[] { "9", "", "." })) 
            { 
                return false; 
            } //("4D_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            if (ValidateIn(hcmeds, new string[] { "9", "", "." })) 
            { 
                return false; 
            } //("5A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            if (ValidateIn(hbpmeds, new string[] { "9", "", "." })) 
            { 
                return false; 
            } //("5B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); }
            if (ValidateIn(dmeds, new string[] { "9", "", "." })) 
            { 
                return false; 
            } //("5C_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_dmeds == "5C_1,";
            if (ValidateIn(fruit, new string[] { "99", "", "." })) 
            { 
                return false; 
            } //("7A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_fruit == "7A_1,";
            if (fruit_num > 50 && !ValidateIn(fruit, new string[] { "88", "99" })) 
            { 
                return false; 
            } //("7A_3", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_fruit == strip(error_fruit)|| "7A_3,";
            if (ValidateIn(vegetables, new string[] { "", ".", "99" })) 
            { 
                return false; 
            } //("7B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_vegetables == "7B_1,";
            if (vegetables_num > 15 && !ValidateIn(vegetables, new string[] { "88", "99" })) 
            { 
                return false; 
            } //("7B_3", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_vegetables == strip(error_vegetables)|| "7B_3,";
            if (ValidateIn(fish, new string[] { "9", "", "." })) 
            { 
                return false; 
            } //("7C_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_fish == "7C_1,";
            if (ValidateIn(grains, new string[] { "9", "", "." })) 
            { 
                return false; 
            } //("7D_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_grains == "7D_1,";
            if (ValidateIn(sugar, new string[] { "9", "", "." })) 
            { 
                return false; 
            } //("7E_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_sugar == "7E_1,";
            if (ValidateIn(pamod, new string[] { "999", "", "." })) 
            { 
                return false; 
            } //("8A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_pamod == "8A_1,";
            if (ValidateIn(pavig, new string[] { "999", "", "." })) 
            { 
                return false; 
            } //("8B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_pavig == "8B_1,";
            if (ValidateIn(smoker, new string[] { "9", "", "." })) 
            { 
                return false; 
            } //("9A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_smoker == "9A_1,";
            if (ValidateIn(height, new string[] { "99", "", "." })) 
            { 
                return false; 
            } //("11A_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_height == strip(error_height)|| "11A_1,";
            if (((height_num < 48 && height != ".") || height_num > 76) && !ValidateIn(height, new string[] { "77", "88", "99" })) 
            { 
                return false; 
            } //("11A_2", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_height == strip(error_height)|| "11A_2,";
            if (ValidateIn(height, new string[] { "77" })) 
            { 
                return false; 
            } //("11A_4", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_height == "11A_4,";
            if (ValidateIn(height, new string[] { "88" })) 
            { 
                return false; 
            } //("11A_5", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_height == strip(error_height)|| "11A_5,";
            if (ValidateIn(weight, new string[] { "999", "", "." })) 
            { 
                return false; 
            } //("11B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_weight == "11B_1,";
            if (((weight_num < 74 && weight != ".") | weight_num > 460) & !ValidateIn(weight, new string[] { "777", "888", "999" })) 
            { 
                return false; 
            } //("11B_2", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_weight == strip(error_weight)|| "11B_2,";
            if (ValidateIn(sbp1, new string[] { "999", "", "." })) 
            { 
                return false; 
            } //("12B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_spbi1 == "12B_1,";
            if (sbp1 == "888") 
            { 
                return false; 
            } //("12B_3", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_spbi1 == strip(error_spbi1)|| "12B_3,";
            if (((sbp1_num < 74 && sbp1 != ".") || sbp1_num > 260) && !ValidateIn(sbp1, new string[] { "777", "888", "999" })) 
            { 
                return false; 
            } //("12B_2", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_spbi1 == strip(error_spbi1)|| "12B_2,";
            if (sbp1 == "777") 
            { 
                return false; 
            } //("12B_5", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_spbi1 == strip(error_spbi1)|| "12B_5,";

            //DBP1; 12C
            if (ValidateIn(dbp1, new string[] { "999", "", "." })) 
            { 
                return false; 
            } //rror_dbp1 == "12C_1,";
            if (dbp1 == "888") 
            { 
                return false; 
            } //rror_dbp1 == strip(error_dbp1)|| "12C_3,";
            if (((dbp1_num < 2 && dbp1 != ".") || dbp1_num > 156) && !ValidateIn(dbp1, new string[] { "777", "888", "999" })) 
            { 
                return false; 
            } //rror_dbp1 == strip(error_dbp1)|| "12C_2,";
            if ((dbp1_num >= 2 && dbp1_num <= 12) || (dbp1_num >= 122 && dbp1_num <= 156)) 
            { 
                return false; 
            } //rror_dbp1 == strip(error_dbp1)|| "12C_4,";
            if (dbp1 == "777") 
            { 
                return false; 
            } //rror_dbp1 == strip(error_dbp1)|| "12C_5,";
            if (ValidateIn(dbp1, new string[] { "777", "888", "999" }) && !ValidateIn(dbp2, new string[] { "777", "888", "999" })) 
            { 
                return false; 
            } //rror_dbp1 == strip(error_dbp1)|| "12C_6,";
            if ((sbp1 == "777" && dbp1 != "777") || (sbp1 != "777" && dbp1 == "777")) 
            { 
                return false; 
            } //rror_dbp1 == strip(error_dbp1)|| "12C_7,";

            if (ValidateIn(totchol, new string[] { "999", "", "." })) 
            { 
                return false; 
            } //("14B_1", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_totchol == "14B_1,";
            if (((totchol_num < 44 && totchol != ".") | totchol_num > 702) && !ValidateIn(totchol, new string[] { "777", "888", "999" })) 
            { 
                return false; 
            } //("14B_2", stfips, _row, bpdatestr, encodeid, screensiteid, verror); } //rror_totchol == strip(error_totchol)|| "14B_2,";

            // 15B OR 15C
            //GLUCOSE;
            var glucosePresent = true; // until proven otherwise
            if (ValidateIn(glucose, new string[] { "", "." })) { glucosePresent = false; } //rror_glucose == strip(error_glucose)|| "15B_1,";
            if (glucose == "999" && a1c == "9999") { glucosePresent = false; } //rror_glucose == strip(error_glucose)|| "15B_2,";
            if (((glucose_num < 37 && glucose != ".") | glucose_num > 571) && !ValidateIn(glucose, new string[] { "777", "888", "999" })) { glucosePresent = false; } //rror_glucose == strip(error_glucose)|| "15B_3,";
            if ((glucose_num >= 37 && glucose_num <= 50) || (glucose_num >= 275 && glucose_num <= 571)) { glucosePresent = false; } //rror_glucose == strip(error_glucose)|| "15B_4,";
            if (glucose == "777") { glucosePresent = false; } //rror_glucose == strip(error_glucose)|| "15B_5,";
            if (glucose == "888" && a1c == "8888") { glucosePresent = false; } //rror_glucose == strip(error_glucose)|| "15B_6,";
            if (fast == "2" && glucose != "999" && bpdate > toDateTime("01012015")) { glucosePresent = false; } /*Changed here for MDE Version 902*/

            //A1C;
            var a1cPresent = true; // until proven otherwise
            if (ValidateIn(a1c, new string[] { "", "." }) && ValidateIn(itype, new string[] { "1", "2" })) { a1cPresent = false; } //rror_a1c == strip(error_a1c)|| "15C_1,";
            if ((a1c_num < 2.8 && a1c != ".") || (a1c_num > 16.2 && !ValidateIn(a1c, new string[] { "7777", "8888", "9999" }))) { a1cPresent = false; } //rror_a1c == strip(error_a1c)|| "15C_2,";
            if ((a1c_num >= 2.8 && a1c_num <= 4) || (a1c_num >= 13 && a1c_num <= 16.2)) { a1cPresent = false; } //rror_a1c == strip(error_a1c)|| "15C_3,";
            if (a1c == "7777") { a1cPresent = false; } //rror_a1c == strip(error_a1c)|| "15C_4,";

            if (!glucosePresent && !a1cPresent)
                return false;

            return true;
        }

    }
}