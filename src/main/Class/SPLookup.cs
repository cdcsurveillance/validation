﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WisewomanMVCSite.Class
{
    public class SPLookup
    {

        public Dictionary<string, Dictionary<string, string>> MasterDictionary = new Dictionary<string, Dictionary<string, string>>
        {
            //This is the entire key to the Lookup Dictionary. You should add the dictionary key here as well as the dictionary name.
            {"StFIPS", _StFIPS},
            {"Type", _Type},
            {"Latino", _Latino},
            {"Race1or2", _Race1or2},
            {"Education", _Education},
            {"Language", _Language},
            {"YesNoGeneric", _YesNoGeneric},
            {"TimePer", _TimePer},
            {"MedsGeneric", _MedsGeneric},
            {"AdhereGeneric", _AdhereGeneric},
            {"BPFreq", _BPFreq},
            {"BPHome", _BPHome},
            {"BPSend", _BPSend},
            {"Fruit", _Fruit},
            {"Vegetables", _Vegetables},
            {"FishGrains", _FishGrains},
            {"Smoker", _Smoker},
            {"AlertGeneric", _AlertGeneric},
            {"RRCGeneric", _RRCGeneric},
            {"RTC", _RTC},
            {"ContactType", _ContactType},
            {"Setting", _Setting},
            {"LSPHCComp", _LSPHCComp},
            {"TobResType", _TobResType},
            {"TResComp", _TResComp}

        };

        static Dictionary<string, string> _StFIPS = new Dictionary<string, string>
        {
	     
            {"01", "Alabama (AL)"},
            {"05", "Arkansas (AR)"},
            {"06", "California (CA)"},
            {"08", "Colorado (CO)"},
            {"17", "Illinois (IL)"},
            {"18", "Indiana (IN)"},
            {"19", "Iowa (IA)"},
            {"26", "Michigan (MI)"},
            {"29", "Missouri (MO)"},
            {"31", "Nebraska (NE)"},
            {"37", "North Carolina (NC)"},
            {"41", "Oregon (OR)"},
            {"42", "Pennsylvania (PA)"},
            {"44", "Rhode Island (RI)"},
            {"45", "South Carolina (SC)"},
            {"49", "Utah (UT)"},
            {"50", "Vermont (VT)"},
            {"54", "West Virginia (WV)"},
            {"55", "Wisconsin (WI)"},
            {"85", "Southeast Alaska"},
            {"92", "Southcentral Foundation (SCF)"}
        };

        //public static bool isValidStFIPS(string word)
        //{
        //    if (_StFIPS.ContainsKey(word)){return true;}else{return false;}
        //}

        static Dictionary<string, string> _Type = new Dictionary<string, string>
        {
            {"1", "Screening"},
            {"2", "Rescreening"},
            {"3", "Follow-up – LSP/HC complete"},
            {"4", "Complete LSP/HC"},
            {"9", "Follow-up – LSP/HC incomplete"}
        };

        static Dictionary<string, string> _Latino = new Dictionary<string, string>
        {
            {"1", "​Yes"},
            {"2", "No"},
            {"7", "Unknown"},
            {"9", "No answer recorded"}
        };

        static Dictionary<string, string> _Race1or2 = new Dictionary<string, string>
        {
            {"1", "White"},
            {"2", "Black or African American"},
            {"3", "Asian"},
            {"4", "Native Hawaiian or Other Pacific Islander"},
            {"5", "American Indian or Alaska Native"},
            {"7", "Unknown"},
            {"9", "No answer recorded"}
        };

        static Dictionary<string, string> _Education = new Dictionary<string, string>
        {
            {"1", "9th grade - Participant reports that she did not attend high school"},
            {"2", "Some high school"},
            {"3", "High school graduate or equivalent of a high school diploma"},
            {"4", "Some college or higher"},
            {"7", "Don’t know/Not sure "},
            {"8", "Don’t want to answer"},
            {"9", "No answer recorded"}
        };

        static Dictionary<string, string> _Language = new Dictionary<string, string>
        {
            {"01", "English"},
            {"02", "Spanish"},
            {"03", "Arabic"},
            {"04", "Chinese"},
            {"05", "French"},
            {"06", "Italian"},
            {"07", "Japanese"},
            {"08", "Korean"},
            {"09", "Polish"},
            {"10", "Russian"},
            {"11", "Tagalog"},
            {"12", "Vietnamese"},
            {"13", "Creole"},
            {"14", "Portuguese"},
            {"15", "Hmong"},
            {"16", "Other Language"},
            {"88", "Don’t want to answer"},
            {"99", "No answer recorded"}
        };

        static Dictionary<string, string> _YesNoGeneric = new Dictionary<string, string>
        {
            //YesNoGeneric Dictionary to be used for SRHC, SRHB, SRD, SRHA, Sugar, SaltWatch
            {"1", "Yes"},
            {"2", "No"},
            {"7", "Don’t know/Not sure"},
            {"8", "Don’t want to answer"},
            {"9", "No answer recorded"}
        };

        static Dictionary<string, string> _TimePer = new Dictionary<string, string>
        {
            {"0", "6-month period 2 Baseline screening took place between 01/01/18 and 06/30/18"},
            {"9", "6-month period 1 Baseline screening took place between 07/01/17 and 12/31/17"},
            {"8", "6-month period 2 Baseline screening took place between 01/01/17 and 06/30/17"},
            {"7", "6-month period 1 Baseline screening took place between 07/01/16 and 12/31/16"},
            {"6", "6-month period 2 Baseline screening took place between 01/01/16 and 06/30/16"},
            {"5", "6-month period 1 Baseline screening took place between 07/01/15 and 12/31/15"},
            {"4", "6-month period 2 Baseline screening took place between 01/01/15 and 06/30/15"},
            {"3", "6-month period 1 Baseline screening took place between 07/01/14 and 12/31/14"},
            {"2", "6-month period 2 Baseline screening took place between 01/01/14 and 06/30/14"},
            {"1", "6-month period 1 Baseline screening took place between 07/01/13 and 12/31/13"}
        };

        static Dictionary<string, string> _MedsGeneric = new Dictionary<string, string>
        {
            //MedsGeneric Dictionary to be used for HCMeds, HBPMeds, DMeds
            {"1", "Yes"},
            {"2", "No"},
            {"3", "No - Could not obtain"},
            {"5", "Not Applicable"},
            {"7", "Don’t know/Not sure"},
            {"8", "Don’t want to answer"},
            {"9", "No answer recorded"}
        };

        static Dictionary<string, string> _AdhereGeneric = new Dictionary<string, string>
        {
            //AdhereGeneric Dictionary to be used for HCAdhere, HBPAdhere, Dadhere
            {"01", "Number of Days"},
            {"02", "Number of Days"},
            {"03", "Number of Days"},
            {"04", "Number of Days"},
            {"05", "Number of Days"},
            {"06", "Number of Days"},
            {"07", "Number of Days"},
            {"00", "None in the past 7 days"},
            {"55", "Not Applicable"},
            {"77", "Don't know/Not sure"},
            {"88", "Don't want to answer"},
            {"99", "No answer recorded"}
        };

        static Dictionary<string, string> _BPHome = new Dictionary<string, string>
        {
            {"1", "Yes"},
            {"2", "No"},
            {"3", "No - Does not know"},
            {"4", "No - Does not have equipment to measure"},
            {"5", "Not Applicable"},
            {"7", "Don’t know/Not sure"},
            {"8", "Don’t want to answer"},
            {"9", "No answer recorded"}
        };

        static Dictionary<string, string> _BPFreq = new Dictionary<string, string>
        {
            {"1", "Multiple Times Per Day"},
            {"2", "Daily Participant"},
            {"3", "A few times per week"},
            {"4", "Weekly"},
            {"5", "Monthly"},
            {"6", "Not Applicable"},
            {"7", "Don’t know/Not sure"},
            {"8", "Don’t want to answer"},
            {"9", "No answer recorded"}
        };

        static Dictionary<string, string> _BPSend = new Dictionary<string, string>
        {
            {"1", "Yes"},
            {"2", "No"},
            {"5", "Not Applicable"},
            {"7", "Don’t know/Not sure"},
            {"8", "Don’t want to answer"},
            {"9", "No answer recorded"}
        };

        static Dictionary<string, string> _Fruit = new Dictionary<string, string>
        {
            {"00", "None Participant does not consume fruit in an average day"},
            {"01", ""},
            {"02", ""},
            {"03", ""},
            {"04", ""},
            {"05", ""},
            {"06", ""},
            {"07", ""},
            {"08", ""},
            {"09", ""},
            {"10", ""},
            {"11", ""},
            {"12", ""},
            {"13", ""},
            {"14", ""},
            {"15", ""},
            {"16", ""},
            {"17", ""},
            {"18", ""},
            {"19", ""},
            {"20", ""},
            {"21", ""},
            {"22", ""},
            {"23", ""},
            {"24", ""},
            {"25", ""},
            {"26", ""},
            {"27", ""},
            {"28", ""},
            {"29", ""},
            {"30", ""},
            {"31", ""},
            {"32", ""},
            {"33", ""},
            {"34", ""},
            {"35", ""},
            {"36", ""},
            {"37", ""},
            {"38", ""},
            {"39", ""},
            {"40", ""},
            {"41", ""},
            {"42", ""},
            {"43", ""},
            {"44", ""},
            {"45", ""},
            {"46", ""},
            {"47", ""},
            {"48", ""},
            {"49", ""},
            {"50", ""},
            {"88", "Don't want to answer"},
            {"99", "No answer recorded "}
        };

        static Dictionary<string, string> _Vegetables = new Dictionary<string, string>
        {
            {"00", "None Participant does not consume fruit in an average day"},
            {"01", ""},
            {"02", ""},
            {"03", ""},
            {"04", ""},
            {"05", ""},
            {"06", ""},
            {"07", ""},
            {"08", ""},
            {"09", ""},
            {"10", ""},
            {"11", ""},
            {"12", ""},
            {"13", ""},
            {"14", ""},
            {"15", ""},
            {"88", "Don't want to answer"},
            {"99", "No answer recorded "}
        };

        static Dictionary<string, string> _FishGrains = new Dictionary<string, string>
        {
            {"1", "Yes"},
            {"2", "No"},
            {"8", "Don’t want to answer"},
            {"9", "No answer recorded"}
        };

        static Dictionary<string, string> _Smoker = new Dictionary<string, string>
        {
            {"1", "Current Smoker"},
            {"2", "Quit (1-12 months ago)"},
            {"3", "Quit (More than 12 months ago)"},
            {"4", "Never Smoked"},
            {"8", "Don’t want to answer"},
            {"9", "No answer recorded"}
        };

        static Dictionary<string, string> _AlertGeneric = new Dictionary<string, string>
         //Alert Dictionary to be used for BPAlert, BGAlert
        {
            {".", "Status is Missing - This is valid"},
            {"1", "Workup Complete"},
            {"2", "Follow up - workup by alternate provider"},
            {"3", "Not an alert reading"},
            {"8", "Client refused workup"},
            {"9", "Workup not completed, client lost to follow up"}
        };

        static Dictionary<string, string> _RRCGeneric = new Dictionary<string, string>
         //YesNoUnknown Dictionary to be used for RRCNut, RRCPA, RRCSmoke, RRCMedAdhere,
        {
            {"1", "Yes"},
            {"2", "No"},
            {"7", "Unknown"},
        };

        static Dictionary<string, string> _RTC = new Dictionary<string, string>
        {
            {"1", "Pre-contemplation"},
            {"2", "Contemplation"},
            {"3", "Preparation"},
            {"4", "Action"},
            {"5", "Maintenance"},
            {"8", "Refused"},
            {"9", "No answer recorded"}
        };

        static Dictionary<string, string> _ContactType = new Dictionary<string, string>
        {
            {"1", "Face-to-face"},
            {"2", "Phone"},
            {"3", "Smart phone/tablet Application"},
            {"4", "Evidence that mailed materials were opened and reviewed"},
            {"5", "Evidence that audiotape or DVD was opened and reviewed"},
            {"6", "Evidence that non-interactive computer base session was completed"},
            {"7", "Evidenced that interactive computer base session was completed"},
            {"0", "Other"},
            {"9", "No answer recorded"}
        };

        static Dictionary<string, string> _Setting = new Dictionary<string, string>
        {
            {"1", "Individual"},
            {"2", "Group"},
            {"3", "Combination"},
            {"9", "No answer recorded"},
        };

        static Dictionary<string, string> _LSPHCComp = new Dictionary<string, string>
        {
            {"1", "Yes - Lifestype Program/Health Coaching is Complete"},
            {"2", "No - Lifestype Program/Health Coaching is still in progress"},
            {"3", "No - Withdrawal/Discontinued"},
            {"9", "No answer recorded"},
        };

        static Dictionary<string, string> _TobResType = new Dictionary<string, string>
        {
            {"1", "Quit Line"},
            {"2", "Community-based tobacco program"},
            {"3", "Other tobacco cessation resources"},
            {"9", "No answer recorded"},
        };

        static Dictionary<string, string> _TResComp = new Dictionary<string, string>
        {
            {"1", "Yes - Completed tabacco cessation activity"},
            {"2", "No - Partially completed tobacco cessation activity"},
            {"3", "No - Withdraw from tobacco cessation activity when reached"},
            {"4", "No - Could not reach to conduct tobacco cessation activity"},
            {"9", "No answer recorded"},
        };
    }
}