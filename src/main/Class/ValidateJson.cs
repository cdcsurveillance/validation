﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using WisewomanMVCSite.Class;
using WWValidationApi.Class;
using WWValidationApi.JsonModels;

namespace WWValidationApi.Actions
{
    public class ValidateJson
    {
        public Report Validate(string jsonString)
        {
            RootObject rootObject = JsonConvert.DeserializeObject<RootObject>(jsonString);
            List<MDE> mde = MapFields(rootObject, "", "");
            List<SPValidationError> Error_Collection = new List<SPValidationError>();
            List<MDEMetrics> mdeMetrics = new List<MDEMetrics>();

            //*************BEGIN ORIGINAL VALIDATION ROUTINE**********************
            int i = 0;

            int totalrowcount = mde.Count;
            int validrowcount = 0;
            foreach (var MDE_Record in mde)
            {
                i++;
                int _row = i;

                SPValidation valObject = new SPValidation(MDE_Record, MDE_Record.rownum, totalrowcount);
                SPValidationError verror = new SPValidationError();
                valObject.ValidateMDE(Error_Collection);
                MDE_Record.isvalid = valObject.isvalidrow;
                MDE_Record.IsComplete = valObject.iscompleterow;
                MDE_Record.BPPlus = valObject.isbpplusrow;

                mdeMetrics.Add(new MDEMetrics
                {
                    RowNum = MDE_Record.rownum,
                    IsValid = MDE_Record.isvalid,
                    IsBPPlus = MDE_Record.BPPlus,
                    IsComplete = MDE_Record.IsComplete
                });

                if (valObject.isvalidrow)
                { validrowcount++; }

            }

            var completeRecords = mde
                .Count(x => x.IsComplete);

            var firstBPDateRecord = Error_Collection
                .Where(x => x.dtBPDate != null)
                .OrderBy(x => x.dtBPDate)
                .First();

            var lastBPDateRecord = Error_Collection
                .Where(x => x.dtBPDate != null)
                .OrderByDescending(x => x.dtBPDate)
                .First();

            var totalErrorCount = Error_Collection
                .Count(x => x.ValidationType.Equals("Error"));

            ///// ADDED
            var totalCompleteErrorCount = Error_Collection
                .Count(x => x.ValidationType.Equals("Error")
                && !x.InvalidRow
                && !x.MDEFieldName.Contains("_f"));

            //// ADDED
            var totalCompleteRecords = Error_Collection
                .Count(x => !x.InvalidRow);

            System.Diagnostics.Debug.WriteLine("===> totalCompleteErrorCount = " + totalCompleteErrorCount);
            System.Diagnostics.Debug.WriteLine("===> totalCompleteRecords = " + totalCompleteRecords);

            var totalQualityChecks = Error_Collection
                .Count(x => x.ValidationType.Equals("Quality Check"));

            var totalFormatErrors = Error_Collection
                .Count(x => x.ValidationType.Equals("Format Error"));

            var totalNumberOfRecordsWithEdits = Error_Collection
                .GroupBy(x => x.Row)
                .Distinct()
                .Count();

            float errorRate;
            if (totalCompleteRecords == 0) // avoid division by zero
                errorRate = (totalCompleteErrorCount == 0) ? 0 : 100;
            else
                errorRate = (float) totalCompleteErrorCount / (totalCompleteRecords * 84) * 100;

            var duplicateCount = mde
                .GroupBy(x => new { x.screensiteid, x.encodeid, x.bpdate })
                .Where(x => x.Count() > 1)
                .Select(x => x.Key)
                .Count();

            var totalEdits = Error_Collection.Count();

            var invalidRecords = mde
                .Count(x => !x.isvalid && !x.BPPlus);

            var uniqueRecords = mde
                .Distinct()
                .Count();

            var exactDuplicates = totalrowcount - uniqueRecords;

            var bpPlusRecords = mde
                .Count(x => !x.IsComplete && x.BPPlus);

            var recordsWithoutEdits = totalrowcount - totalNumberOfRecordsWithEdits;

            var incompleteCount = totalrowcount - completeRecords - bpPlusRecords;

            var totalComplete = totalrowcount - invalidRecords - bpPlusRecords;
            
            var totalEditsOnCompleteRecords = mde       // outer sequence
                .Join(Error_Collection,                 // inner sequence 
                      mderecord => mderecord.rownum,    // outerKeySelector
                      error => error.Row,               // innerKeySelector
                      (mderecord, error) => new         // result selector
                      {
                          mderecord,
                          error
                      })
                .Where(o => o.mderecord.IsComplete)
                .Select(o => o.error)
                .Count();

            var totalEditsOnRecords = totalErrorCount + totalQualityChecks;

            var baselineScreenings = mde
                .Count(x => x.type.Equals("1"));

            var rescreenings = mde
                .Count(x => x.type.Equals("2"));

            var followUpScreenings = mde
                .Count(x => x.type.Equals("3") || x.type.Equals("4"));

            var lifestyleProgramAndHealthCoachingSessions = mde
                .Count(x => x.lsphcrec.Length > 0);

            var validationSummary = new ValidationSummary
            {
                CompleteRecords = completeRecords,
                IncompleteRecords = incompleteCount,
                TotalMDERecords = totalrowcount,
                TotalQualityChecks = totalQualityChecks,
                TotalRecordsWithEdits = totalNumberOfRecordsWithEdits,
                TotalErrorCount = totalErrorCount,
                ErrorRate = errorRate,
                TotalEdits = totalEdits,
                InvalidRecords = invalidRecords,
                TotalFormatErrors = totalFormatErrors,
                BPPlusRecords = bpPlusRecords,
                RecordsWithoutEdits = recordsWithoutEdits,
                FirstBPDate = firstBPDateRecord.dtBPDate,
                LastBPDate = lastBPDateRecord.dtBPDate,
                TotalEditsOnCompleteRecords = totalEditsOnCompleteRecords,
                Duplicates = duplicateCount,
                ExactDuplicates = exactDuplicates,
                TotalComplete = totalComplete,
                TotalEditsOnRecords = totalEditsOnRecords,
                BaselineScreenings = baselineScreenings,
                Rescreenings = rescreenings,
                FollowUpScreenings = followUpScreenings,
                LifestyleProgramAndHealthCoachingSessions = lifestyleProgramAndHealthCoachingSessions
            };

            var report = new Report
            {
                Summary = validationSummary,
                Errors = Error_Collection,
                Metrics = mdeMetrics
            };

            return report;
        }

        public List<MDE> MapFields(RootObject rootObject, string sUser, string sState)
        {
            var MDE_Collection = new List<MDE>();
            int iRowNum = 0;
            rootObject.rows.ForEach(row =>
            {
                iRowNum += 1;
                MDE mde_object = new MDE();
                mde_object.rownum = iRowNum;
                mde_object.addedbyuser = sUser;
                mde_object.addedbystate = sState;
                mde_object.mdever = row.FieldValue(1, 0);
                mde_object.stfips = row.FieldValue(2, 0);
                mde_object.hdansi = row.FieldValue(3, 0);
                mde_object.enrollsiteid = row.FieldValue(4, 0);
                mde_object.screensiteid = row.FieldValue(5, 0);
                mde_object.timeper = row.FieldValue(6, 0);
                mde_object.nscreen = row.FieldValue(7, 0);
                mde_object.type = row.FieldValue(8, 0);
                mde_object.type_f = row.FieldValue(8, 1);
                mde_object.encodeid = row.FieldValue(9, 0);
                mde_object.resansi = row.FieldValue(10, 0);
                mde_object.zip = row.FieldValue(11, 0);
                mde_object.myb = row.FieldValue(12, 0);
                mde_object.latino = row.FieldValue(13, 0);
                mde_object.race1 = row.FieldValue(14, 0);
                mde_object.race2 = row.FieldValue(15, 0);
                mde_object.education = row.FieldValue(16, 0);
                mde_object.language = row.FieldValue(17, 0);
                mde_object.srhc = row.FieldValue(18, 0);
                mde_object.srhc_f = row.FieldValue(18, 1);
                mde_object.srhb = row.FieldValue(19, 0);
                mde_object.srhb_f = row.FieldValue(19, 1);
                mde_object.srd = row.FieldValue(20, 0);
                mde_object.srd_f = row.FieldValue(20, 1);
                mde_object.srha = row.FieldValue(21, 0);
                mde_object.srha_f = row.FieldValue(21, 1);
                mde_object.hcmeds = row.FieldValue(22, 0);
                mde_object.hcmeds_f = row.FieldValue(22, 1);
                mde_object.hbpmeds = row.FieldValue(23, 0);
                mde_object.hbpmeds_f = row.FieldValue(23, 1);
                mde_object.dmeds = row.FieldValue(24, 0);
                mde_object.dmeds_f = row.FieldValue(24, 1);
                mde_object.hcadhere = row.FieldValue(25, 0);
                mde_object.hcadhere_f = row.FieldValue(25, 1);
                mde_object.hbpadhere = row.FieldValue(26, 0);
                mde_object.hbpadhere_f = row.FieldValue(26, 1);
                mde_object.dadhere = row.FieldValue(27, 0);
                mde_object.dadhere_f = row.FieldValue(27, 1);
                mde_object.bphome = row.FieldValue(28, 0);
                mde_object.bphome_f = row.FieldValue(28, 1);
                mde_object.bpfreq = row.FieldValue(29, 0);
                mde_object.bpfreq_f = row.FieldValue(29, 1);
                mde_object.bpsend = row.FieldValue(30, 0);
                mde_object.bpsend_f = row.FieldValue(30, 1);
                mde_object.fruit = row.FieldValue(31, 0);
                mde_object.fruit_f = row.FieldValue(31, 1);
                mde_object.vegetables = row.FieldValue(32, 0);
                mde_object.vegetables_f = row.FieldValue(32, 1);
                mde_object.fish = row.FieldValue(33, 0);
                mde_object.fish_f = row.FieldValue(33, 1);
                mde_object.grains = row.FieldValue(34, 0);
                mde_object.grains_f = row.FieldValue(34, 1);
                mde_object.sugar = row.FieldValue(35, 0);
                mde_object.sugar_f = row.FieldValue(35, 1);
                mde_object.saltwatch = row.FieldValue(36, 0);
                mde_object.saltwatch_f = row.FieldValue(36, 1);
                mde_object.pamod = row.FieldValue(37, 0);
                mde_object.pamod_f = row.FieldValue(37, 1);
                mde_object.pavig = row.FieldValue(38, 0);
                mde_object.pavig_f = row.FieldValue(38, 1);
                mde_object.smoker = row.FieldValue(39, 0);
                mde_object.smoker_f = row.FieldValue(39, 1);
                mde_object.sechand = row.FieldValue(40, 0);
                mde_object.sechand_f = row.FieldValue(40, 1);
                mde_object.qolph = row.FieldValue(41, 0);
                mde_object.qolph_f = row.FieldValue(41, 1);
                mde_object.qolmh = row.FieldValue(42, 0);
                mde_object.qolmh_f = row.FieldValue(42, 1);
                mde_object.qoleffect = row.FieldValue(43, 0);
                mde_object.qoleffect_f = row.FieldValue(43, 1);
                mde_object.height = row.FieldValue(44, 0);
                mde_object.weight = row.FieldValue(45, 0);
                mde_object.weight_f = row.FieldValue(45, 1);
                mde_object.waist = row.FieldValue(46, 0);
                mde_object.waist_f = row.FieldValue(46, 1);
                mde_object.hip = row.FieldValue(47, 0);
                mde_object.hip_f = row.FieldValue(47, 1);
                mde_object.bpdate = row.FieldValue(48, 0);
                mde_object.bpdate_f = row.FieldValue(48, 1);
                mde_object.sbp1 = row.FieldValue(49, 0);
                mde_object.sbp1_f = row.FieldValue(49, 1);
                mde_object.dbp1 = row.FieldValue(50, 0);
                mde_object.dbp1_f = row.FieldValue(50, 1);
                mde_object.sbp2 = row.FieldValue(51, 0);
                mde_object.sbp2_f = row.FieldValue(51, 1);
                mde_object.dbp2 = row.FieldValue(52, 0);
                mde_object.dbp2_f = row.FieldValue(52, 1);
                mde_object.fast = row.FieldValue(53, 0);
                mde_object.fast_f = row.FieldValue(53, 1);
                mde_object.tcdate = row.FieldValue(54, 0);
                mde_object.tcdate_f = row.FieldValue(54, 1);
                mde_object.totchol = row.FieldValue(55, 0);
                mde_object.totchol_f = row.FieldValue(55, 1);
                mde_object.hdl = row.FieldValue(56, 0);
                mde_object.hdl_f = row.FieldValue(56, 1);
                mde_object.ldl = row.FieldValue(57, 0);
                mde_object.ldl_f = row.FieldValue(57, 1);
                mde_object.trigly = row.FieldValue(58, 0);
                mde_object.trigly_f = row.FieldValue(58, 1);
                mde_object.bgdate = row.FieldValue(59, 0);
                mde_object.bgdate_f = row.FieldValue(59, 1);
                mde_object.glucose = row.FieldValue(60, 0);
                mde_object.glucose_f = row.FieldValue(60, 1);
                mde_object.a1c = row.FieldValue(61, 0);
                mde_object.a1c_f = row.FieldValue(61, 1);
                mde_object.bpalert = row.FieldValue(62, 0);
                mde_object.bpalert_f = row.FieldValue(62, 1);
                mde_object.bpdidate = row.FieldValue(63, 0);
                mde_object.bpdidate_f = row.FieldValue(63, 1);
                mde_object.bgalert = row.FieldValue(64, 0);
                mde_object.bgalert_f = row.FieldValue(64, 1);
                mde_object.bgdidate = row.FieldValue(65, 0);
                mde_object.bgdidate_f = row.FieldValue(65, 1);
                mde_object.rrcdate = row.FieldValue(66, 0);
                mde_object.rrcdate_f = row.FieldValue(66, 1);
                mde_object.rrccomplete = row.FieldValue(67, 0);
                mde_object.rrccomplete_f = row.FieldValue(67, 1);
                mde_object.rrcnut = row.FieldValue(68, 0);
                mde_object.rrcnut_f = row.FieldValue(68, 1);
                mde_object.rrcpa = row.FieldValue(69, 0);
                mde_object.rrcpa_f = row.FieldValue(69, 1);
                mde_object.rrcsmoke = row.FieldValue(70, 0);
                mde_object.rrcsmoke_f = row.FieldValue(70, 1);
                mde_object.rrcmedadhere = row.FieldValue(71, 0);
                mde_object.rrcmedadhere_f = row.FieldValue(71, 1);
                mde_object.rtcdate = row.FieldValue(72, 0);
                mde_object.rtcdate_f = row.FieldValue(72, 1);
                mde_object.rtc = row.FieldValue(73, 0);
                mde_object.rtc_f = row.FieldValue(73, 1);
                mde_object.refdate = row.FieldValue(74, 0);
                mde_object.refdate2 = row.FieldValue(74, 1);
                mde_object.lsphcrec = row.FieldValue(75, 0);
                mde_object.intervention = row.FieldValue(76, 0);
                mde_object.intervention2 = row.FieldValue(76, 1);
                mde_object.intervention3 = row.FieldValue(76, 2);
                mde_object.intervention4 = row.FieldValue(76, 3);
                mde_object.intervention5 = row.FieldValue(76, 4);
                mde_object.intervention6 = row.FieldValue(76, 5);
                mde_object.intervention7 = row.FieldValue(76, 6);
                mde_object.intervention8 = row.FieldValue(76, 7);
                mde_object.intervention9 = row.FieldValue(76, 8);
                mde_object.intervention10 = row.FieldValue(76, 9);
                mde_object.intervention11 = row.FieldValue(76, 10);
                mde_object.intervention12 = row.FieldValue(76, 11);
                mde_object.intervention13 = row.FieldValue(76, 12);
                mde_object.intervention14 = row.FieldValue(76, 13);
                mde_object.intervention15 = row.FieldValue(76, 14);
                mde_object.intervention16 = row.FieldValue(76, 15);
                mde_object.lsphcid = row.FieldValue(77, 0);
                mde_object.lsphcid2 = row.FieldValue(77, 1);
                mde_object.lsphcid3 = row.FieldValue(77, 2);
                mde_object.lsphcid4 = row.FieldValue(77, 3);
                mde_object.lsphcid5 = row.FieldValue(77, 4);
                mde_object.lsphcid6 = row.FieldValue(77, 5);
                mde_object.lsphcid7 = row.FieldValue(77, 6);
                mde_object.lsphcid8 = row.FieldValue(77, 7);
                mde_object.lsphcid9 = row.FieldValue(77, 8);
                mde_object.lsphcid10 = row.FieldValue(77, 9);
                mde_object.lsphcid11 = row.FieldValue(77, 10);
                mde_object.lsphcid12 = row.FieldValue(77, 11);
                mde_object.lsphcid13 = row.FieldValue(77, 12);
                mde_object.lsphcid14 = row.FieldValue(77, 13);
                mde_object.lsphcid15 = row.FieldValue(77, 14);
                mde_object.lsphcid16 = row.FieldValue(77, 15);
                mde_object.lsphctime = row.FieldValue(78, 0);
                mde_object.lsphctime2 = row.FieldValue(78, 1);
                mde_object.lsphctime3 = row.FieldValue(78, 2);
                mde_object.lsphctime4 = row.FieldValue(78, 3);
                mde_object.lsphctime5 = row.FieldValue(78, 4);
                mde_object.lsphctime6 = row.FieldValue(78, 5);
                mde_object.lsphctime7 = row.FieldValue(78, 6);
                mde_object.lsphctime8 = row.FieldValue(78, 7);
                mde_object.lsphctime9 = row.FieldValue(78, 8);
                mde_object.lsphctime10 = row.FieldValue(78, 9);
                mde_object.lsphctime11 = row.FieldValue(78, 10);
                mde_object.lsphctime12 = row.FieldValue(78, 11);
                mde_object.lsphctime13 = row.FieldValue(78, 12);
                mde_object.lsphctime14 = row.FieldValue(78, 13);
                mde_object.lsphctime15 = row.FieldValue(78, 14);
                mde_object.lsphctime16 = row.FieldValue(78, 15);
                mde_object.contacttype = row.FieldValue(79, 0);
                mde_object.contacttype2 = row.FieldValue(79, 1);
                mde_object.contacttype3 = row.FieldValue(79, 2);
                mde_object.contacttype4 = row.FieldValue(79, 3);
                mde_object.contacttype5 = row.FieldValue(79, 4);
                mde_object.contacttype6 = row.FieldValue(79, 5);
                mde_object.contacttype7 = row.FieldValue(79, 6);
                mde_object.contacttype8 = row.FieldValue(79, 7);
                mde_object.contacttype9 = row.FieldValue(79, 8);
                mde_object.contacttype10 = row.FieldValue(79, 9);
                mde_object.contacttype11 = row.FieldValue(79, 10);
                mde_object.contacttype12 = row.FieldValue(79, 11);
                mde_object.contacttype13 = row.FieldValue(79, 12);
                mde_object.contacttype14 = row.FieldValue(79, 13);
                mde_object.contacttype15 = row.FieldValue(79, 14);
                mde_object.contacttype16 = row.FieldValue(79, 15);
                mde_object.setting = row.FieldValue(80, 0);
                mde_object.setting2 = row.FieldValue(80, 1);
                mde_object.setting3 = row.FieldValue(80, 2);
                mde_object.setting4 = row.FieldValue(80, 3);
                mde_object.setting5 = row.FieldValue(80, 4);
                mde_object.setting6 = row.FieldValue(80, 5);
                mde_object.setting7 = row.FieldValue(80, 6);
                mde_object.setting8 = row.FieldValue(80, 7);
                mde_object.setting9 = row.FieldValue(80, 8);
                mde_object.setting10 = row.FieldValue(80, 9);
                mde_object.setting11 = row.FieldValue(80, 10);
                mde_object.setting12 = row.FieldValue(80, 11);
                mde_object.setting13 = row.FieldValue(80, 12);
                mde_object.setting14 = row.FieldValue(80, 13);
                mde_object.setting15 = row.FieldValue(80, 14);
                mde_object.setting16 = row.FieldValue(80, 15);
                mde_object.lsphccomp = row.FieldValue(81, 0);
                mde_object.lsphccomp2 = row.FieldValue(81, 1);
                mde_object.lsphccomp3 = row.FieldValue(81, 2);
                mde_object.lsphccomp4 = row.FieldValue(81, 3);
                mde_object.lsphccomp5 = row.FieldValue(81, 4);
                mde_object.lsphccomp6 = row.FieldValue(81, 5);
                mde_object.lsphccomp7 = row.FieldValue(81, 6);
                mde_object.lsphccomp8 = row.FieldValue(81, 7);
                mde_object.lsphccomp9 = row.FieldValue(81, 8);
                mde_object.lsphccomp10 = row.FieldValue(81, 9);
                mde_object.lsphccomp11 = row.FieldValue(81, 10);
                mde_object.lsphccomp12 = row.FieldValue(81, 11);
                mde_object.lsphccomp13 = row.FieldValue(81, 12);
                mde_object.lsphccomp14 = row.FieldValue(81, 13);
                mde_object.lsphccomp15 = row.FieldValue(81, 14);
                mde_object.lsphccomp16 = row.FieldValue(81, 15);
                mde_object.tobresdate = row.FieldValue(82, 0);
                mde_object.tobresdate2 = row.FieldValue(82, 1);
                mde_object.tobresdate3 = row.FieldValue(82, 2);
                mde_object.tobrestype = row.FieldValue(83, 0);
                mde_object.tobrestype2 = row.FieldValue(83, 1);
                mde_object.tobrestype3 = row.FieldValue(83, 2);
                mde_object.trescomp = row.FieldValue(84, 0);
                mde_object.trescomp2 = row.FieldValue(84, 1);
                mde_object.trescomp3 = row.FieldValue(84, 2);

                MDE_Collection.Add(mde_object);
            });

            return MDE_Collection;

        }

    }

    static class RowExtension
    {
        public static string FieldValue(this Row row, int fieldNumber, int valueIndex)
        {
            Field field = row.fields.Find(f => f.fieldNumber == fieldNumber);
            if (valueIndex < field.values.Count)
            {
                return field.values[valueIndex] == null ? string.Empty : field.values[valueIndex].Trim();
            }
            return string.Empty;
        }

    }
}
