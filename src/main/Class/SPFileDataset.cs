﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace WisewomanMVCSite.Class
{
    public class SPFileDataset
    {
        public DataTable BuildDT()
        {
            DataTable table = new DataTable("MDEDataset");
            //DataColumn FileIDColumn = new DataColumn();
            //FileIDColumn.DataType = System.Type.GetType("System.Guid");
            //FileIDColumn.ColumnName = "FileID";
            //table.Columns.Add(FileIDColumn);
            table.Columns.Add("mID", typeof(Guid));
            table.Columns.Add("FileID", typeof(Guid));
            table.Columns.Add("RowNum", typeof(int));
            table.Columns.Add("MDEVer", typeof(string));
            table.Columns.Add("StFIPS", typeof(string));
            table.Columns.Add("HdANSI", typeof(string));
            table.Columns.Add("EnrollSiteID", typeof(string));
            table.Columns.Add("ScreenSiteID", typeof(string));
            table.Columns.Add("TimePer", typeof(string));
            table.Columns.Add("NScreen", typeof(string));
            table.Columns.Add("Type", typeof(string));
            table.Columns.Add("EncodeID", typeof(string));
            table.Columns.Add("ResANSI", typeof(string));
            table.Columns.Add("ZIP", typeof(string));
            table.Columns.Add("MYB", typeof(string));
            table.Columns.Add("Latino", typeof(string));
            table.Columns.Add("Race1", typeof(string));
            table.Columns.Add("Race2", typeof(string));
            table.Columns.Add("Education", typeof(string));
            table.Columns.Add("Language", typeof(string));
            table.Columns.Add("SRHC", typeof(string));
            table.Columns.Add("SRHB", typeof(string));
            table.Columns.Add("SRD", typeof(string));
            table.Columns.Add("SRHA", typeof(string));
            table.Columns.Add("HCMeds", typeof(string));
            table.Columns.Add("HBPMeds", typeof(string));
            table.Columns.Add("DMeds", typeof(string));
            table.Columns.Add("HCAdhere", typeof(string));
            table.Columns.Add("HBPAdhere", typeof(string));
            table.Columns.Add("DAdhere", typeof(string));
            table.Columns.Add("BPHome", typeof(string));
            table.Columns.Add("BPFreq", typeof(string));
            table.Columns.Add("BPSend", typeof(string));
            table.Columns.Add("Fruit", typeof(string));
            table.Columns.Add("Vegetables", typeof(string));
            table.Columns.Add("Fish", typeof(string));
            table.Columns.Add("Grains", typeof(string));
            table.Columns.Add("Sugar", typeof(string));
            table.Columns.Add("SaltWatch", typeof(string));
            table.Columns.Add("PAMod", typeof(string));
            table.Columns.Add("PAVig", typeof(string));
            table.Columns.Add("Smoker", typeof(string));
            table.Columns.Add("Sechand", typeof(string));
            table.Columns.Add("QOLPH", typeof(string));
            table.Columns.Add("QOLMH", typeof(string));
            table.Columns.Add("QOLEffect", typeof(string));
            table.Columns.Add("Height", typeof(string));
            table.Columns.Add("Weight", typeof(string));
            table.Columns.Add("Waist", typeof(string));
            table.Columns.Add("Hip", typeof(string));
            table.Columns.Add("BPDate", typeof(string));
            table.Columns.Add("SBP1", typeof(string));
            table.Columns.Add("DBP1", typeof(string));
            table.Columns.Add("SBP2", typeof(string));
            table.Columns.Add("DBP2", typeof(string));
            table.Columns.Add("Fast", typeof(string));
            table.Columns.Add("TCDate", typeof(string));
            table.Columns.Add("TotChol", typeof(string));
            table.Columns.Add("HDL", typeof(string));
            table.Columns.Add("LDL", typeof(string));
            table.Columns.Add("Trigly", typeof(string));
            table.Columns.Add("BGDate", typeof(string));
            table.Columns.Add("Glucose", typeof(string));
            table.Columns.Add("A1C", typeof(string));
            table.Columns.Add("BPAlert", typeof(string));
            table.Columns.Add("BPDiDate", typeof(string));
            table.Columns.Add("BGAlert", typeof(string));
            table.Columns.Add("BGDiDate", typeof(string));
            table.Columns.Add("RRCDate", typeof(string));
            table.Columns.Add("RRCComplete", typeof(string));
            table.Columns.Add("RRCNut", typeof(string));
            table.Columns.Add("RRCPA", typeof(string));
            table.Columns.Add("RRCSmoke", typeof(string));
            table.Columns.Add("RRCMedAdhere", typeof(string));
            table.Columns.Add("RTCDate", typeof(string));
            table.Columns.Add("RTC", typeof(string));
            table.Columns.Add("RefDate", typeof(string));
            table.Columns.Add("LSPHCRec", typeof(string));
            table.Columns.Add("Intervention", typeof(string));
            table.Columns.Add("LSPHCID", typeof(string));
            table.Columns.Add("LSPHCTime", typeof(string));
            table.Columns.Add("ContactType", typeof(string));
            table.Columns.Add("Setting", typeof(string));
            table.Columns.Add("LSPHCComp", typeof(string));
            table.Columns.Add("TobResDate", typeof(string));
            table.Columns.Add("TobResType", typeof(string));
            table.Columns.Add("TResComp", typeof(string));

            return table;
        }
    }
}