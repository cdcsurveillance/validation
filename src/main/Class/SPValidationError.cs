﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
//using WisewomanMVCSite.Models;
using Microsoft.VisualBasic;

namespace WisewomanMVCSite.Class
{
    public class SPValidationError
    {
        public Guid fileID { get; set; }
        public string ValidationType { get; set; }//The type of error, error(E), quality check (Q), or some other issue (O)
        public string MDEFieldName { get; set; } //The name of the element
        public string MDEItemNumber { get; set; } //The corresponding MDE Item/Edit #
        public string Location { get; set; } //The location of the element in the file
        public int Row { get; set; } //The row number of the element in the file
        public int Round { get; set; } //The round of the field; i.e. the index of the element in the array of values
        public string State { get; set; } //The state of the corresponding item in the file
        public string MDEValue { get; set; } //The value of the item in the file
        public string Description { get; set; } //The description of the element in the file
        public string BPDate { get; set; }
        public string EncodeID { get; set; }
        public string ScreenSiteID { get; set; }
        public string Type { get; set; }
        public string ErrorKey { get; set; }
        public bool ErrorFormat { get; set; }
        public bool InvalidRow { get; set; }
        public bool BPPlus { get; set; }
        public string Message { get; set; }
        public int ErrorCnt { get; set; }

        //public List<SPValidationError> GetValidationErrorsForFile(Guid fileID)
        //{
        //    List<SPValidationError> errors = new List<SPValidationError>();

        //    using (var db = new WisewomanDBContext())
        //    {
        //        string ssql = "";
        //        ssql += " SELECT fileID, ValidationType, MDEFieldName, MDEItemNumber, MDEItemNumber, Location, Row, State, MDEValue, Description, ";
        //        ssql += " BPDate, EncodeID, ScreenSiteID, Type, ErrorKey, Message, ErrorCnt FROM ValidationErrors WHERE FileID = @fileid ";
        //        SqlParameter fileid = new SqlParameter("fileid", fileID);
        //        var query = db.Database.SqlQuery<SPValidationError>(ssql, fileid);
        //        errors = query.ToList();
        //    }

        //    return errors;
        //}

        public DateTime? dtBPDate
        {
            get
            {

                DateTime? dtReturn = null;
                DateTime dt = new DateTime();
                string[] formats = new[] { "MMddyyyy", "MMyyyy" };
                try
                {
                    DateTime.TryParseExact(BPDate, formats, new CultureInfo("en-US"), DateTimeStyles.None, out dt);
                }
                catch
                {
                    //dt = new DateTime(2000, 01, 01);
                    //dt = null;
                }
                if (dt != DateTime.MinValue)
                {
                    dtReturn = dt;
                }

                if (dtReturn == null)
                {
                    //string s = "testing";
                }

                return dtReturn;
            }
        }
        //public string TotRowCount { get; set; } //The total number of MDE records submitted. Need this to perform performance calculation  (It is not a true error characteristic of an error).
        //public string MDEItemNumber { get; set; }
        //public string Message { get; set; }
        //public string TimePer { get; set; }
        //public string NScreen { get; set; }

        private DataTable CreateTable()
        {
            DataTable table = new DataTable("ValidationErrors");

            table.Columns.Add("veID", typeof(Guid));
            table.Columns.Add("fileID", typeof(Guid));
            table.Columns.Add("ValidationType", typeof(string));
            table.Columns.Add("MDEFieldName", typeof(string));
            table.Columns.Add("MDEItemNumber", typeof(string));
            table.Columns.Add("Location", typeof(string));
            table.Columns.Add("Row", typeof(int));
            table.Columns.Add("State", typeof(string));
            table.Columns.Add("MDEValue", typeof(string));
            table.Columns.Add("Description", typeof(string));
            table.Columns.Add("BPDate", typeof(string));
            //table.Columns.Add("dtBPDate", typeof(DateTime));

            DataColumn colDtBPDate;
            colDtBPDate = new DataColumn("dtBPDate",
                System.Type.GetType("System.DateTime"));
            colDtBPDate.AllowDBNull = true;
            table.Columns.Add(colDtBPDate);


            table.Columns.Add("EncodeID", typeof(string));
            table.Columns.Add("ScreenSiteID", typeof(string));
            table.Columns.Add("Type", typeof(string));
            table.Columns.Add("ErrorKey", typeof(string));
            table.Columns.Add("Message", typeof(string));
            table.Columns.Add("ErrorCnt", typeof(int));
            table.Columns.Add("FormatError", typeof(bool));
            table.Columns.Add("InvalidRow", typeof(bool));
            table.Columns.Add("BPPlus", typeof(bool));

            return table;
        }

        public DataTable ConvertListToDataTable(List<SPValidationError> errors)
        {
            DataTable dt = CreateTable();

            foreach (SPValidationError e in errors)
            {
                DataRow row = dt.NewRow();



                row["veID"] = Guid.NewGuid();
                row["fileID"] = e.fileID;
                row["ValidationType"] = e.ValidationType;
                row["MDEFieldName"] = e.MDEFieldName;
                row["MDEItemNumber"] = e.MDEItemNumber;
                row["Location"] = e.Location;
                row["Row"] = e.Row;
                row["State"] = e.State;
                row["MDEValue"] = e.MDEValue;
                row["Description"] = e.Description == null ? "" : (e.Description.Length > 2000 ? e.Description.Substring(0, 2000) : e.Description);
                row["BPDate"] = e.BPDate;
                //row["dtBPDate"] = e.dtBPDate;
                if (e.dtBPDate != null) 
                {
                    row["dtBPDate"] = e.dtBPDate;
                }
                else
                {
                    row["dtBPDate"] = DBNull.Value;
                }
                row["EncodeID"] = e.EncodeID;
                row["ScreenSiteID"] = e.ScreenSiteID;
                row["Type"] = e.Type;
                row["ErrorKey"] = e.ErrorKey;
                row["Message"] = e.Message == null ? "" : (e.Message.Length > 500 ? e.Message.Substring(0, 500) : e.Message);
                row["ErrorCnt"] = e.ErrorCnt;
                row["FormatError"] = e.ErrorFormat;
                row["InvalidRow"] = e.InvalidRow;
                row["BPPlus"] = e.BPPlus;
                

                dt.Rows.Add(row);
            }

            return dt;
        }
        

        //public void BulkCopyToDB(DataTable dt)
        //{
        //    string ssql = String.Empty;
        //    Guid gFileID = new System.Guid();

        //    string dbConnString = ConfigurationManager.ConnectionStrings["WisewomanDBContext"].ConnectionString;
        //    //BulkCopy records to DB
        //    using (SqlConnection conn = new SqlConnection(dbConnString))
        //    {
        //        conn.Open();

        //        try
        //        {
        //            //First DELETE existing ValidationErrors from file in DB
        //            ssql = "DELETE FROM ValidationErrors WHERE fileID = @FileID; ";  //If any Validation Erros exist on this FileID - REMOVE THEM
        //            ssql += " UPDATE MDEFile SET Validated = 1, ValidatedOn = getdate()";  //and...  Mark the file record as VALIDATED
        //            ssql += " WHERE fileID = @FileID ";
        //            using (SqlCommand cmd = new SqlCommand(ssql))
        //            {
        //                cmd.Connection = conn;
        //                cmd.CommandTimeout = 0;
        //                gFileID = new Guid(dt.Rows[0][1].ToString());
        //                cmd.Parameters.Add("@FileID", SqlDbType.UniqueIdentifier).Value = gFileID;
        //                cmd.ExecuteNonQuery();
        //            }
        //        }
        //        catch(Exception ex0)
        //        {
        //            throw ex0;
        //        }



        //        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn))
        //        {
        //            bulkCopy.DestinationTableName = "ValidationErrors";
        //            bulkCopy.BulkCopyTimeout = 0;
        //            try
        //            {
        //                //INSERT records to DB
        //                bulkCopy.WriteToServer(dt);
        //            }
        //            catch (Exception ex1)
        //            {
        //                throw ex1;
        //            }

        //        }



        //        try
        //        {
        //            string sproc = "CreateValidationSummaryRecord";
        //            using (SqlCommand cmd = new SqlCommand(sproc))
        //            {
        //                cmd.Connection = conn;
        //                cmd.CommandTimeout = 0;
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.Add("@FileID", SqlDbType.UniqueIdentifier).Value = gFileID;
        //                cmd.ExecuteNonQuery();
        //            }

        //            //ValidationSummary vs = new ValidationSummary();
        //            //vs.CreateSummaryRecord(gFileID.ToString());
        //        }
        //        catch (Exception ex2)
        //        {
        //            throw ex2;
        //        }

        //        conn.Close();



        //    }
        //}




    }
}