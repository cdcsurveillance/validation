﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using WisewomanMVCSite.Models;

namespace WisewomanMVCSite.Class
{
    public class SPValidationReport
    {
        public int iRow = 0;
        public int totalrowcount = 0;
        public int validrowcount = 0;
        public List<SPValidationError> errors;
        public List<MDE> mde;
        public string sCreatedDate = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
        public string sScreeningHeader = "Screening MDE Data Validation";
        public string sSubmissionHeader = "Report generated on " + DateTime.Now.ToShortDateString();
        public string sProgramHeader = "";
        public string sCreatedOnHeader = "Created on ";
        public string sUser = "";
        public string sState = "";


        public XLWorkbook CreateReport(Guid gFileID, string user, string state, WisewomanDBContext db)
        {
            sUser = user;
            sState = state;

            //Run validation rules against the data first 

            bool bValidated = (from f in db.MDEFile
                               where f.FileID == gFileID
                               select f.Validated).SingleOrDefault();


            if (bValidated) //IF the file has already been validated then
            {
                //you are going to need the errors collection and the mde collection

                //simply pull errors out of the DB
                SPValidationError e = new SPValidationError();
                errors = e.GetValidationErrorsForFile(gFileID);

                //for the mde collection you will pull the data and reparse
                SPValidator v = new SPValidator();
                //      Get the raw MDE data.
                string sRawData = v.GetRawFileData(gFileID);
                SPMDEParse p = new SPMDEParse();
                //      and reparse 
                mde = p.GetMDECollection(sRawData, sUser, sState);

            }
            else            //ELSE
            {
                //proceed to validate the data
                ValidateData(gFileID);
            }


            XLWorkbook xlsValidationReport = CreateValidationReport();

            return xlsValidationReport;
        }
        private void ValidateData(Guid gFileID)
        {
            //List<XmlDocument> xmlRules = GetRules();
            SPValidator v = new SPValidator();
            //Get the raw data.
            string sRawData = v.GetRawFileData(gFileID);
            SPMDEParse p = new SPMDEParse();
            //Parse the raw data
            mde = p.GetMDECollection(sRawData, sUser, sState);
            List<SPValidationError> Error_Collection = new List<SPValidationError>();

            //*************BEGIN ORIGINAL VALIDATION ROUTINE**********************
            int i = 0;

            totalrowcount = mde.Count;

            foreach (var MDE_Record in mde)
            {
                i++;
                int _row = i;

                //Validation valObject = new Validation(MDE_Record, _row, totalrowcount);
                SPValidation valObject = new SPValidation(MDE_Record, MDE_Record.rownum, totalrowcount);
                SPValidationError verror = new SPValidationError();
                valObject.ValidateMDE(Error_Collection);
                MDE_Record.isvalid = valObject.isvalidrow;
                if (valObject.isvalidrow)
                { validrowcount++; }

            }

            errors = Error_Collection;
            errors.All(e => { e.fileID = gFileID; return true; });

            SPValidationError ve = new SPValidationError();
            DataTable dt = ve.ConvertListToDataTable(errors);
            ve.BulkCopyToDB(dt);



        }
        public XLWorkbook CreateValidationReport()
        {
            // Create the workbook
            XLWorkbook workbook = new XLWorkbook();

            workbook = BuildSummarySheet(workbook, "Summary", 1);

            workbook = BuildDetailSheet(workbook, "Detail", 2);

            workbook = BuildIncompleteRecordsSheet(workbook, "Incomplete Records", 3);

            workbook = BuildDuplicateRecordsSheet(workbook, "Duplicate Records", 4);

            workbook = BuildRescreeningChecksSheet(workbook, "Rescreening Checks", 5);

            workbook = BuildFormattingChecksSheet(workbook, "Formatting Checks", 6);

            return workbook;
        }
        private XLWorkbook BuildFormattingChecksSheet(XLWorkbook wb, string sheetName, int sheetIndex)
        {
            var ws6 = wb.Worksheets.Add(sheetName, sheetIndex);

            var rangeTable6 = ws6.Range("A1:G7");

            ws6.Cell("A1").Value = sScreeningHeader;
            ws6.Cell("A1").Style.Font.SetBold();
            rangeTable6.Range("A1:C1").Merge();

            ws6.Cell("A2").Value = sSubmissionHeader;
            ws6.Cell("A2").Style.Font.SetBold();
            rangeTable6.Range("A2:C2").Merge();

            ws6.Cell("A3").Value = sProgramHeader;
            ws6.Cell("A3").Style.Font.SetBold();
            rangeTable6.Range("A3:C3").Merge();

            ws6.Cell("A4").Value = sCreatedOnHeader + " " + sCreatedDate;
            ws6.Cell("A4").Style.Font.SetBold();
            rangeTable6.Range("A4:C4").Merge();

            ws6.Cell("A7").Value = "Encode ID";
            ws6.Cell("B7").Value = "Screen Site ID";
            ws6.Cell("C7").Value = "Blood Pressure Date";
            ws6.Cell("D7").Value = "Type Value";
            ws6.Cell("E7").Value = "Field Name";
            ws6.Cell("F7").Value = "Formatting Check Description";
            ws6.Cell("G7").Value = "Value";

            rangeTable6.Range("A7:G7").Style.Font.SetBold();
            rangeTable6.Range("A7:G7").Style.Border.BottomBorder = XLBorderStyleValues.Thin;

            //Put Formatting checks here.
            //What constitutes a formatting check??
            var query = from m in mde
                        join e in errors on m.rownum equals e.Row
                        where e.ErrorFormat == true
                        select new { m.encodeid, m.screensiteid, m.bpdate, m.type, e.MDEFieldName, e.Message, e.MDEValue };

            var FormatErrors = query.ToList();


            iRow = 7;  //Data should start on row 8.
            foreach (var e in FormatErrors)
            {
                iRow += 1;
                ws6.Cell("A" + iRow.ToString()).Value = e.encodeid;
                ws6.Cell("B" + iRow.ToString()).Value = e.screensiteid;
                ws6.Cell("C" + iRow.ToString()).Value = e.bpdate;
                ws6.Cell("D" + iRow.ToString()).Value = e.type;
                ws6.Cell("E" + iRow.ToString()).Value = e.MDEFieldName;
                ws6.Cell("F" + iRow.ToString()).Value = e.Message;
                ws6.Cell("G" + iRow.ToString()).Value = e.MDEValue;
            }

            ws6.Columns().AdjustToContents();

            return wb;
        }
        private XLWorkbook BuildRescreeningChecksSheet(XLWorkbook wb, string sheetName, int sheetIndex)
        {
            var ws5 = wb.Worksheets.Add(sheetName, sheetIndex);
            var rangeTable5 = ws5.Range("A1:D10");

            ws5.Cell("A1").Value = sScreeningHeader;
            ws5.Cell("A1").Style.Font.SetBold();
            rangeTable5.Range("A1:D1").Merge();

            ws5.Cell("A2").Value = sSubmissionHeader;
            ws5.Cell("A2").Style.Font.SetBold();
            rangeTable5.Range("A2:D2").Merge();

            ws5.Cell("A3").Value = sProgramHeader;
            ws5.Cell("A3").Style.Font.SetBold();
            rangeTable5.Range("A3:D3").Merge();

            ws5.Cell("A4").Value = sCreatedOnHeader + " " + sCreatedDate;
            ws5.Cell("A4").Style.Font.SetBold();
            rangeTable5.Range("A4:D4").Merge();

            ws5.Cell("A10").Value = "Encode ID";
            ws5.Cell("B10").Value = "Screen Site ID";
            ws5.Cell("C10").Value = "Blood Pressure Date - Current";
            ws5.Cell("D10").Value = "Blood Pressure Date - Previous";

            ws5.Cell("A4").Value = sCreatedOnHeader + " " + sCreatedDate;
            ws5.Cell("A4").Style.Font.SetBold();
            rangeTable5.Range("A4:C4").Merge();

            ws5.Cell("A6").Value = "Total Number of Records Submitted:";
            rangeTable5.Range("A6:C6").Merge();
            ws5.Cell("D6").Value = mde.Count().ToString();

            ws5.Cell("A7").Value = "Total Number of Valid Records:";
            rangeTable5.Range("A7:C7").Merge();
            ws5.Cell("D7").Value = validrowcount.ToString();

            ws5.Cell("A8").Value = "Total Number of Records Submitted with BPDate Outside the 11-18 month screening window:";
            rangeTable5.Range("A8:C8").Merge();
            ws5.Cell("D8").Value = "0";

            ws5.Cell("A10").Value = "Encode ID";
            ws5.Cell("B10").Value = "Screen Site ID";
            ws5.Cell("C10").Value = "Blood Pressure Date - Current";
            ws5.Cell("D10").Value = "Blood Pressure Date - Previous";

            rangeTable5.Range("A10:D10").Style.Font.SetBold();
            rangeTable5.Range("A10:D10").Style.Border.BottomBorder = XLBorderStyleValues.Thin;

            //Put Rescreening checks here.

            ws5.Columns().AdjustToContents();

            return wb;
        }
        private XLWorkbook BuildDuplicateRecordsSheet(XLWorkbook wb, string sheetName, int sheetIndex)
        {
            var ws4 = wb.Worksheets.Add(sheetName, sheetIndex);
            var rangeTable4 = ws4.Range("A1:E10");

            ws4.Cell("A1").Value = sScreeningHeader;
            ws4.Cell("A1").Style.Font.SetBold();
            rangeTable4.Range("A1:C1").Merge();

            ws4.Cell("A2").Value = sSubmissionHeader;
            ws4.Cell("A2").Style.Font.SetBold();
            rangeTable4.Range("A2:C2").Merge();

            ws4.Cell("A3").Value = sProgramHeader;
            ws4.Cell("A3").Style.Font.SetBold();
            rangeTable4.Range("A3:C3").Merge();


            ws4.Cell("A4").Value = sCreatedOnHeader + " " + sCreatedDate;
            ws4.Cell("A4").Style.Font.SetBold();
            rangeTable4.Range("A4:C4").Merge();

            ws4.Cell("A6").Value = "Total Number of Records Submitted:";
            rangeTable4.Range("A6:C6").Merge();
            ws4.Cell("D6").Value = totalrowcount.ToString();

            ws4.Cell("A7").Value = "Total Number of Valid Records:";
            rangeTable4.Range("A7:C7").Merge();
            ws4.Cell("D7").Value = validrowcount.ToString();

            ws4.Cell("A8").Value = "Total Number of Duplicate Records Submitted:";
            rangeTable4.Range("A8:C8").Merge();
            ws4.Cell("D8").Value = "0";

            ws4.Cell("A10").Value = "Encode ID";
            ws4.Cell("B10").Value = "Screen Site ID";
            ws4.Cell("C10").Value = "MYB";
            ws4.Cell("D10").Value = "Blood Pressure Date";
            ws4.Cell("E10").Value = "Exact Duplicates";


            rangeTable4.Range("A10:E10").Style.Font.SetBold();
            rangeTable4.Range("A10:E10").Style.Border.BottomBorder = XLBorderStyleValues.Thin;

            //Put Dups here.

            ws4.Columns().AdjustToContents();

            return wb;
        }
        private XLWorkbook BuildIncompleteRecordsSheet(XLWorkbook wb, string sheetName, int sheetIndex)
        {
            var ws3 = wb.Worksheets.Add(sheetName, sheetIndex);
            var rangeTable3 = ws3.Range("A1:I10");

            ws3.Cell("A1").Value = sScreeningHeader;
            ws3.Cell("A1").Style.Font.SetBold();
            rangeTable3.Range("A1:C1").Merge();

            ws3.Cell("A2").Value = sSubmissionHeader;
            ws3.Cell("A2").Style.Font.SetBold();
            rangeTable3.Range("A2:C2").Merge();

            ws3.Cell("A3").Value = sProgramHeader;
            ws3.Cell("A3").Style.Font.SetBold();
            rangeTable3.Range("A3:C3").Merge();


            ws3.Cell("A4").Value = sCreatedOnHeader + " " + sCreatedDate;
            ws3.Cell("A4").Style.Font.SetBold();
            rangeTable3.Range("A4:C4").Merge();

            ws3.Cell("A6").Value = "Total Number of Records Submitted:";
            rangeTable3.Range("A6:C6").Merge();
            ws3.Cell("D6").Value = totalrowcount.ToString();

            ws3.Cell("A7").Value = "Total Number of Valid Records:";
            rangeTable3.Range("A7:C7").Merge();
            ws3.Cell("D7").Value = validrowcount.ToString();

            int iIncompleteRecords = (totalrowcount - validrowcount);
            ws3.Cell("A8").Value = "Total Number of Incomplete Records Submitted:";
            rangeTable3.Range("A8:C8").Merge();
            ws3.Cell("D8").Value = iIncompleteRecords.ToString();

            ws3.Cell("A10").Value = "Encode ID";
            ws3.Cell("B10").Value = "Screen Site ID";
            ws3.Cell("C10").Value = "Blood Pressure Date";
            ws3.Cell("D10").Value = "Type Value";
            ws3.Cell("E10").Value = "Field Name";
            ws3.Cell("F10").Value = "Item #";
            ws3.Cell("G10").Value = "Error/Quality Check";
            ws3.Cell("H10").Value = "Description";
            ws3.Cell("I10").Value = "Value";

            rangeTable3.Range("A10:I10").Style.Font.SetBold();
            rangeTable3.Range("A10:I10").Style.Border.BottomBorder = XLBorderStyleValues.Thin;

            var query = from m in mde
                        join e in errors on m.rownum equals e.Row
                        where m.isvalid == false
                        select new { m.encodeid, m.screensiteid, m.bpdate, m.type, e.MDEFieldName, e.ErrorKey, e.ValidationType, e.Message, m };

            var invalidRecords = query.ToList();

            iRow = 10;  //Data should start on row 11.
            foreach (var e in invalidRecords)
            {
                iRow += 1;
                ws3.Cell("A" + iRow.ToString()).Value = e.encodeid;
                ws3.Cell("B" + iRow.ToString()).Value = e.screensiteid;
                ws3.Cell("C" + iRow.ToString()).Value = e.bpdate;
                ws3.Cell("D" + iRow.ToString()).Value = e.type;
                ws3.Cell("E" + iRow.ToString()).Value = e.MDEFieldName;
                ws3.Cell("F" + iRow.ToString()).Value = e.ErrorKey;
                ws3.Cell("G" + iRow.ToString()).Value = e.ValidationType;
                ws3.Cell("H" + iRow.ToString()).Value = e.Message;
                //Get the MDE Value that failed validation
                object failedvalue;
                try
                {
                    failedvalue = typeof(MDE).GetProperty(e.MDEFieldName.ToLower()).GetValue(e.m, null);
                }
                catch (Exception)
                {
                    failedvalue = "";
                }

                //Show failed value
                ws3.Cell("I" + iRow.ToString()).Value = failedvalue.ToString();
            }

            ws3.Columns().AdjustToContents();

            return wb;
        }
        private XLWorkbook BuildSummarySheet(XLWorkbook wb, string sheetName, int sheetIndex)
        {
            var ws1 = wb.Worksheets.Add(sheetName, sheetIndex);
            var rangeTable1 = ws1.Range("A1:D19");

            ws1.Cell("A1").Value = sScreeningHeader;
            ws1.Cell("A1").Style.Font.SetBold();
            rangeTable1.Range("A1:C1").Merge();

            ws1.Cell("A2").Value = sSubmissionHeader;
            ws1.Cell("A2").Style.Font.SetBold();
            rangeTable1.Range("A2:C2").Merge();

            ws1.Cell("A3").Value = sProgramHeader;
            ws1.Cell("A3").Style.Font.SetBold();
            rangeTable1.Range("A3:C3").Merge();


            ws1.Cell("A4").Value = sCreatedOnHeader + " " + sCreatedDate;
            ws1.Cell("A4").Style.Font.SetBold();
            rangeTable1.Range("A4:C4").Merge();

            ws1.Cell("A5").Value = "First BPDate in File:";
            ws1.Cell("A5").Style.Font.SetBold();
            rangeTable1.Range("A5:C5").Merge();

            ws1.Cell("A6").Value = "Last BPDate in File:";
            ws1.Cell("A6").Style.Font.SetBold();
            rangeTable1.Range("A6:C6").Merge();

            ws1.Cell("A8").Value = "Total Number of Records Submitted:";
            rangeTable1.Range("A8:C8").Merge();
            ws1.Cell("D8").Value = mde.Count.ToString();

            ws1.Cell("A9").Value = "Total Number of Completed Records Submitted:";
            rangeTable1.Range("A9:C9").Merge();
            ws1.Cell("D9").Value = validrowcount.ToString();

            ws1.Cell("A10").Value = "Total Number of BP+ Records:";
            rangeTable1.Range("A10:C10").Merge();
            ws1.Cell("D10").Value = "??";

            int iIncompleteRecords = (totalrowcount - validrowcount);
            ws1.Cell("A11").Value = "Total Number of Incomplete Records Submitted:";
            rangeTable1.Range("A11:C11").Merge();
            ws1.Cell("D11").Value = iIncompleteRecords.ToString();

            ws1.Cell("A13").Value = "Total Number of Edits on All Records:";
            rangeTable1.Range("A13:C13").Merge();
            ws1.Cell("D13").Value = errors.Count.ToString();

            //Any row with an Error or Quality check
            //so perform a LINQ query that looks for DISTINCT rows from the errors list
            //-------------**CONFIRM WITH MATHEMATICA**---------------------//
            var distinct_rows = (from e in errors
                                 select e.Row).Distinct();
            ws1.Cell("A14").Value = "Total Number of Records with One or More Edits:";
            rangeTable1.Range("A14:C14").Merge();
            ws1.Cell("D14").Value = distinct_rows.Count().ToString();

            //Any row with an Error or Quality check
            //and the row IsValid = true
            //-------------**CONFIRM WITH MATHEMATICA**---------------------//
            var distinct_complete_edits = (from e in errors
                                           join m in mde on e.Row equals m.rownum
                                           where m.isvalid == true
                                           select new { e.Row }).Distinct();
            ws1.Cell("A15").Value = "Total Number of Screening Data Edits on Complete Records Only*:";
            rangeTable1.Range("A15:C15").Merge();
            ws1.Cell("D15").Value = distinct_complete_edits.Count().ToString();

            ws1.Cell("A16").Value = "Percent Error Rate (Screening Date Edits on Complete Records Only):";
            rangeTable1.Range("A16:C16").Merge();
            ws1.Cell("D16").Value = "??";

            ws1.Cell("A17").Value = "* Note: Edits on all records (including BP+ and Incomplete) are shown in the Summary and Detail tabs, but do not count towards the Error Rate.";
            ws1.Cell("A17").Style.Font.SetItalic();
            rangeTable1.Range("A17:C17").Merge();

            ws1.Cell("A19").Value = "Item #";
            ws1.Cell("B19").Value = "Status";
            ws1.Cell("C19").Value = "Description";
            ws1.Cell("D19").Value = "Number of Occurences";

            rangeTable1.Range("A19:D19").Style.Font.SetBold();
            rangeTable1.Range("A19:D19").Style.Border.BottomBorder = XLBorderStyleValues.Thin;

            //Grouping of Error and Quality checks 
            var unordered = from e in errors
                            group e by new { e.ErrorKey, e.ValidationType, e.Message } into err
                            select new { ItemNum = err.Key.ErrorKey, Status = err.Key.ValidationType, Description = err.Key.Message, ItemCount = err.Count() };
            //Order the list by count of occurrences.
            var ordered = from e in unordered
                          orderby e.ItemCount descending
                          select e;


            var results = ordered.ToList();
            int iRow = 0;
            iRow = 19;  //At this time the starting row is 20 for this data
            foreach (var r in results)
            {
                iRow += 1;
                ws1.Cell("A" + iRow.ToString()).Value = r.ItemNum;
                ws1.Cell("B" + iRow.ToString()).Value = r.Status;
                ws1.Cell("C" + iRow.ToString()).Value = r.Description;
                ws1.Cell("D" + iRow.ToString()).Value = r.ItemCount.ToString();

            }

            ws1.Columns().AdjustToContents();

            return wb;
        }
        private XLWorkbook BuildDetailSheet(XLWorkbook wb, string sheetName, int sheetIndex)
        {
            var ws2 = wb.Worksheets.Add(sheetName, sheetIndex);
            var rangeTable2 = ws2.Range("A1:I7");

            ws2.Cell("A1").Value = sScreeningHeader;
            ws2.Cell("A1").Style.Font.SetBold();
            rangeTable2.Range("A1:I1").Merge();

            ws2.Cell("A2").Value = sSubmissionHeader;
            ws2.Cell("A2").Style.Font.SetBold();
            rangeTable2.Range("A2:I2").Merge();

            ws2.Cell("A3").Value = sProgramHeader;
            ws2.Cell("A3").Style.Font.SetBold();
            rangeTable2.Range("A3:I3").Merge();

            ws2.Cell("A4").Value = sCreatedOnHeader + " " + sCreatedDate;
            ws2.Cell("A4").Style.Font.SetBold();
            rangeTable2.Range("A4:I4").Merge();

            ws2.Cell("A7").Value = "Encode ID";
            ws2.Cell("B7").Value = "Screen Site ID";
            ws2.Cell("C7").Value = "Blood Pressure Date";
            ws2.Cell("D7").Value = "Type Value";
            ws2.Cell("E7").Value = "Field Name";
            ws2.Cell("F7").Value = "Item #";
            ws2.Cell("G7").Value = "Error/Quality Check";
            ws2.Cell("H7").Value = "Description";
            ws2.Cell("I7").Value = "Value";

            rangeTable2.Range("A7:I7").Style.Font.SetBold();
            rangeTable2.Range("A7:I7").Style.Border.BottomBorder = XLBorderStyleValues.Thin;


            iRow = 7;  //At this time the starting row is 8 for this data
            //All validation errors
            foreach (SPValidationError e in errors)
            {
                iRow += 1;
                ws2.Cell("A" + iRow.ToString()).Value = e.EncodeID;
                ws2.Cell("B" + iRow.ToString()).Value = e.ScreenSiteID;
                ws2.Cell("C" + iRow.ToString()).Value = e.BPDate;
                ws2.Cell("D" + iRow.ToString()).Value = "1";
                ws2.Cell("E" + iRow.ToString()).Value = e.MDEFieldName;
                ws2.Cell("F" + iRow.ToString()).Value = e.ErrorKey;
                ws2.Cell("G" + iRow.ToString()).Value = e.ValidationType;
                ws2.Cell("H" + iRow.ToString()).Value = e.Message;
                ws2.Cell("I" + iRow.ToString()).Value = e.MDEValue;
            }
            //Adjust Column Widths to Fit Content
            ws2.Columns().AdjustToContents();

            return wb;
        }

    }
}