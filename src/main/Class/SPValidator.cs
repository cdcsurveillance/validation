﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml;

namespace WisewomanMVCSite.Class
{
    public class SPValidator
    {
        public string _dbConnString = ConfigurationManager.ConnectionStrings["WisewomanDBContext"].ConnectionString;

        public string GetRawFileData(Guid gFileID)
        {
            string sRawData = "";

            try
            {
                string query = "SELECT FileContent FROM MDEFile WHERE FileID = @FileID";

                using (SqlConnection conn = new SqlConnection(_dbConnString))
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    cmd.CommandTimeout = 120;
                    cmd.Parameters.Add("@FileID", SqlDbType.UniqueIdentifier).Value = gFileID;
                    conn.Open();
                    var dbValue = cmd.ExecuteScalar();
                    if (dbValue != null) { sRawData = dbValue.ToString(); }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return sRawData;
        }

        public List<XmlDocument> GetRules()
        {
            string query = "SELECT * FROM Rules";
            SqlDataReader rdr = null;
            List<XmlDocument> xmlRules = new List<XmlDocument>();

            // Save rule to DB
            using (SqlConnection conn = new SqlConnection(_dbConnString))
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                cmd.CommandTimeout = 120;
                rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    XmlDocument xml = new XmlDocument();
                    xml.LoadXml(rdr["RuleXML"].ToString());
                    xmlRules.Add(xml);
                }
                conn.Close();
            }

            return xmlRules;
        }


        public List<SPValidationError> ValidateFile(Guid gFileID)
        {
            List<XmlDocument> xmlRules = GetRules();
            string sRawData = GetRawFileData(gFileID);
            SPMDEParse p = new SPMDEParse();
            List<MDE> MDE_Collection = p.GetMDECollection(sRawData,"","");
            List<SPValidationError> Error_Collection = new List<SPValidationError>();

            //*************BEGIN ORIGINAL VALIDATION ROUTINE**********************
            int i = 0;
            int totrowcount = 0;
            int validrowcount = 0;

            foreach (var MDE_Record in MDE_Collection)
            {
                i++;
                int _row = i;
                totrowcount = MDE_Collection.Count;
                //Validation valObject = new Validation(MDE_Record, _row, oConfig, _totrowcount);
                SPValidation valObject = new SPValidation(MDE_Record, _row, totrowcount);
                SPValidationError verror = new SPValidationError();
                valObject.ValidateMDE(Error_Collection);
                if (valObject.isvalidrow)
                { validrowcount++; }
            }
            //*************END ORIGINAL VALIDATION ROUTINE**********************


            //foreach (var MDE_Record in MDE_Collection)
            //{
            //    foreach (XmlDocument xmlRule in xmlRules)
            //    {
            //        string sRule = xmlRule.InnerXml;
            //        Evaluator<MDE> eval = new Evaluator<MDE>(sRule, Storage.LoadRuleXml);
            //        try
            //        {
            //            i += 1;
            //            MDE mde = MDE_Record;
            //            mde = mde.NormalizeData(mde);
            //            Debug.WriteLine(DateTime.Now.ToLongTimeString() + " - " + i.ToString() + Environment.NewLine);
            //            bool ruleOutput = eval.Evaluate(mde);

            //            if (ruleOutput)
            //            {
            //                SPValidationError v = new SPValidationError();

            //                XmlDocument xml = new XmlDocument();
            //                xml.LoadXml(sRule); // suppose that myXmlString contains "<Names>...</Names>"

            //                string sName = xml.GetElementsByTagName("name")[0].InnerText;
            //                string sDesc = xml.GetElementsByTagName("description")[0].InnerText;

            //                char[] chars = new char[] {'|'};
            //                string[] splitter = sName.Split(chars);
            //                string sKey = splitter[0];
            //                string sField = splitter[1];

            //                splitter = sDesc.Split(chars);
            //                string sType = splitter[0];
            //                string sMessage = splitter[1];

            //                v.ValidationType = sType;
            //                v.MDEFieldName = sField;
            //                v.Row = mde.rownum.ToString();
            //                v.ErrorKey = sKey;
            //                v.Message = sMessage;
            //                v.ValidationType = sType;

            //                v.BPDate = mde.bpdate;
            //                v.EncodeID = mde.encodeid;
            //                v.ScreenSiteID = mde.screensiteid;
            //                v.Type = mde.type;

            //                Error_Collection.Add(v);


            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            string sError = ex.Message.ToString();
            //        }

            //    }
            //}

            return Error_Collection;

        }

        //*************BEGIN ORIGINAL VALIDATION ROUTINE**********************
        //int i = 0;
        //int totrowcount = 0;
        //int validrowcount = 0;

        //foreach (var MDE_Record in MDE_Collection)
        //{
        //    i++;
        //    int _row = i;
        //    totrowcount = MDE_Collection.Count;
        //    //Validation valObject = new Validation(MDE_Record, _row, oConfig, _totrowcount);
        //    Validation valObject = new Validation(MDE_Record, _row, totrowcount);
        //    SPValidationError verror = new SPValidationError();
        //    valObject.ValidateMDE(Error_Collection);
        //    if (valObject.isvalidrow)
        //    { validrowcount++; }
        //}


        //UploadMDE(Error_Collection, spfoldname, vallistitem, validrowcnt);
        //CreateErrorReport(Error_Collection, spfoldname, vallistitem, totrowcount, validrowcnt);
        //*************END ORIGINAL VALIDATION ROUTINE**********************

    }
}