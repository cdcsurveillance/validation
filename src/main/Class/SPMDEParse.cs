﻿//using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using WWValidationApi.JsonModels;

namespace WisewomanMVCSite.Class
{
    public class SPMDEParse
    {
        //public DataTable ParseFile(Dictionary<int, string> dFile)
        //{
        //    //Create DataTable to parse data into
        //    SPFileDataset fd = new SPFileDataset();
        //    DataTable dt = fd.BuildDT();
        //    Guid gFileID = System.Guid.NewGuid();
        //    foreach (KeyValuePair<int, string> row in dFile)
        //    {
        //        int RowNum = row.Key;
        //        string s = row.Value.ToString();
        //        Guid pk = System.Guid.NewGuid();
        //        dt.Rows.Add(pk, gFileID, RowNum, s.Substring(0, 3), s.Substring(3, 2), s.Substring(5, 5), s.Substring(10, 5), s.Substring(15, 10), s.Substring(25, 1), s.Substring(26, 2), s.Substring(28, 2), s.Substring(30, 15), s.Substring(45, 5), s.Substring(50, 5), s.Substring(55, 6), s.Substring(61, 1), s.Substring(62, 1), s.Substring(63, 1), s.Substring(64, 1), s.Substring(65, 2), s.Substring(67, 1), s.Substring(69, 1), s.Substring(71, 1), s.Substring(73, 1), s.Substring(75, 1), s.Substring(77, 1), s.Substring(79, 1), s.Substring(81, 2), s.Substring(85, 2), s.Substring(89, 2), s.Substring(93, 3), s.Substring(99, 1), s.Substring(101, 1), s.Substring(103, 2), s.Substring(107, 2), s.Substring(111, 1), s.Substring(113, 1), s.Substring(115, 1), s.Substring(117, 1), s.Substring(119, 3), s.Substring(125, 3), s.Substring(131, 1), s.Substring(133, 2), s.Substring(137, 2), s.Substring(141, 2), s.Substring(145, 2), s.Substring(149, 2), s.Substring(151, 3), s.Substring(157, 2), s.Substring(161, 2), s.Substring(165, 8), s.Substring(181, 3), s.Substring(187, 3), s.Substring(193, 3), s.Substring(199, 3), s.Substring(205, 1), s.Substring(207, 8), s.Substring(223, 3), s.Substring(229, 3), s.Substring(235, 3), s.Substring(241, 4), s.Substring(249, 8), s.Substring(265, 3), s.Substring(271, 4), s.Substring(279, 1), s.Substring(281, 8), s.Substring(297, 1), s.Substring(299, 8), s.Substring(315, 8), s.Substring(331, 8), s.Substring(347, 1), s.Substring(349, 1), s.Substring(351, 1), s.Substring(353, 1), s.Substring(355, 8), s.Substring(371, 1), s.Substring(373, 8), s.Substring(389, 2), s.Substring(391, 8), s.Substring(519, 10), s.Substring(679, 3), s.Substring(727, 1), s.Substring(743, 1), s.Substring(759, 1), s.Substring(775, 8), s.Substring(799, 1), s.Substring(802, 1));

        //    }

        //    return dt;
        //}
        //        public string ParseFile(StreamReader reader, Guid gFileID)
        //public string ParseFile(string sFileContent, Guid gFileID, string sUser, string sState)
        //{
        //    string s = "";

        //    var MDE_Collection = new List<MDE>();
        //    MDE_Collection = GetMDECollection(sFileContent, sUser, sState);

        //    var dtMDE = new DataTable();

        //    //Convert List of MDE objects to DataTable            
        //    dtMDE = ConvertListToDataTable(MDE_Collection, gFileID);
        //    //Save data to DB
        //    //SaveFileContent(reader);
        //    BulkCopyData(dtMDE);



        //    return s;
        //}
        //public Guid SaveFileContent(StreamReader rdr, string FileName, string FilePath)
        //{
        //    string sContent;
        //    using (rdr)
        //    {
        //        sContent = rdr.ReadToEnd();
        //    }


        //    string dbConnString = ConfigurationManager.ConnectionStrings["WisewomanDBContext"].ConnectionString;
        //    string query = " INSERT INTO MDEFile (FileID, FileName, FilePath, FileSize, FileContent) " +
        //                    " VALUES (@FileID, @FileName, @FilePath, @FileSize, @FileContent) ";
        //    Guid gFileID = Guid.NewGuid();
        //    // Save rule to DB
        //    using (SqlConnection conn = new SqlConnection(dbConnString))
        //    using (SqlCommand cmd = new SqlCommand(query, conn))
        //    {
        //        cmd.CommandTimeout = 120;
        //        cmd.Parameters.Add("@FileID", SqlDbType.UniqueIdentifier).Value = gFileID;
        //        cmd.Parameters.Add("@FileName", SqlDbType.NVarChar, 50).Value = FileName;
        //        cmd.Parameters.Add("@FilePath", SqlDbType.NVarChar, 250).Value = FilePath;
        //        cmd.Parameters.Add("@FileSize", SqlDbType.Int).Value = sContent.Length;
        //        cmd.Parameters.Add("@FileContent", SqlDbType.NVarChar, -1).Value = sContent;

        //        conn.Open();
        //        cmd.ExecuteNonQuery();
        //        conn.Close();
        //    }

            
        //    ParseFile(sContent, gFileID,"","");

        //    return gFileID;
        //}
        //public void BulkCopyData(DataTable dt)
        //{

        //    string dbConnString = ConfigurationManager.ConnectionStrings["WisewomanDBContext"].ConnectionString;
        //    //BulkCopy records to DB
        //    using (SqlConnection conn = new SqlConnection(dbConnString))
        //    {
        //        conn.Open();
        //        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conn))
        //        {
        //            bulkCopy.DestinationTableName = "MDE";
        //            try
        //            {
        //                //INSERT records to DB
        //                bulkCopy.WriteToServer(dt);
        //            }
        //            catch (Exception ex)
        //            {
        //                throw ex;
        //            }
        //        }
        //    }


        //}
        //public List<MDE> GetMDECollection(string sRawData, string sUser, string sState)
        //{
        //    var MDE_Collection = new List<MDE>();

        //    try
        //    {
        //        using (TextFieldParser parser = new TextFieldParser(new StringReader(sRawData)))
        //        {
        //            parser.TextFieldType = FieldType.FixedWidth;
        //            parser.SetFieldWidths(3, 2, 5, 5, 10, 1, 2, 1, 1, 15, 5, 5, 6, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 1, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 3, 3, 3, 3, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 2, 2, 2, 2, 8, 8, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 8, 8, 3, 3, 3, 3, 3, 3, 4, 4, 8, 8, 3, 3, 4, 4, 1, 1, 8, 8, 1, 1, 8, 8, 8, 8, 8, 8, 1, 1, 1, 1, 1, 1, 1, 1, 8, 8, 1, 1, 8, 8, 2, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 8, 8, 8, 1, 1, 1, 1, 1, 1);
        //            //Map fields from parser to List of MDE objects
        //            MDE_Collection = MapFields(parser, sUser, sState);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return MDE_Collection;
        //}
        //public DataTable ConvertListToDataTable(List<MDE> MDE_List, Guid gFileID)
        //{
        //    //Create DataTable to parse data into
        //    MDEDataTable mdt = new MDEDataTable();
        //    DataTable dt = mdt.CreateTable();
        //    //Guid gFileID = System.Guid.NewGuid();
        //    int iRow = 0;
        //    foreach (MDE mdeRow in MDE_List)
        //    {
        //        iRow += 1;
        //        DataRow row = dt.NewRow();
        //        row["MdeID"] = System.Guid.NewGuid();
        //        row["FileID"] = gFileID;
        //        row["RowNum"] = iRow;
        //        row["MDEVer"] = mdeRow.mdever;
        //        row["StFIPS"] = mdeRow.stfips;
        //        row["HdANSI"] = mdeRow.hdansi;
        //        row["EnrollSiteID"] = mdeRow.enrollsiteid;
        //        row["ScreenSiteID"] = mdeRow.screensiteid;
        //        row["TimePer"] = mdeRow.timeper;
        //        row["NScreen"] = mdeRow.nscreen;
        //        row["Type"] = mdeRow.type;
        //        row["Type_f"] = mdeRow.type_f;
        //        row["EncodeID"] = mdeRow.encodeid;
        //        row["ResANSI"] = mdeRow.resansi;
        //        row["ZIP"] = mdeRow.zip;
        //        row["MYB"] = mdeRow.myb;
        //        row["Latino"] = mdeRow.latino;
        //        row["Race1"] = mdeRow.race1;
        //        row["Race2"] = mdeRow.race2;
        //        row["Education"] = mdeRow.education;
        //        row["Language"] = mdeRow.language;
        //        row["SRHC"] = mdeRow.srhc;
        //        row["SRHC_f"] = mdeRow.srhc_f;
        //        row["SRHB"] = mdeRow.srhb;
        //        row["SRHB_f"] = mdeRow.srhb_f;
        //        row["SRD"] = mdeRow.srd;
        //        row["SRD_f"] = mdeRow.srd_f;
        //        row["SRHA"] = mdeRow.srha;
        //        row["SRHA_f"] = mdeRow.srha_f;
        //        row["HCMeds"] = mdeRow.hcmeds;
        //        row["HCMeds_f"] = mdeRow.hcmeds_f;
        //        row["HBPMeds"] = mdeRow.hbpmeds;
        //        row["HBPMeds_f"] = mdeRow.hbpmeds_f;
        //        row["DMeds"] = mdeRow.dmeds;
        //        row["DMeds_f"] = mdeRow.dmeds_f;
        //        row["HCAdhere"] = mdeRow.hcadhere;
        //        row["HCAdhere_f"] = mdeRow.hcadhere_f;
        //        row["HBPAdhere"] = mdeRow.hbpmeds;
        //        row["HBPAdhere_f"] = mdeRow.hbpmeds_f;
        //        row["DAdhere"] = mdeRow.dadhere;
        //        row["DAdhere_f"] = mdeRow.dadhere_f;
        //        row["BPHome"] = mdeRow.bphome;
        //        row["BPHome_f"] = mdeRow.bphome_f;
        //        row["BPFreq"] = mdeRow.bpfreq;
        //        row["BPFreq_f"] = mdeRow.bpfreq_f;
        //        row["BPSend"] = mdeRow.bpsend;
        //        row["BPSend_f"] = mdeRow.bpsend_f;
        //        row["Fruit"] = mdeRow.fruit;
        //        row["Fruit_f"] = mdeRow.fruit_f;
        //        row["Vegetables"] = mdeRow.vegetables;
        //        row["Vegetables_f"] = mdeRow.vegetables_f;
        //        row["Fish"] = mdeRow.fish;
        //        row["Fish_f"] = mdeRow.fish_f;
        //        row["Grains"] = mdeRow.grains;
        //        row["Grains_f"] = mdeRow.grains_f;
        //        row["Sugar"] = mdeRow.sugar;
        //        row["Sugar_f"] = mdeRow.sugar_f;
        //        row["SaltWatch"] = mdeRow.saltwatch;
        //        row["SaltWatch_f"] = mdeRow.saltwatch_f;
        //        row["PAMod"] = mdeRow.pamod;
        //        row["PAMod_f"] = mdeRow.pamod_f;
        //        row["PAVig"] = mdeRow.pavig;
        //        row["PAVig_f"] = mdeRow.pavig_f;
        //        row["Smoker"] = mdeRow.smoker;
        //        row["Smoker_f"] = mdeRow.smoker_f;
        //        row["Sechand"] = mdeRow.sechand;
        //        row["Sechand_f"] = mdeRow.sechand_f;
        //        row["QOLPH"] = mdeRow.qolph;
        //        row["QOLPH_f"] = mdeRow.qolph_f;
        //        row["QOLMH"] = mdeRow.qolmh;
        //        row["QOLMH_f"] = mdeRow.qolmh_f;
        //        row["QOLEffect"] = mdeRow.qoleffect;
        //        row["QOLEffect_f"] = mdeRow.qoleffect_f;
        //        row["Height"] = mdeRow.height;
        //        row["Weight"] = mdeRow.weight;
        //        row["Weight_f"] = mdeRow.weight_f;
        //        row["Waist"] = mdeRow.waist;
        //        row["Waist_f"] = mdeRow.waist_f;
        //        row["Hip"] = mdeRow.hip;
        //        row["Hip_f"] = mdeRow.hip_f;
        //        row["BPDate"] = mdeRow.bpdate;
        //        row["BPDate_f"] = mdeRow.bpdate_f;
        //        row["SBP1"] = mdeRow.sbp1;
        //        row["SBP1_f"] = mdeRow.sbp1_f;
        //        row["DBP1"] = mdeRow.dbp1;
        //        row["DBP1_f"] = mdeRow.dbp1_f;
        //        row["SBP2"] = mdeRow.sbp2;
        //        row["SBP2_f"] = mdeRow.sbp2_f;
        //        row["DBP2"] = mdeRow.dbp2;
        //        row["DBP2_f"] = mdeRow.dbp2_f;
        //        row["Fast"] = mdeRow.fast;
        //        row["Fast_f"] = mdeRow.fast_f;
        //        row["TCDate"] = mdeRow.tcdate;
        //        row["TCDate_f"] = mdeRow.tcdate_f;
        //        row["TotChol"] = mdeRow.totchol;
        //        row["TotChol_f"] = mdeRow.totchol_f;
        //        row["HDL"] = mdeRow.hdl;
        //        row["HDL_f"] = mdeRow.hdl_f;
        //        row["LDL"] = mdeRow.ldl;
        //        row["LDL_f"] = mdeRow.ldl_f;
        //        row["Trigly"] = mdeRow.trigly;
        //        row["Trigly_f"] = mdeRow.trigly_f;
        //        row["BGDate"] = mdeRow.bgdate;
        //        row["BGDate_f"] = mdeRow.bgdate_f;
        //        row["Glucose"] = mdeRow.glucose;
        //        row["Glucose_f"] = mdeRow.glucose_f;
        //        row["A1C"] = mdeRow.a1c;
        //        row["A1C_f"] = mdeRow.a1c_f;
        //        row["BPAlert"] = mdeRow.bpalert;
        //        row["BPAlert_f"] = mdeRow.bpalert_f;
        //        row["BPDiDate"] = mdeRow.bpdidate;
        //        row["BPDiDate_f"] = mdeRow.bpdidate_f;
        //        row["BGAlert"] = mdeRow.bgalert;
        //        row["BGAlert_f"] = mdeRow.bgalert_f;
        //        row["BGDiDate"] = mdeRow.bgdidate;
        //        row["BGDiDate_f"] = mdeRow.bgdidate_f;
        //        row["RRCDate"] = mdeRow.rrcdate;
        //        row["RRCDate_f"] = mdeRow.rrcdate_f;
        //        row["RRCComplete"] = mdeRow.rrccomplete;
        //        row["RRCComplete_f"] = mdeRow.rrccomplete_f;
        //        row["RRCNut"] = mdeRow.rrcnut;
        //        row["RRCNut_f"] = mdeRow.rrcnut_f;
        //        row["RRCPA"] = mdeRow.rrcpa;
        //        row["RRCPA_f"] = mdeRow.rrcpa_f;
        //        row["RRCSmoke"] = mdeRow.rrcsmoke;
        //        row["RRCSmoke_f"] = mdeRow.rrcsmoke_f;
        //        row["RRCMedAdhere"] = mdeRow.rrcmedadhere;
        //        row["RRCMedAdhere_f"] = mdeRow.rrcmedadhere_f;
        //        row["RTCDate"] = mdeRow.rtcdate;
        //        row["RTCDate_f"] = mdeRow.rtcdate_f;
        //        row["RTC"] = mdeRow.rtc;
        //        row["RTC_f"] = mdeRow.rtc_f;
        //        row["RefDate"] = mdeRow.refdate;
        //        row["RefDate2"] = mdeRow.refdate2;
        //        row["LSPHCRec"] = mdeRow.lsphcrec;
        //        row["Intervention"] = mdeRow.intervention;
        //        row["Intervention2"] = mdeRow.intervention2;
        //        row["Intervention3"] = mdeRow.intervention3;
        //        row["Intervention4"] = mdeRow.intervention4;
        //        row["Intervention5"] = mdeRow.intervention5;
        //        row["Intervention6"] = mdeRow.intervention6;
        //        row["Intervention7"] = mdeRow.intervention7;
        //        row["Intervention8"] = mdeRow.intervention8;
        //        row["Intervention9"] = mdeRow.intervention9;
        //        row["Intervention10"] = mdeRow.intervention10;
        //        row["Intervention11"] = mdeRow.intervention11;
        //        row["Intervention12"] = mdeRow.intervention12;
        //        row["Intervention13"] = mdeRow.intervention13;
        //        row["Intervention14"] = mdeRow.intervention14;
        //        row["Intervention15"] = mdeRow.intervention15;
        //        row["Intervention16"] = mdeRow.intervention16;
        //        row["LSPHCID"] = mdeRow.lsphcid;
        //        row["LSPHCID2"] = mdeRow.lsphcid2;
        //        row["LSPHCID3"] = mdeRow.lsphcid3;
        //        row["LSPHCID4"] = mdeRow.lsphcid4;
        //        row["LSPHCID5"] = mdeRow.lsphcid5;
        //        row["LSPHCID6"] = mdeRow.lsphcid6;
        //        row["LSPHCID7"] = mdeRow.lsphcid7;
        //        row["LSPHCID8"] = mdeRow.lsphcid8;
        //        row["LSPHCID9"] = mdeRow.lsphcid9;
        //        row["LSPHCID10"] = mdeRow.lsphcid10;
        //        row["LSPHCID11"] = mdeRow.lsphcid11;
        //        row["LSPHCID12"] = mdeRow.lsphcid12;
        //        row["LSPHCID13"] = mdeRow.lsphcid13;
        //        row["LSPHCID14"] = mdeRow.lsphcid14;
        //        row["LSPHCID15"] = mdeRow.lsphcid15;
        //        row["LSPHCID16"] = mdeRow.lsphcid16;
        //        row["LSPHCTime"] = mdeRow.lsphctime;
        //        row["LSPHCTime2"] = mdeRow.lsphctime2;
        //        row["LSPHCTime3"] = mdeRow.lsphctime3;
        //        row["LSPHCTime4"] = mdeRow.lsphctime4;
        //        row["LSPHCTime5"] = mdeRow.lsphctime5;
        //        row["LSPHCTime6"] = mdeRow.lsphctime6;
        //        row["LSPHCTime7"] = mdeRow.lsphctime7;
        //        row["LSPHCTime8"] = mdeRow.lsphctime8;
        //        row["LSPHCTime9"] = mdeRow.lsphctime9;
        //        row["LSPHCTime10"] = mdeRow.lsphctime10;
        //        row["LSPHCTime11"] = mdeRow.lsphctime11;
        //        row["LSPHCTime12"] = mdeRow.lsphctime12;
        //        row["LSPHCTime13"] = mdeRow.lsphctime13;
        //        row["LSPHCTime14"] = mdeRow.lsphctime14;
        //        row["LSPHCTime15"] = mdeRow.lsphctime15;
        //        row["LSPHCTime16"] = mdeRow.lsphctime16;
        //        row["ContactType"] = mdeRow.contacttype;
        //        row["ContactType2"] = mdeRow.contacttype2;
        //        row["ContactType3"] = mdeRow.contacttype3;
        //        row["ContactType4"] = mdeRow.contacttype4;
        //        row["ContactType5"] = mdeRow.contacttype5;
        //        row["ContactType6"] = mdeRow.contacttype6;
        //        row["ContactType7"] = mdeRow.contacttype7;
        //        row["ContactType8"] = mdeRow.contacttype8;
        //        row["ContactType9"] = mdeRow.contacttype9;
        //        row["ContactType10"] = mdeRow.contacttype10;
        //        row["ContactType11"] = mdeRow.contacttype11;
        //        row["ContactType12"] = mdeRow.contacttype12;
        //        row["ContactType13"] = mdeRow.contacttype13;
        //        row["ContactType14"] = mdeRow.contacttype14;
        //        row["ContactType15"] = mdeRow.contacttype15;
        //        row["ContactType16"] = mdeRow.contacttype16;
        //        row["Setting"] = mdeRow.setting;
        //        row["Setting2"] = mdeRow.setting2;
        //        row["Setting3"] = mdeRow.setting3;
        //        row["Setting4"] = mdeRow.setting4;
        //        row["Setting5"] = mdeRow.setting5;
        //        row["Setting6"] = mdeRow.setting6;
        //        row["Setting7"] = mdeRow.setting7;
        //        row["Setting8"] = mdeRow.setting8;
        //        row["Setting9"] = mdeRow.setting9;
        //        row["Setting10"] = mdeRow.setting10;
        //        row["Setting11"] = mdeRow.setting11;
        //        row["Setting12"] = mdeRow.setting12;
        //        row["Setting13"] = mdeRow.setting13;
        //        row["Setting14"] = mdeRow.setting14;
        //        row["Setting15"] = mdeRow.setting15;
        //        row["Setting16"] = mdeRow.setting16;
        //        row["LSPHCComp"] = mdeRow.lsphccomp;
        //        row["LSPHCComp2"] = mdeRow.lsphccomp2;
        //        row["LSPHCComp3"] = mdeRow.lsphccomp3;
        //        row["LSPHCComp4"] = mdeRow.lsphccomp4;
        //        row["LSPHCComp5"] = mdeRow.lsphccomp5;
        //        row["LSPHCComp6"] = mdeRow.lsphccomp6;
        //        row["LSPHCComp7"] = mdeRow.lsphccomp7;
        //        row["LSPHCComp8"] = mdeRow.lsphccomp8;
        //        row["LSPHCComp9"] = mdeRow.lsphccomp9;
        //        row["LSPHCComp10"] = mdeRow.lsphccomp10;
        //        row["LSPHCComp11"] = mdeRow.lsphccomp11;
        //        row["LSPHCComp12"] = mdeRow.lsphccomp12;
        //        row["LSPHCComp13"] = mdeRow.lsphccomp13;
        //        row["LSPHCComp14"] = mdeRow.lsphccomp14;
        //        row["LSPHCComp15"] = mdeRow.lsphccomp15;
        //        row["LSPHCComp16"] = mdeRow.lsphccomp16;
        //        row["TobResDate"] = mdeRow.tobresdate;
        //        row["TobResDate2"] = mdeRow.tobresdate2;
        //        row["TobResDate3"] = mdeRow.tobresdate3;
        //        row["TobResType"] = mdeRow.tobrestype;
        //        row["TobResType2"] = mdeRow.tobrestype2;
        //        row["TobResType3"] = mdeRow.tobrestype3;
        //        row["TResComp"] = mdeRow.trescomp;
        //        row["TResComp2"] = mdeRow.trescomp2;
        //        row["TResComp3"] = mdeRow.trescomp3;

        //        dt.Rows.Add(row);
        //    }

        //    return dt;
        //}
        public List<MDE> MapFields(RootObject rootObject, string sUser, string sState)
        {
            


            var MDE_Collection = new List<MDE>();
            int iRowNum = 0;
            rootObject.rows.ForEach(row =>
            {
                iRowNum += 1;
                MDE mde_object = new MDE();
                mde_object.rownum = iRowNum;
                mde_object.addedbyuser = sUser;
                mde_object.addedbystate = sState;
                mde_object.mdever = row.FieldValue(1, 0);
                mde_object.stfips = row.FieldValue(2, 0);
                mde_object.hdansi = row.FieldValue(3, 0);
                mde_object.enrollsiteid = row.FieldValue(4, 0);
                mde_object.screensiteid = row.FieldValue(5, 0);
                mde_object.timeper = row.FieldValue(6, 0);
                mde_object.nscreen = row.FieldValue(7, 0);
                mde_object.type = row.FieldValue(8, 0);
                mde_object.type_f = row.FieldValue(8, 1);
                mde_object.encodeid = row.FieldValue(9, 0);
                mde_object.resansi = row.FieldValue(10, 0);
                mde_object.zip = row.FieldValue(11, 0);
                mde_object.myb = row.FieldValue(12, 0);
                mde_object.latino = row.FieldValue(13, 0);
                mde_object.race1 = row.FieldValue(14, 0);
                mde_object.race2 = row.FieldValue(15, 0);
                mde_object.education = row.FieldValue(16, 0);
                mde_object.language = row.FieldValue(17, 0);
                mde_object.srhc = row.FieldValue(18, 0);
                mde_object.srhc_f = row.FieldValue(18, 1);
                mde_object.srhb = row.FieldValue(19, 0);
                mde_object.srhb_f = row.FieldValue(19, 1);
                mde_object.srd = row.FieldValue(20, 0);
                mde_object.srd_f = row.FieldValue(20, 1);
                mde_object.srha = row.FieldValue(21, 0);
                mde_object.srha_f = row.FieldValue(21, 1);
                mde_object.hcmeds = row.FieldValue(22, 0);
                mde_object.hcmeds_f = row.FieldValue(22, 1);
                mde_object.hbpmeds = row.FieldValue(23, 0);
                mde_object.hbpmeds_f = row.FieldValue(23, 1);
                mde_object.dmeds = row.FieldValue(24, 0);
                mde_object.dmeds_f = row.FieldValue(24, 1);
                mde_object.hcadhere = row.FieldValue(25, 0);
                mde_object.hcadhere_f = row.FieldValue(25, 1);
                mde_object.hbpadhere = row.FieldValue(26, 0);
                mde_object.hbpadhere_f = row.FieldValue(26, 1);
                mde_object.dadhere = row.FieldValue(27, 0);
                mde_object.dadhere_f = row.FieldValue(27, 1);
                mde_object.bphome = row.FieldValue(28, 0);
                mde_object.bphome_f = row.FieldValue(28, 1);
                mde_object.bpfreq = row.FieldValue(29, 0);
                mde_object.bpfreq_f = row.FieldValue(29, 1);
                mde_object.bpsend = row.FieldValue(30, 0);
                mde_object.bpsend_f = row.FieldValue(30, 1);
                mde_object.fruit = row.FieldValue(31, 0);
                mde_object.fruit_f = row.FieldValue(31, 1);
                mde_object.vegetables = row.FieldValue(32, 0);
                mde_object.vegetables_f = row.FieldValue(32, 1);
                mde_object.fish = row.FieldValue(33, 0);
                mde_object.fish_f = row.FieldValue(33, 1);
                mde_object.grains = row.FieldValue(34, 0);
                mde_object.grains_f = row.FieldValue(34, 1);
                mde_object.sugar = row.FieldValue(35, 0);
                mde_object.sugar_f = row.FieldValue(35, 1);
                mde_object.saltwatch = row.FieldValue(36, 0);
                mde_object.saltwatch_f = row.FieldValue(36, 1);
                mde_object.pamod = row.FieldValue(37, 0);
                mde_object.pamod_f = row.FieldValue(37, 1);
                mde_object.pavig = row.FieldValue(38, 0);
                mde_object.pavig_f = row.FieldValue(38, 1);
                mde_object.smoker = row.FieldValue(39, 0);
                mde_object.smoker_f = row.FieldValue(39, 1);
                mde_object.sechand = row.FieldValue(40, 0);
                mde_object.sechand_f = row.FieldValue(40, 1);
                mde_object.qolph = row.FieldValue(41, 0);
                mde_object.qolph_f = row.FieldValue(41, 1);
                mde_object.qolmh = row.FieldValue(42, 0);
                mde_object.qolmh_f = row.FieldValue(42, 1);
                mde_object.qoleffect = row.FieldValue(43, 0);
                mde_object.qoleffect_f = row.FieldValue(43, 1);
                mde_object.height = row.FieldValue(44, 0);
                mde_object.weight = row.FieldValue(45, 0);
                mde_object.weight_f = row.FieldValue(45, 1);
                mde_object.waist = row.FieldValue(46, 0);
                mde_object.waist_f = row.FieldValue(46, 1);
                mde_object.hip = row.FieldValue(47, 0);
                mde_object.hip_f = row.FieldValue(47, 1);
                mde_object.bpdate = row.FieldValue(48, 0);
                mde_object.bpdate_f = row.FieldValue(48, 1);
                mde_object.sbp1 = row.FieldValue(49, 0);
                mde_object.sbp1_f = row.FieldValue(49, 1);
                mde_object.dbp1 = row.FieldValue(50, 0);
                mde_object.dbp1_f = row.FieldValue(50, 1);
                mde_object.sbp2 = row.FieldValue(51, 0);
                mde_object.sbp2_f = row.FieldValue(51, 1);
                mde_object.dbp2 = row.FieldValue(52, 0);
                mde_object.dbp2_f = row.FieldValue(52, 1);
                mde_object.fast = row.FieldValue(53, 0);
                mde_object.fast_f = row.FieldValue(53, 1);
                mde_object.tcdate = row.FieldValue(54, 0);
                mde_object.tcdate_f = row.FieldValue(54, 1);
                mde_object.totchol = row.FieldValue(55, 0);
                mde_object.totchol_f = row.FieldValue(55, 1);
                mde_object.hdl = row.FieldValue(56, 0);
                mde_object.hdl_f = row.FieldValue(56, 1);
                mde_object.ldl = row.FieldValue(57, 0);
                mde_object.ldl_f = row.FieldValue(57, 1);
                mde_object.trigly = row.FieldValue(58, 0);
                mde_object.trigly_f = row.FieldValue(58, 1);
                mde_object.bgdate = row.FieldValue(59, 0);
                mde_object.bgdate_f = row.FieldValue(59, 1);
                mde_object.glucose = row.FieldValue(60, 0);
                mde_object.glucose_f = row.FieldValue(60, 1);
                mde_object.a1c = row.FieldValue(61, 0);
                mde_object.a1c_f = row.FieldValue(61, 1);
                mde_object.bpalert = row.FieldValue(62, 0);
                mde_object.bpalert_f = row.FieldValue(62, 1);
                mde_object.bpdidate = row.FieldValue(63, 0);
                mde_object.bpdidate_f = row.FieldValue(63, 1);
                mde_object.bgalert = row.FieldValue(64, 0);
                mde_object.bgalert_f = row.FieldValue(64, 1);
                mde_object.bgdidate = row.FieldValue(65, 0);
                mde_object.bgdidate_f = row.FieldValue(65, 1);
                mde_object.rrcdate = row.FieldValue(66, 0);
                mde_object.rrcdate_f = row.FieldValue(66, 1);
                mde_object.rrccomplete = row.FieldValue(67, 0);
                mde_object.rrccomplete_f = row.FieldValue(67, 1);
                mde_object.rrcnut = row.FieldValue(68, 0);
                mde_object.rrcnut_f = row.FieldValue(68, 1);
                mde_object.rrcpa = row.FieldValue(69, 0);
                mde_object.rrcpa_f = row.FieldValue(69, 1);
                mde_object.rrcsmoke = row.FieldValue(70, 0);
                mde_object.rrcsmoke_f = row.FieldValue(70, 1);
                mde_object.rrcmedadhere = row.FieldValue(71, 0);
                mde_object.rrcmedadhere_f = row.FieldValue(71, 1);
                mde_object.rtcdate = row.FieldValue(72, 0);
                mde_object.rtcdate_f = row.FieldValue(72, 1);
                mde_object.rtc = row.FieldValue(73, 0);
                mde_object.rtc_f = row.FieldValue(73, 1);
                mde_object.refdate = row.FieldValue(74, 0);
                mde_object.refdate2 = row.FieldValue(74, 1);
                mde_object.lsphcrec = row.FieldValue(75, 0);
                mde_object.intervention = row.FieldValue(76, 0);
                mde_object.intervention2 = row.FieldValue(76, 1);
                mde_object.intervention3 = row.FieldValue(76, 2);
                mde_object.intervention4 = row.FieldValue(76, 3);
                mde_object.intervention5 = row.FieldValue(76, 4);
                mde_object.intervention6 = row.FieldValue(76, 5);
                mde_object.intervention7 = row.FieldValue(76, 6);
                mde_object.intervention8 = row.FieldValue(76, 7);
                mde_object.intervention9 = row.FieldValue(76, 8);
                mde_object.intervention10 = row.FieldValue(76, 9);
                mde_object.intervention11 = row.FieldValue(76, 10);
                mde_object.intervention12 = row.FieldValue(76, 11);
                mde_object.intervention13 = row.FieldValue(76, 12);
                mde_object.intervention14 = row.FieldValue(76, 13);
                mde_object.intervention15 = row.FieldValue(76, 14);
                mde_object.intervention16 = row.FieldValue(76, 16);
                mde_object.lsphcid = row.FieldValue(77, 0);
                mde_object.lsphcid2 = row.FieldValue(77, 1);
                mde_object.lsphcid3 = row.FieldValue(77, 2);
                mde_object.lsphcid4 = row.FieldValue(77, 3);
                mde_object.lsphcid5 = row.FieldValue(77, 4);
                mde_object.lsphcid6 = row.FieldValue(77, 5);
                mde_object.lsphcid7 = row.FieldValue(77, 6);
                mde_object.lsphcid8 = row.FieldValue(77, 7);
                mde_object.lsphcid9 = row.FieldValue(77, 8);
                mde_object.lsphcid10 = row.FieldValue(77, 9);
                mde_object.lsphcid11 = row.FieldValue(77, 10);
                mde_object.lsphcid12 = row.FieldValue(77, 11);
                mde_object.lsphcid13 = row.FieldValue(77, 12);
                mde_object.lsphcid14 = row.FieldValue(77, 13);
                mde_object.lsphcid15 = row.FieldValue(77, 14);
                mde_object.lsphcid16 = row.FieldValue(77, 15);
                mde_object.lsphctime = row.FieldValue(78, 0);
                mde_object.lsphctime2 = row.FieldValue(78, 1);
                mde_object.lsphctime3 = row.FieldValue(78, 2);
                mde_object.lsphctime4 = row.FieldValue(78, 3);
                mde_object.lsphctime5 = row.FieldValue(78, 4);
                mde_object.lsphctime6 = row.FieldValue(78, 5);
                mde_object.lsphctime7 = row.FieldValue(78, 6);
                mde_object.lsphctime8 = row.FieldValue(78, 7);
                mde_object.lsphctime9 = row.FieldValue(78, 8);
                mde_object.lsphctime10 = row.FieldValue(78, 9);
                mde_object.lsphctime11 = row.FieldValue(78, 10);
                mde_object.lsphctime12 = row.FieldValue(78, 11);
                mde_object.lsphctime13 = row.FieldValue(78, 12);
                mde_object.lsphctime14 = row.FieldValue(78, 13);
                mde_object.lsphctime15 = row.FieldValue(78, 14);
                mde_object.lsphctime16 = row.FieldValue(78, 15);
                mde_object.contacttype = row.FieldValue(79, 0);
                mde_object.contacttype2 = row.FieldValue(79, 1);
                mde_object.contacttype3 = row.FieldValue(79, 2);
                mde_object.contacttype4 = row.FieldValue(79, 3);
                mde_object.contacttype5 = row.FieldValue(79, 4);
                mde_object.contacttype6 = row.FieldValue(79, 5);
                mde_object.contacttype7 = row.FieldValue(79, 6);
                mde_object.contacttype8 = row.FieldValue(79, 7);
                mde_object.contacttype9 = row.FieldValue(79, 8);
                mde_object.contacttype10 = row.FieldValue(79, 9);
                mde_object.contacttype11 = row.FieldValue(79, 10);
                mde_object.contacttype12 = row.FieldValue(79, 11);
                mde_object.contacttype13 = row.FieldValue(79, 12);
                mde_object.contacttype14 = row.FieldValue(79, 13);
                mde_object.contacttype15 = row.FieldValue(79, 14);
                mde_object.contacttype16 = row.FieldValue(79, 15);
                mde_object.setting = row.FieldValue(80, 0);
                mde_object.setting2 = row.FieldValue(80, 1);
                mde_object.setting3 = row.FieldValue(80, 2);
                mde_object.setting4 = row.FieldValue(80, 3);
                mde_object.setting5 = row.FieldValue(80, 4);
                mde_object.setting6 = row.FieldValue(80, 5);
                mde_object.setting7 = row.FieldValue(80, 6);
                mde_object.setting8 = row.FieldValue(80, 7);
                mde_object.setting9 = row.FieldValue(80, 8);
                mde_object.setting10 = row.FieldValue(80, 9);
                mde_object.setting11 = row.FieldValue(80, 10);
                mde_object.setting12 = row.FieldValue(80, 11);
                mde_object.setting13 = row.FieldValue(80, 12);
                mde_object.setting14 = row.FieldValue(80, 13);
                mde_object.setting15 = row.FieldValue(80, 14);
                mde_object.setting16 = row.FieldValue(80, 15);
                mde_object.lsphccomp = row.FieldValue(81, 0);
                mde_object.lsphccomp2 = row.FieldValue(81, 1);
                mde_object.lsphccomp3 = row.FieldValue(81, 2);
                mde_object.lsphccomp4 = row.FieldValue(81, 3);
                mde_object.lsphccomp5 = row.FieldValue(81, 4);
                mde_object.lsphccomp6 = row.FieldValue(81, 5);
                mde_object.lsphccomp7 = row.FieldValue(81, 6);
                mde_object.lsphccomp8 = row.FieldValue(81, 7);
                mde_object.lsphccomp9 = row.FieldValue(81, 8);
                mde_object.lsphccomp10 = row.FieldValue(81, 9);
                mde_object.lsphccomp11 = row.FieldValue(81, 10);
                mde_object.lsphccomp12 = row.FieldValue(81, 11);
                mde_object.lsphccomp13 = row.FieldValue(81, 12);
                mde_object.lsphccomp14 = row.FieldValue(81, 13);
                mde_object.lsphccomp15 = row.FieldValue(81, 14);
                mde_object.lsphccomp16 = row.FieldValue(81, 15);
                mde_object.tobresdate = row.FieldValue(82, 0);
                mde_object.tobresdate2 = row.FieldValue(82, 1);
                mde_object.tobresdate3 = row.FieldValue(82, 2);
                mde_object.tobrestype = row.FieldValue(83, 0);
                mde_object.tobrestype2 = row.FieldValue(83, 1);
                mde_object.tobrestype3 = row.FieldValue(83, 2);
                mde_object.trescomp = row.FieldValue(84, 0);
                mde_object.trescomp2 = row.FieldValue(84, 1);
                mde_object.trescomp3 = row.FieldValue(84, 2);

                MDE_Collection.Add(mde_object);
            });

            return MDE_Collection;

        }

        //public void BulkUploadFileToDB(DataTable dt)
        //{

        //}
    }

    public static class RowExtension
    {
        public static string FieldValue(this Row row, int fieldNumber, int valueIndex)
        {
            return row.fields.Find(f => f.fieldNumber == fieldNumber).values[valueIndex];
        }

    }
}