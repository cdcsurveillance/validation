﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using WWValidationApi.JsonModels;

namespace WWValidationApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValidateController : ControllerBase
    {
        private readonly ILogger<ValidateController> _logger;
        public ValidateController(ILogger<ValidateController> logger)
        {
            _logger = logger;
        }

        // POST api/validate
        [HttpPost]
        [RequestSizeLimit(100_000_000)]
        public Report Post()
        {
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                string body = reader.ReadToEnd();
                //_logger.LogInformation("body: {0}", body);
                //JObject o = JObject.Parse(body);
                //JArray rows = (JArray)o["rows"];

                return new Actions.ValidateJson().Validate(body);
            }
        }

    }
}
