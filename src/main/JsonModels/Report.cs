﻿using System;
using System.Collections.Generic;
using WisewomanMVCSite.Class;
using WWValidationApi.Class;

namespace WWValidationApi.JsonModels
{
    public class Report
    {

        /// <summary>
        /// Gets or sets the summary.
        /// </summary>
        /// <value>The summary.</value>
        public ValidationSummary Summary { get; set; }

        /// <summary>
        /// Gets or sets the errors.
        /// </summary>
        /// <value>The errors.</value>
        public List<SPValidationError> Errors { get; set; }

        /// <summary>
        /// Gets or sets the metrics.
        /// </summary>
        /// <value>The metrics.</value>
        public List<MDEMetrics> Metrics { get; set; }
    }
}
