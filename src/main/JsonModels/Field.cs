﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WWValidationApi.JsonModels
{
    public class Field
    {
        public int fieldNumber { get; set; }
        public string fieldItem { get; set; }
        public List<string> values { get; set; }
    }
}
