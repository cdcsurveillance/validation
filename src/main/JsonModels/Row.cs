﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WWValidationApi.JsonModels
{
    public class Row
    {
        public int rowNumber { get; set; }
        public List<Field> fields { get; set; }
    }
}
