﻿using System;
namespace WWValidationApi.Class
{
    public class MDEMetrics
    {
        public int RowNum { get; set; }

        public bool IsValid { get; set; }

        public bool IsBPPlus { get; set; }

        public bool IsComplete { get; set; }
    }
}
