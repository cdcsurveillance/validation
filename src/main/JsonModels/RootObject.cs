﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WWValidationApi.JsonModels
{
    public class RootObject
    {
        public string fileName { get; set; }
        public List<Row> rows { get; set; }
    }
}
