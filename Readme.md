# Overview

Web API version of the WW (WiseWoman) validation.  Previously, this was not a service and was embedded in the WiseWoman DMS 2.0 web application.  The WW validation service provides validation of the data submissions based on an embedded set of rules and checks.

# Build Instructions

```text
./buildservice.sh
```

This script will remove any exiting docker container and image, build the project and create the docker image.

If you uncomment out last line of the buildservice script it will also run it.