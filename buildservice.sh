#!/bin/bash 

# Remove Container
docker rm validation

# Remove Image
docker rmi /esurveillance/validation

# Build the app in the project Directory
cd src/main
dotnet publish -c Release

# from command line to build docker
docker build -t esurveillance/validation .

cd ../..

# to Run
#docker run -d -p 10002:10002 --name validation esurveillance/validation 